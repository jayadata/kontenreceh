<div class="bg-white">
    <div class="card-body pt-0 px-3">
        @if(Auth::user())
            <div class="m-profile pb-2 mt-4 collpased noarrow">
                <div class="row sg mb-4">
                    <div class="col-xs-3 pr-3">
                        <div class="avatar-wrapper">
                            <img src="https://be2ad46f1850a93a8329-aa7428b954372836cd8898750ce2dd71.ssl.cf6.rackcdn.com/avatars/1524124742.1402.jpg"class="img-circle avatar">
                            
                            @if(Auth::user()->expire > date('Y-m-d'))
                            <img src="{{ StaticAsset('assets/frontend/img/m-menu2/v.png') }}" class="vip-icon" alt="">
                            @endif
                        </div>
                        
                        {{-- <img src="{{ StaticAsset('assets/frontend/img/m-menu2/ava.png') }}" alt=""> --}}
                    </div>
                    <div class="col-xs-9 pl-0">
                        <h4 class="text-15 text-uppercase mt-3">{{ Auth::user()->name }}</h4>
                    </div>
                </div>
                {{-- ID akun dan Total Poin --}}
                <div class="row sg mx-0 mb-4">
                    <div class="col-xs-6">
                        <div class="row sg">
                            <div class="col-xs-4 pr-1">
                                <img src="{{ StaticAsset('assets/frontend/img/m-menu2/id.png') }}"width="25"  alt="">
                            </div>
                            <div class="col-xs-8 pl-0">
                                <div class="text-11 text-gray2">
                                    ID Akun
                                </div>
                                <div class="text-13">
                                    {{ Auth::user()->msisdn }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(Auth::user())
            <div class="px-2">
            
                <div class="mb-4">
                    <div class="row sg text-14">
                        <div class="col-xs-6">
                            <a href="{{ url('member/profile') }}"><img src="{{ StaticAsset('assets/frontend/img/m-menu2/setings.png') }}" width="20" class="mr-2" alt=""> Pengaturan</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><img src="{{ StaticAsset('assets/frontend/img/m-menu2/out.png') }}" width="20" class="mr-2" alt=""> <span class="text-danger">Keluar</span></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
        
                <hr class="my-0">

                <div class=" py-3">
                    <div class="m-menu2-link">
                        <a href="{{ route('member.content.redeem') }}"></a>
                        <div class="row sg">
                            <div class="col-xs-2 pr-0">
                                <img src="{{ StaticAsset('assets/frontend/img/m-menu2/pwcode.png') }}" width="30" alt="">
                            </div>
                            <div class="col-xs-10 pl-0">
                                <div class="text-15 text-semibold">Kode Qomiqu</div>
                                <div class="text-13 text-gray">Cek kode Qomiqu yang belum kamu gunakan</div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="my-0">
                
                <hr class="my-0">
            
                <div class="m-menu2-link py-4">
                    <a href="{{ route('member.content') }}"></a>
                    <div class="text-15 text-semibold">Daftar Konten</div>
                    <div class="text-13 text-gray pr-4">Konten digital yang pernah dibeli</div>
                </div>
            </div>
        @else
            <div class="bg-gray">
                <div class="py-4">
                    <div class="row">
                        
                        <div class="col-xs-12">
                            <a href="{{ url('auth/signin') }}"><img src="{{ StaticAsset('assets/frontend/img/m-menu2/out.png') }}" width="25" class="mr-2" alt=""> <span class="text-danger">Daftar / Login</span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>