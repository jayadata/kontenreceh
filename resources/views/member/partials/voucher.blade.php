<div class="pw_widget"  style="background: #fff;">
<div class="row hargakoin">
<div class="col-md-12">
    <h3 style="font-family: ProximaNovaBold;font-weight: bold;margin-bottom: 15px;">Tambah Koin</h3>
    @foreach ($voucher as $row)
        {{-- <label>
        <input type="radio" value="{{$row->id}}" name="tambah_saldo"> IDR {{number_format($row->amount)}} = {{number_format($row->koin)}} Koin
        </label><br> --}}
        <label>
            <div class="left">
                <input type="radio" value="{{$row->id}}" name="tambah_saldo"
                data-harga="{{number_format($row->amount)}}" data-koin="{{$row->default_koin}}" data-extra="{{$row->bonus}}"
                data-pw="{{$row->pw}}"
                data-sms="http://www.gudangapp.com/xlp/?kc={{$row->pw}}&sdc=97788&cb=http://playworld.id/rand&desc=Kamu+akan+membeli+{{$row->koin}}+KOIN+PLAYWORLD+senilai+Rp.+{{$row->amount}}+%28inc.+tax%29+%2F+Segera+gunakan+KOIN+nya+untuk+bisa+mengumpulkan+POIN+dan+memilih+hadiah+langsung+yang+kamu+inginkan+di+www.playworld.id&img={{StaticAsset('assets/frontend/img/banner_btn_non_berlangganan.png') }}&eid=9e179"> IDR {{number_format($row->amount)}}

                <span class="ghost"></span>
            </div>
            <div class="right">
                <span class="label label-success">
                    {{$row->default_koin}} Koin
                </span>
                @if($row->bonus != 0)
                <span class="label label-warning">
                        +{{$row->bonus}} Koin
                </span>
                @endif
            </div>
        </label>
    @endforeach

</div>

<div class="col-md-12 clearfix">

    <!-- form payment method -->
    <form action="#" class="paymentmethod ">
        <label for="usepulsa hide">
            <input type="radio" checked="checked" value="pulsa" id="usepulsa" name="paymentmethod"> Bayar pakai <strong>PULSA</strong>
        </label>
        <label for="usecard hide">
            <input type="radio" value="card" id="usecard" name="paymentmethod"> Bayar pakai <strong>DEBIT</strong> atau <strong>CREDIT CARD</strong>
        </label>
        <button class="btn btn-default btn-lg" id="paymenttrigger">
            &nbsp; BELI &nbsp;
        </button>
    </form>


</div>
</div>
</div>  