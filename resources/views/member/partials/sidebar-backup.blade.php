<div class="col-md-3 show-for-large">
    <div class="card mb-sm-0 mb-md-3 mb-3">

        {{-- Profile --}}
        <div class="card-body"> 
            <div class="row sg">
                <div class="col-md-3 col-xs-2 col-sm-1">
                    @if(Auth::user()->avatar == '')
                        @if(Auth::user()->fb_login == 1)
                            <img src="https://graph.facebook.com/v2.6/{{ Auth::user()->fb_id }}/picture?type=normal" class="avatar img-rounded img-thumbnail">
                        @else
                            <img src="{{ StaticAsset('assets/frontend/img/member/avatar-fallback.png') }}" alt="{{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }}" class="avatar img-rounded img-thumbnail"> 
                        @endif

                    @else
                        <img src="{{ avatar_storage(Auth::user()->avatar) }}" alt="{{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }}" class="img-rounded avatar">
                    @endif
                </div>
                <div class="col-md-9 col-xs-10 col-sm-11">
                    <strong class="text-16">{{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }} </strong>
                </div>
            </div>

            <a href="javascript:;" class="globalToggler dashboard-menu hide-for-large" data-target="user_m_menu">
                <i class="fa fa-bars"></i>
            </a>
        </div>

        <div class="card-body user-sidebar-menu">
            @include('member_new_design.partial.membermenu')
        </div>

    </div>
</div>