<!-- popup pulsa -->
<div class="mt-outtest beli_pulsa">
<div class="mt-app">
    <nav class="headermt">
        <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
        <div class="header-content">
            <h1 class="logo-store">
                <img alt="qomiqu" src="{{ asset('assets/frontend/favicon.png') }}">
            </h1>
        </div>
    </nav>

    <div class="button-main show">
        <a class="button-main-content text-center tutupaja show-for-large" href="javascript:;">
            <div class="text-button-main text-center">Tutup</div>
        </a>
        <a class="button-main-content text-center hide-for-large text_sms" href="javascript:;">
            <div class="text-button-main text-center ">Lanjutkan Pembelian</div>
        </a>
    </div> <!-- /.button-main show -->

    <div class="pop-wrapper has-close">
        <span class="pop"></span>
        <div class="pop-close-wrapper">
            <a href="javascript:;" class="touch-area">
                <span class="sprite modal-close-dark"></span>
            </a>
        </div>
    </div> <!-- /.pop-wrapper has-close -->

    <div class="container-fluid">
        <div class="row">
            <div class="main-container">
                <div class="page-container scroll">
                    <!-- item -->
                    <div class="card-container cctitle-box">
                        <div class="amount">
                            <div class="amount-title">
                                <span class="text-amount-title">Rincian Belanja</span>
                            </div>
                        </div>
                    </div> <!-- .card-container -->

                    <!-- item -->
                    <div class="card-container">
                        <div class="amount">
                            <div class="amount-title">
                                <span class="text-amount-title">JUMLAH</span>
                            </div>
                            <div class="amount-content">
                                <span class="text-amount-rp">Rp</span><span class="text-amount-amount text_harga">8,800</span>
                            </div>
                        </div>
                    </div> <!-- .card-container -->

                    <!-- item -->
                    <div class="card-container">
                        <div class="amount">
                            <div class="amount-title">
                                <span class="text-amount-title">KOIN</span>
                            </div>
                            <div class="amount-content">
                                <span class="text-amount-amount text_koin">40</span>
                            </div>
                        </div>
                    </div> <!-- .card-container -->

                    <!-- item -->
                    <div class="card-container">
                        <div class="amount">
                            <div class="amount-title">
                                <span class="text-amount-title">KOIN EXTRAAAA</span>
                            </div>
                            <div class="amount-content">
                                <span class="text-amount-amount text_extra">5</span>
                            </div>
                        </div>
                    </div> <!-- .card-container -->

                    <ul class="mt-list">
                        <li class="show-for-large">
                            Untuk melanjutkan pembelian silahkan sms ke 97788 dengan format sebagai berikut <br>
                            <div class="label label-success ">
                                Ketik <kbd class="text_pw">PW</kbd> kirim ke <kbd class="text_nomor">97788</kbd>
                            </div>
                        </li>
                        <li class="show-for-large">
                            Anda bisa menggunakan hape (handphone) untuk kemudahan proses pembelian KOIN
                        </li>
                        <li class="hide-for-large">
                            Untuk melanjutkan proses pembelian silahkan tekan tombol "Lanjukan Pembelian" di bawah ini.
                        </li>
                        <li>
                            Pembelian KOIN dengan PULSA saat ini didukung oleh <br>
                            <img src="{{ StaticAsset('assets/frontend/img/newfoo/XL_logo_2016.svg.png') }}" alt="" style="width:40px"> &nbsp;&nbsp;
                            <img src="{{ StaticAsset('assets/frontend/img/newfoo/Indosat_Ooredoo_logo.svg.png') }}" alt="" style="width:60px">
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="mt-outtest gak_aktif">
<div class="mt-app">
    <nav class="headermt">
        <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
        <div class="header-content">
            <h1 class="logo-store">
                <img alt="qomiqu" src="{{ asset('assets/frontend/favicon.png') }}">
            </h1>
            <span class="text-page-title">Rincian Penukaran Poin</span>
        </div>
    </nav>

    <div class="button-main show proses_button">
       <a class="button-main-content text-center text_sms tutupaja" href="javascript:;">
           <div class="text-button-main text-center">TUTUP</div>
       </a>
    </div> <!-- /.button-main show -->

    <div class="pop-wrapper has-close">
        <span class="pop"></span>
        <div class="pop-close-wrapper">
            <a href="javascript:;" class="touch-area">
                <span class="sprite modal-close-dark"></span>
            </a>
        </div>
    </div> <!-- /.pop-wrapper has-close -->

    <div class="container-fluid">
        <div class="row">
            <div class="main-container">
                <div class="page-container scroll">

                        <div class="text-center">
                            <div class="ciricon">
                                <i class="fa fa-exclamation"></i>
                            </div>

                            <h1>BELUM AKTIF</h1>
                            <p>
                                Metode pembayaran ini belum dapat digunakan,<br>
                                silahkan gunakan metode pembayaran lain,<br>
                                mohon maaf atas ketidaknyamanan ini.
                            </p>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div> <!-- mt-outtest -->

<!-- TRANSFER KOIN -->
<div class="mt-outtest poincukup arisanpopup">
<div class="mt-app">
    <nav class="headermt">
        <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
        <div class="header-content">
            <h1 class="logo-store">
                <img alt="PLAYWORLD" src="https://vtcheckout-sandbox-assets.s3.amazonaws.com/snap/logos/M101875/thumb_retina_snap_2Flogos_2FM101875_2F63352767-d777-431a-81f9-c1915086481f_2Fplayworld%2Blogo%2B02-01.png">
            </h1>
            <span class="text-page-title">Transfer Koin</span>
        </div>
    </nav>
    <div class="button-main show proses_button">
       <a class="button-main-content text-center text_sms" href="javascript:;">
           <div class="text-button-main text-center lanjut_transfer">Lanjutkan Transfer Koin</div>
       </a>
    </div> <!-- /.button-main show -->
    <div class="pop-wrapper has-close">
        <span class="pop"></span>
        <div class="pop-close-wrapper">
            <a href="javascript:;" class="touch-area">
                <span class="sprite modal-close-dark"></span>
            </a>
        </div>
    </div> <!-- /.pop-wrapper has-close -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-container">
                <div class="page-container scroll">
                    <div class="mx-3 my-4">
                        <div class="card-container">
                            <div class="amount">
                                <div class="amount-title">
                                    <span class="text-amount-title">SISA KOIN</span>

                                    <div class="amount-content">
                                        <span class="text-amount-amount text_harga text_my_balance text-warning"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-container">
                            <div class="amount">
                                <div class="amount-title">
                                    <span class="text-amount-title">MSISDN TUJUAN</span>
                                    <div class="amount-content">
                                        <span class="text-amount-amount text_harga text_msisdn_tujuan text-warning"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-container">
                            <div class="amount">
                                <div class="amount-title">
                                    <span class="text-amount-title">KOIN DITRANSFER</span>
                                    <div class="amount-content">
                                        <span class="text-amount-amount text_harga text_jumlah_transfer text-warning"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="mt-list noliststyle px-5 pb-5 pb-md-0">
                        <li>
                            <div class="row sg">
                                <div class="col-xs-2 pr-0">
                                     <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_InfoBlue.png') }}" alt="">
                                </div>
                                <div class="col-xs-10 pt-3">
                                    <p class="text-small p-0">
                                        Saya telah menyetujui Syarat dan Ketentuan TRANSFER KOIN <br>
                                        Klik tombol "Lanjutkan Transfer KOIN" di bawah ini untuk melanjutkan
                                    </p>
                                </div>
                            </div> 
                        </li> 
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div> <!-- mt-outtest -->

<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
POPUP KOIN TIDAK CUKUP
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
<!-- popup pulsa -->
<div class="mt-outtest pointidakcukup arisanpopup">
<div class="mt-app">
    <nav class="headermt">
        <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
        <div class="header-content">
            <h1 class="logo-store">
                <img alt="PLAYWORLD" src="https://vtcheckout-sandbox-assets.s3.amazonaws.com/snap/logos/M101875/thumb_retina_snap_2Flogos_2FM101875_2F63352767-d777-431a-81f9-c1915086481f_2Fplayworld%2Blogo%2B02-01.png">
            </h1>
            <span class="text-page-title">Rincian Transfer Koin</span>
        </div>
    </nav>
    <div class="button-main show proses_button dead">
       <a class="button-main-content text-center text_sms tutupaja" href="javascript:;">
           <div class="text-button-main text-center">Tutup</div>
       </a>
    </div> <!-- /.button-main show -->
    <div class="pop-wrapper has-close">
        <span class="pop"></span>
        <div class="pop-close-wrapper">
            <a href="javascript:;" class="touch-area">
                <span class="sprite modal-close-dark"></span>
            </a>
        </div>
    </div> <!-- /.pop-wrapper has-close -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-container">
                <div class="page-container scroll">
                    <div class="mx-5 my-4">
                        <div class="row text-center mt-5 pt-3">
                            <div class="col-xs-12 mb-3">
                                <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90">
                            </div>
                            <div class="col-xs-12 text-gray">
                                <div class="text-huge text-semibold">
                                    TRANSFER KOIN GAGAL
                                </div>
                                <div class="my-3">
                                    KOIN tidak berhasil ditukar karena alasan berikut:
                                </div>
                                <div class="text-semibold my-3">
                                    KOIN anda tidak mencukupi untuk melanjutkan proses Transfer KOIN
                                </div>
                                <p class="p-0">Silahkan tutup jendela ini masukan kembali jumlah KOIN yang lebih rendah atau sama dengan Sisa KOIN anda saat ini</p> <br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> <!-- mt-outtest -->
<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
POPUP BELOM LOGIN
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
<!-- popup pulsa -->
<div class="mt-outtest nologin">
<div class="mt-app">
    <nav class="headermt">
        <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
        <div class="header-content">
            <h1 class="logo-store">
                <img alt="PLAYWORLD" src="https://vtcheckout-sandbox-assets.s3.amazonaws.com/snap/logos/M101875/thumb_retina_snap_2Flogos_2FM101875_2F63352767-d777-431a-81f9-c1915086481f_2Fplayworld%2Blogo%2B02-01.png">
            </h1>
            <span class="text-page-title">Rincian Penukaran Poin</span>
        </div>
    </nav>
    <div class="button-main show proses_button">
       <a class="button-main-content text-center text_sms" href="{{ url('auth/signin') }}">
           <div class="text-button-main text-center">LOGIN</div>
       </a>
    </div> <!-- /.button-main show -->
    <div class="pop-wrapper has-close">
        <span class="pop"></span>
        <div class="pop-close-wrapper">
            <a href="javascript:;" class="touch-area">
                <span class="sprite modal-close-dark"></span>
            </a>
        </div>
    </div> <!-- /.pop-wrapper has-close -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-container">
                <div class="page-container scroll">
                    <div class="text-center">
                        <div class="ciricon">
                            <i class="fa fa-exclamation"></i>
                        </div>

                        <h1>ANDA BELUM LOGIN</h1>
                        <p>
                            Silahkan klik tombol LOGIN di bawah ini untuk melanjutkan proses penukaran POIN
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div> <!-- mt-outtest -->

<!-- 
transaksi gagal
-->
<div class="mt-outtest transaksi_gagal">
    <div class="mt-app">
        <nav class="headermt">
            <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
            <div class="header-content">
                <h1 class="logo-store">
                    <img alt="PLAYWORLD" src="https://vtcheckout-sandbox-assets.s3.amazonaws.com/snap/logos/M101875/thumb_retina_snap_2Flogos_2FM101875_2F63352767-d777-431a-81f9-c1915086481f_2Fplayworld%2Blogo%2B02-01.png">
                </h1>
                <span class="text-page-title">Transfer Koin</span>
            </div>
        </nav>

        <div class="button-main show proses_button dead">
           <a class="button-main-content text-center text_sms tutupaja" href="javascript:;">
               <div class="text-button-main text-center">TUTUP</div>
           </a>
        </div> <!-- /.button-main show -->

        <div class="pop-wrapper has-close">
            <span class="pop"></span>
            <div class="pop-close-wrapper">
                <a href="javascript:;" class="touch-area">
                    <span class="sprite modal-close-dark"></span>
                </a>
            </div>
        </div> <!-- /.pop-wrapper has-close -->

        <div class="container-fluid">
            <div class="row">
                <div class="main-container">
                    <div class="page-container scroll">
                        <div class="mx-5 my-4">
                            <div class="row text-center mt-5 pt-3">
                                <div class="col-xs-12 mb-3">
                                    <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90">
                                </div>
                                <div class="col-xs-12 text-gray">
                                    <div class="text-huge text-semibold">
                                        TRANSFER KOIN GAGAL
                                    </div>
                                    <div class="my-3">
                                        KOIN tidak berhasil ditukar karena alasan berikut:
                                    </div>
                                    <div class="text-semibold my-3">
                                        <p class="fail_text"></p>
                                    </div>
                                    <p class="p-0">Silahkan tutup jendela ini masukan kembali nomor MSISDN yang benar dan terdaftar</p> <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- mt-outtest -->

<!-- 
transaksi berhasil
-->
<div class="mt-outtest transaksi_berhasil">
    <div class="mt-app">
        <nav class="headermt">
            <a class="header-back tutupaja" href="javascript:;"><span class="sprite modal-close-dark"></span></a>
            <div class="header-content">
                <h1 class="logo-store">
                    <img alt="PLAYWORLD" src="https://vtcheckout-sandbox-assets.s3.amazonaws.com/snap/logos/M101875/thumb_retina_snap_2Flogos_2FM101875_2F63352767-d777-431a-81f9-c1915086481f_2Fplayworld%2Blogo%2B02-01.png">
                </h1>
                <span class="text-page-title">Rincian Penukaran Poin</span>
            </div>
        </nav>

        <div class="button-main show proses_button">
           <a class="button-main-content text-center text_sms" href="{{ url('member/history_penggunaan_koin') }}">
               <div class="text-button-main text-center">Lihat Histori KOIN</div>
           </a>
        </div> <!-- /.button-main show -->

        <div class="pop-wrapper has-close">
            <span class="pop"></span>
            <div class="pop-close-wrapper">
                <a href="javascript:;" class="touch-area">
                    <span class="sprite modal-close-dark"></span>
                </a>
            </div>
        </div> <!-- /.pop-wrapper has-close -->

        <div class="container-fluid">
            <div class="row">
                <div class="main-container">
                    <div class="page-container scroll">
                        <div class="mx-5 my-4">
                            <div class="row text-center mt-5 pt-3">
                                <div class="col-xs-12 mb-3">
                                    <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90">
                                </div>
                                <div class="col-xs-12 text-gray">
                                    <div class="text-huge text-semibold">
                                        TRANSFER KOIN BERHASIL
                                    </div>
                                    <div class="my-3">
                                        <p>
                                            Anda telah berhasil mentransfer koin andake MSISDN tujuan. Untuk melihat bukti Transfer KOIN, silahkan akses Histori KOIN dengan mengklik tombol di bawah ini.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- mt-outtest -->