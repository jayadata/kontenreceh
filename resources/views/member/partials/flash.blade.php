@if (Session::has('flash_notification.message'))
    <div class="alert alert-{{ Session::get('flash_notification.level') }} py-4 px-3 text-medium">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        
        {{ Session::get('flash_notification.message') }}
    </div>
@endif