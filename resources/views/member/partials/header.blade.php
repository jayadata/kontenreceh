
@if($user->email == '')
<span class="alert alert-warning profile-notification">
    <i class="fa fa-fw fa-exclamation-triangle"></i> Segera <a href="{{ url('member/profile') }}">lengkapi profil</a> kamu untuk kenyamanan bertransaksi dan proses recovery akun kamu.
</span>
@endif

@if($user->email != '' && $user->email_confirmed == 0)
<span class="alert alert-success profile-notification">
    <i class="fa fa-fw fa-exclamation-triangle"></i> Silahkan anda klik link confirmasi yang telah kami kirim ke {{ $user->email }}, klik <a href="{{ url('member/re_confirm') }}">link</a> ini untuk mengirim ulang email konfirmasi
</span>
@endif

@if(Session::has('message'))
    <div class="updatenotification">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="alert profile-notification">
                        <i class="fa fa-fw fa-exclamation-circle"></i> {!! Session::get('message'); !!}
                    </span>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Session::has('re_confirm'))
    <span class="alert profile-notification">
        <i class="fa fa-fw fa-exclamation-circle"></i> {!! Session::get('re_confirm'); !!}
    </span>
@endif

@if(Session::has('email_confirmation'))
    <span class="alert profile-notification">
        <i class="fa fa-fw fa-exclamation-circle"></i> {!! Session::get('email_confirmation'); !!}
    </span>
@endif

<section class="profile-navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="mobile-user-menu hide-for-large">
                    <span class="user-text-menu"><i class="fa fa-bars"></i> MENU</span>
                </div>
                <ul id="profile-menu" class="list-inline">
                    <li>
                        <a class="{{ setActive(['member/dashboard*', 'member']) }}" href="{{ url('member/dashboard') }}">
                            <img src="{{ StaticAsset('assets/frontend/img/member/dashboard-icon.svg') }}" alt="Dashboard" class="nav-icon inject">
                            <div class="nav-item">Beranda</div>
                        </a>
                    </li>
                    <li>
                        <a class="{{ setActive('member/profile*') }}" href="{{ url('member/profile') }}">
                            <img src="{{ StaticAsset('assets/frontend/img/member/profile-icon.svg') }}" alt="Profile" class="nav-icon inject">
                            <div class="nav-item">Profil</div>
                        </a>
                    </li>
                    <li>
                        <a class="{{ setActive('member/balance*') }} memmenu" href="{{ url('member/balance') }}">
                            <div class="iconmem koin"></div>

                            <div class="nav-item">Koin</div>
                        </a>
                    </li>
                    <li>

                        <a class="{{ setActive('member/point*') }} memmenu " href="{{ url('/member/point') }}">
                            <div class="iconmem poin"></div>

                            <div class="nav-item">Poin</div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('member/history') }}" class="memmenu {{ setActive('member/history*') }}">
                            <div class="iconmem exchange"></div>
                            <div class="nav-item">Histori Menang</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>