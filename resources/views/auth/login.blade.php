@extends('frontend.layouts.app')
@section('title')
Playworld Signin
@stop

@section('addons')

@stop
@section('content')
<?php 
$attributes = [
    'data-theme' => 'light',
    'data-type' => 'image',
];
?>
<link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<style type="text/css">
.split.button {
width: 100%; }
.split.button.facebook {
background: #3b5998;
text-transform: uppercase; }
.split.button.facebook span {
background: #2d4373; }
.split.button.facebook span:after {
border: none;
font-family: "foundation-icons";
content: "\f1c4";
font-size: 3rem;
line-height: 6.25rem;
margin-left: 0.3rem; }
.split.button.twitter {
background: #55acee;
text-transform: uppercase; }
.split.button.twitter span {
background: #2795e9; }
.split.button.twitter span:after {
border: none;
font-family: "foundation-icons";
content: "\f1e4";
font-size: 3rem;
line-height: 5.25rem;
margin-left: 0.3rem; }
.split.button.google {
background: #d50f25;
text-transform: uppercase; }
.split.button.google span {
background: #a50c1d; }
.split.button.google span:after {
border: none;
font-family: "foundation-icons";
content: "\f1ca";
font-size: 3rem;
line-height: 6.25rem;
margin-left: 0.3rem;}
.split.button.left-icon {
text-align: right; }
.split.button.left-icon span {
left: 0; }

button.button.split{
border: none!important;
position: relative;
padding-right: 5.0625rem;

-webkit-appearance: none;
-moz-appearance: none;
border-radius: 0;
border-style: solid;
border-width: 0;
cursor: pointer;
font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
font-weight: normal;
line-height: normal;
margin: 0 0 1.25rem;
position: relative;
text-align: center;
text-decoration: none;
display: inline-block;
padding: 2.5rem 3rem 2.5rem 4.7rem;
font-size: 1.3rem;
background-color: #008CBA;
border-color: #007095;
color: #FFFFFF;
transition: background-color 300ms ease-out;
}
button.split.button span {
display: block;
height: 100%;
position: absolute;
right: 0;
top: 0;
width: 56px;
}
.makeitnarrow {
	max-width: 640px;
	margin: 0 auto;
}
</style>
@if(Request::get('msisdn') != '')
<style type="text/css">
	.member-login .login-container
	{
		max-width: 600px!important;
	}
</style>

@endif
<!-- Main Section -->
<div class="center-wrapper">
	<section class="center-wrapper-inner member-login">
		@if(Request::get('msisdn') == '')

		<div class="container my-4" style="max-width: 960px">
			<div class="row hide">
				<div class="col-md-12 hide-for-large">
					<div class="member-tab-button">
						<a href="#" class="btn-tab active" data-target=".daftar">Daftar</a>
						<a href="#" class="btn-tab" data-target=".login">Login</a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					@include('member.partials.flash')
				</div>
				
				<?php /*
				<div class="col-md-6 daftar">
					<div class="card box-shadow">
						<div class="card-body px-2 px-md-5">
							<div class="pre-process">
								<h3 class="text-semibold text-gray2 text-center" style="font-size:24px;">
									Daftar Menjadi Member
								</h3>
								<p class="text-center text-gray text-medium">Masukkan nomor handphone anda pada kotak di bawah ini,<br>dan kemudian klik tombol <strong>"Daftar"</strong></p>
								<hr class="my-3 my-md-3">

								{{-- FORM DAFTAR --}}
								<form class="makeitnarrow px-0 px-md-5 my-4">
								  	<div class="px-3">
									  	<div class="row">
									  		<div class="col-md-12">
								  				<div class="form-row align-items-center">
								  				  <div class="col-auto">
								  				    <div class="input-group mb-2 mb-sm-0">
								  				      <div class="input-group-addon" style="padding:0 15px">+62</div>
								  				      <input style="padding: 35px 12px" name="nomorhandphone" type="text" class="form-control" id="nomorhandphone" placeholder="Nomor Handphone" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								  				    </div>
								  				  </div>
								  				</div>
									  		</div>
									  		<div class="col-md-12 my-3">
									  			<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="{{env('CAPTCHA_SITEKEY')}}"></div>
									  		</div>
									  		<div class="clearfix"></div>
									  		<div class="col-md-12">
									  			<div class="btn btn-warning btn-lg btn-block success-trigger text-left" style="padding:15px"><i class="fa fa-chevron-right right" style="margin-top:5px;"></i><a href="javascript:;" class="" style="color: #fff;">DAFTAR</a></div>
									  		</div>
									  	</div>
								  	</div>
								</form>
								<div class="clearfix"></div>
								<div class="clearfix loader-container">
					                <div class="loader">
					                  <div class="dot"></div>
					                  <div class="dot"></div>
					                  <div class="dot"></div>
					                  <div class="dot"></div>
					                  <div class="dot"></div>
					                </div>
					            </div>

					            {{-- INFO PENDAFTARAN --}}

					            <div class="px-5">
					            	<a href="javascript:;" class="text-left btn btn-block text-gray globalToggler px-4" data-target="section-info" style="background:transparent;"><i class="fa fa-chevron-down right"></i><i class="fa fa-info mr-2"></i> Info pendaftaran </a>
					            </div>

					            <div class="section-info daftarinfo px-3" id="section-info">
					            	<div class="row">
					            		<div class="col-md-12">
					            			<hr>
					            		</div>
					            		<div class="col-md-12 col-xs-12">
					            			<ol class="px-4 pt-3">
					            				<li><strong>Pendaftaran GRATIS</strong> tanpa ada biaya tersembunyi</li>
					            				<li><strong>GRATIS 3 Koin</strong> untuk pendaftaran pertama</li>
					            				<li>Setelah menekan tombol "Daftar" anda akan menerima SMS berisi link/URL seperti berikut
					            					<br>
					            					<img src="{{ StaticAsset('assets/frontend/img/IMG_886C12019F23-1.jpg') }}" alt="">
					            				</li>
					            				<li>
					            					Setelah meng-klik link/URL di atas, anda akan diarahkan ke halaman Playworld sebagai berikut, dan segera selesaikan proses pendaftaran dengan menekan tombol "Sign In With Facebook" <br>
					            					<img src="{{ StaticAsset('assets/frontend/img/2017-10-31_115940.jpg') }}" alt="">
					            				</li>
					            				<li>
					            					Setelah terhubung dengan Facebook anda akan diarahkan ke halaman profil dimana anda dapat melihat data Histori KOIN, Histori POIN, Histori Menang, Beli KOIN, Transfer Koin dan Pengaturan Profile anda. Pada langkah ini pendaftaran telah sukses dan selesai
					            					<br>
					            					<img src="{{ StaticAsset('assets/frontend/img/2017-10-31_120232.jpg') }}" alt="">
					            				</li>
					            				<li>
					            					Jika mengalami kendala atau informasi lebih lanjut hubungi Customer Service kami di <strong><a href="https://playworld.id/bantuan">https://playworld.id/bantuan</a></strong>
					            				</li>
					            			</ol>
					            		</div>
					            	</div>
					            </div>
							</div>								

				            <div class="berhasil text-center">
				            	<h1>BERHASIL</h1>
				            	<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
				            	<br><br>
				            	<p class="success_messages"></p>
				            </div>
				            <div class="gagal text-center">
				            	<h1>GAGAL</h1>
				            	<img src="{{ StaticAsset('assets/frontend/img/cancel.png') }}" alt="">
				            	<br><br>
				            	<p class="fail_messages"></p>
				            	<br />
				            	<a href="javascript:;" class="btn btn-success back">Ulang Kembali</a>
				            </div>

						</div>
					</div>
				</div> */ ?>

				<div class="col-md-offset-3 col-md-6 login">
					<div class="card box-shadow">
						<div class="card-body">
							<h3 class="text-semibold text-gray2 text-center" style="font-size:24px;">
								Login Member
							</h3>
							<p class="text-center text-gray text-medium">Silahkan Login dengan menggunakan akun facebook anda</p>
							<hr>
							<div class="makeitnarrow px-0 px-md-5 my-4">
								{{-- @include('member.partials.flash') --}}
							  	<div class="px-3">
							  		{{-- @if(isset($msisdn_get))
							  		<a href="{{url("/fb_login?sos=facebook&msisdn=$msisdn_get")}}"><button class="facebook button split"> <span></span>sign in with facebook</button></a>
							  		@else
							  		<a href="{{url("/fb_login?sos=facebook")}}"><button class="facebook button split"> <span></span>sign in with facebook</button></a>
							  		<br />
							  		<a href="{{url("member/google_redirect").'?next='.Request::get('next')}}"><button class="google button split"> <span></span>sign in with Google</button></a>
							  		@endif --}}

							  		{{-- INI BUAT SOSMED ASLI --}}
							  		<a href="{{ url('/auth/facebook') }}"><button class="facebook button split"> <span></span>sign in with facebook</button></a>
							  		{{-- <br />
							  		<a href="{{ url('/auth/google') }}"><button class="google button split"> <span></span>sign in with Google</button></a> --}}
							  	</div>
							</div>
								{{-- Notif  --}}
			                    
			                
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
	@else 
	<section class="center-wrapper-inner member-login">
		<div class="login-container">
			<br>
			<h2 class="login-title">SATU LANGKAH LAGI UNTUK BERMAIN DI PLAYWORLD</h2>

			<div class="row">
				<div class="col-md-12 col-xs-12">

					{{-- <h3 class="logconfirm text-left"><img src="{{ StaticAsset('assets/frontend/img/checked.png') }}" alt="">Benar, Saya Member</h3> --}}
					<div class="alert alert-warning">
						<div class="row">
							<div class="col-md-2 col-xs-2 text-center">
								<img src="{{ StaticAsset('assets/frontend/img/redeemicon/information.png') }}" alt="" class="lgnwrn" style="max-width: 40px">
							</div>
							<div class="col-md-10 col-xs-10">
								<p class="text-left">{{(Session::has('message'))?Session::get('message'):'Kami hanya membutuhkan verifikasi dari Facebook untuk memastikan anda bukanlah robot, untuk melanjutkan silahkan klik tombol Facebook di bawah ini'}}</p>
							</div>
						</div>
					</div>
						@if(Session::has('message'))
						<br />&nbsp;
						
						{{-- <a href="{{url("/fb_login_ver?sos=facebook")}}"><button class="facebook button split"> <span></span>sign in with facebook</button></a> --}}
						<a href="{{url("/fb_login?sos=facebook")}}"><button class="facebook button split pixel"> <span></span>sign in with facebook</button></a>
						@else
						<a href="{{url("/fb_login?sos=facebook")}}"><button class="facebook button split pixel"> <span></span>sign in with facebook</button></a>

						{{-- SET-DOLAN --}}
						
						{{-- <a href="https://dolan.online/fb_login?sos=facebook"><button class="facebook button split"> <span></span>sign in with facebook</button></a> --}}
						<br />
						{{-- <a href="{{url("member/google_redirect").'?next='.Request::get('next')}}"><button class="google button split"> <span></span>sign in with Google</button></a> --}}
						@endif
						
				</div>

			</div>

		</div>
	</section>

	@endif
</div>

<div class="popupketentuan  nonberlangganan">
	<div class="popupket-inner">
		<h3 class="text-center">
			Ketentuan Non Berlangganan
		</h3>
		<ol>
			<li>
				Tarif Berlangganan Rp 16.500
			</li>

			<li>
				Jika proses pendaftaran sukses, anda akan mendapat 60 KOIN Playworld
			</li>
		</ol>
		<a href="#" class="btn btn-lg btn-block btn-primary tutuppopup">Tutup</a>
	</div>
</div>

<div class="popupketentuan berlangganan">
	<div class="popupket-inner">
		<h3 class="text-center">
			Ketentuan  Berlangganan
		</h3>
		<ol>
			<li>
				Tarif Berlangganan Rp 2.200
			</li>
			<li>
				Akan ada 3x SMS per minggu dengan tarif Rp 2.200 per SMS
			</li>
			<li>
				Jika proses pendaftaran sukses, anda akan mendapat 8 KOIN Playworld
			</li>
		</ol>
		<a href="#" class="btn btn-lg btn-block btn-primary tutuppopup">Tutup</a>
	</div>
</div>
@stop

@section('scripts')
    <script>
    	var door = 0;
    	function recaptchaCallback() {
    		door = 1;
    	};
    	jQuery(document).ready(function($){
    		$('.success-trigger').click(function(e){
	            e.preventDefault();
	            if(door == 0)
	        	{
	        		alert('Captcha belum di pilih!');
	        		return false;
	        	}
	            var nomorhandphone = $('#nomorhandphone').val();
	            $('.user-respond').addClass('is_loading');
	            $('.loader-container').show();
	            $.post('{{ url('number_registration') }}', 
	            	{
	            		_token      : '{!!csrf_token()!!}',
	            		nomorhandphone : nomorhandphone 
	            	}, 
	            	function(response){
	            		if(response['error'] == true)
	            		{
	            			$('.gagal').show();
	            			$('.fail_messages').html(response['messages']['nomorhandphone']);
	             			$('.pre-process').hide();
	            		}
	            		else
	            		{
	            			$('.berhasil').show();
	            			$('.success_messages').html(response['messages']['nomorhandphone']);
	             			$('.pre-process').hide();
	            		}
	            		$('.user-respond').removeClass('is_loading');
	            		$('.loader-container').hide();
	            	});
	        });
	        $('.back').click(function(){
	        	$('.gagal').hide();
     			$('.pre-process').show();
	        });

	        $('.pixel').click(function(e){
	        	fbq('track', 'CompleteRegistration');
	        });
    	});
    </script>
    <script src="https://www.google.com/recaptcha/api.js"
        async defer>
    </script>
@stop

