<header class="kr-header">
    <div class="container-fluid">
        <div class="row sm align-items-center">
            <div class="col-4 col-md-2">
                <div class="kr-logo-wrapper">
                    <img src="{{ asset('assets/frontend/logo.png') }}" alt="">
                </div>
            </div>
            <div class="col-8 col-md-10 text-right">
                <a href="{{ route('auth.signin') }}">Download Page</a>
            </div>
        </div>
    </div>
</header>
