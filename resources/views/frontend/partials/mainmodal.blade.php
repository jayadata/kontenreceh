<div class="pw-modal">
    <div data-reactroot="" class="app-container" id="application">
        <nav class="header" id="header"><a class="header-back" href="#"><span class="sprite modal-close-dark"></span></a>
            <div class="header-content">
                <h1 class="logo-store"><img alt="qomiqu" src="{{ asset('assets/frontend/favicon.png') }}"></h1><span class="text-page-title"><p class="text-page-title-content">Order Summary</p></span></div>
        </nav>
        <div class="button-main show">
            <a class="button-main-content" href="#">
                <div class="text-button-main"><span>Continue</span></div>
                <div class="sprite btn-main-next"></div>
            </a>
        </div>
        <div class="pop-wrapper has-close"><span class="pop"></span>
            <div class="pop-close-wrapper"><a href="#" class="touch-area"><span class="sprite modal-close-dark"></span></a></div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="main-container">
                    <div class="page-container scroll">

                        <div class="pw-modal--loader">
                            <img src="{{ StaticAsset('assets/frontend/img/pie.gif') }}" alt="">
                        </div>

                        <div class="pw-modal--data-storage"></div>

                        {{-- <div class="card-container">
                            <div class="amount">
                                <div class="amount-title pull-left"><span class="text-amount-title">amount</span></div>
                                <div class="amount-content pull-right"><span class="text-amount-rp">Rp </span><span class="text-amount-amount">25,000</span></div>
                            </div>
                            <div class="order-id-optional"><strong>Order ID</strong>
                                <div class="pull-right">VT1812040001</div>
                            </div>
                        </div>
                        <div class="card-container">
                            <div class="tabs">
                                <ul class="nav-tabs nav-justified">
                                    <li class="active"><a href="#" class="text-actionable"><span>order details</span></a></li>
                                    <li class=""><a href="#" class="text-actionable"><span>customer details</span></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active">
                                        <div class="content-table">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th class="table-item text-body-title">item(s)</th>
                                                        <th class="table-amount text-body-title">amount</th>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-item text-body"><span class="item-name"><!-- react-text: 58 -->Keanggotaan VIP 40 Hari<!-- /react-text --><!-- react-text: 59 --> <!-- /react-text --></span></td>
                                                        <td class="table-amount text-body">25,000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane">
                                        <div class="content-text-block">
                                            <div class="col-xs-6">
                                                <div class="text-block">
                                                    <div class="text-body-title">Name</div>
                                                    <div class="text-body">Njum Ahmad Indonesia</div>
                                                </div>
                                                <div class="text-block">
                                                    <div class="text-body-title">Phone number</div>
                                                    <div class="text-body">+6281211735338</div>
                                                </div>
                                                <div class="text-block">
                                                    <div class="text-body-title">Email</div>
                                                    <div class="text-body">dev@jayadata.co.id</div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>