@include('frontend.partials.callcenter')

<footer class="hide">
    <section class="footer">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-md-6">
                    <div class="footer-copy">
                        <img class="footer-logo" src="{{ StaticAsset('assets/frontend/img/Qomiqu-text.png') }}" alt="Qomiqu">
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 show-for-large">
                    <div class="footer-contact">
                        <h3>Hubungi Kami</h3>
                            <strong style="line-height: 1"><a href="tel:02122457232"><img src="{{ StaticAsset('assets/frontend/img/operator.png') }}" alt="" style="width: 20px;height: 20px;"> 021 - 22457232</a><br><small>Senin s/d Jumat 09:00 - 18:00</small></strong><br>
                            <strong style="line-height: 1"><a href="tel:087885157347"><img src="{{ StaticAsset('assets/frontend/img/telephone-call.png') }}" alt="" style="width: 20px;height: 20px;"> 0818 3030 29</a><br><small>24 jam / Slow Response</small></strong>
                            <!-- <i class="fa fa-envelope fa-fw"></i> <a href="mailto:cs@Qomiqu.co.id">cs@Qomiqu.co.id</a> -->
                    </div>
                </div>

            </div>
        </div>
    </section>
</footer>