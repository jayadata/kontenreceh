<div class="header2">
    <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    TOPBAR
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
    <div class="hdrstickyhandler">
        <div class="hdr2-topbar-handler">
            <div class="hdr2-topbar"> 
                <div class="hdr2-search m_search" style="display: none;">
                    <form action="{{ url('search') }}" method="get">
                        <input type="text" name="query" value="{{Request::get('query')}}" placeholder="Search...">
                        <button><i class="fa fa-search"></i></button>
                    </form>
                </div> <!-- //end .hdr-2-search -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-xs-12">
                            <!-- Menu Topbar -->
                            <div class="hdr-m-menutrig hide-for-large">
                                <i class="fa fa-bars"></i>
                            </div>

                            <div class="hide-for-large m_brands">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6">
                                        <a href="{{ url('/') }}"><img class="site-logo" src="{{ StaticAsset('assets/frontend/logo.png') }}" alt="Playworld"></a>
                                    </div>
                                    <div class="col-md-6 col-xs-6 text-right">
                                        <div class="hdr2-social">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="hdr2-tb-menu">
                                <li class="logo-sticky"><a href="{{ url('/') }}"><img src="{{ StaticAsset('assets/frontend/logo.png') }}" alt="Playworld"></a></li>
                                @if(!Auth::user())
                                <li class="{{setCurrent('auth/signin')}} hide-for-large">
                                    <a href="{{ url('auth/signin') }}">
                                        <img src="{{ StaticAsset('assets/frontend/img/menu/icon_account.png') }}" alt="Home" class="nav-icon inject hide-for-large">
                                        LOGIN/DAFTAR
                                    </a>
                                </li>
                                @else
                                <li class="{{setCurrent('member*')}} hide-for-large">
                                    <a href="{{ url('member') }}">
                                        <img src="https://be2ad46f1850a93a8329-aa7428b954372836cd8898750ce2dd71.ssl.cf6.rackcdn.com/avatars/1524124742.1402.jpg" class="img-circle avatar">

                                       {{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }} 
                                    </a>
                                </li>
                                @endif
                            </ul> <!-- //end .hdr2-topbar-menu -->
                        </div>
                        <div class="col-md-3 rcolorind  show-for-large">
                            <div class="user-detail makeabs notif-trigger" data-notiftarget="user-menu">
                                <div class="row xs">
                                    @if(Auth::user())
                                        <div class="col-md-12 col-xs-12">   
                                            <div class="avatar-wrapper">
                                                <a href="{{ url('member') }}" >
                                                        <img src="https://be2ad46f1850a93a8329-aa7428b954372836cd8898750ce2dd71.ssl.cf6.rackcdn.com/avatars/1524124742.1402.jpg" class="img-circle avatar">

                                                    @if(Auth::user()->expire > date('Y-m-d'))
                                                    <img src="{{ StaticAsset('assets/frontend/img/m-menu2/v.png') }}" class="vip-icon" alt="">
                                                    @endif
                                                   
                                                </a>
                                           </div>
                                           <div class="user-name">
                                               <div class="row xs" style="line-height:21px;color: #000">
                                                   <div class="col-xs-12">
                                                       @if(Auth::user())
                                                           <div class="lh-1 mt-3 ml-2">{{ (Auth::user()->name) != '' ? Auth::user()->name : Auth::user()->msisdn }}</div>
                                                           <div class="clearfix"></div>
                                                       @else 
                                                            Kumpulin <span class="label label-danger">POIN</span> nya<br>Tukar jadi hadiah langsung
                                                       @endif
                                                   </div>
                                               </div>
                                               <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21.825 21.825" style="enable-background:new 0 0 21.825 21.825;position:absolute;right: 0;top: 50%;transform:translateY(-50%);width:15px;height:15px;" xml:space="preserve"><path style="fill:#1E201D;" d="M16.791,13.254c0.444-0.444,1.143-0.444,1.587,0c0.429,0.444,0.429,1.143,0,1.587l-6.65,6.651c-0.206,0.206-0.492,0.333-0.809,0.333c-0.317,0-0.603-0.127-0.81-0.333l-6.65-6.651c-0.444-0.444-0.444-1.143,0-1.587s1.143-0.444,1.587,0l4.746,4.762V1.111C9.791,0.492,10.299,0,10.918,0c0.619,0,1.111,0.492,1.111,1.111v16.904L16.791,13.254z"/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                                           </div>                                         
                                        </div>
                                    @else 
                                        <div class="col-md-12 col-xs-6 {{(Request::is('member*'))?'show-for-large':''}} ">
                                            <div class="text-left" style="font-size: 16px;">
                                                {{-- Kumpulin <span class="label label-danger">POIN</span> nya<br>Tukar jadi hadiah langsung   --}}
                                                <a href="{{ url('auth/signin') }}" style="display:block;padding:9px 0;">Login/Register
                                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;position:absolute;right: 0;top: 50%;transform:translateY(-50%);width:15px;height:15px;" xml:space="preserve"><path style="fill:#1E201D;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- //end .hdr2-topbar -->

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
Notifikasi
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
<div class="container height-zero">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            &nbsp;
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="notif-box" data-notif="user-notif"><span class="label label-warning"></span>
                <ul>
                    <li><a href="{{ url('member/barang_yang_didapat') }}?notif=trx">Barang yang didapatkan<span>@if(isset($notification_redeem) && count($notification_redeem) > 0){{ count($notification_redeem) }} @else 0 @endif</span></a></li>
                </ul>
            </div>

            @if(Auth::user())
            <div class="notif-box py-4" id="usermenu-hov" data-notif="user-menu">
                @include('member_new_design.partial.membermenu3')
            </div>
            @endif
        </div>
    </div>
</div>