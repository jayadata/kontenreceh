<section class="help_search py-5 mt-0" style="border-top: 1px solid #d1d1d1">
        <div class="container py-3">
            <div class="row sg">
                <div class="col-md-2 col-xs-4 show-for-large">
                    <img src="{{ StaticAsset('assets/frontend/logo.png') }}" alt="" width="140">
                </div>
                <div class="col-md-10 col-xs-12 helpsearch pl-3 pl-md-5">
                    <div class="row kontaklist mt-0 sg">
                        <div class="col-md-3 col-xs-6">
                            <div class="left">
                                <img src="{{ StaticAsset('assets/frontend/img/bantuan/cs.png') }}" alt="">
                            </div>
                            <div class="left">
                                <h2><strong><a href="tel:02122457232">021 - 22457232</a></strong></h2>
                                <span>Hari Kerja 09.00 - 18.00</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="left">
                                <img src="{{ StaticAsset('assets/frontend/img/bantuan/sms.png') }}" alt="">
                            </div>
                            <div class="left">
                                <h2><strong><a href="https://api.whatsapp.com/send?phone=6287885410267" target="_BLANK">0878-8541-0267 (XL)</a></strong></h2>
                                <h2><strong><a href="https://api.whatsapp.com/send?phone=6285591680173" target="_BLANK">0855-9168-0173 (ISAT)</a></strong></h2>
                                <span>Whatsapp / SMS</span>
                            </div>
                        </div>
                        <div class="clearfix hide-for-large"></div>
                        <div class="col-md-3 col-xs-6">
                            <div class="left">
                                <img src="{{ StaticAsset('assets/frontend/img/bantuan/smartphone.png') }}" alt="">
                            </div>
                            <div class="left">
                                <h2><strong><a href="tel:087885410267">0878-8541-0267 (XL)</a></strong></h2>
                                <h2><strong><a href="tel:085591680173">0855-9168-0173 (ISAT)</a></strong></h2>
                                <span>24 jam slow response</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <div class="left">
                                <img src="{{ StaticAsset('assets/frontend/img/bantuan/email.png') }}" alt="">
                            </div>
                            <div class="left">
                                <h2><strong><a href="mailto:halo@qomiqu.com">halo@qomiqu.com</a></strong></h2>
                                <span>Email Official</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-4 col-xs-12  hubungi">
                    <div class="hubkam">
                        HUBUNGI KAMI<br>
                        <strong><a href="tel:02122457232">021 - 22457232</a></strong><br>
                        <small>Senin s/d Jumat 09:00 - 18:00</small> <br>
                        <strong class="slowresponse"><a href="https://api.whatsapp.com/send?phone=6287885157347">0818 3030 29</a><br><small>24 Jam / Slow Response</small></strong>
                    </div>
                </div> -->
            </div>
        </div>
    </section>