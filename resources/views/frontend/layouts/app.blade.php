<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="Qomiqu.com - Bikin Kamu Ketawa Adalah Tujuan Kami">
	<meta name="keywords" content="yagitu, ketawa, humor, jokes, receh, ngakak, senyum, kesel, unch, uwu, konten">
	<meta name="author" content="Qomiqu.com">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'QOMIQU.COM') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
	<link rel="stylesheet" href="{{ asset('assets/frontend/css/app-dist.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
	<link href="{{ asset('assets/frontend/css/menuv2.css') }}?ver=<?php echo rand('1', '9999'); ?>" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="{{ asset('assets/frontend/css/modal.css') }}" rel="stylesheet">
	<link rel="shortcut icon" href="{{ asset('assets/frontend/favicon.ico') }}">
    <style>
		@media screen and (max-width: 500px) {
			.kr-card .kr-card-body {
				padding: 10px;
			}
			.text-small {
				font-size:10px;
			}
			.btn {
				padding-left: 5px;
				padding-right: 5px;
			}
		}
	</style>
</head>
<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=607732106401784";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
@if( Request::is('/') ||  Request::is('comic/*') )
	<body class="kelas" id="kelas">
@else
	<body>
@endif
    <div id="app">
		@include('frontend.partials.m-menu')
        @include('frontend.partials.header')

        <main class="py-0">
            @yield('content')
		</main>
		
        @include('frontend.partials.footer')
	</div>
	<script type="text/javascript" src="{{ StaticAsset('assets/frontend/js/lib/jquery.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript" src="{{ asset('assets/frontend/js/plugins.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/frontend/js/app.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/frontend/js/modal.js') }}"></script>
	<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
	@yield('script')
</body>
</html>
