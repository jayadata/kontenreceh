@extends('frontend.layouts.app')

@section('content')

<section>
	<div class="container">
		<div class="row mt-4">
			<div class="col-md-12">
				<p>Kami menjaga privasi pelanggan kami dengan serius dan kami hanya akan mengumpulkan, merekam, menahan, menyimpan, mengungkapkan, dan menggunakan informasi pribadi Anda seperti yang diuraikan di bawah.
                    Perlindungan data adalah masalah kepercayaan dan privasi Anda sangat penting bagi kami. Kami hanya akan menggunakan nama Anda dan informasi yang berhubungan dengan Anda sebagaimana yang dinyatakan dalam Kebijakan Privasi berikut. Kami hanya akan mengumpulkan informasi yang kami perlukan dan kami hanya akan mengumpulkan informasi yang relevan dengan transaksi antara kami dengan Anda.
                    Kami hanya akan menyimpan informasi privasi Anda selama diwajibkan oleh hukum atau selama informasi tersebut masih relevan dengan tujuan pengumpulan informasi.
                    Anda dapat mengunjungi Platform kami (Seperti yang dijelaskan pada Syarat & Ketentuan) dan menjelajahinya tanpa harus memberikan informasi pribadi. Selama kunjungan Anda ke situs kami, identitas Anda akan tetap terjaga dan kami tidak akan bisa mengidentifikasi Anda kecuali Anda memiliki akun dalam situs kami dan masuk dengan menggunakan username dan password Anda.
                    Jika Anda memiliki komentar, saran atau keluhan, Anda dapat menghubungi kami (dan Data Protection Officer kami) melalui e-mail di halo@qomiqu.com
                    Pengumpulan Informasi Pribadi<br><br>
                    Ketika Anda membuat akun Qomiqu, atau memberikan informasi pribadi Anda melalui Platform, informasi pribadi yang kami kumpulkan dapat meliputi:<br>
                        Nama<br>
                        Alamat Pengiriman<br>
                        Alamat Email<br>
                        Nomor Telepon<br>
                        Nomor Ponsel<br><br>
                    Anda harus menyerahkan kepada kami atau dalam Platform, informasi yang akurat, lengkap dan tidak menyesatkan. Anda harus tetap memperbarui dan menginformasikannya kepada kami apabila ada perubahan (informasi lebih lanjut di bawah). Kami berhak meminta dokumentasi untuk melakukan verifikasi informasi yang Anda berikan.
                    Kami hanya akan dapat mengumpulkan informasi pribadi Anda jika Anda secara sukarela menyerahkan informasi kepada kami. Jika Anda memilih untuk tidak mengirimkan informasi pribadi Anda kepada kami atau kemudian menarik persetujuan menggunakan informasi pribadi Anda, maka hal itu dapat menyebabkan kami tidak dapat menyediakan layanan kami kepada Anda. Anda dapat mengakses dan memperbarui informasi pribadi yang Anda berikan kepada kami setiap saat seperti yang dijelaskan di bawah.
                    Jika Anda memberikan informasi pribadi dari pihak ketiga manapun kepada kami, maka kami menganggap bahwa Anda telah memperoleh izin yang diperlukan dari pihak ketiga terkait untuk berbagi dan mentransfer informasi pribadinya kepada kami.
                    Jika Anda mendaftar menggunakan akun media sosial Anda atau menghubungkan akun Qomiqu ke akun media sosial Anda atau menggunakan fitur media sosial Qomiqu lainnya, kami dapat mengakses informasi tentang Anda yang terdapat dalam media sosial Anda sesuai dengan kebijakan penyedia social media bersangkutan, dan kami akan mengelola data pribadi Anda yang telah kami kumpulkan sesuai dengan kebijakan privasi Qomiqu Indonesia.
                </p>
                <p>
                    Penggunaan dan Pengungkapan Informasi Pribadi
                    Informasi pribadi yang kami kumpulkan dari Anda dapat digunakan, atau dibagikan dengan pihak ketiga (termasuk perusahaan terkait, penyedia jasa layanan pihak ketiga), untuk beberapa atau semua tujuan berikut:
                    Untuk memfasilitasi penggunaan Layanan (sebagaimana didefinisikan dalam Syarat & Ketentuan) dan / atau akses ke Platform;
                    Untuk memproses barang yang Anda dapatkan melalui Platform, baik produk yang dijual oleh Qomiqu maka pembayaran produk bisa Anda lakukan melalui Platform, sehingga akan langsung diproses oleh kami;
                    Untuk mengirimkan barang yang telah dapatkan melalui Platform, baik yang dijual atau diberikan oleh Qomiqu. Kami dapat menyampaikan informasi pribadi Anda kepada pihak ketiga dalam rangka pengiriman produk kepada Anda (misalnya untuk kurir atau supplier kami).
                    Untuk memberi informasi pengiriman produk pada Anda, baik yang dijual melalui Platform oleh Qomiqu atau penjual pihak ketiga, dan untuk kebutuhan customer support;
                    Untuk membandingkan informasi, dan memverifikasinya dengan pihak ketiga dalam rangka memastikan keakuratan informasi.
                    Selanjutnya, kami akan menggunakan informasi yang Anda berikan untuk mengelola akun Anda; memverifikasi dan melakukan transaksi keuangan dalam kaitannya dengan pembayaran yang Anda buat secara online; mengaudit pengunduhan data dari Platform; memperbarui layout dan / atau konten dari halaman Platform dan menyesuaikannya untuk pengguna; mengidentifikasi pengunjung pada Platform; melakukan penelitian tentang demografi dan perilaku pengguna kami; menyediakan informasi yang kami pikir mungkin berguna bagi Anda atau yang telah Anda minta dari kami, termasuk informasi tentang produk dan layanan kami atau penjual pihak ketiga, dimana jika Anda telah tidak keberatan dihubungi untuk tujuan ini;
                    Ketika Anda mendaftar menggunakan akun Qomiqu atau memberikan kepada kami informasi pribadi Anda melalui Platform, kami juga akan menggunakan informasi pribadi Anda untuk mengirimkan pemasaran dan / atau materi promosi tentang produk dan layanan kami atau penjual pihak ketiga dari waktu ke waktu. Anda dapat berhenti dari berlangganan dengan cara menghubungi costumer support kami.
                     Dalam keadaan tertentu, Qomiqu mungkin perlu untuk mengungkapkan informasi pribadi, seperti ketika ada alasan kuat yang dapat dipercaya bahwa pengungkapan tersebut diperlukan untuk mencegah ancaman terhadap nyawa atau kesehatan, untuk tujuan penegakan hukum, atau untuk permintaan pemenuhan persyaratan hukum dan peraturan terkait.
                </p>
                <p>      
                    Qomiqu dapat menggunakan informasi pribadi Anda dengan pihak ketiga dan afiliasi kami untuk tujuan tersebut di atas, khususnya, menyelesaikan transaksi dengan Anda, mengelola akun Anda dan hubungan kami dengan Anda, dalam rangka pemasaran dan pemenuhan persyaratan hukum atau peraturan dan permintaan yang dianggap perlu oleh Qomiqu. Dalam hal ini, kami berusaha untuk memastikan bahwa pihak ketiga dan afiliasi kami menjaga informasi pribadi Anda aman dari akses yang tidak sah, pengumpulan, penggunaan, pengungkapan, atau risiko sejenis dan menyimpan informasi pribadi Anda selama informasi pribadi Anda dibutuhkan untuk tujuan yang disebutkan di atas.
                    Dalam mengungkapkan atau pentransferan informasi pribadi Anda kepada pihak ketiga dan afiliasi kami yang berlokasi di luar negeri. Qomiqu mengambil langkah-langkah untuk memastikan bahwa yurisdiksi setempat telah mempunyai standar perlindungan informasi pribadi.
                    Qomiqu tidak terlibat dalam bisnis memperjualbelikan informasi pribadi pelanggan kepada pihak ketiga.
                    Penarikan Persetujuan
                    Anda dapat komunikasikan keberatan Anda atas penggunaan terus-menerus dan / atau pengungkapan informasi pribadi Anda untuk tujuan dan dengan cara tersebut di atas setiap saat dengan menghubungi kami di alamat e-mail kami di bawah.
                    Perlu diketahui bahwa jika Anda menyatakan keberatan Anda untuk menggunakan dan / atau pengungkapan informasi pribadi Anda atas tujuan dan dengan cara yang kami nyatakan di atas, tergantung pada sifat dari keberatan Anda, kami mungkin saja tidak dapat menyediakan produk atau layanan kami kepada Anda atau melakukan kesepakatan apapun yang kami miliki dengan Anda. Dalam hal ini hak dan upaya hukum kami telah dinyatakan secara tertulis.
                    Memperbarui Informasi Pribadi Anda
                    Anda dapat memperbarui Informasi pribadi Anda kapan saja dengan mengakses akun dalam Platform Qomiqu. Jika Anda tidak memiliki akun dengan kami, maka Anda dapet menghubungi kami pada alamat email di bawah.
                    Kami mengambil langkah-langkah untuk berbagi pembaruan informasi pribadi Anda dengan pihak ketiga dan afiliasi kami jika informasi pribadi Anda masih diperlukan untuk tujuan tersebut di atas.
                    Mengakses Personal Informasi Anda
                    Jika Anda ingin melihat informasi pribadi Anda yang kami miliki atau menanyakan tentang informasi pribadi Anda yang telah atau mungkin akan digunakan atau diungkapkan oleh Qomiqu dalam satu tahun terakhir, silahkan hubungi kami di alamat e-mail kami di bawah. 
                    Jika Anda memiliki akun Qomiqu, silakan masuk ke Platform karena di sini Anda dapat melihat rincian pesanan Anda yang telah selesai, rincian yang masih terbuka, rincian yang segera akan dikirim, dan pengelolaan alamat lengkap Anda, rincian bank, dan setiap newsletter yang mungkin telah berlangganan dengan Anda. Anda mengusahakan username, password dan rincian permintaan terjaga kerahasiaannya dan tidak memberikannya kepada pihak ketiga yang tidak berwenang. Kami tidak memiliki kewajiban menanggung penyalahgunaan username, password Qomiqu atau rincian pesanan, kecuali sebagaimana yang tercantum dalam Syarat & Ketentuan.
                </p>
                <p>    
                    Keamanan Informasi Pribadi Anda
                    Qomiqu memastikan bahwa seluruh informasi yang dikumpulkan tersimpan dengan aman. Kami menjaga informasi pribadi Anda dengan cara:
                    Membatasi akses ke informasi pribadi
                    Mengikuti kemajuan teknologi pengamanan untuk mencegah akses komputer tidak sah
                    Dengan aman menghilangkan informasi pribadi Anda ketika tidak lagi digunakan untuk keperluan hukum atau bisnis.
                    Qomiqu menggunakan teknologi enkripsi 128-bit SSL (secure socket layer) saat memproses rincian keuangan Anda. Enkripsi 128 – bit SSL membutuhkan waktu sekurang-kurangnya satu triliun tahun untuk dipecahkan, dan merupakan standar dalam industri.
                    Jika Anda yakin bahwa privasi Anda telah dilanggar oleh Qomiqu, silahkan hubungi kami di alamat e-mail kami di bawah.
                    Password Anda adalah kunci untuk masuk akun Anda. Silakan gunakan nomor unik, huruf dan karakter khusus, dan jangan berbagi password Qomiqu Anda kepada siapa pun. Jika Anda berbagi password Anda dengan orang lain, Anda akan bertanggung jawab untuk semua tindakan dan konsekuensi yang diambil atas nama akun Anda. Jika Anda kehilangan kontrol password Anda, Anda mungkin kehilangan kontrol atas informasi pribadi Anda dan informasi lainnya yang disampaikan kepada Qomiqu. Anda juga bisa dikenakan tindakan yang mengikat secara hukum yang diambil atas nama Anda. Oleh karena itu, jika password Anda telah diganggu untuk alasan apapun atau jika Anda memiliki alasan yang dapat dipercaya bahwa password Anda telah diganggu, Anda harus segera menghubungi kami dan mengganti password Anda. Anda diingatkan untuk selalu log off dari akun Anda dan menutup browser ketika selesai menggunakan komputer bersama.
                    Pengumpulan Data Komputer
                    Qomiqu atau penyedia layanan resmi kami mungkin menggunakan cookies, web beacons, dan teknologi serupa lainnya untuk menyimpan informasi dalam rangka memberi Anda pengalaman yang lebih baik, lebih cepat, lebih aman dan personal ketika Anda menggunakan Layanan dan / atau mengakses Platform.
                    Ketika Anda mengunjungi Qomiqu, server perusahaan kami akan secara otomatis menyimpan informasi bahwa browser Anda mengunjungi sebuah website. Data ini mungkin termasuk:
                    Alamat IP komputer Anda
                    Tipe Browser
                    Halaman Web yang Anda kunjungi sebelum Anda datang ke platform kami
                    Halaman-halaman dalam Platform yang Anda kunjungi
                    Waktu yang dihabiskan pada halaman tersebut, barang dan informasi yang dicari pada Platform, waktu akses dan tanggal, dan statistik lainnya.
                    Informasi ini dikumpulkan untuk analisa dan evaluasi guna membantu kami meningkatkan Platform beserta layanan dan produk yang kami sediakan.
                </p>
                <p>    
                    Cookies adalah file teks kecil (biasanya terdiri dari huruf dan angka) yang ditempatkan dalam memori browser atau perangkat ketika Anda mengunjungi website atau melihat pesan. Cookies memungkinkan kita untuk mengenali perangkat atau browser tertentu dan membantu kita dalam personalisasi konten yang sesuai dengan minat Anda lebih cepat, dan untuk membuat Layanan dan Landasan kita lebih nyaman dan berguna bagi Anda.
                    Anda dapat mengatur dan menghapus cookies melalui browser atau pengaturan perangkat Anda. Untuk informasi lebih lanjut mengenai bagaimana caranya, kunjungi materi bantuan pada browser atau perangkat Anda.
                    No Spam, Spyware, or Virus
                    Spam, Spyware, atau virus tidak diperbolehkan dalam Platform. Harap mengatur dan menjaga preferensi komunikasi Anda sehingga kami dapat mengirimkan komunikasi seperti yang Anda inginkan. Anda tidak memiliki ijin atau tidak diizinkan untuk menambahkan pengguna lain (bahkan pengguna yang telah membeli item dari Anda) ke milis Anda (email atau surat fisik) tanpa persetujuan mereka. Anda tidak boleh mengirim pesan yang mengandung spam, spyware atau virus melalui Platform. Jika Anda ingin melaporkan pesan yang mencurigakan, silahkan hubungi kami di alamat email kami di bawah.
                    Perubahan pada Kebijakan Privasi
                    Qomiqu dapat secara berkala meninjau kecukupan Kebijakan Privasi ini. Kami berhak untuk memodifikasi dan mengubah kebijakan privasi setiap saat. Setiap perubahan kebijakan ini akan dipublikasikan pada Platform.
                    Hak Qomiqu
                    ANDA MEMAHAMI DAN MENYETUJUI BAHWA QOMIQU MEMILIKI HAK UNTUK MENGUNGKAPKAN INFORMASI PRIBADI ANDA PADA SETIAP HUKUM, PERATURAN, PEMERINTAHAN, PAJAK, PENEGAKAN HUKUM ATAU PEMERINTAH ATAU PEMILIK HAK TERKAIT, JIKA QOMIQU MEMILIKI ALASAN WAJAR YANG DAPAT DIPERCAYA BAHWA PENGUNGKAPAN INFORMASI PRIBADI ANDA DIPERLUKAN UNTUK KEWAJIBAN APAPUN, SEBAGAI PERSYARATAN ATAU PENGATURAN, BAIK SUKARELA ATAU WAJIB, SEBAGAI AKIBAT DARI PESANAN, PEMERIKSAAN DAN / ATAU PERMINTAAN PIHAK TERKAIT. SEJAUH DIIZINKAN OLEH HUKUM YANG BERLAKU, DALAM HAL INI ANDA SETUJU UNTUK TIDAK MELAKUKAN TUNTUTAN APAPUN TERHADAP QOMIQU UNTUK PENGUNGKAPAN INFORMASI PRIBADI ANDA.
                    Menghubungi Qomiqu
                    Jika Anda ingin menarik persetujuan Anda dalam penggunaan informasi pribadi, meminta akses dan / atau koreksi dari informasi pribadi Anda, memiliki pertanyaan, komentar atau masalah, atau memerlukan bantuan mengenai hal-hal teknis atau terkait dengan cookies, jangan ragu untuk hubungi kami (dan Data Protection Officer kami) di halo@qomiqu.id
                    PEMBERITAHUAN PRIVASI KEPADA PENGGUNA REKENING PEMBAYARAN QOMIQU -- INDONESIA
                    Pemberitahuan ini berlaku terhadap akses dan pemakaian Anda di rekening pembayaran virtual Qomiqu (“Rekening Pembayaran Qomiqu”). Pemberitahuan ini menetapkan bagaimana kami dapat mengumpulkan, menggunakan, mengungkapkan, dan dengan cara lain mengolah “data pribadi”, “data pribadi yang teridentifikasi” atau data pribadi Anda yang lainnya (secara bersama-sama, “Data Pribadi”) atas nama Qomiqu. 
                    Istilah dalam huruf besar yang digunakan dalam Pemberitahuan Privasi ini tetapi tidak didefinisikan di sini mempunyai arti yang ditetapkan terhadapnya dalam Syarat dan Ketentuan Pengguna Rekening Pembayaran Qomiqu (“Syarat dan Ketentuan”). Sebagaimana ditetapkan dalam Syarat dan Ketentuan, Rekening Pembayaran Qomiqu Anda dapat dipakai untuk melakukan pembayaran atas pembelian barang dan jasa melalui Rekening Pembayaran Qomiqu sesuai dengan Syarat dan Ketentuan.
                    A. PENGUMPULAN DATA PRIBADI
                    Kami dapat memperoleh Data Pribadi Anda dari berbagai sumber (e.g., dari atau melalui pihak ketiga), termasuk:
                    1. Informasi yang diperoleh (secara langsung atau tidak langsung) saat Anda mendaftarkan diri sebagai pengguna Rekening Pembayaran Qomiqu, termasuk nama, alamat, nomor telpon, alamat e-mail dan informasi perangkat Anda (secara bersama-sama, “Informasi Pendaftaran”).
                    2. Informasi yang diperoleh (secara langsung atau tidak langsung) saat pemakaian Rekening Pembayaran Qomiqu oleh Anda, termasuk nomor rekening bank Anda, informasi penagihan dan pengiriman, alamat penagihan, data transaksi, nomor kartu kredit/debit dan tanggal berakhir serta informasi lainnya dari cek dan perintah pengiriman uang (secara bersama-sama, “Informasi Rekening”).
                    3. Informasi Pendaftaran, Informasi Rekening atau informasi lainnya dapat diakses atau dikumpulkan (secara otomatis atau manual) selama pendaftaran Anda sebagai pengguna Rekening Pembayaran Qomiqu dan/atau dalam waktu pemakaian Anda atas Rekening Pembayaran Qomiqu.
                    Informasi di atas yang kami peroleh dapat merupakan Data Pribadi Anda. Kami telah mengambil langkah-langkah untuk memastikan bahwa kami tidak mengumpulkan informasi lebih (baik apakah informasi tersebut merupakan Data Pribadi atau tidak) dari Anda melebihi yang kami butuhkan untuk menyediakan jasa kami kepada anda, untuk melaksanakan fungsi yang ditetapkan di dalam Bagian B pemberitahuan ini, untuk menjaga rekening Anda, menaati semua kewajiban hukum kami, dan untuk mengoperasikan usaha kami.
                </p>
                <p>    
                    B. PEMAKAIAN DATA PRIBADI
                    Kami dapat memakai Data Pribadi yang kami peroleh mengenai Anda untuk tujuan-tujuan sebagai berikut:
                    1. Memeriksa identitas Anda, termasuk pada saat pendaftaran Anda sebagai pengguna Rekening Pembayaran Qomiqu.
                    2. Memeriksa kelayakan Anda untuk mendaftar sebagai pengguna Rekening Pembayaran Qomiqu atau untuk memakai fitur dan fungsi apapun dari Rekening Pembayaran Qomiqu.
                    3. Memproses pendaftaran Anda sebagai pengguna, mengizinkan Anda untuk masuk (log-in) menggunakan ID masuk Lazada Anda dan merawat dan mengola pendaftaran Anda.
                    4. Menyediakan Anda dengan Rekening Pembayaran Qomiqu dan jasa pelanggan terkait untuk Anda, termasuk memfasilitasi penyelesaian pembelian barang dan jasa, pengiriman dan jasa terkait pembelian, tolak bayar, mengirimkan pemberitahuan mengenai transaksi Anda, dan membalas semua pertanyaan, kritik dan saran, klaim atau sengketa.
                    5. Meningkatkan, dan memperluas penawaran kami dengan melalui riset dan pengembangan dari fungsi Rekening Pembayaran Qomiqu Anda atau produk dan jasa baru lainnya yang dapat kami tawarkan dari waktu ke waktu.
                    6. Melaksanakan riset, analisis statistik atau survei, maupun secara lisan atau secara tertulis, agar dapat mengelola dan melindungi usaha kami termasuk infrastruktur informasi teknologi kami, untuk mengukur kinerja dari Rekening Pembayaran Qomiqu dan layanan lainnya yang kami tawarkan dan untuk memastikan kepuasan Anda dengan layanan kami.
                    7. Menganalisis tren, kebiasaan dan tingkah laku lainnya (baik secara individu atau secara kumpulan), yang membantu kami untuk memahami lebih baik bagaimana Anda dan pengguna kami secara kolektif mengakses dan menggunakan Rekening Pembayaran Qomiqu dan melakukan aktivitias komersil yang mendasari (underlying), termasuk untuk tujuan meningkatkan layanan kami dengan menjawab semua pertanyaan dan preferensi pelanggan.
                    8. Mengelola resiko, melaksanakan pemeriksaan kelayakan kredit dan tingkat solvensi, atau menilai, mendeteksi, menyelidiki, mencegah dan/atau mengatasi penipuan atau aktivitas yang berpotensi dilarang atau melawan hukum lainnya dan menjaga integritas dari platform pembayaran kami dengan cara lain.
                    9. Mendeteksi, menyelidiki, mencegah atau mengatasi semua pelanggaran terhadap Syarat dan Ketentuan, setiap kebjiakan internal yang berlaku, standar industri yang relevan, pedoman, hukum dan peraturan perundang-undangan.
                    10. Membuat pengungkapan sebagaimana diharuskan oleh hukum atau peraturan perundang-undangan apapun dari negara mana pun yang berlaku kepada kami atau afiliasi kami, pejabat pemerintah atau pihak ketiga lainnya, termasuk setiap asosiasi kartu atau jaringan pembayaran lainnya, Pengungkapan dapat juga dibuat sesuai dengan setiap somasi, perintah pengadilan, permintaan hukum atau proses hukum lainnya atau ketentuan dalam negara mana pun yang berlaku kepada kami atau kepada afiliasi kami.
                    11. Membuat pengungkapan apapun untuk mencegah bahaya atau kerugian finansial apa pun, untuk melaporkan setiap aktivitas yang patut dicurigai melawan hukum atau untuk mengatasi segala klaim atau klaim yang berpotensi terhadap kami atau afiliasi kami.
                    12. Melaksanakan setiap uji tuntas dan penilaian lainnya atau evaluasi untuk penggabungan, pengambilalihan, transaksi pembiayaan atau perusahaan patungan, baik untuk sesungguhnya atau sebagai rencana.
                    13. Tujuan usaha sah apapun lainnya, seperti untuk menjaga Anda dan pengguna Rekening Pembayaran Qomiqu lainnya dari kerugian, melindungi nyawa, mempertahankan keamanan sistem dan produk kami, dan menjaga hak dan/atau kepemilikan kami.
                    Kami juga dapat memakai Data Pribadi dalam cara lainnya, cara lain mana telah kami sediakan pemberitahuan khususnya pada saat pengumpulan Data Pribadi atau yang Anda setujui setelahnya.
                    C. TINDAKAN KEAMANAN DAN PENYIMPANAN
                </p>
                <p>    
                    Kami mengambil semua langkah-langkah yang wajar, termasuk pengamanan teknis, administratif dan fisik untuk membantu menjaga Data Pribadi Anda yang kami kelola dari kerugian, penyalahgunaan dan akses yang tidak diizinkan, pengungkapan, pengubahan dan penghancuran.
                    Kami akan menyimpan dan memastikan penyedia jasa kami untuk menyimpan Data Pribadi Anda hanya selama yang diperlukan untuk tujuan yang ditetapkan dalam pemberitahuan ini dan sesuai dengan semua kewajiban hukum dan peraturan yang berlaku.
                    Kami merekomendasikan Anda untuk tidak membocorkan kata sandi Anda kepada siapa pun. Personil kami tidak akan pernah menanyakan kata sandi Anda dalam panggilan telfon atau e-mail yang tidak diminta. Jika Anda memakai computer bersama orang lain, Anda seharusnya tidak menyimpan informasi masuk (log-in) Anda (misalnya ID pengguna dan kata sandi) di dalam komputer yang sama tersebut.
                    D. LAYANAN DAN SITUS WEB PIHAK KETIGA
                    Rekening Pembayaran Qomiqu dapat menyediakan link kepada situs web dan layanan lainnya (termasuk aplikasi yang dioperasikan oleh pihak ketiga) untuk kenyamanan dan informasi Anda. Layanan-layanan dan situs-situs web ini dapat beroperasi secara independen dari kami dan dapat memiliki pemberitahuan atau kebijakan privasi mereka sendiri, kami rekomendasikan dengan tegas kepada Anda untuk mengulas sebelum Anda memakai layanan apapun dari mereka atau melakukan aktivitas apa pun dalam situs web tersebut. Sejauh link situs web apapun yang Anda kunjungi tidak dimiliki atau dikendalikan oleh kami, kami tidak bertanggung jawab terhadap isi dari situs web tersebut, praktek kerahasiaan dan kualitas dari pelayanan mereka.
                    E. INFORMASI LEBIH LANJUT
                    Jika Anda ingin menghubungi kami, termasuk jika Anda ingin melaksanakan hak apapun yang mungkin Anda miliki berdasarkan hukum yang berlaku (seperti melaksanakan hak untuk memilih keluar (opt-out) dari pengolahan data), harap dilakukan dengan menghubungi Petugas Pengamanan Data dengan cara-cara sebagai berikut:
                    Kirim email ke halo@qomiqu.id
                    </p>
			</div>
		</div>
	</div>
</section>
@stop

@section('script')

@stop