@extends('frontend.layouts.default')
@section('meta')

<meta property="og:locale" content="id_ID"/>
<meta property="og:type" content="website" />
<meta property="og:site_name" content="{!! 'Playworld.id '!!}" />
<meta property="fb:admins" content="100006462279538" />
<meta property="fb:app_id" content="107188393464738" />
<meta name="twitter:card" content="summary">
<meta name="twitter:creator" content="@playworld">
<meta name="twitter:site" content="@playworld">

{{-- Set Meta from database --}}
{{-- @foreach ($meta as $met) --}}

    {{-- If Type = Property --}}
    {{-- @if ($met->type == 'property')
        <meta property="{{ $met->property_name }}" content="{{ $met->content }}" /> --}}

    {{-- Not Property --}}
    {{-- @else
        <meta name="{{ $met->property_name }}" content="{{ $met->content }}" />
    @endif

@endforeach --}}
<meta property="og:image" content="{!! StaticAsset('assets/frontend/images/eskul.jpg') !!}" />
<meta property="og:locale" content="id_ID"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ route('kelas-alt') }}"/>
<meta property="og:title" content="Fakta - Asah Minat dan Bakat Kamu" />
<meta property="og:site_name" content="{!! 'fakta.co' !!}" />
<meta property="fb:admins" content="100006462279538" />
<meta property="fb:app_id" content="107188393464738" />
<meta property="og:description" content="{!!'Tingkatkan minat dan bakat kamu dengan Eskul. Kamu akan dibimbing oleh professional yang berpengalaman di bidangnya melalui materi khas pengajar dan 100% eksklusif. Jangan lewatkan Homework dan Event untuk meningkatkan keahlian kamu.'!!}" />
<meta name="twitter:card" content="summary">
<meta name="twitter:creator" content="@playworld">
<meta name="twitter:site" content="@playworld">
<meta name="twitter:title" content="Fakta - Asah Minat dan Bakat Kamu">
<meta name="twitter:description" content="{!! 'Tingkatkan minat dan bakat kamu dengan Eskul. Kamu akan dibimbing oleh professional yang berpengalaman di bidangnya melalui materi khas pengajar dan 100% eksklusif. Jangan lewatkan Homework dan Event untuk meningkatkan keahlian kamu.'!!}">
<meta name="twitter:image" content="{!! StaticAsset('assets/frontend/images/eskul.jpg') !!}" />
<meta name="description" content="PLAYWORLD adalah media portal hiburan yang menyajikan berita terkini dengan konsep pemberitaan dari sisi yang beda, lain, tidak membosankan dan menghibur">
<meta name="keywords" content=" PLAYWORLD, Playworld, Play, Main, hiburan, entertainment, entertain, komedi, lucu, ngakak, buset, ngetrend, trend, trending, juara, apa, fokus, nyeleneh, selfie, asik, seru, instagram, facebook, google, twitter, serem, teknologi, tekno, mewah, pacar, gebetan, cinta, romantis, hadiah, undian, unik, artis, selebriti, startup, jayadata, indonesia, jakarta, bandung, musik, film, olahraga, kreatif, travel, event, bisnis, gaul, hewan, komunitas, otomotif, makanan, dunia, gaya, kesehatan, lingkungan, sains">

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var gptadslots = [];
  var googletag = googletag || {cmd:[]};
</script>
<script>
  googletag.cmd.push(function() {
    var mapping1 = googletag.sizeMapping()
                            .addSize([1024, 768], [[970, 250], [728, 90]])
                            .addSize([800, 600], [[728, 90]])
                            .addSize([0, 0], [[320, 50]])
                            .build();

    //Adslot 1 declaration
    gptadslots.push(googletag.defineSlot('/160553881/playworld/article', [[970,250],[728,90]], 'div-gpt-ad-5274233-1')
                             .setTargeting('pos', ['Leaderboard'])
                             .defineSizeMapping(mapping1)
                             .addService(googletag.pubads()));
    //Adslot 2 declaration
    gptadslots.push(googletag.defineSlot('/160553881/playworld/article', [[300,600],[300,250]], 'div-gpt-ad-5274233-2')
                             .setTargeting('pos', ['MR1'])
                             .addService(googletag.pubads()));

    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<style>
	.fakta-item .text-warning{
		color: #1aff00!important;
	}
	.fakta-item .btn-warning{
		background: #1aff00!important;
		border-color: #1aff00!important;
		color: #000!important;
	}
</style>
@stop
@section('title')
Playworld - Bikin Kamu Terhibur dan Gak Bosenin
@stop

@section('content')
<?php
$string = New App\Http\Library\String_library;
?>

<section>
		<div class="container">
			{{-- <div class="row py-4">
				<div class="col-md-12 text-center">
					<img src="{{ StaticAsset('assets/frontend/img/eskul.png') }}" alt="" width="200">
				</div>
			</div> --}}
			<div class="row mt-4 faktalogo">
				<div class="col-md-12">
					<img src="{{ StaticAsset('assets/frontend/img/kelas/faktamenariq.png') }}" alt="">
				</div>
			</div>
			<div class="row mt-4">
				<div class="col-md-12">
					<div class="py-5 px-5" style="background: #212121;">
						<div class="row mb-3 mb-md-5">
							<div class="col-md-4 mb-3 mb-md-0">
								<div class="kelas-icon">
									<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
								</div>
								<div class="text-xs-18">ORIGINAL & EXCLUSIVE VIDEO CONTENT</div>
							</div>
							<div class="clearfix hide-for-large"></div>
							<div class="col-md-4 mb-3 mb-md-0">
								<div class="kelas-icon">
									<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
								</div>
								<div class="text-xs-18">SOCIAL MEDIA FRIENDLY</div>
							</div>
							<div class="clearfix hide-for-large"></div>
							<div class="col-md-4 mb-3 mb-md-0">
								<div class="kelas-icon">
									<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
								</div>
								<div class="text-xs-18">INCREASE YOUR FOLLOWERS</div>
							</div>
						</div>
						<div class="row sg">
							<div class="col-md-1 col-xs-2">
								<img src="{{ StaticAsset('assets/frontend/img/lampu.png') }}" alt="">
							</div>
							<div class="col-md-7 col-xs-10 brd-r-1 m-brd-r-0">
								<h4 class="text-bold text-xs-18">MENYAJIKAN FAKTA-FAKTA YANG INFORMATIF DAN MENARIQ!</h4>
								<div class="text-gray rdmore">
									Buat kamu yang suka dengan fakta fakta menarik seputar sejarah, sains, teknologi dan informasi terkini FAKTA MENARIQ bisa memenuhinya. Konten yang kami buat sangat original dan ekslusif serta cocok untuk kamu yang ingin meningkatkkan followers di social media.
								</div>
							</div>
							<div class="clearfix hide-for-large"></div>
							<div class="col-md-4 pt-3">
								<div class="row">
									<div class="col-md-12">
										<div class="pl-md-5">
											<strong>TAMPILKAN FAKTA:</strong>
										</div>
									</div>
									<div class="col-md-12">
										<div class="pl-md-5">
											<select name="filter_fakta" class="form-control mt-2 filter_fakta">
												<option value="">Semua Fakta</option>
												@foreach($kategori as $data)
													<option value="{{ $data->id }}" {{(Request::get('filter_fakta') == $data->id)?'selected="selected':''}} >{{ $data->name }}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<section class="mt-0 pt-0">
	<div class="container">
		<div class="row">

			@foreach($fakta as $data)
				<div class="col-md-4">
					<div class="kelas-item fakta-item mb-5">
						<a href="{{ route('fakta-kategori', slug($data->slug)) }}">
							<img src="{{ StaticAsset('assets/frontend/img/kelas/'.$data->img) }}" alt="">
							{{-- <img data-src="{{ StaticAsset($data->img) }}" alt=""> --}}
						</a>
						<div class="kelas-caption">
							<div class="kelas-switch-up">
								<h2 class="text-huge text-bold mb-1 mt-0"><a href="{{ route('fakta-kategori', slug($data->slug)) }}" class="text-warning">{{ $data->name }}</a></h2>
							</div>
							<div class="kelas-switch-down">
								<a href="{{ route('fakta-kategori', slug($data->slug)) }}" class="btn btn-warning px-5">MASUK</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach

		</div>
	</div>
</section>
<script>
    jQuery(document).ready(function($){
        $('.filter_fakta').change(function(){
            //alert('test');
            if($(this).val() == '')
            {
                window.location.href ="{{ url('fakta') }}";
            }
            else
            {
                window.location.href ="{{ url('fakta') }}?filter_fakta="+$(this).val();

            }
        });
    });
</script>
@stop