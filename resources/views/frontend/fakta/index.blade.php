@extends('frontend.layouts.app')

@section('content')

<section>
	<div class="container">
		<div class="row mt-4">
			<div class="col-md-12">
				<div class="py-5 px-5" style="background: #212121;color: #fff;">
					<div class="row mb-3 mb-md-5">
						<div class="col-md-4 mb-3 mb-md-0">
							<div class="kelas-icon">
								<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
							</div>
							<div class="text-xs-18">ORIGINAL & EXCLUSIVE VIDEO CONTENT</div>
						</div>
						<div class="clearfix hide-for-large"></div>
						<div class="col-md-4 mb-3 mb-md-0">
							<div class="kelas-icon">
								<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
							</div>
							<div class="text-xs-18">SOCIAL MEDIA FRIENDLY</div>
						</div>
						<div class="clearfix hide-for-large"></div>
						<div class="col-md-4 mb-3 mb-md-0">
							<div class="kelas-icon">
								<img src="{{ StaticAsset('assets/frontend/img/success.png') }}" alt="">
							</div>
							<div class="text-xs-18">INCREASE YOUR FOLLOWERS</div>
						</div>
					</div>
					<div class="row sg">
						<div class="col-md-1 col-xs-2">
							<img src="{{ StaticAsset('assets/frontend/img/lampu.png') }}" alt="">
						</div>
						<div class="col-md-7 col-xs-10 brd-r-1 m-brd-r-0">
							<h4 class="text-bold text-xs-18">MENYAJIKAN FAKTA-FAKTA YANG INFORMATIF DAN MENARIQ!</h4>
							<div class="text-gray rdmore">
								Buat kamu yang suka dengan fakta fakta menarik seputar sejarah, sains, teknologi dan informasi terkini FAKTA MENARIQ bisa memenuhinya. Konten yang kami buat sangat original dan ekslusif serta cocok untuk kamu yang ingin meningkatkkan followers di social media.
							</div>
						</div>
						<div class="clearfix hide-for-large"></div>
						<div class="col-md-4 pt-3">
							<div class="row">
								<div class="col-md-12">
									<div class="pl-md-5">
										<strong>TAMPILKAN FAKTA:</strong>
									</div>
								</div>
								<div class="col-md-12">
									<div class="pl-md-5">
										<select name="category" class="form-control mt-2 category">
											<option value="">Semua Kategori</option>
											@foreach($content_categories as $data)
												<option value="{{ $data->id }}" {{(Request::get('category') == $data->id)?'selected="selected':''}} >{{ $data->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-0 pt-0">
	<div class="container">
		<div class="row">
			@foreach($content_account as $data)
				<div class="col-md-4">
					<div class="kelas-item fakta-item mb-5">
						<a href="{{ route('content.catalog', $data->slug) }}">
							<img src="{{ contentImageUrl($data->image) }}" alt="">
						</a>
						<div class="kelas-caption">
							<div class="kelas-switch-up">
								<h2 class="text-huge text-bold mb-1 mt-0"><a href="#" class="text-warning">{{ $data->name }}</a></h2>
							</div>
							<div class="kelas-switch-down">
								<a href="{{ route('content.catalog', $data->slug) }}" class="btn btn-warning px-5">MASUK</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</section>
@stop

@section('script')
<script>
    jQuery(document).ready(function($){
        $('.category').change(function(){
            if($(this).val() == '')
            {
                window.location.href ="{{ url('/') }}";
            }
            else
            {
                window.location.href ="{{ url('/') }}?category="+$(this).val();

            }
        });
    });
</script>
@stop