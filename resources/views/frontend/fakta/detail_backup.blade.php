@extends('frontend.layouts.default')
@section('meta')
    <meta property="og:image" content="{!! StaticAsset('assets/frontend/images/cover.png') !!}" />
    <meta property="og:locale" content="id_ID"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url('playunik') }}"/>
    <meta property="og:title" content="Playworld - Playunik" />
    <meta property="og:site_name" content="{!! 'Playworld.id '!!}" />
    <meta property="fb:admins" content="100006462279538" />
    <meta property="fb:app_id" content="107188393464738" />
    <meta property="og:description" content="{!!'PLAYWORLD ID adalah social entertainment news and interaction website, yang menyuguhkan informasi hiburan dan sosial, yang dikemas dengan interaksi mengumpulkan POIN'!!}" />
    <meta name="twitter:card" content="summary">
    <meta name="twitter:creator" content="@playworld">
    <meta name="twitter:site" content="@playworld">
    <meta name="twitter:title" content="Playworld - Playunik">
    <meta name="twitter:description" content="{!! 'PLAYWORLD ID adalah social entertainment news and interaction website, yang menyuguhkan informasi hiburan dan sosial, yang dikemas dengan interaksi mengumpulkan POIN'!!}">
    <meta name="twitter:image" content="{!! StaticAsset('assets/frontend/images/cover.png') !!}" />
<meta name="description" content="PLAYWORLD adalah media portal hiburan yang menyajikan berita terkini dengan konsep pemberitaan dari sisi yang beda, lain, tidak membosankan dan menghibur">
<meta name="keywords" content=" PLAYWORLD, Playworld, Play, Main, hiburan, entertainment, entertain, komedi, lucu, ngakak, buset, ngetrend, trend, trending, juara, apa, fokus, nyeleneh, selfie, asik, seru, instagram, facebook, google, twitter, serem, teknologi, tekno, mewah, pacar, gebetan, cinta, romantis, hadiah, undian, unik, artis, selebriti, startup, jayadata, indonesia, jakarta, bandung, musik, film, olahraga, kreatif, travel, event, bisnis, gaul, hewan, komunitas, otomotif, makanan, dunia, gaya, kesehatan, lingkungan, sains">

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var gptadslots = [];
  var googletag = googletag || {cmd:[]};
</script>
<script>
  googletag.cmd.push(function() {
    var mapping1 = googletag.sizeMapping()
                            .addSize([1024, 768], [[970, 250], [728, 90]])
                            .addSize([800, 600], [[728, 90]])
                            .addSize([0, 0], [[320, 50]])
                            .build();

    //Adslot 1 declaration
    gptadslots.push(googletag.defineSlot('/160553881/playworld/article', [[970,250],[728,90]], 'div-gpt-ad-5274233-1')
                             .setTargeting('pos', ['Leaderboard'])
                             .defineSizeMapping(mapping1)
                             .addService(googletag.pubads()));
    //Adslot 2 declaration
    gptadslots.push(googletag.defineSlot('/160553881/playworld/article', [[300,600],[300,250]], 'div-gpt-ad-5274233-2')
                             .setTargeting('pos', ['MR1'])
                             .addService(googletag.pubads()));

    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
@stop
@section('title')
Playworld - Bikin Kamu Terhibur dan Gak Bosenin
@stop

@section('content')
<?php
$string = New App\Http\Library\String_library;
?>
@include('frontend.mainmodal')

<section style="background: #353535" class="py-0"> 
	@foreach($portofolio as $data)
		<div class="kelas-video-banner">
			{!! $data->embed !!}

			<div class="kelas-video-caption text-center">
				<h1 class="text-bold text-large text-warning mb-0 uppercase">{{ $data->kelas->title }}</h1>
				<h2 class="text-white text-light mb-0">{{ $data->sub_title }}</h2>
			</div>
		</div>
	@endforeach
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="kelas-summary position-relative">
					<div class="row">
						<div class="col-md-6">
							<div class="inline text-hugeup">
								{{ $kelas->topik() }} Topik
							</div>
							<div class="inline ml-0 ml-md-5 text-hugeup">
								100% EKSKLUSIF
							</div>
							<div class="clearfix"></div>
							<div class="mt-4 text-gray">
								Setiap materi akan dipaparkan secara mendalam namun mudah dipahami juga penuh dengan tips & trik yang akan membuat kamu semakin jago memasak
							</div>
						</div>
						<div class="col-md-6 position-static">
							<h3 class="mt-0 text-hugeup" style="line-height:31px;margin-bottom: 25px;">BELI SEMUA LEBIH HEMAT</h3>
							<div class="text-gray mt-4">
								<div class="row sg position-static">
									<div class="col-md-8">
										Dapatkan seluruh materi dan tanpa batas waktu dengan sekali bayar
									</div>
									<div class="col-md-4 text-right position-static">
									<a href="#" class="btn btn-outline-gold sejajar popup-trigger" data-onshow="vipcheck()" data-buttonaction="pwmodal('close')" >Rp {{ number_format($kelas->grand_price()) }}</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="kelas-materi-list">
					
					<!-- materi item -->
					<?php $i = 1 ?>
					@foreach($main as $data)
						<div class="materi-item">
							<div class="row">
								<div class="col-md-9">
									<div class="mi-numb text-huge text-warning text-bold">
										{{ $i++ }}.
									</div>
									<div>
										<h2 class="m-0"><a href="#">{{ $data->title }}</a></h2>
										
										<div class="rating"><span data-width="{{ $data->star }}"></span></div>

										<div class="clearfix"></div>
										<p>{{ $data->description }}</p>
									</div>
								</div>
								<div class="col-md-3 text-right">
									<img src="{{ StaticAsset('assets/frontend/img/orange-check.png') }}" alt="" class="inline mr-5" width="50">
									
									@if($i > 2)
										@if(Auth::user())
											<a href="#" class="btn btn-outline-warning btn-md text-bold">
											{{-- <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_KoinYellow.png') }}" class="mr-2 inline" alt="" width="20"> --}}
											Rp {{ number_format($data->price) }}
										@else
											<a href="{{ url('auth/signin') }}" class="btn btn-outline-info btn-md text-bold">
											MASUK <i class="ml-3 fa fa-play-circle"></i>
										@endif
									@else
										<a href="#" class="btn btn-outline-success btn-md">GRATIS <i class="ml-3 fa fa-play-circle"></i></a>
									@endif
								</a>
								</a>
								</div>
							</div>
						</div><!-- /materi item -->
					@endforeach
					<div class="kelas-materi-list-2">
						@include('frontend.kelas.loadmore')
					</div>
					
					<div class="text-center p-4 btn_laod_more">
						<button class="btn btn-warning btn-lg text-big loadmore">Lihat semua</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row sg mb-4">
					<div class="col-md-6">
						<h4 class="text-bold text-warning text-huge">EVENT</h4>
					</div>
					<div class="col-md-6 text-left text-md-right">
						<a href="#" class="btn btn-outline-gold">Lihat Semua</a>
					</div>
				</div>

				<div class="kelas-summary hide">
					<div class="row">
						<div class="col-md-6">
							<div class="inline text-big">
								<img src="{{ StaticAsset('assets/frontend/img/kelas/ticket.png') }}" class="mr-2" alt="" width="40">
								100% EKSKLUSIF
							</div>
							<div class="inline ml-5 text-big">
								<img src="{{ StaticAsset('assets/frontend/img/kelas/calendar.png') }}" class="mr-2" alt="" width="30">
								2 KONTEN BARU / BULAN
							</div>
							<div class="clearfix"></div>
							<div class="mt-4 text-gray">
								Dapatkan materi tambahan di luar Kelas Utama. Materi tambahan ini akan menambah wawasan dan melatih keterampilan kamu dengan trend yang berkembang saat ini
							</div>
						</div>
						<div class="col-md-6">
							<h3 class="mt-0 text-big" style="line-height:31px">&nbsp;</h3>
							<div class="text-gray mt-4">
								<div class="row sg">
									<div class="col-md-12 text-right">
										<a href="#" class="btn btn-warning">Lihat Semua</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="ekskul bg-white text-gray2">
					<div class="row equalizerv2">
						<?php $i=1 ?>
						@foreach($ekstra as $data)
							<div class="col-md-6 eq_target">
								
								<div class="py-5 px-5">
									<div class="row sg">
										<div class="col-md-12 text-small">
											<img src="{{ StaticAsset('assets/frontend/img/kelas/timer.png') }}" width="20" class="inline mr-2" alt="">
											{{-- 2018/06/01 14:00:00 --}}
											{{ $data->created_at }}
										</div>
									</div>

									<div class="row sg mt-3">
										<div class="col-md-5">
											<img src="{{ newsImageUrl($data->thumbnail) }}" alt="">
										</div>
										<div class="col-md-7">
											<h3 class="mt-0 mb-3"><a href="javascript:;">{{ $data->sub_title }}</a></h3>
											{{-- Berikut materi utama yang dijelaskan :<br>
											- Menyiapkan salmon beku sebelum dimasak<br>
											- Seasoning salmon dengan benar<br>
											- Cara menggoreng salmon yang benar --}}
											{{ substr($data->description, 0, 170) }}...
											<div class="clearfix mb-3"></div>

											@if($i++ > 2)
												@if(Auth::user())
													<a href="#" class="btn btn-outline-warning btn-md text-bold text-16 py-3">
													{{-- <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_KoinYellow.png') }}" class="mr-2 inline" alt="" width="20"> --}}
													Rp {{ number_format($data->price) }}</a>
												@else
													<a href="{{ url('auth/signin') }}" class="btn btn-outline-info btn-md text-bold text-16 py-3">
													MASUK <i class="ml-3 fa fa-play-circle"></i></a>
												@endif
											@else
												<a href="#" class="btn btn-outline-success btn-md text-bold text-16 py-3">GRATIS <i class="ml-3 fa fa-play-circle"></i></a>
											@endif
											<?php $i++ ?>
										</div>
									</div>
								</div>

							</div>
						@endforeach()
						{{-- <div class="col-md-6 eq_target">
							
							<div class="py-5 px-5">
								<div class="row sg">
									<div class="col-md-12 text-small">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/timer.png') }}" width="20" class="inline mr-2" alt="">
										2018/06/01 14:00:00
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-5">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/marinka.png') }}" alt="">
									</div>
									<div class="col-md-7">
										<h3 class="mt-0 mb-0"><a href="javascript:;">Tips cepat dan mudah memasak salmon dengan baik</a></h3>
										<div class="rating"><span data-width="80"></span></div>
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-12">
										Berikut materi utama yang dijelaskan :<br>
										- Menyiapkan salmon beku sebelum dimasak<br>
										- Seasoning salmon dengan benar<br>
										- Cara menggoreng salmon yang benar
										<div class="clearfix"></div>
										<a href="#" class="btn btn-outline-success mt-3">GRATIS <i class="ml-2 fa fa-play-circle"></i></a>
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-6 eq_target">
							
							<div class="py-5 px-5">
								<div class="row sg">
									<div class="col-md-12 text-small">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/timer.png') }}" width="20" class="inline mr-2" alt="">
										2018/06/01 14:00:00
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-5">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/marinka.png') }}" alt="">
									</div>
									<div class="col-md-7">
										<h3 class="mt-0 mb-0"><a href="javascript:;">Tips cepat dan mudah memasak salmon dengan baik</a></h3>
										<div class="rating"><span data-width="80"></span></div>
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-12">
										Berikut materi utama yang dijelaskan :<br>
										- Menyiapkan salmon beku sebelum dimasak<br>
										- Seasoning salmon dengan benar<br>
										- Cara menggoreng salmon yang benar
										<div class="clearfix"></div>
										<a href="#" class="btn btn-outline-success mt-3">GRATIS <i class="ml-2 fa fa-play-circle"></i></a>
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-6 eq_target">
							
							<div class="py-5 px-5">
								<div class="row sg">
									<div class="col-md-12 text-small">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/timer.png') }}" width="20" class="inline mr-2" alt="">
										2018/06/01 14:00:00
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-5">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/marinka.png') }}" alt="">
									</div>
									<div class="col-md-7">
										<h3 class="mt-0 mb-0"><a href="javascript:;">Tips cepat dan mudah memasak salmon dengan baik</a></h3>
										<div class="rating"><span data-width="80"></span></div>
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-12">
										Berikut materi utama yang dijelaskan :<br>
										- Menyiapkan salmon beku sebelum dimasak<br>
										- Seasoning salmon dengan benar<br>
										- Cara menggoreng salmon yang benar
										<div class="clearfix"></div>
										<a href="#" class="btn btn-outline-success mt-3">GRATIS <i class="ml-2 fa fa-play-circle"></i></a>
									</div>
								</div>
							</div>

						</div> --}}
						
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

{{-- <section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="text-bold text-warning text-huge">TANYA JAWAB</h4>
			
				<div class="kelas-summary">
					<div class="row">
						<div class="col-md-6">
							<div class="text-gray">
								Tukarkan KOIN dan POIN kamu, yang meraih POIN tertinggi dialah yang berhak memenangkan merchandise keren di bawah ini
							</div>
						</div>
						<div class="col-md-6">
							<div class="text-gray">
								<div class="row sg mt-4">
									<div class="col-md-12 text-right">
										<a href="#" class="btn btn-success">Kirimkan Pertanyaan</a>
										<a href="#" class="btn btn-warning">Lihat Semua</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="ekskul bg-white">
					<div class="row equalizerv2 text-gray2">
						<div class="col-md-6 eq_target">
							
							<div class="py-5 px-5">
								<div class="row sg">
									<div class="col-md-12 text-small">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/timer.png') }}" width="20" class="inline mr-2" alt="">
										2018/06/01 14:00:00
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-5">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/marinka.png') }}" alt="">
									</div>
									<div class="col-md-7">
										<h3 class="mt-0 mb-0"><a href="javascript:;">Tips cepat dan mudah memasak salmon dengan baik</a></h3>
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-12">
										<a href="#" class="btn btn-outline-success mt-3">GRATIS <i class="ml-2 fa fa-play-circle"></i></a>
									</div>
								</div>
							</div>

						</div>
						<div class="col-md-6 eq_target">
							
							<div class="py-5 px-5">
								<div class="row sg">
									<div class="col-md-12 text-small">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/timer.png') }}" width="20" class="inline mr-2" alt="">
										2018/06/01 14:00:00
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-5">
										<img src="{{ StaticAsset('assets/frontend/img/kelas/marinka.png') }}" alt="">
									</div>
									<div class="col-md-7">
										<h3 class="mt-0 mb-0"><a href="javascript:;">Tips cepat dan mudah memasak salmon dengan baik</a></h3>
									</div>
								</div>

								<div class="row sg mt-3">
									<div class="col-md-12">
										<a href="#" class="btn btn-outline-success mt-3">GRATIS <i class="ml-2 fa fa-play-circle"></i></a>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> --}}

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="text-bold text-warning text-huge">MERCHANDISE</h4>

				<div class="kelas-summary">
					<div class="row">
						<div class="col-md-6">
							<div class="text-gray">
								Tukarkan KOIN dan POIN kamu, yang meraih POIN tertinggi dialah yang berhak memenangkan merchandise keren di bawah ini
							</div>
						</div>
						<div class="col-md-6">
							<div class="text-gray">
								<div class="row sg mt-0">
									<div class="col-md-12 text-right">
										<a href="{{ route('detail-partner', [$list_partner->id, slug($list_partner->title)]) }}" class="btn btn-warning">Lihat Semua</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="bg-white pt-5 pb-4 px-5">
					<div class="row sg simpler fpb2">
						@foreach($partner as $row )
						    <div class="col-md-3 col-xs-12 cell-item">
						        <a href="{{url('redeem', $row->id).'/'.slug($row->title)}}">
						            <div class="nc-item forpoin">
						                <div class="nc-item-thumb">
						                    <img src="{{ newsImageUrl($row->image) }}" alt="">
						                    <div class="nc-item-meta hide-for-large">
						                        <div class="nc-item-meta-normal">
						                            {{nomor_cantik($row->min_point)}} POIN 
						                        </div>
						                    </div>
						                </div>
						                <div class="nc-item-meta show-for-large">
						                    <div class="nc-item-meta-normal">
						                        {{nomor_cantik($row->min_point)}} POIN 
						                    </div>
						                </div>
						                <div class="clearfix"></div>
						                <div class="nc-item-inner">
						                    <div class="p-3">
						                        <div class="rdm-title">
						                            <strong>{{$row->title}} </strong>
						                            <div class="clearfix"></div>
						                        </div>
						                        <div class="tarobawah">
						                            <div class="col-xs-12">
						                                <a href="{{url('redeem', $row->id).'/'.slug($row->title)}}" title="Dapatkan Sekarang" class="btn btn-sm btn-outline-warning btn-block">Dapatkan Sekarang</a>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						            </div>
						        </a>
						    </div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 class="text-bold text-warning text-huge">PROFILE</h4>

				<div class="kelas-summary py-5">
					<div class="row">
						<div class="col-md-12">
							<div class="text-light kelas-social">
								@foreach($social as $data)
									@if($data->type == 1)
										<a href="{{ $data->url }}" target="_blank"><i class="fa fa-instagram"></i> {{ $data->social }}</a>
									@endif

									@if($data->type == 2)
										<a href="{{ $data->url }}" target="_blank"><i class="fa fa-twitter"></i> {{ $data->social }}</a>
									@endif

									@if($data->type == 3)
										<a href="{{ $data->url }}" target="_blank"><i class="fa fa-facebook"></i> {{ $data->social }}</a>
									@endif
								
								@endforeach
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<div class="py-5 px-5" style="background:url({{ newsImageUrl($kelas->banner) }}) no-repeat center right;background-size:cover">
					<div class="row text-light py-3">
						<div class="col-md-7">
							<div class="cover-profilx">
								<h1 class="uppercase">{{ $kelas->title }}</h1>
								<p>{{ $kelas->desc }}</p>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

{{-- PURCHASE VIP --}}
<div class="purchaseVIP hide">
    <div>
        {{-- STEP 1 --}}
        <div class="vip-step step-1 active">
            <div class="card-container">
                <div class="text-14 px-4 pt-3">
                    Dengan Pulsa
                </div>
                <hr>
                <div class="card-body">
                    <a href="#" class="btn btn-block btn-success to-step-2" data-target="xl-axis">XL &amp; Axis</a>
                    <a href="#" class="btn btn-block btn-success to-step-2" data-target="indosat">Indosat</a>
                    <a href="#" class="btn btn-block btn-success to-step-2" data-target="telkomsel">Telkomsel</a>
                </div>
            </div>
            <div class="card-container">
                <div class="text-14 px-4 pt-3">
                    Tanpa Pulsa
                </div>
                <hr>
                <div class="card-body">
                    <a href="#" class="btn btn-block btn-success to-step-2" data-target="bca">Transfer ke Bank BCA</a>
                </div>
            </div>
        </div>

        {{-- Step 2 --}}
        <div class="vip-step step-2">
            <div class="card-container xl-axis mt-3">
                <div class="text-14 px-4 pt-3">
                   XL & AXIS
                </div>
                <hr>
                <div class="pwmodal-vip-items">
                    <?php 
                        $axis = array(
                            array(
                                "nama" => "Judul Materi",
                                "harga" => $main_sum,
                            ),
                        );
                    ?>
                    @foreach ($axis as $item)
                        <div class="pwmodal-vip-item" data-action="xlaxis_subscribe()" data-nama="{{ $item['nama'] }}" data-harga="{{ $item['harga'] }}">
                            <div class="row xs">
                                <div class="col-xs-2">
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                    @else
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                    @endif
                                </div>
                                <div class="col-xs-10">
                                    <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                    @endif
                                    <div class="text-13 mt-2">
                                        <strong>Rp. {{ $item['harga'] }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-container indosat mt-3">
                <div class="text-14 px-4 pt-3">
                   Indosat
                </div>
                <hr>
                <div class="pwmodal-vip-items">
                    <div class="pwmodal-vip-item">
                        <div class="row xs">
                            <div class="col-xs-2">
                                 <img src="{{ StaticAsset('assets/frontend/img/koin/vip-s.svg') }}" width="80" alt="">
                            </div>
                            <div class="col-xs-10">
                                <div class="text-16 text-light bg-warning px-2 py-1 mb-4 mb-md-3" style="display:block;border-radius:5px">
                                    Minggu Ke-1<br>
                                    <strong>7 Hari VIP</strong>
                                </div>

                                <div class="text-16 text-light bg-warning px-2 py-1 mb-3" style="display:block;border-radius:5px">
                                    Minggu Selanjutnya<br>
                                    <strong>5 Hari VIP</strong>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Pembelian Pertama mendapatkan 2 Hari VIP Seharga Rp. 2200</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Selanjutnya Perpanjangan Hari VIP dikirim dalam 2 SMS per minggu</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Rp. 2200 per SMS untuk Perpanjangan 2 Hari VIP</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>SMS Ke-3 disetiap minggunya akan diberikan Extra Perpanjangan 1 Hari VIP</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Harga sudah termasuk pajak PPN 10%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-container telkomsel mt-3">
                <div class="text-14 px-4 pt-3">
                   Telkomsel
                </div>
                <hr>
                <div class="pwmodal-vip-items">
                    <?php 
                        $telkomsels = array(
                            array(
                                "nama" => "Judul Materi",
                                "harga" => $main_sum,
                                "hari" => 2,
                                "id" => 40141,
                            ),
                        );
                    ?>
                    @foreach ($telkomsels as $item)
                        <div class="pwmodal-vip-item" data-action="telkomsel_dialog('{{ $item['harga'] }}', '{{ $item['hari'] }}', '{{ $item['id'] }}')">
                            <div class="row xs">
                                <div class="col-xs-2">
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                    @else
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                    @endif
                                </div>
                                <div class="col-xs-10">
                                    <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                    @endif
                                    <div class="text-13 mt-2">
                                        <strong>Rp. {{ $item['harga'] }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-container bca mt-3">
                <div class="text-14 px-4 pt-3">
                   Transfer ke Bank BCA
                </div>
                <hr>
                <div class="pwmodal-vip-items">
                    <?php 
                        $bca = array(
                            array(
                                "nama" => "Judul materi",
                                "harga" => $main_sum,
                                "voucher" => 12,
                            ),
                        );
                    ?>
                    @foreach ($bca as $item)
                        <div class="pwmodal-vip-item pay-button" data-voucher="{{ $item['voucher'] }}">
                            <div class="row xs">
                                <div class="col-xs-2">
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                    @else
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                    @endif
                                </div>
                                <div class="col-xs-10">
                                    <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                    @endif
                                    <div class="text-13 mt-2">
                                        <strong>Rp. {{ $item['harga'] }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="vip-step step-3"></div>
    </div>
</div>

<script type="text/javascript">

	// Beli
    beliCoy('harga') {

	}

    $('.loadmore').click(function(){

        jQuery.ajax({
            type: "post",
            url: '{{ route('loadmore-detail-kelas') }}',
            dataType: 'html',
            data: {
                'id'      : {{ $kelas->id }},
                '_token'    :'{!!csrf_token()!!}'
            },
            beforeSend: function(data) {

            },
            success: function (response) {
                console.log(response);
                $('.kelas-materi-list-2').html(response);
                $('.btn_laod_more').hide();
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
                var err = eval("(" + xhr.responseText + ")");
                //alert(err.Message);

            } 
        });
    });
</script>
@stop

@section('scripts')
<script src="https://www.google.com/recaptcha/api.js"
    async defer>
</script>
<script>
    var $ = jQuery;
    @if (Auth::user())
        var obj = {
            'nama': '{{ Auth::user()->first_name.' '.Auth::user()->last_name }}',
            'telp': '{{ Auth::user()->no_telp }}',
            'mail': '{{ Auth::user()->email }}'
        };
        var user = JSON.stringify(obj);
    @endif;

    function verification() {
        pwModalPushContent( $('.tocopy').html() );
        pwModalHeaderText('Verification');
        pwModalBtnText('Tutup');
        pwmodal('loaded');
    }

    function back() {
        $('.header-back').unbind("click").on('click', function(e){
            e.preventDefault();
            pwmodal('close');
            return false;
        });
    }

    function back_1() {
        $('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
        if( $('.header-back').find('.header-prev').length ) {
            $('.header-back').unbind("click").on('click', function(e){
                e.preventDefault();
                var $step = $(this).data('back');
                $('.pw-modal .vip-step.step-1').addClass('active');
                $('.pw-modal .vip-step.step-2').removeClass('active').find('.card-container').hide();
                $('.pw-modal .vip-step.step-3').removeClass('active').html('');
                $('.header-back').find('.sprite').addClass('modal-close-dark').removeClass('header-prev');
                back();
                //alert('anjing');
                return false;
            });
        }
    }

    function back_2() {
        if( $('.header-back').find('.header-prev').length ) {
            $('.header-back').unbind("click").on('click', function(e){
                e.preventDefault();
                $('.pw-modal .vip-step.step-2').addClass('active').find('.card-container');
                $('.pw-modal .vip-step.step-3').removeClass('active').html('');
                $('.pw-modal .vip-step.step-1').removeClass('active');
                back_1();
                return false;
            });
        }
    }

    function vipcheck() {
        @if(Auth::user())
		var login = 
			'   <div class="text-center mt-5 pt-5">  '  + 
			'       <svg width="55" height="55" x="0" y="0" class="stencil--easier-to-select" style="opacity: 1;"><defs></defs><rect x="0" y="0" width="55" height="55" fill="transparent" class="stencil__selection-helper"></rect><path d="M24.75 41.25C24.75 41.25 30.25 41.25 30.25 41.25 30.25 41.25 30.25 24.75 30.25 24.75 30.25 24.75 24.75 24.75 24.75 24.75 24.75 24.75 24.75 41.25 24.75 41.25 24.75 41.25 24.75 41.25 24.75 41.25M27.5 0C12.32 0 0 12.32 0 27.5 0 42.68 12.32 55 27.5 55 42.68 55 55 42.68 55 27.5 55 12.32 42.68 0 27.5 0 27.5 0 27.5 0 27.5 0M27.5 49.5C15.372499999999999 49.5 5.5 39.6275 5.5 27.5 5.5 15.372499999999999 15.372499999999999 5.5 27.5 5.5 39.6275 5.5 49.5 15.372499999999999 49.5 27.5 49.5 39.6275 39.6275 49.5 27.5 49.5 27.5 49.5 27.5 49.5 27.5 49.5M24.75 19.25C24.75 19.25 30.25 19.25 30.25 19.25 30.25 19.25 30.25 13.75 30.25 13.75 30.25 13.75 24.75 13.75 24.75 13.75 24.75 13.75 24.75 19.25 24.75 19.25 24.75 19.25 24.75 19.25 24.75 19.25" stroke-width="0" stroke="none" stroke-dasharray="none" fill="rgb(0, 0, 0)" fill-rule="evenOdd"></path></svg>  '  + 
			'       <br><br>  '  + 
			'     '  + 
			'       <h1 class="text-dark text-semibold mb-3" style="font-size:20px;">Anda yakin?</h1>'  + 
			'       <a href="javascript:;" class="hook-beli btn btn-warning">  '  + 
			'           Beli'  + 
			'       </a>  '  + 
			'   </div>  ' ; 
			pwModalPushContent( login );

			$('.hook-beli').unbind('click').on('click', function(){
				$('.pw-modal--data-storage').html('');
				setTimeout(function() {
					pwModalPushContent( $('.purchaseVIP > div').html() );
					//Step 1 to 2
					$('.pw-modal .to-step-2').unbind("click").on('click', function(e){
						e.preventDefault();
						var target = $(this).data('target');
						$('.'+target).show(0, function(){
							back_1();
						});
						$('.pw-modal .vip-step.step-2').addClass('active').siblings('.pw-modal .vip-step').removeClass('active');
						return false;
					});

					/*
						* Step 2 to 3
						*/
					// XL axis
					$('.pw-modal .xl-axis .pwmodal-vip-item').unbind("click").on('click', function(){
						var nama = $(this).data('nama'),
							harga = $(this).data('harga');

						$('.vip-step.step-2').removeClass('active');
						render_harga( 'Amount', harga, false );
						render_productDetail( nama, harga, user, false );
						render_peymentMethod(true, "xl_sendsms('"+harga+"', 'REG')", null);
						if( $('.header-back').find('.sprite').hasClass('modal-close-dark') ) {
							$('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
						}
						setTimeout(function() {
							$('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
							$('.pw-modal .step-3').addClass('active');
							back_2();
							tab_click();
						}, 100);
					});

					// Indosat
					$('.pw-modal .indosat .pwmodal-vip-item').unbind("click").on('click', function(){
						var nama = $(this).data('nama'),
							harga = $(this).data('harga');
						$('.vip-step.step-2').removeClass('active');
						if( $('.header-back').find('.sprite').hasClass('modal-close-dark') ) {
							$('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
						}
						indosat_subscribe('2.200', '', '2', '0');
						setTimeout(function() {
							$('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
							$('.pw-modal .step-3').addClass('active');
							back_2();
							tab_click();
						}, 100);
					});

					// Telkomsel
					$('.pw-modal .telkomsel .pwmodal-vip-item').unbind("click").on('click', function(e){
						var nama = $(this).data('nama'),
							harga = $(this).data('harga');
						$('.vip-step.step-2').removeClass('active');
						if( $('.header-back').find('.sprite').hasClass('modal-close-dark') ) {
							$('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
						}
						setTimeout(function() {
							$('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
							$('.pw-modal .step-3').addClass('active');
							back_2();
							tab_click();
						}, 100);
						
						// if there is any callback
						var callback = jQuery(e.currentTarget).data("action");

						var x = eval(callback)
						if (typeof x == 'function') {
							x();
						}                       
					});
				}, 100);
			});
        @else
            pleaselogin();
        @endif
        pwModalBtnText('Tutup');
        pwmodal('loaded');
    }

    /* AXIS/XL BUY PROCESS */
    function xl_sendsms($id, $type) {
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $id;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor telkomsel yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('xl:send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

     /* INDOSAT LANGGANAN */
    function indosat_subscribe( $harga, $sms, $vip, $extra ){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip) + parseInt($extra);
            render_harga( 'Amount', $harga, false );

            // Render product detail
            render_productDetail( 'VIP BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "indosat_sendsms('1850', 'REG')", '*123*595*5#');

            // Change text
            pwModalBtnText('Tutup');

            // end load state
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);

            // render_text( subscribehtml );
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* INDOSAT BUY PROCESS */
    function indosat_sendsms($id, $type) {
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $id;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor telkomsel yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('indosat:send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    /* TELKOMSEL POPUP DIALOG */
    function telkomsel_dialog( $harga, $vip, $id){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip);
            render_harga( 'Amount', $harga, false );

            // Render product detil
            render_productDetail( 'VIP NON BERLANGGANAN '+parseInt($totalvip)+' Hari<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "telkomsel_sendsms('"+$id+"')", null);

            // end load state
            pwModalBtnText('Tutup');
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* TELKOMSEL BUY PROCESS */
    function telkomsel_sendsms($id){
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var product_id = $id;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor telkomsel yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ url('api/mdmedia/request') }}",
                data: {
                    product_id      : product_id,
                    nomorhandphone  : nomorhandphone,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {

                    $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                        <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                    </div>');

                    //GAGAL:
                    /*$('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                        <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                    </div>');*/

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    jQuery(document).ready(function($){
        $('.pay-button').click(function (e) {
            e.preventDefault();
            var voucher_id = $(this).attr('data-voucher');

            // fbq('track', 'Purchase', {
            //     value: 1,
            //     currency: 'IDR',
            // });
            //alert(voucher_id);
            $.ajax({
                type: "post",
                url: base_url +'/member/payment/buy',
                data: {
                    _token     :  '{!!csrf_token()!!}',
                   voucher_id  : voucher_id
                },
                beforeSend: function(response) {

                },
                success: function (response) {
                    // console.log(response);
                    var err = eval("(" + response.responseText + ")");
                    //alert(err.Message);

                    //snap.pay(response);

                    snap.pay(response,{
                        onSuccess: function(response){console.log('success');console.log(response);},
                        onPending: function(response){
                            console.log('pending');
                            console.log(response);
                            location.href = "https://playworld.id/member/status_transfer";
                        },
                        onError: function(response){console.log('error');console.log(response);},
                        onClose: function(){console.log('customer closed the popup without finishing the payment');}
                    });
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);

                }
                
                // error: function(response){
                //     if (!$.trim(response)){   
                //         alert("What follows is blank: " + response);
                //     }
                //     else{   
                //         alert("What follows is not blank: " + response);
                //     }
                //    console.log(response);
                // } 
            });
        });
    });
</script>
@stop