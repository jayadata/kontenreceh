@foreach($review as $data)
	<div class="col-md-4 mb-5">
		<?php
		    // Variables to pass on
		    $name = $data->first_name;
		    $ava = avatar_storage($data->avatar);
		    $rating = $data->rate;
		    $comment = $data->review;
		?>
		{{ review_item( $name, $ava, $rating, $comment ) }}
	</div>
@endforeach