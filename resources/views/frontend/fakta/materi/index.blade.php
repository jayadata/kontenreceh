@extends('frontend.layouts.app')

@section('content')
<section class="mb-0 pt-2">
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12">
                
                <div class="kelas-materi-list">
                    
                    <!-- materi item -->
                    <?php $i = 1 ?>
                    @foreach($content as $data)
                        <div class="materi-item">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="mi-numb text-24 text-xs-20 text-dark text-bold">
                                        {{ $i++ }}.
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-3 mb-3 mb-md-0">
                                                <img src="{{ contentImageUrl($data->img) }}" alt="">
                                            </div>
                                            <div class="col-md-9">
                                                @if(Auth::user())
                                                    <h2 class="m-0 text-xs-20 pt-1 pt-md-0"><a href="#" class="text-dark">{{ $data->title }}</a></h2>
                                                @else
                                                    <h2 class="m-0 text-xs-20 pt-1 pt-md-0"><a href="#" class="text-dark">{{ $data->title }}</a></h2>
                                                @endif
                                                
                                                <div class="rating"><span data-width="{{ $data->rating($data->id) }}"></span></div>

                                                <div class="clearfix"></div>
                                                <div class="rdmore">{!! $data->content !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 text-right">
                                    @if(Auth::user())
                                        @if($data->buy(Auth::user()->id ,$data->id) != NULL || $data->voucher->amount == 0)
                                            <img src="{{ StaticAsset('assets/frontend/img/orange-check.png') }}" alt="" class="inline mr-2 purchased-kelas" width="50">
                                        @endif
                                    @endif
                                    
                                    @if($data->voucher->amount != 0)
                                        @if(Auth::user())
                                            @if($data->buy(Auth::user()->id ,$data->id) != NULL)
                                                <a href="{{ route('content.detail', $data->slug) }}" class="btn btn-outline-info btn-md text-bold">MASUK <i class="ml-3 fa fa-play-circle"></i></a>
                                            @else
                                                <a href="#" class="btn btn-outline-warning btn-md text-bold satuan popup-trigger" data-onshow="vipcheck( '{{ number_format($data->voucher->amount) }}', '{{ $data->title }}', '1', '{{ $data->voucher->code }}', 'hide', 'PULL')" data-buttonaction="pwmodal('close')">
                                            Rp {{ number_format($data->voucher->amount) }}</a>
                                            @endif
                                        @else
                                            <a href="#" class="btn btn-outline-warning btn-md text-bold satuan popup-trigger" data-onshow="vipcheck( '{{ number_format($data->voucher->amount) }}', '{{ $data->title }}', '1', '{{ $data->voucher->code }}', 'hide', 'PULL')" data-buttonaction="pwmodal('close')">
                                            Rp {{ number_format($data->voucher->amount) }}</a>
                                        @endif
                                    @else
                                        @if(Auth::user())
                                            <a href="{{ route('content.detail', slug($data->slug)) }}" class="btn btn-outline-success btn-md">GRATIS <i class="ml-3 fa fa-play-circle"></i></a>
                                        @else
                                            <a href="{{ route('content.detail', slug($data->slug)) }}" class="btn btn-outline-success btn-md satuan popup-trigger" data-onshow="vipcheck( '{{ number_format($data->voucher->amount) }}', '{{ $data->title }}', '1', '{{ $data->voucher->code }}', 'hide', 'PULL')" data-buttonaction="pwmodal('close')">GRATIS <i class="ml-3 fa fa-play-circle"></i></a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div><!-- /materi item -->
                    @endforeach
                    <div class="kelas-materi-list-2">
                        @include('frontend.fakta.loadmore')
                    </div>
                    <?php $i = $i - 1; ?>
                    @if( $i != $count )
                        <div class="text-center p-4 btn_laod_more">
                            <button class="btn btn-warning btn-lg text-big loadmore" data-offset="{{ $content->count() }}">Lihat semua</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

<div class="purchaseVIP hide">
    <div>
        {{-- STEP 1 --}}
        <div class="vip-step step-1 active">
            <div class="card-container">
                <div class="text-14 px-4 pt-3">
                    Dengan Pulsa
                </div>
                <hr>
                <div class="card-body">
                    <a href="#" class="btn btn-block btn-success to-step-2" data-target="xl-axis">XL &amp; Axis</a>
                    {{-- <a href="#" class="btn btn-block btn-success to-step-2" data-target="indosat">Indosat</a> --}}
                </div>
            </div>
        </div>

        {{-- Step 2 --}}
        <div class="vip-step step-2">
            <div class="card-container xl-axis mt-3">
                <div class="text-14 px-4 pt-3">
                    XL &amp; Axis
                </div>
                <hr>
                <div class="pwmodal-vip-items">
                    <?php 
                        $axis = array(
                            array(
                                "nama" => "VIP 2 Hari",
                                "harga" => "2.200",
                            ),
                            array(
                                "nama" => "VIP 3 Hari",
                                "harga" => "3.300",
                            ),
                            array(
                                "nama" => "VIP 5 Hari",
                                "harga" => "5.500",
                            ),
                            array(
                                "nama" => "VIP 8 Hari",
                                "harga" => "8.800",
                            ),
                            array(
                                "nama" => "VIP 11 + Xtra 1 Hari",
                                "harga" => "11.000",
                                "hemat" => true,
                            ),
                            array(
                                "nama" => "VIP 15 + Xtra 2 Hari",
                                "harga" => "16.500",
                                "hemat" => true,
                            )
                        );
                    ?>
                    @foreach ($axis as $item)
                        <div class="pwmodal-vip-item" data-action="xlaxis_subscribe()" data-nama="{{ $item['nama'] }}" data-harga="{{ $item['harga'] }}">
                            <div class="row xs">
                                <div class="col-xs-2">
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                    @else
                                        <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                    @endif
                                </div>
                                <div class="col-xs-10">
                                    <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                    @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                        <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                    @endif
                                    <div class="text-13 mt-2">
                                        <strong>Rp. {{ $item['harga'] }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-container indosat mt-3">
                <div class="text-14 px-4 pt-3">
                    Indosat
                </div>
                <hr>
                <div class="pwmodal-vip-items">
                    <div class="pwmodal-vip-item">
                        <div class="row xs">
                            <div class="col-xs-2">
                                    <img src="{{ StaticAsset('assets/frontend/img/koin/vip-s.svg') }}" width="80" alt="">
                            </div>
                            <div class="col-xs-10">
                                <div class="text-16 text-light bg-warning px-2 py-1 mb-4 mb-md-3" style="display:block;border-radius:5px">
                                    Minggu Ke-1<br>
                                    <strong>7 Hari VIP</strong>
                                </div>

                                <div class="text-16 text-light bg-warning px-2 py-1 mb-3" style="display:block;border-radius:5px">
                                    Minggu Selanjutnya<br>
                                    <strong>5 Hari VIP</strong>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Pembelian Pertama mendapatkan 2 Hari VIP Seharga Rp. 2200</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Selanjutnya Perpanjangan Hari VIP dikirim dalam 2 SMS per minggu</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Rp. 2200 per SMS untuk Perpanjangan 2 Hari VIP</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>SMS Ke-3 disetiap minggunya akan diberikan Extra Perpanjangan 1 Hari VIP</div>
                                </div>

                                <div class="my-3">
                                    <div class="circha orange"><i class="fa fa-check"></i></div>
                                    <div>Harga sudah termasuk pajak PPN 10%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vip-step step-3"></div>
    </div>
</div>

@include('frontend.partials.mainmodal')
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.loadmore').click(function(){
                var offset = $('.loadmore').data('offset');
                jQuery.ajax({
                    type: "post",
                    url: '{{ route('content.loadmore') }}',
                    dataType: 'html',
                    data: {
                        'slug'      : '{{ $content_account->slug }}',
                        'offset'    : offset,
                        '_token'    : '{!! csrf_token() !!}'
                    },
                    success: function (response) {
                        $('.kelas-materi-list-2').html(response);
                        $('.btn_laod_more').hide();
                        // var offset = $('.loadmore').data('offset');
                        // $('.loadmore').attr('data-offset', offset+5);
                        
                        setTimeout(() => {
                            $('.popup-trigger').on('click', function(e){
                                e.preventDefault();
                                // if there is any callback
                                var callback = $(this).data("onshow");
                                var onclosefunc = $(this).data("buttonaction");

                                if( ! $(this).attr('disabled') ) {
                                    // show the modal first
                                    $('.pw-modal').show(0, function(){
                                        var x = eval(callback);
                                        if (typeof x == 'function') {
                                            x();
                                        }

                                        $(this).find('.button-main-content').data('onclick', onclosefunc);
                                    });
                                } 
                            });

                            // $('.rdmore').each(function(){
                            //     $(this).readmore({
                            //         speed: 75,
                            //         collapsedHeight: 70,
                            //         lessLink: '<a href="#">Read less</a>'
                            //     });
                            // });
                            
                        }, 200);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                        var err = eval("(" + xhr.responseText + ")");
                    } 
                });
            });
        });
    </script>

<script src="https://www.google.com/recaptcha/api.js"
    async defer>
</script>
<script>
    var $ = jQuery;
    @if (Auth::user())
        var obj = {
            'nama': '{{ Auth::user()->name.' '.Auth::user()->name }}',
            'telp': '{{ Auth::user()->telp }}',
            'mail': '{{ Auth::user()->email }}'
        };
        var user = JSON.stringify(obj);
    @endif;

    function verification() {
        pwModalPushContent( $('.tocopy').html() );
        pwModalHeaderText('Verification');
        pwModalBtnText('Tutup');
        pwmodal('loaded');
    }

    function back() {
        $('.header-back').unbind("click").on('click', function(e){
            e.preventDefault();
            pwmodal('close');
            return false;
        });
    }

    function back_1() {
        $('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
        if( $('.header-back').find('.header-prev').length ) {
            $('.header-back').unbind("click").on('click', function(e){
                e.preventDefault();
                var $step = $(this).data('back');
                $('.pw-modal .vip-step.step-1').addClass('active');
                $('.pw-modal .vip-step.step-2').removeClass('active').find('.card-container').hide();
                $('.pw-modal .vip-step.step-3').removeClass('active').html('');
                $('.header-back').find('.sprite').addClass('modal-close-dark').removeClass('header-prev');
                back();
                //alert('anjing');
                return false;
            });
        }
    }

    function back_2() {
        if( $('.header-back').find('.header-prev').length ) {
            $('.header-back').unbind("click").on('click', function(e){
                e.preventDefault();
                $('.pw-modal .vip-step.step-2').addClass('active').find('.card-container');
                $('.pw-modal .vip-step.step-3').removeClass('active').html('');
                $('.pw-modal .vip-step.step-1').removeClass('active');
                back_1();
                return false;
            });
        }
    }

    function vipcheck( $harga, $nama, $midtransid=null, $pw, $hide=null, $type ) {
        // alert('b');
        var nama = $nama,
            harga = $harga,
            pw = $pw,
            type = $type;
            
        @if(Auth::user())
            pwModalPushContent( $('.purchaseVIP > div').html() );

            setTimeout(function() {
                if( $hide == 'hide' ) {
                    $('.bcacard').addClass('hide');
                } else {
                    $('.bcacard').removeClass('hide');
                }
            }, 100);

            //Step 1 to 2
            $('.pw-modal .to-step-2').unbind("click").on('click', function(e){
                e.preventDefault();

                var target = $(this).data('target');
                if( $('.header-back').find('.sprite').hasClass('modal-close-dark') ) {
                    $('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
                }
                
                if( target == 'xl-axis' ) {
                    if(type == 'PULL'){
                        $('.vip-step.step-2').removeClass('active');
                        render_harga( 'Amount', harga, false );
                        render_productDetail( nama, harga, user, false );
                        render_peymentMethod(true, "xl_sendsms('"+pw+"', 'NON-REG')", null);
                        setTimeout(function() {
                            $('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
                            $('.pw-modal .step-3').addClass('active');
                            back_1();
                            tab_click();
                        }, 100);
                    }else{
                        // Render Harga
                        // var $totalvip = parseInt($vip) + parseInt($extra);
                        render_harga( 'Amount', $harga, false );

                        // Render product detail
                        render_productDetail( 'VIP BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

                        // Render payment method
                        render_peymentMethod(true, "xl_sendsms('"+$harga+"', 'REG')", '*123*789*40*1*1#', false);

                        // Change text
                        pwModalBtnText('Tutup');

                        // end load state
                        pwmodal('loaded');

                        setTimeout(function() {
                            $('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
                            $('.pw-modal .step-3').addClass('active');
                            back_1();
                            tab_click();
                        }, 100);
                    }
                } 
                
                if( target == 'indosat' ){
                    if(type == 'PULL'){
                        $('.vip-step.step-2').removeClass('active');
                        pwModalPushContent('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Metode ini belum tersedia</p>\
                        </div>');
                        setTimeout(function() {
                            $('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-2');
                            $('.pw-modal .step-2').addClass('active');
                            back_1();
                            tab_click();
                        }, 100);

                        // Change text
                        pwModalBtnText('Tutup');

                        // end load state
                        pwmodal('loaded');

                        setTimeout(function() {
                            $('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
                            $('.pw-modal .step-3').addClass('active');
                            back_1();
                            tab_click();
                        }, 100);
                    }else{
                        render_harga( 'Amount', $harga, false );

                        // Render product detail
                        render_productDetail( 'VIP INDOSAT BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

                        // Render payment method
                        render_peymentMethod(true, "indosat_sendsms('1850', 'REG')", '*123*595*5#');

                        // Change text
                        pwModalBtnText('Tutup');

                        // end load state
                        pwmodal('loaded');

                        setTimeout(function() {
                            $('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
                            $('.pw-modal .step-3').addClass('active');
                            back_1();
                            tab_click();
                        }, 100);
                    }
                }

                $('.pw-modal .vip-step.step-2').addClass('active').siblings('.pw-modal .vip-step').removeClass('active');
                return false;
            });
        @else
            if(harga == 0){
                pleaselogin( 'https://qomiqu.com/auth/signin' );  
            }else{
                pleaselogin( 'https://qomiqu.com/auth/signin' );
            }
        @endif
        pwModalBtnText('Tutup');
        pwmodal('loaded');
    }

    /* AXIS/XL BUY PROCESS */
    function xl_sendsms($pw, $type) {
        @if (Auth::user())

            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $pw;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor xl/axis yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('vip.xl.send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    /* INDOSAT LANGGANAN */
    function indosat_subscribe( $harga, $sms, $vip, $extra ){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip) + parseInt($extra);
            render_harga( 'Amount', $harga, false );

            // Render product detail
            render_productDetail( 'VIP BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "indosat_sendsms('1850', 'REG')", '*123*595*5#');

            // Change text
            pwModalBtnText('Tutup');

            // end load state
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);

            // render_text( subscribehtml );
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* INDOSAT BUY PROCESS */
    function indosat_sendsms($id, $type) {
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $id;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor xl/axis yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('vip.indosat.send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }
</script>
@stop


