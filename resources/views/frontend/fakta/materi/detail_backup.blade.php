@extends('frontend.layouts.default')
@section('meta')
<meta property="og:image" content="{!! StaticAsset('assets/frontend/images/cover.png') !!}" />
<meta property="og:locale" content="id_ID"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('playunik') }}"/>
<meta property="og:title" content="Playworld - Playunik" />
<meta property="og:site_name" content="{!! 'Playworld.id '!!}" />
<meta property="fb:admins" content="100006462279538" />
<meta property="fb:app_id" content="107188393464738" />
<meta property="og:description" content="{!!'PLAYWORLD ID adalah social entertainment news and interaction website, yang menyuguhkan informasi hiburan dan sosial, yang dikemas dengan interaksi mengumpulkan POIN'!!}" />
<meta name="twitter:card" content="summary">
<meta name="twitter:creator" content="@playworld">
<meta name="twitter:site" content="@playworld">
<meta name="twitter:title" content="Playworld - Playunik">
<meta name="twitter:description" content="{!! 'PLAYWORLD ID adalah social entertainment news and interaction website, yang menyuguhkan informasi hiburan dan sosial, yang dikemas dengan interaksi mengumpulkan POIN'!!}">
<meta name="twitter:image" content="{!! StaticAsset('assets/frontend/images/cover.png') !!}" />
<meta name="description" content="PLAYWORLD adalah media portal hiburan yang menyajikan berita terkini dengan konsep pemberitaan dari sisi yang beda, lain, tidak membosankan dan menghibur">
<meta name="keywords" content=" PLAYWORLD, Playworld, Play, Main, hiburan, entertainment, entertain, komedi, lucu, ngakak, buset, ngetrend, trend, trending, juara, apa, fokus, nyeleneh, selfie, asik, seru, instagram, facebook, google, twitter, serem, teknologi, tekno, mewah, pacar, gebetan, cinta, romantis, hadiah, undian, unik, artis, selebriti, startup, jayadata, indonesia, jakarta, bandung, musik, film, olahraga, kreatif, travel, event, bisnis, gaul, hewan, komunitas, otomotif, makanan, dunia, gaya, kesehatan, lingkungan, sains">


<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var gptadslots = [];
  var googletag = googletag || {cmd:[]};
</script>
<script>
  googletag.cmd.push(function() {
    var mapping1 = googletag.sizeMapping()
                            .addSize([1024, 768], [[970, 250], [728, 90]])
                            .addSize([800, 600], [[728, 90]])
                            .addSize([0, 0], [[320, 50]])
                            .build();

    //Adslot 1 declaration
    gptadslots.push(googletag.defineSlot('/160553881/playworld/article', [[970,250],[728,90]], 'div-gpt-ad-5274233-1')
                             .setTargeting('pos', ['Leaderboard'])
                             .defineSizeMapping(mapping1)
                             .addService(googletag.pubads()));
    //Adslot 2 declaration
    gptadslots.push(googletag.defineSlot('/160553881/playworld/article', [[300,600],[300,250]], 'div-gpt-ad-5274233-2')
                             .setTargeting('pos', ['MR1'])
                             .addService(googletag.pubads()));

    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<style>
    .wistia_responsive_wrapper {
        position:relative!important;
    }

    @media screen and (max-width: 990px) {
        .m-min-mar {
            margin: 0 -15px
        }
    }
    
</style>
<link rel="stylesheet" href="{{ StaticAsset('assets/frontend/css/dropzone.css') }}">
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">
<script src="{{ StaticAsset('assets/frontend/js/dropzone.js') }}"></script>
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<style>
    .page-container p {
        padding: 0!important;
    }
    .ql-container {
        height: 150px;
    }

    a.btn-outline-success {
        border-color:#1aff00!important;
        color:#1aff00!important;
    }
</style>
@stop
@section('title')
Playworld - Bikin Kamu Terhibur dan Gak Bosenin
@stop

@section('content')
<?php
$string = New App\Http\Library\String_library;
?>

    <section class="text-gray pt-4 pt-md-5" style="background:#232323">
        <div class="container">
            <div class="row mb-4 faktalogo">
                <div class="col-md-12">
                    <img src="{{ StaticAsset('assets/frontend/img/kelas/faktamenariq.png') }}" alt="">
                </div>
            </div>
            <div class="row mb-4 mb-md-4">
                <div class="col-md-12">
                    <h2 class="text-bold text-32 text-xs-22 text-uppercase text-white mt-0">{{ $materi->title }}</h2>
                    
                    @if (Auth::user())
                        @if($materi->buy(Auth::user()->id ,$materi->id) != NULL || $materi->price == 0)
                            <a href="{{ route('fakta-download', [$materi->slug]) }}" class="btn btn-outline-warning mb-4 text-gold" target="_BLANK">DOWNLOAD VIDEO ( {{ $materi->filesize }} )</a> 
                        @else
                            <a href="#" class="btn btn-outline-success btn-md text-bold popup-trigger" data-onshow="vipcheck( '{{ number_format($materi->price) }}', '{{ $materi->title }}', '{{ $materi->voucher_id }}', '{{ $materi->pw }}', 'hide' )" data-buttonaction="pwmodal('close')"> @if($materi->price != 0) Rp {{ number_format($materi->price) }} @endif UNTUK DOWNLOAD</a>
                        @endif
                    @else 
                        {{-- <a href="{{ route('fakta-download', [$materi->slug]) }}" class="btn btn-outline-success btn-sm btn-md text-bold popup-trigger" data-onshow="vipcheck( '{{ number_format($materi->price) }}', '{{ $materi->title }}', '{{ $materi->voucher_id }}', '{{ $materi->pw }}', 'hide' )" data-buttonaction="pwmodal('close')">@if($materi->price != 0) Rp {{ number_format($materi->price) }} @endif DOWNLOAD</a> --}}
                        <a href="{{ route('fakta-download', [$materi->slug]) }}" class="btn btn-outline-warning btn-sm mb-4 text-gold">@if($materi->price != 0) Rp {{ number_format($materi->price) }} @endif DOWNLOAD VIDEO ( {{ $materi->filesize }} )</a> 
                    @endif
                    <div class="clearfix"></div>
                </div>                   
            </div>
            <div class="row mb-md-4">
                <div class="col-md-7">
                    <div class="m-min-mar">
                        <!-- 16:9 aspect ratio -->
                        @if($materi->price == 0)
                            {{-- <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{ $materi->embed }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div> --}}
                            {!! $materi->embed !!}
                        @else
                            {!! $materi->embed !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-5 mt-5 mt-md-0">
                    <div class="materi-hero-text">
                        <div style="line-height: 22.8571px">
                            <div class="rdmore-m">
                                {!! $materi->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    @include('frontend.fakta.materi.reviews')

    <section class="bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="hadiah-wrapper">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#facebook-com" aria-controls="facebook" role="tab" data-toggle="tab">
                                    <div class="text-semibold mb-2" style="color: #3d5a99">
                                        <img src="{{ StaticAsset('assets/frontend/img/newsicon/icon_facebook_color.png') }}" alt="" width="25"> Facebook Comments
                                    </div>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#disqus-com" aria-controls="disqus" role="tab" data-toggle="tab">
                                    <div class="text-semibold mb-2" style="color: #5094cf">
                                        <img src="{{ StaticAsset('assets/frontend/img/newsicon/icon_disqus_color.png') }}" alt="" width="25"> Disqus Comments
                                    </div>
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content bg-white">
                            <div role="tabpanel" class="tab-pane active" id="facebook-com">
                                {{-- FACEBOOK COMMENT --}}
                                <div class="card-body my-4">
                                    <div class="fb-comments" data-href="{{ Request::url() }}" data-width="100%" data-numposts="5"></div>
                                    <script>
                                        $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
                                            FB.init({
                                                appId: '107188393464738',
                                                version: 'v2.8' // or v2.0, v2.1, v2.0
                                            });
                                    
                                            FB.Event.subscribe('comment.create', function(response){ 
                                                var msg = response.message;
                                                $.post('{{ route('comment:eskul', ['materi', 'facebook']) }}', 
                                                {
                                                    _token   : '{!!csrf_token()!!}',
                                                    _id      : '{{ $materi->id }}',
                                                    _msg     :  msg
                                                },
                                                function(response)
                                                {
                                                    console.log(response);
                                                });
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="disqus-com">
                                {{-- DISQUS COMMENT --}}
                                <div class="card-body my-4">
                                    <div id="disqus_thread"></div>
                                    <script>
                                        var disqus_config = function () {
                                            this.page.url = '{{ Request::url() }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                            this.callbacks.onNewComment = [
                                                function(comment) {
                                                    var msg = comment.text;
                                                    /* alert(comment.id);
                                                    alert(comment.text);*/
                                                    //post point goes here
                                                    $.post('{{ route('comment:eskul', ['materi', 'disqus']) }}', 
                                                    {
                                                        _token   : '{!!csrf_token()!!}',
                                                        _id      : '{{ $materi->id }}',
                                                        _msg     :  msg
                                                    }, 
                                                    function(response)
                                                    {
                                                        console.log(response);
                                                        $('#compoinearned').modal('show');
                                                    })
                                                }
                                            ];
                                        };
                                    (function() { // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = '//playworld-id.disqus.com/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                    })();
                                    </script>
                                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .hadiah-wrapper -->   
                </div>
            </div>
        </div>
    </section>


    @include('frontend.mainmodal')
    {{-- PURCHASE VIP --}}
    <div class="purchaseVIP hide">
        <div>
            {{-- STEP 1 --}}
            <div class="vip-step step-1 active">
                <div class="card-container">
                    <div class="text-14 px-4 pt-3">
                        Dengan Pulsa
                    </div>
                    <hr>
                    <div class="card-body">
                        <a href="#" class="btn btn-block btn-success to-step-2" data-target="xl-axis">XL &amp; Axis</a>
                    </div>
                </div>
                <div class="card-container bcacard">
                    <div class="text-14 px-4 pt-3">
                        Tanpa Pulsa
                    </div>
                    <hr>
                    <div class="card-body">
                        <a href="#" class="btn btn-block btn-success pay-button-2" data-voucher="{{ $materi->voucher_id }}">Transfer ke Bank BCA</a>
                    </div>
                </div>
            </div>
    
            {{-- Step 2 --}}
            <div class="vip-step step-2">
                <div class="card-container xl-axis mt-3">
                    <div class="text-14 px-4 pt-3">
                        XL &amp; Axis
                    </div>
                    <hr>
                    <div class="pwmodal-vip-items">
                        <?php 
                            $axis = array(
                                array(
                                    "nama" => "VIP 2 Hari",
                                    "harga" => "2.200",
                                ),
                                array(
                                    "nama" => "VIP 3 Hari",
                                    "harga" => "3.300",
                                ),
                                array(
                                    "nama" => "VIP 5 Hari",
                                    "harga" => "5.500",
                                ),
                                array(
                                    "nama" => "VIP 8 Hari",
                                    "harga" => "8.800",
                                ),
                                array(
                                    "nama" => "VIP 11 + Xtra 1 Hari",
                                    "harga" => "11.000",
                                    "hemat" => true,
                                ),
                                array(
                                    "nama" => "VIP 15 + Xtra 2 Hari",
                                    "harga" => "16.500",
                                    "hemat" => true,
                                )
                            );
                        ?>
                        @foreach ($axis as $item)
                            <div class="pwmodal-vip-item" data-action="xlaxis_subscribe()" data-nama="{{ $item['nama'] }}" data-harga="{{ $item['harga'] }}">
                                <div class="row xs">
                                    <div class="col-xs-2">
                                        @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                        @else
                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                        @endif
                                    </div>
                                    <div class="col-xs-10">
                                        <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                        @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                            <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                        @endif
                                        <div class="text-13 mt-2">
                                            <strong>Rp. {{ $item['harga'] }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-container indosat mt-3">
                    <div class="text-14 px-4 pt-3">
                        Indosat
                    </div>
                    <hr>
                    <div class="pwmodal-vip-items">
                        <div class="pwmodal-vip-item">
                            <div class="row xs">
                                <div class="col-xs-2">
                                        <img src="{{ StaticAsset('assets/frontend/img/koin/vip-s.svg') }}" width="80" alt="">
                                </div>
                                <div class="col-xs-10">
                                    <div class="text-16 text-light bg-warning px-2 py-1 mb-4 mb-md-3" style="display:block;border-radius:5px">
                                        Minggu Ke-1<br>
                                        <strong>7 Hari VIP</strong>
                                    </div>
    
                                    <div class="text-16 text-light bg-warning px-2 py-1 mb-3" style="display:block;border-radius:5px">
                                        Minggu Selanjutnya<br>
                                        <strong>5 Hari VIP</strong>
                                    </div>
    
                                    <div class="my-3">
                                        <div class="circha orange"><i class="fa fa-check"></i></div>
                                        <div>Pembelian Pertama mendapatkan 2 Hari VIP Seharga Rp. 2200</div>
                                    </div>
    
                                    <div class="my-3">
                                        <div class="circha orange"><i class="fa fa-check"></i></div>
                                        <div>Selanjutnya Perpanjangan Hari VIP dikirim dalam 2 SMS per minggu</div>
                                    </div>
    
                                    <div class="my-3">
                                        <div class="circha orange"><i class="fa fa-check"></i></div>
                                        <div>Rp. 2200 per SMS untuk Perpanjangan 2 Hari VIP</div>
                                    </div>
    
                                    <div class="my-3">
                                        <div class="circha orange"><i class="fa fa-check"></i></div>
                                        <div>SMS Ke-3 disetiap minggunya akan diberikan Extra Perpanjangan 1 Hari VIP</div>
                                    </div>
    
                                    <div class="my-3">
                                        <div class="circha orange"><i class="fa fa-check"></i></div>
                                        <div>Harga sudah termasuk pajak PPN 10%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-container telkomsel mt-3">
                    <div class="text-14 px-4 pt-3">
                        Telkomsel
                    </div>
                    <hr>
                    <div class="pwmodal-vip-items">
                        <?php 
                            $telkomsels = array(
                                array(
                                    "nama" => "VIP 2 Hari",
                                    "harga" => "2.000",
                                    "hari" => 2,
                                    "id" => 40141,
                                ),
                                array(
                                    "nama" => "VIP 5 Hari",
                                    "harga" => "5.500",
                                    "hari" => 5,
                                    "id" => 40065,
                                ),
                                array(
                                    "nama" => "VIP 11 + Xtra 1 Hari",
                                    "harga" => "11.000",
                                    "hemat" => true,
                                    "hari" => 12,
                                    "id" => 40066,
                                )
                            );
                        ?>
                        @foreach ($telkomsels as $item)
                            <div class="pwmodal-vip-item" data-action="telkomsel_dialog('{{ $item['harga'] }}', '{{ $item['hari'] }}', '{{ $item['id'] }}')">
                                <div class="row xs">
                                    <div class="col-xs-2">
                                        @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                        @else
                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                        @endif
                                    </div>
                                    <div class="col-xs-10">
                                        <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                        @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                            <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                        @endif
                                        <div class="text-13 mt-2">
                                            <strong>Rp. {{ $item['harga'] }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-container bca mt-3">
                    <div class="text-14 px-4 pt-3">
                        Transfer ke Bank BCA
                    </div>
                    <hr>
                    <div class="pwmodal-vip-items">
                        <?php 
                            $bca = array(
                                array(
                                    "nama" => "VIP 16 Hari",
                                    "harga" => "10.000",
                                    "voucher" => 12,
                                ),
                                array(
                                    "nama" => "VIP 40 Hari",
                                    "harga" => "25.000",
                                    "voucher" => 9,
                                ),
                                array(
                                    "nama" => "VIP 80 Hari",
                                    "harga" => "50.000",
                                    "voucher" => 10,
                                ),
                                array(
                                    "nama" => "VIP 160 Hari",
                                    "harga" => "100.000",
                                    "voucher" => 11,
                                )
                            );
                        ?>
                        @foreach ($bca as $item)
                            <div class="pwmodal-vip-item pay-button" data-voucher="{{ $item['voucher'] }}">
                                <div class="row xs">
                                    <div class="col-xs-2">
                                        @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                        @else
                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                        @endif
                                    </div>
                                    <div class="col-xs-10">
                                        <h3 class="mt-0 text-16 mb-0">{{ $item['nama'] }}</h3>
                                        @if ( array_key_exists('hemat', $item) && $item['hemat'] == true )
                                            <span class="btn btn-outline-success btn-xs text-10"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                        @endif
                                        <div class="text-13 mt-2">
                                            <strong>Rp. {{ $item['harga'] }}</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="vip-step step-3"></div>
        </div>
    </div>
@stop 

@section('scripts')
<script type="text/javascript">
    function fetchcontent( $url ) {
        $.ajax({
            url: $url,
            type:'GET',
            success: function(data){
                var img = $(data).find('.sc-img img').attr('src');
                var judul = $(data).find('.sc-head').html();
                var desc = $(data).find('.shot-desc').html();
                var meta = $(data).find('.col-md-4.text-gray.text-14').html();

                var markup = '<div class="m-drib-con mt-3"><img src="'+img+'" alt=""/>' +
                '<div class="mt-5">'+judul+'</div>' +
                '<div class="mt-3">'+meta+'</div><hr>' +
                '<div class="mt-4">'+desc+'</div></div>';
                
                pwModalPushContent( markup );
                $('.app-container').addClass('m-drib-popup');
                pwmodal('loaded');
            }
        });
    }

    $(document).ready(function() {
        if( !isMobile.any ) {
            $('#showcase, #homework').magnificPopup({
                delegate: 'a.drib-magnify',
                type: 'ajax',
                tLoading: 'Loading content...',
                mainClass: 'mfp-img-mobile',
                overflowY: 'scroll',
                alignTop: true,
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] 
                },
                callbacks: {
                    open: function() {
                        $('html').addClass('mag-active');
                    },
                    close: function() {
                        $('html').removeClass('mag-active');                    
                    },
                    parseAjax: function(mfpResponse) {
                        var mp = $.magnificPopup.instance,
                        t = $(mp.currItem.el[0]);

                        var h=( t.data('custom') );

                        mfpResponse.data = $(mfpResponse.data).find('#showcase-content');

                        //console.log('Ajax content loaded:', mfpResponse);
                    },
                    ajaxContentAdded: function() {
                        //console.log(this.content);
                    }
                }
            });
        } else {
            $('.drib-magnify').click(function(e){
                e.preventDefault();
                var url = $(this).attr('href');
                pwmodal('open', fetchcontent(url) );
                pwModalHeaderText('');
                pwModalBtnText('Tutup');

                $('.button-main-content').unbind('click').on('click', function(){
                    pwmodal('close');
                });
            });
        }
    });
</script>
<script src="https://www.google.com/recaptcha/api.js"
async defer>
</script>
<script>
    var $ = jQuery;
    @if (Auth::user())
        var obj = {
            'nama': '{{ Auth::user()->first_name.' '.Auth::user()->last_name }}',
            'telp': '{{ Auth::user()->no_telp }}',
            'mail': '{{ Auth::user()->email }}'
        };
        var user = JSON.stringify(obj);
    @endif;

    function verification() {
        pwModalPushContent( $('.tocopy').html() );
        pwModalHeaderText('Verification');
        pwModalBtnText('Tutup');
        pwmodal('loaded');
    }

    function back() {
        $('.header-back').unbind("click").on('click', function(e){
            e.preventDefault();
            pwmodal('close');
            return false;
        });
    }

    function back_1() {
        $('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
        if( $('.header-back').find('.header-prev').length ) {
            $('.header-back').unbind("click").on('click', function(e){
                e.preventDefault();
                var $step = $(this).data('back');
                $('.pw-modal .vip-step.step-1').addClass('active');
                $('.pw-modal .vip-step.step-2').removeClass('active').find('.card-container').hide();
                $('.pw-modal .vip-step.step-3').removeClass('active').html('');
                $('.header-back').find('.sprite').addClass('modal-close-dark').removeClass('header-prev');
                back();
                //alert('anjing');
                return false;
            });
        }
    }

    function back_2() {
        if( $('.header-back').find('.header-prev').length ) {
            $('.header-back').unbind("click").on('click', function(e){
                e.preventDefault();
                $('.pw-modal .vip-step.step-2').addClass('active').find('.card-container');
                $('.pw-modal .vip-step.step-3').removeClass('active').html('');
                $('.pw-modal .vip-step.step-1').removeClass('active');
                back_1();
                return false;
            });
        }
    }

    function vipcheck( $harga, $nama, $midtransid=null, $pw, $hide=null ) {
        var nama = $nama,
            harga = $harga,
            pw = $pw;
        @if(Auth::user())
            pwModalPushContent( $('.purchaseVIP > div').html() );

            setTimeout(function() {
                if( $hide == 'hide' ) {
                    $('.bcacard').addClass('hide');
                } else {
                    return console.log(nama + ' - '+ harga + ' - '+ pw);
                    $('.bcacard').removeClass('hide');
                }
            }, 100);

            //Step 1 to 2
            $('.pw-modal .to-step-2').unbind("click").on('click', function(e){
                e.preventDefault();

                var target = $(this).data('target');
                if( $('.header-back').find('.sprite').hasClass('modal-close-dark') ) {
                    $('.header-back').find('.sprite').removeClass('modal-close-dark').addClass('header-prev');
                }
                
                if( target == 'xl-axis' ) {
                    $('.vip-step.step-2').removeClass('active');
                    render_harga( 'Amount', harga, false );
                    render_productDetail( nama, harga, user, false );
                    render_peymentMethod(true, "xl_sendsms('"+pw+"', 'NON-REG')", null);
                    setTimeout(function() {
                        $('.pw-modal--data-storage > .card-container').appendTo('.pw-modal--data-storage .step-3');
                        $('.pw-modal .step-3').addClass('active');
                        back_1();
                        tab_click();
                    }, 100);

                } 


                // $('.'+target).show(0, function(){
                //     back_1();
                // });
                $('.pw-modal .vip-step.step-2').addClass('active').siblings('.pw-modal .vip-step').removeClass('active');
                return false;
            });

            $('.pay-button-2').unbind('click').on('click', function (e) {
                e.preventDefault();
                var voucher_id = $(this).attr('data-voucher');


                // fbq('track', 'Purchase', {
                //     value: 1,
                //     currency: 'IDR',
                // });
                //alert(voucher_id);
                $.ajax({
                    type: "post",
                    url: base_url +'/member/payment/buy',
                    data: {
                        _token     :  '{!!csrf_token()!!}',
                        voucher_id  : voucher_id
                    },
                    beforeSend: function(response) {

                    },
                    success: function (response) {
                        // console.log(response);
                        var err = eval("(" + response.responseText + ")");
                        //alert(err.Message);

                        //snap.pay(response);

                        snap.pay(response,{
                            onSuccess: function(response){console.log('success');console.log(response);},
                            onPending: function(response){
                                console.log('pending');
                                console.log(response);
                                location.href = "https://playworld.id/member/status_transfer";
                            },
                            onError: function(response){console.log('error');console.log(response);},
                            onClose: function(){console.log('customer closed the popup without finishing the payment');}
                        });
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                        var err = eval("(" + xhr.responseText + ")");
                        alert(err.Message);

                    }
                    
                    // error: function(response){
                    //     if (!$.trim(response)){   
                    //         alert("What follows is blank: " + response);
                    //     }
                    //     else{   
                    //         alert("What follows is not blank: " + response);
                    //     }
                    //    console.log(response);
                    // } 
                });
            });
        @else
            pleaselogin( 'https://playworld.id/member/' );
        @endif
        pwModalBtnText('Tutup');
        pwmodal('loaded');
    }

    /* AXIS/XL BUY PROCESS */
    function xl_sendsms($pw, $type) {
        @if (Auth::user())

            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $pw;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor xl/axis yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('xl:send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

        /* INDOSAT LANGGANAN */
    function indosat_subscribe( $harga, $sms, $vip, $extra ){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip) + parseInt($extra);
            render_harga( 'Amount', $harga, false );

            // Render product detail
            render_productDetail( 'VIP BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "indosat_sendsms('1850', 'REG')", '*123*595*5#');

            // Change text
            pwModalBtnText('Tutup');

            // end load state
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);

            // render_text( subscribehtml );
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* INDOSAT BUY PROCESS */
    function indosat_sendsms($id, $type) {
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $id;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor xl/axis yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('indosat:send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    /* TELKOMSEL POPUP DIALOG */
    function telkomsel_dialog( $harga, $vip, $id){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip);
            render_harga( 'Amount', $harga, false );

            // Render product detil
            render_productDetail( 'VIP NON BERLANGGANAN '+parseInt($totalvip)+' Hari<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "telkomsel_sendsms('"+$id+"')", null);

            // end load state
            pwModalBtnText('Tutup');
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* TELKOMSEL BUY PROCESS */
    function telkomsel_sendsms($id){
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var product_id = $id;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor xl/axis yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ url('api/mdmedia/request') }}",
                data: {
                    product_id      : product_id,
                    nomorhandphone  : nomorhandphone,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {

                    $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                        <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                    </div>');

                    //GAGAL:
                    /*$('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                        <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                    </div>');*/

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    jQuery(document).ready(function($){
        $('.pay-button').click(function (e) {
            e.preventDefault();
            var voucher_id = $(this).attr('data-voucher');

            // fbq('track', 'Purchase', {
            //     value: 1,
            //     currency: 'IDR',
            // });
            //alert(voucher_id);
            $.ajax({
                type: "post",
                url: base_url +'/member/payment/buy',
                data: {
                    _token     :  '{!!csrf_token()!!}',
                    voucher_id  : voucher_id
                },
                beforeSend: function(response) {

                },
                success: function (response) {
                    // console.log(response);
                    var err = eval("(" + response.responseText + ")");
                    //alert(err.Message);

                    //snap.pay(response);

                    snap.pay(response,{
                        onSuccess: function(response){console.log('success');console.log(response);},
                        onPending: function(response){
                            console.log('pending');
                            console.log(response);
                            location.href = "https://playworld.id/member/status_transfer";
                        },
                        onError: function(response){console.log('error');console.log(response);},
                        onClose: function(){console.log('customer closed the popup without finishing the payment');}
                    });
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);

                }
                
                // error: function(response){
                //     if (!$.trim(response)){   
                //         alert("What follows is blank: " + response);
                //     }
                //     else{   
                //         alert("What follows is not blank: " + response);
                //     }
                //    console.log(response);
                // } 
            });
        });
    });
</script>
@stop