
<style>
    .rating_wid > input:checked ~ label, .rating_wid:not(:checked) > label:hover, .rating_wid:not(:checked) > label:hover ~ label,
    .rating_wid > input:checked ~ label, .rating_wid:not(:checked) > label:hover, .rating_wid:not(:checked) > label:hover ~ label {
        color: #FF7D24;
    }
    .rating_wid > input:checked ~ label:hover,
    .rating_wid > input:checked ~ label:hover ~ label,
    .rating_wid > label:hover ~ input:checked ~ label {
        color: #e67700!important;
    }
    .rating_wid > label {
        color: #909090;
    }
</style>

<section class="pt-0">
    <div class="container">
        <div class="row bs4 align-items-center mb-2">
            <div class="col-md-6">
                <h2 class="text-bold text-32 text-xs-22">Rating</h2>
            </div>
            <div class="col-md-6 text-md-right">
                <a href="#" class="btn btn-warning popup-trigger @if(@count($reviewed)) disabled @endif" data-onshow="submitReview()" data-buttonaction="reviewSubmit()"><i class="fa fa-pencil mr-2"></i> Berikan Rating</a>
            </div>
        </div>

        <div class="bg-white pt-5 pb-4 px-5 text-dark">
            <div class="row bs4 pb-4 reviews-wrapper">
                @if(@count($review) > 0)
                    @foreach($review as $data)
                        <div class="col-md-4 mb-5">
                            <?php
                                // Variables to pass on
                                $name = $data->customer->name;
                                $ava = contentImageUrl($data->customer->avatar);
                                $rating = $data->rate;
                                $comment = $data->review;
                            ?>
                            {{ review_item( $name, $ava, $rating, $comment ) }}
                        </div>
                    @endforeach
                @else
                    <div class="col-12 ekskul bg-white text-gray2 text-center py-5 px-5">
                        Tidak ada Data
                    </div>
                @endif
            </div>
            @if(@count($review) > 0)
                <div class="row bs4">
                    <div class="col-12 text-center click_me">
                        <a href="#" class="btn btn-outline-warning load-more-review" data-paging="2"><i class="fa fa-refresh"></i> Load More</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>

<script>

    function ratingstars() {
        var rating = '<fieldset class="rating_wid"><input type="radio" class="star" id="star5" name="rating" value="100"/><label class="full" for="star5" title="Awesome - 5 stars"></label><input type="radio" class="star" id="star4half" name="rating" value="90"/><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label><input type="radio" class="star" id="star4" name="rating" value="80"/><label class="full" for="star4" title="Pretty good - 4 stars"></label><input type="radio" class="star" id="star3half" name="rating" value="70"/><label class="half" for="star3half" title="Meh - 3.5 stars"></label><input type="radio" class="star" id="star3" name="rating" value="60"/><label class="full" for="star3" title="Meh - 3 stars"></label><input type="radio" class="star" id="star2half" name="rating" value="50"/><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label><input type="radio" class="star" id="star2" name="rating" value="40"/><label class="full" for="star2" title="Kinda bad - 2 stars"></label><input type="radio" class="star" id="star1half" name="rating" value="30"/><label class="half" for="star1half" title="Meh - 1.5 stars"></label><input type="radio" class="star" id="star1" name="rating" value="20"/><label class="full" for="star1" title="Sucks big time - 1 star"></label><input type="radio" class="star" id="starhalf" name="rating" value="10"/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label></fieldset>';
        return rating;
    }
    
    // Review Content
    function submitReview() {
        @if( Auth::user() )
            pwModalHeaderText('Rating Submission');
            pwModalPushContent( '<div class="px-4 py-4">'+
            '<input id="gambar" type="hidden" />'+
            '<h4>Berikan rating untuk materi ini</h4>'+
            '<textarea rows="5" class="form-control mb-4 review-text" placeholder=""></textarea>'+
            ratingstars()+
            '<div class="clearfix mb-4"></div>'+
            '<div id="review-captcha"></div>'+
            '</div>'+
            '</div>' );
            $('.button-main').addClass('disabled');

            // recaptcha
            grecaptcha.render('review-captcha', {
                sitekey: '6LdM5qoUAAAAAJafMCuguG0xIfaP8soCacKh5ory',
                callback: function(response) {
                    $('#review-captcha').parents('.pw-modal').find('.button-main').removeClass('disabled').find('a').removeAttr('disabled');
                }
            });
        @else
            pwModalHeaderText('');
            pleaselogin( '{{ url('auth/signin') }}' );
        @endif


        setTimeout(() => {
            pwmodal('loaded');
        }, 400);
    }

    // Submit
    function reviewSubmit() {
        var text = $('.review-text').val();
        var star = $('input[type=radio][name=rating]:checked').val();

        @if(Auth::user())
            $.ajax({
                type: "post",
                url: "{{ route('content.review.post') }}",
                data: {
                    _token      : '{!!csrf_token()!!}',
                    customer    : '{{ Auth::user()->id }}',
                    id          : '{{ $review_id }}',
                    where       : '{{ $review_where }}',
                    review      : text,
                    rating      : star
                },
                beforeSend: function() {
                    $('.pw-modal--loader').fadeIn(300);
                    pwModalBtnText('Loading...');
                },
                success: function (response) {
                    console.log(response); 

                    var res = JSON.parse( JSON.stringify( response ) );
                    console.log(res);

                    if( res == 200 ) {
                        $('.pw-modal--loader').fadeOut(300, function(){
                            pwModalPushContent('');

                            var login = 
                            '   <div class="text-center mt-5 pt-5">  '  + 
                            '       <img src="{{ StaticAsset("assets/frontend/img/redeemicon/icon_success.png") }}"/>'  + 
                            '       <br><br>  '  + 
                            '     '  + 
                            '       <h1 class="text-dark text-semibold" style="font-size:20px;">Your review has been submitted</h1>  '  +  
                            '   </div>  ' ; 
                            render_text(login);
                            pwModalBtnText('Close');
                            $('.button-main-content').unbind('click').on('click', function(){
                                pwmodal('close');
                                location.reload();
                            });

                            //
                        });                                       
                    }                                     
                }
            });
        @endif 
    }

    function load_more(){
        $('.load-more-review').on('click', function(e){
            e.preventDefault();

            var loadmore = parseInt( $(this).attr('data-paging') ); //1

            console.log(loadmore);

            $.ajax({
                type: "post",
                url: "{{ route('content.review.more') }}",
                data: {
                    _token      : '{!!csrf_token()!!}',
                    id          : '{{ $review_id }}',
                    paging      : loadmore,
                    where       : '{{ $review_where }}'
                },
                beforeSend: function() {
                    
                },
                success: function (response) {
                    // if (response != '201') {
                        $('.reviews-wrapper').append(response);
                        $('.rdmore').each(function(){
                            $(this).readmore({
                                speed: 75,
                                collapsedHeight: 70,
                                lessLink: '<a href="#">Read less</a>'
                            });
                        });

                        var next = parseInt( loadmore + 1 );

                        $('.load-more-review').attr('data-paging', next);
                    
                    // }else{
                    //     $('.click_me').css('display', 'none');
                    // }
                }
            });
        });
    }

    load_more();

</script>