<?php $i = 7; ?>
@if(isset($more))
	@foreach($more as $index => $data)
	@if ($data->id >= $i)
		<div class="materi-item">
			<div class="row">
				<div class="col-md-9">
					<div class="mi-numb text-24 text-xs-20 text-dark text-bold">
						{{ $i++ }}.
					</div>
					<div>
						<div class="row">
							<div class="col-md-3 mb-3 mb-md-0">
								<img src="{{ contentImageUrl($data->img) }}" alt="">
							</div>
							<div class="col-md-9">
								@if(Auth::user())
									@if($data->buy(Auth::user()->id, $data->id) != NULL)
										<h2 class="m-0 text-xs-20 pt-1 pt-md-0"><a href="{{ route('content.detail', $data->slug) }}">{{ $data->title }}</a></h2>
									@else
										<h2 class="m-0 text-xs-20 pt-1 pt-md-0"><a href="#" class="text-dark">{{ $data->title }}</a></h2>
									@endif
								@else
									<h2 class="m-0 text-xs-20 pt-1 pt-md-0"><a href="#" class="text-dark">{{ $data->title }}</a></h2>
								@endif
								
								<div class="rating"><span data-width="5"></span></div>

								<div class="clearfix"></div>
								<div class="rdmore">{!! $data->content !!}</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 text-right">
					@if(Auth::user())
						@if($data->buy(Auth::user()->id ,$data->id) != NULL || $data->voucher->amount == 0)
							<img src="{{ StaticAsset('assets/frontend/img/orange-check.png') }}" alt="" class="inline mr-2 purchased-kelas" width="50">
						@endif
					@endif
					
					@if($data->voucher->amount != 0)
						@if(Auth::user())
							@if($data->buy(Auth::user()->id ,$data->id) != NULL)
								<a href="{{ route('content.detail', $data->slug) }}" class="btn btn-outline-info btn-md text-bold">MASUK <i class="ml-3 fa fa-play-circle"></i></a>
							@else
								<a href="#" class="btn btn-outline-warning btn-md text-bold satuan popup-trigger" data-onshow="vipcheck( '{{ number_format($data->voucher->amount) }}', '{{ $data->title }}', '1', '{{ $data->voucher->code }}', 'hide', 'PULL')" data-buttonaction="pwmodal('close')">
							Rp {{ number_format($data->voucher->amount) }}</a>
							@endif
						@else
							<a href="#" class="btn btn-outline-warning btn-md text-bold satuan popup-trigger" data-onshow="vipcheck( '{{ number_format($data->voucher->amount) }}', '{{ $data->title }}', '1', '{{ $data->voucher->code }}', 'hide', 'PULL')" data-buttonaction="pwmodal('close')">
							Rp {{ number_format($data->voucher->amount) }}</a>
						@endif
					@else
						@if(Auth::user())
							<a href="{{ route('content.detail', slug($data->slug)) }}" class="btn btn-outline-success btn-md">GRATIS <i class="ml-3 fa fa-play-circle"></i></a>
						@else
							<a href="{{ route('content.detail', slug($data->slug)) }}" class="btn btn-outline-success btn-md satuan popup-trigger" data-onshow="vipcheck( '{{ number_format($data->voucher->amount) }}', '{{ $data->title }}', '1', '{{ $data->voucher->code }}', 'hide', 'PULL')" data-buttonaction="pwmodal('close')">GRATIS <i class="ml-3 fa fa-play-circle"></i></a>
						@endif
					@endif
				</div>
			</div>
		</div><!-- /materi item -->
		@endif
	@endforeach
@endif