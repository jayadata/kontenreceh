@extends('frontend.layouts.app')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">
            
             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">     
                
                
                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                PROFILE
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-0 mt-0"><strong>Pengaturan Akun</strong></h4>
                        <h3 class="mb-4 mt-1 subhead">Profil Akun</h3>
                        <hr>
                        <div class="mt-md-4 mt-0">
                            <div class="hadiah-wrapper">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profil Akun</a></li>
                                </ul>

                                <div class="tab-content hadiahpanel">
                                    <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                                    PROFILE
                                    ooooooooooooooooooooooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane active" id="profile">
                                        <div class="card-body pr-3 pl-3">
                                            <form class="profileform" role="form" method="post" action="{{ route('member.profile.update') }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row sg">
                                                    <div class="col-md-12">
                                                        <div class="ps-ava mr-5 left">
                                                            <img src="{{ contentImageUrl(Auth::user()->avatar) }}" alt="">
                                                        </div>
                                                        <div class="ps-name left">
                                                            <strong>{{ Auth::user()->name }}</strong>
                                                            <div class="clearfix"></div>
                                                            <label for="avatar">
                                                                <span class="btn btn-warning text-white ml-0">Change Image</span>
                                                                <input type="file" name="avatar" id="avatar" class="hidden">
                                                            </label>
                                                        </div>
                                                        <div class="clearfix mb-5"></div>
                                                        <hr>
                                                    </div>
                                                </div>

                                                {{-- FORM --}}
                                                <div class="mt-5">
                                                    <div class="row sg">
                                                        <div class="col-md-6 form-group">
                                                            <label class="control-label">PW ID</label>
                                                            <input class="form-control" name="msisdn" readonly value="{{ Auth::user()->msisdn }}" type="text">
                                                                <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label class="control-label">Email</label>
                                                            @if(Auth::user()->email == 1)
                                                            <div class="">
                                                                <input class="form-control" {{-- {{ (Auth::user()->email_confirmed == 1) ? 'readonly' : '' }} --}} value="{{ Auth::user()->email }}" name="email" type="text" required> 
                                                            </div>
                                                            @else
                                                            <div class="">
                                                                <input class="form-control" {{-- {{ (Auth::user()->email_confirmed == 1) ? 'readonly' : '' }} --}} value="{{ Auth::user()->email }}" name="email" type="text" required> 
                                                            </div>
                                                            {{-- <a href="{{ url('member/merge') }}" class="btn btn-info"> Merge Account</a>  --}}
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="row sg">
                                                        <div class="col-md-6 form-group">
                                                            <label class="control-label">Username</label>
                                                            <div class="">
                                                                <input class="form-control" name="name" value="{{ Auth::user()->name }}" type="text" required> 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label class="control-label">Handphone</label>
                                                            <div class="">
                                                                <input class="form-control" name="telp" value="{{ Auth::user()->telp }}" type="text" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row sg mt-4">
                                                        <div class="col-md-4 form-group">
                                                            <label class="control-label"></label>
                                                            <div class="">
                                                                {{-- <input class="btn" value="Cancel" type="reset"> --}}
                                                                <input class="btn btn-success text-white" value="Simpan" type="submit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> {{-- end .profileform --}}
                                                <div class="clearfix"></div>
                                            </form>
                                        </div> <!-- end .card-body -->
                                    </div> <!-- end #profile -->
                                </div> <!-- end .tab-content -->
                            </div>
                        </div>
                    </div>
                </div> {{-- .card --}}

            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hapus Alamat?</h4>
            </div>
            <form method="post" action="#" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="clearfix mb-3">Apakah anda yakin akan menghapus alamat berikut:</div>
                    <input type="hidden" class="id-alamat-delete" name="id_alamat_delete">
                    <strong class="namaalamat-placeholder"></strong>
                    <div class="row">
                        <div class="col-md-7">
                            <div class="alamatdetail-placeholder"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="deletebutton btn btn-danger" data-delete="">Hapus Alamat</button>
                    <button type="button" class="btn" data-dismiss="modal">Batalkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="hapusNomor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hapus Nomor Handphone?</h4>
            </div>
            <form method="post" action="#" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="clearfix mb-3">Apakah anda yakin akan menghapus nomor berikut:</div>
                    <input type="hidden" class="id-contact-delete" name="id_contact_delete">
                    <strong class="namanomor-placeholder"></strong>
                    <div class="nomordetil-placeholder"></div>
                </div>
                <div class="modal-footer text-left">
                    <button type="submit" class="deletebutton btn btn-danger" data-delete="">Hapus Nomor</button>
                    <button type="button" class="btn" data-dismiss="modal">Batalkan</button>
                </div>
            </form>
        </div>
    </div>
</div>


@include('member.partials.footer')
@stop
@section('scripts')
<script>
    jQuery(document).ready(function($){
        $(".jenis_nomor").change(function() {
            val = $(this).val();
            //alert(val);
            if(val == 'Bank') {
                $('.jenis_bank').removeClass('hide');
            }else{
                $('.jenis_bank').addClass('hide');
            }
        });
        // MODULAR MODAL DIALOGUE FOR HAPUS ALAMAT
        $('#hapusModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var namaalamat = button.data('namaalamat'); // Extract info from data-namaalamat attribute
            var alamatid = button.data('alamatid'); // Extract info from data-alamatID attribute
            var alamatdetil = button.data('alamatdetil'); // Extract info from data-alamatdetil attribute
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.namaalamat-placeholder').text(namaalamat);
            modal.find('.alamatdetail-placeholder').text(alamatdetil);
            modal.find('.id-alamat-delete').val(alamatid);
            modal.find('.deletebutton').attr('data-delete', alamatid);
        });

        // MODULAR MODAL DIALOGUE FOR HAPUS ALAMAT
        $('#hapusNomor').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var namanomor = button.data('namanomor'); // Extract info from data-namaalamat attribute
            var nomorid = button.data('nomorid'); // Extract info from data-alamatID attribute
            var nomordetil = button.data('nomordetil'); // Extract info from data-alamatdetil attribute
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.namanomor-placeholder').text(namanomor);
            modal.find('.nomordetil-placeholder').text(nomordetil);
            modal.find('.id-contact-delete').val(nomorid);
            modal.find('.deletebutton').attr('data-delete', nomorid);
        });

        jQuery(document).ready(function($) {
            init_daerah(0);
        });
        $('.show_hide').click(function() {
            if ($('.test').attr('type') == 'password') {
                $('.test').attr('type', 'text');
            } else {
                $('.test').attr('type', 'password');
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //$('.avatar-image').attr('src', e.target.result);
                    $('.avatar-image').css("background-image", "url("+e.target.result+")"); 
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#avatar").change(function(){
            readURL(this);
        });

        var ajax_target = 0;

        $('.aktifkan_aku').click(function(){
            var data_aktifkan = $(this).attr('data-aktifkan');
            var provinsi_id   = $('.prov_aktifkan_'+data_aktifkan).val();
            var kabupaten_id  = $(this).attr('kabupaten-id');
            var kecamatan_id  = $(this).attr('kecamatan-id');
            var kelurahan_id  = $(this).attr('kelurahan-id');
            var ajax_target   = $(this).attr('data-aktifkan');

            init_daerah(data_aktifkan);
            // pinta data kabupaten
            $.getJSON(base_url+'/data/kabupaten/'+provinsi_id, 
            function(json, textStatus) {
              $('.kabupaten_aktifkan_'+data_aktifkan).empty();
              $('.kabupaten_aktifkan_'+data_aktifkan).append("<option value=''>Pilih Kabupaten</option>");
              $.each(json, function(i, value) {
                  $('.kabupaten_aktifkan_'+data_aktifkan).append($('<option>').text(value.nama).attr('value', value.id));
              });
              setTimeout(function(){
                $('.kabupaten_aktifkan_'+data_aktifkan).val(kabupaten_id);
                /*tarik data kecamatan*/
                $.getJSON(base_url+'/data/kecamatan/'+kabupaten_id, 
                    function(json, textStatus) {
                      $('.kecamatan_aktifkan_'+data_aktifkan).empty();
                      $('.kecamatan_aktifkan_'+data_aktifkan).append("<option value=''>Pilih Kecamatan</option>");
                      $.each(json, function(i, value) {
                          $('.kecamatan_aktifkan_'+data_aktifkan).append($('<option>').text(value.nama).attr('value', value.id));
                      });
                    setTimeout(function(){
                        $('.kecamatan_aktifkan_'+data_aktifkan).val(kecamatan_id);
                            /*tarik data kelurahan*/
                            $.getJSON(base_url+'/data/kelurahan/'+kecamatan_id, 
                            function(json, textStatus) {
                              $('.kelurahan_aktifkan_'+data_aktifkan).empty();
                              $('.kelurahan_aktifkan_'+data_aktifkan).append("<option value=''>Pilih Kelurahan</option>");
                              $.each(json, function(i, value) {
                                  $('.kelurahan_aktifkan_'+data_aktifkan).append($('<option>').text(value.nama).attr('value', value.id));
                              });

                              setTimeout(function(){
                                $('.kelurahan_aktifkan_'+data_aktifkan).val(kelurahan_id);
                              }, 300);
                          });
                    }, 300);
                });
              }, 300);
          });

        });

        function init_daerah(ajax_target) {

            /*proses ganti dan tambah alamat*/
            $('.provinsi_id'+ajax_target).change(function(){
                var provinsi_id = $('.provinsi_id'+ajax_target).val();
                $.getJSON(base_url+'/data/kabupaten/'+provinsi_id, 
                function(json, textStatus) {
                  $('.kabupaten_id'+ajax_target).empty();
                  $('.kabupaten_id'+ajax_target).append("<option value=''>Pilih Kabupaten</option>");
                  $.each(json, function(i, value) {
                      $('.kabupaten_id'+ajax_target).append($('<option>').text(value.nama).attr('value', value.id));
                  });
              });
            });

            $('.kabupaten_id'+ajax_target).change(function(){
                var kabupaten_id = $('.kabupaten_id'+ajax_target).val();
                $.getJSON(base_url+'/data/kecamatan/'+kabupaten_id, 
                function(json, textStatus) {
                  $('.kecamatan_id'+ajax_target).empty();
                  $('.kecamatan_id'+ajax_target).append("<option value=''>Pilih Kecamatan</option>");
                  $.each(json, function(i, value) {
                      $('.kecamatan_id'+ajax_target).append($('<option>').text(value.nama).attr('value', value.id));
                  });
              });
            });

            $('.kecamatan_id'+ajax_target).change(function(){
                var kecamatan_id = $('.kecamatan_id'+ajax_target).val();
                $.getJSON(base_url+'/data/kelurahan/'+kecamatan_id, 
                function(json, textStatus) {
                    $('.kelurahan_id'+ajax_target).empty();
                    $('.kelurahan_id'+ajax_target).append("<option value=''>Pilih Kelurahan</option>");
                    $.each(json, function(i, value) {
                        $('.kelurahan_id'+ajax_target).append($('<option>').text(value.nama).attr('value', value.id));
                    });
                });
            });
        }
    });
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop