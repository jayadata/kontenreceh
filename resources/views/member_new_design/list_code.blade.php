@extends('frontend.layouts.app')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<!--End of Zendesk Chat Script-->
<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">

             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                HISTORI PENUKARAN KOIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Kode Qomiqu</strong></h4>
                        <hr>
                        <div class="my-games">
                            <div class="table-responsive">
                                <table class="table user-games mx-0">
                                    <thead>
                                        <tr>
                                            <th width="30">No</th>
                                            <th width="100">Tanggal</th>
                                            <th width="100">Code</th>
                                            <th>Status</th>
                                            <th>Detail</th>
                                            <th width="150">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($code) > 0)
                                            @foreach($code as $data)
                                                <tr>
                                                    <td align="center">{{ ++$i }}</td>
                                                    <td>{{ $data->created_at->format('d/m/Y') }}</td>
                                                    <td>{{ $data->code }}</td>
                                                    <td>
                                                        @if($data->status == 0)
                                                            Active
                                                        @else
                                                            Expire	
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $data->detail($data->trx) }}
                                                    </td>
                                                    <td align="right">
                                                        @if($data->status == 0)
                                                            @if(strlen($data->trx) > 5)
                                                                <a href="{{ route('form.main', [Auth::user()->msisdn.substr($data->trx, -4)]) }}" data-ex="{{ Session::put($data->code, $data->code) }}" class="btn btn-success">Gunakan</a>
                                                            @else
                                                                <a href="{{ route('form.main', [Auth::user()->msisdn.$data->trx]) }}" data-ex="{{ Session::put($data->code, $data->code) }}" class="btn btn-success">Gunakan</a>
                                                            @endif
                                                        @else
                                                            <a href="#" class="btn btn-default disabled" disabled>Expire</a>
                                                        @endif
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" align="center">Tidak ada data</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="mt-3 mb-3">
                            <div class="right">
                                {!!$code->render()!!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    </div>
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>


@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
    jQuery(document).ready(function($){
        $('.filter_balance').change(function(){
            //alert('test');
            if($(this).val() == '')
            {
                window.location.href ="{{ url('member/history_penggunaan_vip') }}";
            }
            else
            {
                window.location.href ="{{ url('member/history_penggunaan_vip') }}?filter_balance="+$(this).val();

            }
        });
    });
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop