{{-- koin --}}
<div class="mt-koin mt-outtest arisanpopup">
    <div class="mt-app">
        <nav class="headermt">
            <a class="header-back tutupaja" href="#"><span class="sprite modal-close-dark"></span></a>
            <div class="header-content">
                <h1 class="logo-store">
                    <img alt="PLAYWORLD" src="https://vtcheckout-sandbox-assets.s3.amazonaws.com/snap/logos/M101875/thumb_retina_snap_2Flogos_2FM101875_2F63352767-d777-431a-81f9-c1915086481f_2Fplayworld%2Blogo%2B02-01.png">
                </h1>
                <span class="text-page-title">Rincian Penukaran Koin</span>
            </div>
        </nav>

        <div class="button-main show proses_button">
            <a class="button-main-content text-center text_sms tukar_koin_menang" href="">
                <div class="text-button-main text-center">Lanjutkan Penukaran Koin</div>
            </a>
        </div> <!-- /.button-main show -->

        <div class="pop-wrapper has-close">
            <span class="pop"></span>
            <div class="pop-close-wrapper">
                <a href="javascript:;" class="touch-area">
                    <span class="sprite modal-close-dark"></span>
                </a>
            </div>
        </div> <!-- /.pop-wrapper has-close -->

        <div class="container-fluid mx-3 mt-4">
            <div class="row pt-3">
                <div class="main-container first_koin">
                    <div class="page-container scroll">
                        <div class="card-container cctitle-box">
                            <div class="amount">
                                <div class="amount-title">
                                    <span class="text-amount-title">Nama Barang</span>
                                    <div class="mt-2">
                                        <span class="text-amount-amount text_harga nama_barang text-warning">blabla</span>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- .card-container -->
                        
                        <!-- .card-container -->
                        <div class="card-container py-0">
                            <div class="amount p-0">
                                <div class="row sg">
                                    <div class="col-md-6 col-xs-6 py-3 bdr-right">
                                        <div class="amount-title">
                                            <span class="text-amount-title">Koin yang akan di Tukarkan</span>
                                            <div class="mt-2">
                                                <span class="text-amount-amount text_harga yang_di_pasang text-warning">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 py-3">
                                        <div class="amount-title">
                                            <span class="text-amount-title">Sisa Koin</span>

                                            <div class="mt-2">
                                                <span class="text-amount-amount text-warning sisa_koin">{{nomor_cantik($home_balance[0]->sisa)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div> <!-- .card-container -->
                        <ul class="mt-list noliststyle text-gray px-0 px-md-5 pb-md-0">
                            <li>
                                <div class="row sg">
                                    <div class="col-md-2 col-xs-3">
                                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_InfoBlue.png') }}" alt="">
                                    </div>
                                    <div class="col-md-10 col-xs-9">
                                        <h3 class="warning-text mt-0 text-big">ANDA YAKIN AKAN MENGIKUTI TUKAR KOIN INI?</h3>
                                        <p class="text-semimedium p-0">
                                            Silahkan klik tombol LANJUTKAN di bawah ini untuk melanjutkan proses penukaran KOIN
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <input type="hidden" class="pw-pajak">
                {{-- response ajax --}}
                <div class="main-container response_koin response_200 hide">
                    <div class="mt-5">
                        <div class="text-center ">
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>

                            <h3 class="warning-text">TUKAR KOIN BERHASIL</h3>
                            <p>
                                Mohon tunggu <br>
                                <img src="{{ StaticAsset('assets/frontend/img/loader/Preloader_4.gif') }}" alt="">
                            </p>
                        </div>
                    </div>
                </div>
                <div class="main-container response_poin response_900 hide">
                    <div class="mt-5">
                        <div class="text-center ">
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>

                            <h3 class="warning-text">PENUKARAN GAGAL</h3>
                            <p>
                                KOIN tidak berhasil ditukar karena alasan berikut
                            </p>
                            <p><b>Data tidak lengkap</b></p>
                            <p>Anda bisa memeriksa jumlah KOIN anda pada halaman profile untuk memastikan KOIN anda tidak terpotong</p>
                        </div>
                    </div>
                </div>
                <div class="main-container response_poin response_901 hide">
                    <div class="mt-5">
                        <div class="text-center ">
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>

                            <h3 class="warning-text">PENUKARAN GAGAL</h3>
                            <p>
                                KOIN tidak berhasil ditukar karena alasan berikut
                            </p>
                            <p><b>Data tidak diketemukan</b></p>
                            <p>anda bisa memeriksa jumlah KOIN anda pada halaman profile untuk memastikan KOIN anda tidak terpotong</p>
                        </div>
                    </div>
                </div>
                <div class="main-container response_poin response_903 hide">
                    <div class="mt-5">
                        <div class="text-center ">
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>

                            <h3 class="warning-text">PENUKARAN GAGAL</h3>
                            <p>
                                KOIN tidak berhasil ditukar karena alasan berikut
                            </p>
                            <p><b>KOIN yang anda tukar melebihi batas poin tersisa senilai <span class="max_valid">0</span> KOIN</b></p>
                            <p>anda bisa memeriksa jumlah KOIN anda pada halaman profile untuk memastikan KOIN anda tidak terpotong</p>
                        </div>
                    </div>
                </div>
                <div class="main-container response_poin response_904 hide">
                    <div class="mt-5">
                        <div class="text-center ">
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>

                            <h3 class="warning-text">PENUKARAN GAGAL</h3>
                            <p>
                                KOIN tidak berhasil ditukar karena alasan berikut
                            </p>
                            <p><b>Jumlah POIN/KOIN yang di tukar tidak mencukup</b></p>
                            <p>anda bisa memeriksa jumlah KOIN anda pada halaman profile untuk memastikan KOIN anda tidak terpotong</p>
                        </div>
                    </div>
                </div>
                <div class="main-container response_poin response_905 hide">
                    <div class="mt-5">
                        <div class="text-center ">
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>

                            <h3 class="warning-text">PENUKARAN GAGAL</h3>
                            <p>
                                KOIN tidak berhasil ditukar karena alasan berikut
                            </p>
                            <p><b>Anda telah mencapai batas maksimal penukaran KOIN untuk hari ini</b></p>
                            <p>anda bisa memeriksa jumlah KOIN anda pada halaman profile untuk memastikan KOIN anda tidak terpotong</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- mt-outtest -->