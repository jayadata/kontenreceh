@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile pt-4">
    <div class="container">
        <div class="row sg">

             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                @include('member_new_design.partial.notifdatauser')
                
                @include('member_new_design.partial.flash_point')

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                HISTORI PENUKARAN POIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Mutasi Poin</strong></h4>
                        <hr>
                        <div class="my-games">
                             <div class="profile-header-total clearfix">
                                <div class="right" style="min-width: 200px;">
                                    Filter Type 
                                    <select name="filter_poin" class="form-control filter_poin right ml-3">
                                       <option value="">All</option>
                                       <option value="dapat" {{(Request::get('filter_poin') == 'dapat')?'selected="selected"':''}}>Dapat</option>
                                       <option value="tukar" {{(Request::get('filter_poin') == 'tukar')?'selected="selected"':''}}>Tukar</option>
                                       <option value="add" {{(Request::get('filter_poin') == 'add')?'selected="selected':''}}>Add</option>
                                        <option value="fraud" {{(Request::get('filter_poin') == 'fraud')?'selected="selected':''}}>Fraud</option>
                                   </select>
                                </div>
                                {{-- <div class="right">
                                    <strong>{{number_format($point[0]->point)}}/{{number_format($point[0]->point_awal)}}</strong>
                                </div> --}}
                            </div>

                            <div class="profile-content p-0">
                                <table class="user-games table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="30">No</th>
                                            <th width="60">Tanggal</th>
                                            <th width="100">Activity</th>
                                            <th width="100">Tipe</th>
                                            <th>Detail</th>
                                            {{-- <th width="80">Koin</th> --}}
                                            <th width="80">Poin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 1;
                                        ?>
                                        @foreach($my_point as $row)
                                        <tr>
                                            <td align="center">{{$i++}}</td>
                                            <td>{{ $row->created_at->format('d/m/Y') }}</td>
                                            <td>
                                                @if($row->flag)
                                                    @if($row->flag == 'add')
                                                        {{$row->activity}}
                                                    @else
                                                        Comment
                                                    @endif
                                                @else
                                                    {{$row->activity}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->flag)
                                                    {{ $row->flag }}
                                                @else
                                                    {{$row->status}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($row->flag)
                                                    @if($row->flag == 'add')
                                                        @if(isset($row->news_id))
                                                        Penambahan Poin karena kesalahan sistem - 
                                                            @if($row->news->is_playgol == '1')
                                                                On Article - <a href="{{ url('playgoool/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                            @elseif($row->news->is_playgol == '0')
                                                                On Article - <a href="{{ url('news/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                            @else
                                                                On Article - <a href="{{ url('piknik/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                            @endif
                                                        @else
                                                            Penambahan Poin karena kesalahan sistem {{ $row->keterangan_ekstra }}
                                                        @endif
                                                    @else
                                                        @if(isset($row->news_id))
                                                          

                                                            @if($row->flag == 'fraud' && $row->customer_redeem_id != '')
                                                                {{ $row->keterangan  }} <br>
                                                            @else
                                                                Sistem mendeteksi komentar anda tidak sesuai dengan isi artikel, atau anda memberikan komentar yang sama berulang-ulang, atau juga komentar anda mengandung kalimat yang tidak layak dan mengandung SARA -
                                                            @endif
                                                        
                                                            @if($row->news->is_playgol == '1')
                                                                On Article - <a href="{{ url('playgoool/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                            @elseif($row->news->is_playgol == '0')
                                                                On Article - <a href="{{ url('news/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                            @else
                                                                On Article - <a href="{{ url('piknik/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                            @endif
                                                        @else
                                                            Mohon maaf, komentar kamu kurang dari 50 karakter
                                                        @endif
                                                    @endif
                                                @else
                                                    @if($row->news_id != '')
                                                        {{-- On Article - {{substr($row->news->title, 0, 25)}}... --}}
                                                        @if($row->news->is_playgol == '1')
                                                            On Article - <a href="{{ url('playgoool/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                        @elseif($row->news->is_playgol == '0')
                                                            On Article - <a href="{{ url('news/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                        @else
                                                            On Article - <a href="{{ url('piknik/view', $row->news_id).'/'.slug($row->news->slug) }}"> {{substr($row->news->title, 0, 25)}}...</a>
                                                        @endif
                                                    @elseif($row->partner_id != '')
                                                        On Partner - <a href="{{ url('partner/detail', $row->partner_id).'/'.slug($row->Partner->title) }}"> {{substr($row->Partner->title, 0, 25)}}...</a>
                                                    @elseif($row->game_id != '')
                                                        Main Game - {{$row->keterangan}}
                                                    @else 
                                                        @if($row->status == 'tukar' && $row->arisan_id != '')
                                                            Tukar poin on - <a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->arisan->title) }}"> {{$row->arisan->title}}</a>
                                                        @else 
                                                            Tukar poin on - {{$row->keterangan}}
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                            {{-- <td align="right"> {{($row->koint == 0)?'':'-'}}{{number_format($row->koint)}}</td> --}}

                                            <td align="left">
                                                @if($row->status == 'tukar')
                                                    <span class="label label-danger">- {{ number_format($row->point) }}</span>
                                                @else
                                                    <span class="label label-success">{{ number_format($row->point) }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="mt-3 mb-3">
                            <div class="right">
                                {!! $my_point->render() !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div> <!-- . card -->
                </div> <!-- .card -->
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>


@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
    jQuery(document).ready(function($){
        $('.filter_poin').change(function(){
            //alert('test');
            if($(this).val() == '')
            {
                window.location.href ="{{ url('member/histori_penggunaan_poin') }}";
            }
            else
            {
                window.location.href ="{{ url('member/histori_penggunaan_poin') }}?filter_poin="+$(this).val();

            }
        });
    });
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop