<?php
 use App\Http\Models\ArisanCustomer;
?>
@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile pt-4">
    <div class="container">
        <div class="row sg">

             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">

                @include('member_new_design.partial.flash_point')

                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-0 mt-0"><strong>Barang Incaran</strong></h4>
                        <h3 class="mb-4 mt-1 subhead">Barang Incaran</h3>
                        <hr>
                        <div class="mt-md-4 mt-0">
                            <div class="hadiah-wrapper">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#statusincaran" aria-controls="statusincaran" role="tab" data-toggle="tab">Status Barang Incaran</a></li>
                                    <li role="presentation"><a href="#sudahselesai" aria-controls="sudahselesai" role="tab" data-toggle="tab">Sudah Selesai</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content hadiahpanel">
                                    <div role="tabpanel" class="tab-pane active" id="statusincaran">
                                        @foreach($data_open as $row)
                                            <?php $status = ArisanCustomer::PeringkatSaya($row->arisan_id); ?>
                                        {{-- hadiah --}}
                                        <?php hadiah( 2, $row, null, null, 'incaran', $status ); ?>

                                        @endforeach
                                        
                                        <div class="mt-3 mb-3">
                                            <div class="right">
                                                {!! $data_open->fragment('statusincaran')->render() !!}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="sudahselesai">
                                        
                                        @foreach($data_close as $row)
                                            <?php $status = ArisanCustomer::PeringkatSaya($row->arisan_id); ?>
                                            <?php hadiah( 2, $row, null, null, 'incaran', $status ); ?>
                                            {{-- hadiah 
                                            <div class="hadiah">
                                                <a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->title) }}" class="h_chevron"><i class="fa fa-chevron-right"></i></a>
                                                <div class="row sg">
                                                    <div class="col-md-2 col-xs-4">
                                                        <a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->title) }}">
                                                            <img src="{{ newsImageUrl($row->banner)}}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-10 col-xs-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->title) }}">{{$row->title}}</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <div class="row sg mb-2">
                                                                <div class="col-md-2 col-xs-4">
                                                                    Status
                                                                </div>
                                                                <div class="col-md-10 col-xs-8">
                                                                    : <strong>
                                                                        {{ArisanCustomer::PeringkatSaya($row->arisan_id)}}
                                                                    </strong>
                                                                </div>
                                                            </div>
                                                            <div class="row sg mb-2">
                                                                <div class="col-md-2 col-xs-4">
                                                                    Tanggal
                                                                </div>
                                                                <div class="col-md-10 col-xs-8">
                                                                    : <strong>{{$row->created_at}}</strong>
                                                                </div>
                                                            </div>
                                                            <div class="row sg mb-2">
                                                                <div class="col-md-2 col-xs-4">
                                                                    Koin ditukar
                                                                </div>
                                                                <div class="col-md-10 col-xs-8">
                                                                    : <strong>{{$row->koin}}</strong>
                                                                </div>
                                                            </div>
                                                            <div class="row sg">
                                                                <div class="col-md-2 col-xs-4">
                                                                    Poin ditukar
                                                                </div>
                                                                <div class="col-md-10 col-xs-8">
                                                                    : <strong>{{$row->point}}</strong>
                                                                </div>
                                                            </div>
                                                        </div> <!-- //.hadiah-detail -->
                                                    </div>
                                                </div>
                                            </div> end hadiah --}}
                                        @endforeach

                                        {{-- hadiah 
                                        <div class="hadiah">
                                            <a href="#" class="h_chevron"><i class="fa fa-chevron-right"></i></a>
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Status
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>Kalah</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div> <!-- //.hadiah-detail -->
                                                </div>
                                            </div>
                                        </div> end hadiah --}}
                                        
                                        <div class="mt-3 mb-3">
                                            <div class="right">
                                                {!! $data_close->fragment('sudahselesai')->render() !!}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div> <!-- .hadiahpanel -->

                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>


@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop