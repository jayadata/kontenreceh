@extends('frontend.layouts.default')
@section('title')
Playworld Tukar Point
@stop

  <!-- FACEBOOK OPENGRAPH TAGS -->
  @section('meta')
  @if(isset($data))
      <meta property="og:title" content="{!! (isset($data) ? slug_fb($data->CustomerRedeem->Redeem->News->title) : '')!!}" />
      <meta property="og:type" content="website" />
      <meta property="og:url" content="{!! (isset($data) ? url('/news/view/').'/'.$data->CustomerRedeem->Redeem->News->id.'/'.$data->CustomerRedeem->Redeem->News->slug : 'http://www.playworld.id')!!}" />
      <meta property="og:description" content="{!! str_replace(array(' p ', 'br'), '', substr(strip_tags($data->CustomerRedeem->Redeem->News->content), 0, 170))!!}" />
      <meta property="og:image" content="{!! newsThumb($data->CustomerRedeem->Redeem->News->image) !!}" />
      <meta property="og:locale" content="id_ID"/>
      <meta property="og:site_name" content="{!! slug_fb($data->CustomerRedeem->Redeem->News->title)!!}" />
      <meta property="fb:app_id" content="107188393464738" />
      
      <meta name="description" content="{!! str_replace(array(' p ', 'br'), '', substr(strip_tags($data->CustomerRedeem->Redeem->News->content), 0, 170))!!}" />
      <!-- Style -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  @endif


@section('content')
  <style>
    main {
      background: #f5f5f5 url(https://www.toptal.com/designers/subtlepatterns/patterns/doodles.png) repeat;
      color: #666;
      font-family: "HelveticaNeueu", Arial, sans-serif;
      padding-bottom: 50px;
      position: relative;
      z-index: 1;
    }

     main:after {
      content:"";
      position: absolute;
      z-index: -1;
      background: #e6e7e8;
      opacity: .6;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
    }

    h4 {
      font-weight: bold;
    }

    img {
      max-width: 100%;
      border-radius: 5px;
    }

    .card {
      -webkit-box-shadow: 0 20px 50px rgba(0,0,0,.075);
      box-shadow: 0 20px 50px rgba(0,0,0,.075);
      border-radius: 8px;
      border: 0;
    }

    .btn.share_facebook {
      border-radius: 90px!important;
      padding: 12px 25px!important;
      background: #3b5998;
    }

    .btn.rounded {
      border-radius: 90px!important;
      padding: 12px 25px!important;
    }

    footer {
      position: absolute;
      background: #353535;
      color: #fff;
      font-size: 12px;
      display: block;
      left: 0;
      right: 0;
      bottom: 0;
      padding: 10px 0;
      text-align: center;
      display: none;
    }
  </style>
<?php
$string = New App\Http\Library\String_library;
use Jenssegers\Agent\Agent;
$agent = new Agent();
?>
{{-- <script type="text/javascript">
(function() { var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = '//api.at.getsocial.io/widget/v1/gs_async.js?id=376144'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })();
</script> --}}

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=107188393464738";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<?php 
    $reId = substr(md5($data->CustomerRedeem->Redeem->News->id),0, 13);
    $reSlug = substr(md5($data->CustomerRedeem->Redeem->News->slug), 0, 13);

    $s_id = $data->CustomerRedeem->Redeem->News->id;
    $s_title = $data->CustomerRedeem->Redeem->News->title;
    $s_content = $data->CustomerRedeem->Redeem->News->content;
    $s_href = url('unik/view', slug($data->CustomerRedeem->Redeem->News->reaction->first()->reaction->name)).'/'.substr(md5($data->CustomerRedeem->Redeem->News->id), 0, 13).'-'.$data->CustomerRedeem->Redeem->News->slug;
?>


<!-- CONTENT -->
  <div class="container pt-5">
    <div class="row justify-content-md-center">
      <div class="col-md-8">
        <div class="card p-3">
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <img src="{!! newsThumb($data->CustomerRedeem->Redeem->News->image) !!}" alt="" class="mb-3 mb-md-0">
              </div>
              <div class="col-md-8">
                <h4>{{ $data->CustomerRedeem->Redeem->News->title }}</h4>
                <p>{{str_replace(array("\r\n","\r"),"",substr(strip_tags($data->CustomerRedeem->Redeem->News->content), 0, 170))}}</p>
                <div class="row sg">
                  <div class="col-md-6">
                    <a href="#" class="btn btn-primary share_facebook rounded btn-block">Bagikan Ke Facebook &raquo;</a>
                  </div>
                  <div class="col-md-6">
                    <a href="javascript:history.back()" class="btn btn-secondary text-dark brd-1 rounded btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix clear"></div>

  <footer>
    <div class="row">
      <div class="col-md-12">
        Copyright Dolan &copy; 2018. All rights reserved
      </div>
    </div>
  </footer>
@stop

@section('scripts')
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
    jQuery(document).ready(function($){
        var $ = jQuery;

        var interval = 2000;  // 1000 = 1 second, 3000 = 3 seconds
        $(".share_facebook").on('click', function(event) {
          event.preventDefault();
          var that = $(this);
          var post = that.parents('article.post-area');
          $.ajaxSetup({ cache: true });
              $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
              FB.init({
                appId: '107188393464738',
                version: 'v2.8' // or v2.0, v2.1, v2.0
              });
              FB.ui({
                  method: 'share',
                  title: '{{$data->CustomerRedeem->Redeem->News->title}}',
                  description: '{{str_replace(array("\r\n","\r"),"",substr(strip_tags($data->CustomerRedeem->Redeem->News->content), 0, 170))}}',
                  href: '{{ $s_href }}',
                },
                function(response) {
                  if (response && !response.error_code) {
                    //succes
                    console.log('Posting completed.');

                    $.ajax({
                        type: "post",
                        url: '{{ route('pemenang_share_exce') }}',
                        data: {
                              _token  : '{!!csrf_token()!!}',
                              id      : '{{$data->id}}'
                        },
                        beforeSend: function(data) {

                        },
                        success: function (response) {
                            if(response == 200)
                            {
                                console.log('success');

                                var interval = 2000;  // 1000 = 1 second, 3000 = 3 seconds
                                window.location.href = "https://playworld.id/member";
                            }
                            else
                            {
                              alert('Ayo bagikan ini ke teman kamu dan dapatkan hadiah, kamu sekarang juga!');
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                            var err = eval("(" + xhr.responseText + ")");
                            alert(err.Message);

                        }
                    });
                  } else {
                    //fail
                    alert('Error: System Error');
                  }
              });
          });
        });
    });

</script>
@stop