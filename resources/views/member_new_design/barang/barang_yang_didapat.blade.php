@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile pt-4">
    <div class="container">
        <div class="row sg">

            @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                
                @include('member_new_design.partial.notifdatauser')

                @include('member_new_design.partial.flash_point')

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                BARANG YANG DIDAPATKAN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-0 mt-0"><strong>Barang Yang Didapatkan</strong></h4>
                        <h3 class="mb-4 mt-1 subhead">Menunggu Respon</h3>
                        <hr>
                        <div class="mt-md-4 mt-0">
                            <div class="hadiah-wrapper makeitDrop">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#tabwait" aria-controls="tabwait" role="tab" data-toggle="tab">Menunggu Respon</a></li>
                                    <li role="presentation"><a href="#tabonproses" aria-controls="tabonproses" role="tab" data-toggle="tab">Sedang Diproses</a></li>
                                    <li role="presentation"><a href="#tabkonfirm" aria-controls="tabkonfirm" role="tab" data-toggle="tab">Konfirmasi Penerimaan</a></li>
                                    <li role="presentation"><a href="#tabditerima" aria-controls="tabditerima" role="tab" data-toggle="tab">Daftar Barang Diterima</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content hadiahpanel">
                                    <!-- ooooooooooooooooooooooooooooooooooooooooooo
                                    MENUNGGU RESPON
                                    oooooooooooooooooooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane active" id="tabwait">
                                        @if ($message = Session::get('error'))
                                            <div class="alert alert-danger alert-dismissable fade in p-3">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <h4><strong>Error! </strong> {{ $message }}</h4>
                                            </div>
                                        @endif

                                        @if(count($menunggu) > 0)
                                        @foreach($menunggu as $row)
                                            
                                            <?php hadiah($row->bentuk_id, $row, $address, $phonenumber, null, null); ?>
                                        
                                        @endforeach 
                                         
                                        @else 
                                        <div class="hadiah">
                                        <h4>Tidak ada barang tersedia</h4>
                                        </div>
                                        @endif

                                        @include('member_new_design.barang.koin')

                                        <div class="mt-3 mb-3">
                                            <div class="text-center text-md-right">
                                                {!! $menunggu->fragment('tabwait')->render() !!}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>                                  

                                    </div>

                                    <!-- ooooooooooooooooooooooooooooooooooooooooooo
                                    ON PROCESS
                                    oooooooooooooooooooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane" id="tabonproses">
                                        @if(count($sedang_diproses) > 0)
                                        @foreach($sedang_diproses as $row)
                                        @if($row->bentuk_id == 1)
                                        
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-3 col-xs-4">
                                                   <?php
                                                        if ($row->CustomerRedeem->arisan['banner'] != '') {
                                                            $gambar = newsImageUrl($row->CustomerRedeem->arisan->banner);
                                                        }else{
                                                            $gambar = newsImageUrl($row->CustomerRedeem->redeem->image);
                                                        }

                                                        if ($row->customerRedeem->arisan['title']) {
                                                            $title = $row->customerRedeem->arisan->title;
                                                        }else{
                                                            $title = $row->customerRedeem->redeem->title;
                                                        }
                                                    ?>
                                                    <a href="#">
                                                        <img src="{{ $gambar }}" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-9 col-xs-8 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">{{ $title }}</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        {{$row->CustomerRedeem->customer_redeem_code}} <span class="show-for-large">|</span> <br class="hide-for-large"> {{($row->bentuk_id == 1)?'Fisik':'Non Fisik'}}
                                                    </div>
                                                    <div class="hadiah-detail mt-2 text-gray show-for-large">
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                @if ($row->customerRedeem->arisan_id == null) 
                                                                    {{-- Koin ditukar: <strong>{{$row->customerRedeem->koint}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->point}}</strong> --}}
                                                                    @if($row->customerRedeem->redeem_id == '458')
                                                                        Harga: <strong> 495.000 Rupiah</strong>
                                                                    @else
                                                                        Harga: <strong>{{$row->CustomerRedeem->point}} Poin</strong> 
                                                                    @endif 
                                                                    
                                                                @else
                                                                    Koin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_koin')}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_point')}}</strong>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                Tanggal: {{$row->created_at->format('d M Y H:i:s')}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6">
                                                                <a href="javascript:;" class="btn btn-dead text-white btn-link pl-0 text-left text-md-center pb-0 pb-md-2">{{$row->created_at->format('d M Y H:i:s')}} <span class="show-for-large">-</span> <br class="hide-for-large">Sedang Diproses</a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-md-2 text-left text-md-center pl-0 pl-md-3" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3 text-gray">
                                                    <div class="col-md-3 text-small col-xs-4">
                                                        <div class="text-dark p-2 p-md-3">TANGGAL</div>
                                                    </div>
                                                    <div class="col-md-9 text-small col-xs-8">
                                                        <div class="text-dark p-2 p-md-3">STATUS</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @foreach($row->CustomerRedeem->log as $log)
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3 col-xs-4">
                                                        <div class="block p-0 p-md-3 text-small text-gray">{{$log->created_at->format('d M Y H:i:s')}}</div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-small">
                                                        <div class="text-dark p-2 p-md-3">{{$log->description}}</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @endforeach
                                            </div>
                                        </div>{{-- end hadiah --}}

                                        @else
                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-3 col-xs-4">
                                                    <?php
                                                        if ($row->CustomerRedeem->arisan['banner'] != '') {
                                                            $gambar = newsImageUrl($row->CustomerRedeem->arisan->banner);
                                                        }else{
                                                            $gambar = newsImageUrl($row->CustomerRedeem->redeem->image);
                                                        }

                                                        if ($row->customerRedeem->arisan['title']) {
                                                            $title = $row->customerRedeem->arisan->title;
                                                        }else{
                                                            $title = $row->customerRedeem->redeem->title;
                                                        }
                                                    ?>
                                                    <a href="#">
                                                        <img src="{{ $gambar }}" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-9 col-xs-8 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;"> {{ $title }}</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                         {{$row->CustomerRedeem->customer_redeem_code}} <span class="show-for-large">|</span> <br class="hide-for-large"> {{($row->bentuk_id == 1)?'Fisik':'Non Fisik'}}
                                                    </div>
                                                    <div class="hadiah-detail mt-2 text-gray">
                                                        <div class="row sg mb-md-2 show-for-large">
                                                            <div class="col-md-12 col-xs-12">
                                                                @if ($row->customerRedeem->arisan_id == null) 
                                                                    Koin ditukar: <strong>{{$row->customerRedeem->koint}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->point}}</strong>
                                                                @else
                                                                    Koin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_koin')}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_point')}}</strong>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                Tanggal: {{$row->created_at->format('d M Y H:i:s')}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="hadiah-detail mt-2 text-gray hide">
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                <a href="javascript:;" class="btn btn-dead text-white btn-link pl-0 text-left text-md-center pb-0 pb-md-2">{{$row->created_at->format('d M Y H:i:s')}} <span class="show-for-large">-</span> <br class="hide-for-large">Sedang Diproses</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6 show-for-large">
                                                                <a href="javascript:;" class="btn btn-dead text-white btn-link pl-0 text-left text-md-center pb-0 pb-md-2">{{$row->created_at->format('d M Y H:i:s')}} <span class="show-for-large">-</span> <br class="hide-for-large">Sedang Diproses</a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-md-2 text-left text-md-center pl-0 pl-md-3" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3 text-gray">
                                                    <div class="col-md-3 text-small col-xs-4">
                                                        <div class="text-dark p-2 p-md-3">TANGGAL</div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-small">
                                                        <div class="text-dark p-2 p-md-3">STATUS</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @foreach($row->CustomerRedeem->log as $log)
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3 col-xs-4">
                                                        <div class="block p-0 p-md-3 text-small text-gray">{{$log->created_at->format('d M Y H:i:s')}}</div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-small">
                                                        <div class="text-dark p-2 p-md-3">{{$log->description}}</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @endforeach
                                            </div>
                                        </div>{{-- end hadiah --}}
                                        @endif
                                        @endforeach
                                        @else
                                        <div class="hadiah">
                                        <h4>Tidak ada barang tersedia</h4>
                                        </div>
                                        @endif

                                        <div class="mt-3 mb-3">
                                            <div class="right">
                                                {!! $sedang_diproses->fragment('tabonproses')->render() !!}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <!-- ooooooooooooooooooooooooooooooooooooooooooo
                                    KONFIRMASI
                                    oooooooooooooooooooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane" id="tabkonfirm">
                                        @if(count($konfirmasi) > 0)
                                        @foreach($konfirmasi as $row)
                                        
                                        @if($row->bentuk_id == 1 || $row->bentuk_id == 3)
                                            {{-- hadiah --}}
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-3 col-xs-4">
                                                        <?php
                                                            if ($row->CustomerRedeem->arisan['banner'] != '') {
                                                                $gambar = newsImageUrl($row->CustomerRedeem->arisan->banner);
                                                            }else{
                                                                $gambar = newsImageUrl($row->CustomerRedeem->redeem->image);
                                                            }

                                                            if ($row->customerRedeem->arisan['title']) {
                                                                $title = $row->customerRedeem->arisan->title;
                                                            }else{
                                                                $title = $row->customerRedeem->redeem->title;
                                                            }
                                                        ?>
                                                        <a href="#">
                                                            <img src="{{ $gambar }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;" class="konfirmasi_title_{{$row->id}}"> {{ $title }}</a></strong></h3>
                                                        <div class="jenis text-medium text-gray">
                                                             {{$row->CustomerRedeem->customer_redeem_code}} <span class="show-for-large">|</span><br class="hide-for-large"> {{($row->bentuk_id == 1)?'Fisik - Delivery':'On Merchant'}}
                                                        </div>
                                                        <div class="hadiah-detail mt-2 text-gray">
                                                            <div class="row sg mb-md-2 show-for-large">
                                                                <div class="col-md-12 col-xs-12">
                                                                    @if ($row->customerRedeem->arisan_id == null) 
                                                                        {{-- Koin ditukar: <strong>{{$row->customerRedeem->koint}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->point}}</strong> --}}

                                                                        @if($row->customerRedeem->redeem_id == '458')
                                                                        Harga: <strong> 495.000 Rupiah</strong>
                                                                    @else
                                                                        Harga: <strong>{{$row->CustomerRedeem->point}} Poin</strong> 
                                                                    @endif 
                                                                    @else
                                                                        Koin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_koin')}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_point')}}</strong>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="row sg mb-md-2">
                                                                <div class="col-md-12 col-xs-12">
                                                                    Tanggal: {{$row->created_at->format('d M Y H:i:s')}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-6">
                                                                    <a href="javascript:;" data-id='{{$row->id}}'
                                                                    customer-redeem-id="{{$row->customer_redeem_id}}"
                                                                     class="btn btn-outline-warning konfimasi_penerimaan mb-2 mb-md-0" >Konfirmasi <span class="show-for-large">Penerimaan</span></a>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2 pl-0" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="toggle-box">
                                                    <hr class="mt-3">
                                                    <div class="row sg mt-3 text-gray">
                                                        <div class="col-md-3 text-small col-xs-4">
                                                            <div class="text-dark p-2 p-md-3">TANGGAL</div>
                                                        </div>
                                                        <div class="col-md-9 text-small col-xs-8">
                                                            <div class="text-dark p-2 p-md-3">STATUS</div>
                                                        </div>
                                                    </div><hr class="mt-3">
                                                    @foreach($row->CustomerRedeem->log as $log)
                                                    <div class="row sg mt-3">
                                                        <div class="col-md-3 col-xs-4">
                                                            <div class="block p-0 p-md-3 text-small text-gray">{{$log->created_at->format('d M Y H:i:s')}}</div>
                                                        </div>
                                                        <div class="col-md-9 col-xs-8 text-small">
                                                            <div class="text-dark p-2 p-md-3">{{$log->description}}</div>
                                                        </div>
                                                    </div><hr class="mt-3">
                                                    @endforeach
                                                </div>
                                            </div>{{-- end hadiah --}}
                                        @else
                                            {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-3 col-xs-4">
                                                    <?php
                                                        if ($row->CustomerRedeem->arisan['banner'] != '') {
                                                            $gambar = newsImageUrl($row->CustomerRedeem->arisan->banner);
                                                        }else{
                                                            $gambar = newsImageUrl($row->CustomerRedeem->redeem->image);
                                                        }

                                                        if ($row->customerRedeem->arisan['title']) {
                                                            $title = $row->customerRedeem->arisan->title;
                                                        }else{
                                                            $title = $row->customerRedeem->redeem->title;
                                                        }
                                                    ?>
                                                    <a href="#">
                                                        <img src="{{ $gambar }}" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-9 col-xs-8 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;" class="konfirmasi_title_{{$row->id}}"> {{ $title }}</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                         {{$row->CustomerRedeem->customer_redeem_code}} <span class="show-for-large">|</span> <br class="hide-for-large"> {{($row->bentuk_id == 1)?'Fisik':'Non Fisik'}}
                                                    </div>
                                                    <div class="hadiah-detail mt-2 text-gray show-for-large">
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                @if ($row->customerRedeem->arisan_id == null) 
                                                                    Koin ditukar: <strong>{{$row->customerRedeem->koint}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->point}}</strong>
                                                                @else
                                                                    Koin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_koin')}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_point')}}</strong>
                                                                @endif</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                Tanggal: {{$row->created_at->format('d M Y H:i:s')}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6">
                                                                <a href="javascript:;" data-id='{{$row->id}}'
                                                                customer-redeem-id="{{$row->customer_redeem_id}}"
                                                                 class="btn btn-outline-warning konfimasi_penerimaan mb-2 mb-md-0" >Konfirmasi <span class="show-for-large">Penerimaan</span></a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2 pl-0" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3 text-gray">
                                                    <div class="col-md-3 text-small col-xs-4">
                                                        <div class="text-dark p-2 p-md-3">TANGGAL</div>
                                                    </div>
                                                    <div class="col-md-9 text-small col-xs-8">
                                                        <div class="text-dark p-2 p-md-3">STATUS</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @foreach($row->CustomerRedeem->log as $log)
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3 col-xs-4">
                                                        <div class="block p-0 p-md-3 text-small text-gray">{{$log->created_at->format('d M Y H:i:s')}}</div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-small">
                                                        <div class="text-dark p-2 p-md-3">{{$log->description}}</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @endforeach
                                            </div>
                                        </div>{{-- end hadiah --}}
                                        @endif

                                        @endforeach
                                        @else
                                        <div>
                                        <h4>Tidak ada barang yang tersedia</h4>
                                        </div>
                                        @endif

                                        <div class="mt-3 mb-3">
                                            <div class="right">
                                                {!! $konfirmasi->fragment('tabkonfirm')->render() !!}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <!-- ooooooooooooooooooooooooooooooooooooooooooo
                                    DITERIMA
                                    oooooooooooooooooooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane" id="tabditerima">
                                        @if(count($finish) > 0)
                                        @foreach($finish as $row)
                                        
                                        @if($row->bentuk_id == 1 || $row->bentuk_id == 3)
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-3 col-xs-4">
                                                    <?php
                                                        if ($row->CustomerRedeem->arisan['banner'] != '') {
                                                            $gambar = newsImageUrl($row->CustomerRedeem->arisan->banner);
                                                        }else{
                                                            $gambar = newsImageUrl($row->CustomerRedeem->redeem->image);
                                                        }

                                                        if ($row->customerRedeem->arisan['title']) {
                                                            $title = $row->customerRedeem->arisan->title;
                                                        }else{
                                                            $title = $row->customerRedeem->redeem->title;
                                                        }
                                                    ?>
                                                    <a href="#">
                                                        <img src="{{ $gambar }}" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-9 col-xs-8 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;"> {{ $title }}</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                         {{$row->CustomerRedeem->customer_redeem_code}} <span class="show-for-large">|</span> <br class="hide-for-large"> {{($row->bentuk_id == 1)?'Fisik - Delivery':'On Merchant'}}
                                                    </div>
                                                    <div class="hadiah-detail mt-2 text-gray">
                                                        <div class="row sg mb-md-2  show-for-large">
                                                            <div class="col-md-12 col-xs-12">
                                                                @if ($row->customerRedeem->arisan_id == null) 
                                                                    {{-- Koin ditukar: <strong>{{$row->customerRedeem->koint}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->point}}</strong> --}}
                                                                    @if($row->customerRedeem->redeem_id == '458')
                                                                        Harga: <strong> 495.000 Rupiah</strong>
                                                                    @else
                                                                        Harga: <strong>{{$row->CustomerRedeem->point}} Poin</strong> 
                                                                    @endif 
                                                                @else
                                                                    Koin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_koin')}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_point')}}</strong>
                                                                @endif</strong>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="row sg mb-md-2 show-for-large">
                                                            <div class="col-md-12 col-xs-12">
                                                                Tanggal: {{$row->created_at->format('d M Y H:i:s')}}
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-6">
                                                                    <a href="javascript:;" class="btn btn-link text-success pl-0 text-green">
                                                                    <span class="show-for-largex">{{$row->created_at->format('d M Y H:i:s')}} <span class="show-for-large">-</span><br class="hide-for-large"></span>Telah diterima</a>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2 pl-0" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3 text-gray">
                                                    <div class="col-md-3 text-small col-xs-4">
                                                        <div class="text-dark p-2 p-md-3">TANGGAL</div>
                                                    </div>
                                                    <div class="col-md-9 text-small col-xs-8">
                                                        <div class="text-dark p-2 p-md-3">STATUS</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @foreach($row->CustomerRedeem->log as $log)
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3 col-xs-4 text-small">
                                                        <div class="text-dark p-2 p-md-3">{{$log->created_at->format('d M Y H:i:s')}}</div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-small">
                                                        <div class="text-dark p-2 p-md-3">{{$log->description}}</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @endforeach
                                            </div>
                                        </div>{{-- end hadiah --}}
                                        @else
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-3 col-xs-4">
                                                    <?php
                                                        if ($row->CustomerRedeem->arisan['banner'] != '') {
                                                            $gambar = newsImageUrl($row->CustomerRedeem->arisan->banner);
                                                        }else{
                                                            $gambar = newsImageUrl($row->CustomerRedeem->redeem->image);
                                                        }

                                                        if ($row->customerRedeem->arisan['title']) {
                                                            $title = $row->customerRedeem->arisan->title;
                                                        }else{
                                                            $title = $row->customerRedeem->redeem->title;
                                                        }
                                                    ?>
                                                    <a href="#">
                                                        <img src="{{ $gambar }}" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-9 col-xs-8 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;"> {{ $title }}</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                         {{$row->CustomerRedeem->customer_redeem_code}} <span class="show-for-large">|</span> <br class="hide-for-large"> {{($row->bentuk_id == 1)?'Fisik':'Non Fisik'}}
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="hadiah-detail mt-2 text-gray show-for-large">
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                @if ($row->customerRedeem->arisan_id == null) 
                                                                    {{-- Koin ditukar: <strong>{{$row->customerRedeem->koint}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->point}}</strong> --}}
                                                                    @if($row->customerRedeem->redeem_id == '458')
                                                                        Harga: <strong> 495.000 Rupiah</strong>
                                                                    @else
                                                                        Harga: 
                                                                        <strong>
                                                                            {{ number_format($row->CustomerRedeem->redeem->min_point) }} 
                                                                            @if($row->CustomerRedeem->redeem->is_unique == 1 || $row->CustomerRedeem->redeem->is_unique == 2)
                                                                                Rupiah
                                                                            @else
                                                                                Poin
                                                                            @endif
                                                                            
                                                                        </strong> 
                                                                    @endif 
                                                                @else
                                                                    Koin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_koin')}}</strong> | Poin ditukar: <strong>{{$row->CustomerRedeem->arisan->detail->sum('saving_point')}}</strong>
                                                                @endif</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-md-2">
                                                            <div class="col-md-12 col-xs-12">
                                                                Tanggal: {{$row->created_at->format('d M Y H:i:s')}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6">
                                                                <a href="javascript:;" class="btn btn-link text-success pl-0 text-green">
                                                                <span class="show-for-largex">{{$row->created_at->format('d M Y H:i:s')}}</span>
                                                                <span class="show-for-large">-</span><br class="hide-for-large">Telah diterima</a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2 pl-0" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3 text-gray">
                                                    <div class="col-md-3 text-small col-xs-4">
                                                        <div class="text-dark p-2 p-md-3">TANGGAL</div>
                                                    </div>
                                                    <div class="col-md-9 text-small col-xs-8">
                                                        <div class="text-dark p-2 p-md-3">STATUS</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @foreach($row->CustomerRedeem->log as $log)
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3 col-xs-4">
                                                        <div class="block p-0 p-md-3 text-small text-gray">{{$log->created_at->format('d M Y H:i:s')}}</div>
                                                    </div>
                                                    <div class="col-md-9 col-xs-8 text-small">
                                                        <div class="text-dark p-2 p-md-3">{{$log->description}}</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                @endforeach
                                            </div>
                                        </div>{{-- end hadiah --}}
                                        @endif
        
                                        @endforeach
                                        @else
                                        <div class="hadia">
                                            <h4>Tidak ada barang yang tersedia</h4>
                                        </div>
                                        @endif

                                        <div class="mt-3 mb-3">
                                            <div class="right">
                                                {!! $finish->fragment('tabditerima')->render() !!}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div> <!-- .hadiahpanel -->

                            </div>
                        </div>
                    </div>
                </div> {{-- .card --}}
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="konfirmasi-mod" tabindex="-1" role="dialog" aria-labelledby="konfirmasiLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-center" id="konfirmasiLabel"><strong class="target_modal_title"></strong></h4>
            </div>
            <div class="modal-body text-center">
                Apakah nomor handphone ini telah benar ?
                <h4 class="target_nomor_telepon"></h4>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success nomor_benar" data-dismiss="modal">Ya Benar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salah</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="konfirmasi_alamat" tabindex="-1" role="dialog" aria-labelledby="konfirmasiLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-center" id="konfirmasiLabel"><strong class="target_modal_title"></strong></h4>
            </div>
            <div class="modal-body text-center">
                <input type="hidden" id="target_address">
                <input type="hidden" id="keterangan_product">

                <span class="text-big">Berikut adalah alamat pengiriman yang Anda pilih: </span>
                <h4 class="target_alamat"></h4>
                <h4 class="target_keterangan"></h4>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success alamat_benar" data-dismiss="modal">Ya Benar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salah</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="konfirmasi_terima" tabindex="-1" role="dialog" aria-labelledby="konfirmasiLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-center" id="konfirmasiLabel"><strong class="target_modal_title"></strong></h4>
            </div>
            <div class="modal-body text-center">
                Apakah anda yakin anda telah menerima barang/pulsa dari playworld.id?
                <h4 class="target_nomor_telepon"></h4>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success terima_benar" data-dismiss="modal">Ya Benar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salah</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="konfirmasi_redeem" tabindex="-1" role="dialog" aria-labelledby="konfirmasiLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-center" id="konfirmasiLabel"><strong class="target_modal_title"></strong></h4>
            </div>
             <form action="{{ route('konfirmasi_redeem') }}" method="POST">
                <div class="modal-body text-center">
                    <h5>Tunjukan form ini pada staff merchant</h5>
                   
                        <input type="hidden" name="track_id" id="track_id" class="">
                        <input type="hidden" name="customer_redeem_id" id="target_customer_redeem" class="">
                        <div class="form-group">
                            <input type="text" class="form-control" id="merchantuser" name="username" placeholder="Masukan Username Merchant" required autofocus>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="merchantpass" name="password" placeholder="Masukan Passowrd Merchant" required>
                        </div>
                    
                        <div class="form-group">
                            <input type="text" class="form-control" id="merchanttrx" name="trx" placeholder="Kode Transaksi" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" id="merchantcustomer" name="customer" placeholder="Nama Customer" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" id="merchanttlp" name="tlp" placeholder="No tlp" required>
                        </div>

                    <h4 class="target_alamat"></h4>
                </div>
                <div class="modal-footer text-center">
                    <button type="submit" class="btn btn-success redeem_benar">Ya Benar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salah</button>
                </div>
            </form>
        </div>
    </div>
</div>

@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
jQuery(document).ready(function($){

    // POPUP
    $('.tukar_koin').click(function(e){
        e.preventDefault();
        var title = $(this).data('nama'),
            jumlah = $(this).data('jumlah'),
            sisa = $(this).data('sisa');
            pajak = $(this).data('pajak');

        $('.mt-koin').show();
        pasang_apa = 'koin';
        $('.nama_barang').html(title);
        $('.yang_di_pasang').html( jumlah );
        $('.text_sisa').html( sisa );
        $('.pw-pajak').html( pajak );
        $('.proses_button').removeClass('hide');
    });

    $('.tukar_koin_menang').click(function(e){
        e.preventDefault();

        var jumlah              = $('.yang_di_pasang').val();
        var sisa                = $('.sisa_koin').val();
        var id_pajak                  = $('.pajak').val();
        //alert(customer_redeem_id+'-'+track_id+'-'+address_id);
        
        $.ajax({
            type: "post",
            url: '{{ route('pajak-koin') }}',
            data: {
                '_token'                  : '{!!csrf_token()!!}',
                'id_pajak'                : id_pajak,
                'arisan_id'               : '222',
                'jumlah'                  : jumlah
            },
            beforeSend: function(data) {

            },
            success: function (response) {
                if(response == 200)
                {
                    setTimeout(function(){
                        window.location.href='{{url(Request::path())}}';
                    }, 1000);
                }
                else
                {
                    alert('ups.. terjadi kesalahan');
                }
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);

            }
        });

        // $.post('{{ route('pajak-koin') }}',
        //     {
        //         _token                  : '{!!csrf_token()!!}',
        //         id_pajak                : id_pajak,
        //         arisan_id               : '222',
        //         jumlah                  : jumlah,
        //     }

        //     ,function(response){
        //         if(response == 200)
        //         {
        //             setTimeout(function(){
        //                 window.location.href='{{url(Request::path())}}';
        //             }, 1000);
        //         }
        //         else
        //         {
        //             alert(response);
        //         }
                
        // });
    });

    var customer_redeem_id  = '';
    var track_id            = '';
    var id_nomor_telepon    = '';
    var nomor_telepon       = '';
    var address_id          = '';
    $('.konfimasi_nomor_telepon').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var id              = $(this).attr('data-id');
        var id_nomor_telepon = $('.nomor_telepon_'+id).val();

        if( isMobile.any ) {
            var nomor_telepon    = $('.nomor_telepon_m_'+id+' option:selected').data('nope');
        } else {
            var nomor_telepon    = $('.nomor_telepon_'+id+' option:selected').data('nope');
        }

        var title           = $(this).attr('data-title');
        if(!nomor_telepon)
        {
            alert('Nomor telepon tidak boleh kosong');
            return false;
        }
        $('#konfirmasi-mod').modal();
        $('.target_nomor_telepon').html(nomor_telepon);
        $('.target_modal_title').html(title);
        $('#konfirmasi-mod').find('.nomor_benar').attr('data-number', nomor_telepon);

        customer_redeem_id  = $(this).attr('customer-redeem-id');
        track_id            = id;
        telpon_id           = id_nomor_telepon;
    });

    $('.nomor_benar').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var nomor_telepon = $(this).data('number');

        // $.post('{{ url(ENV('PREFIX').'/transaction/sedang_diproses') }}', 
        //     {
        //         _token                  : '{!!csrf_token()!!}',
        //         customer_redeem_id      : customer_redeem_id,
        //         track_id                : track_id,
        //         nomor_telepon           : nomor_telepon,
        //         telpon_id               : telpon_id,
        //     }
        //     ,function(response){
        //         console.log(response)
        //         if(response)
        //         {
        //             setTimeout(function(){
        //             window.location.href='{{url(Request::path())}}';
        //             }, 1000);
        //         }
        //         else
        //         {
        //             alert('Ups.. terjadi kesalahan');
        //         }
                
        // });

        $.ajax({
            type: "post",
            url: "{{ url(ENV('PREFIX').'/transaction/sedang_diproses') }}",
            data: {
                _token                  : '{!!csrf_token()!!}',
                customer_redeem_id      : customer_redeem_id,
                track_id                : track_id,
                nomor_telepon           : nomor_telepon,
                telpon_id               : telpon_id,
            },
            beforeSend: function(response) {

            },
            success: function (response) {
                console.log(response);
                setTimeout(function(){
                    window.location.href='{{url(Request::path())}}';
                    }, 1000);
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    });

    // //kofirmasi redeem
    $('.konfirmasi_redeem').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var id                   = $(this).attr('data-id');
        var customer_redeem_id   = $(this).attr('customer-redeem-id');
        var title                = $(this).attr('data-title');
        //alert(title);

        if(title == '')
        {
            alert('Pilihan alamat rumah tidak boleh kosong');
            return false;
        }
        $('#konfirmasi_redeem').modal();
        $('#track_id').val(id);
        $('#target_customer_redeem').val(customer_redeem_id);
        $('.target_modal_title').html(title);
        customer_redeem_id  = $(this).attr('customer-redeem-id');
        track_id            = id;
    });

    $('#asdredeem_benar').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var username            = $('#merchantuser').val();
        var password            = $('#merchantpass').val();
        var track_id            = $('#track_id').val();
        var customer_redeem_id  = $('#target_customer_redeem').val();

        //alert(track_id);

        $.post('{{ route('konfirmasi_redeem') }}',

            {
                _token                  : '{!!csrf_token()!!}',
                customer_redeem_id      : customer_redeem_id,
                track_id                : track_id,
                username                : username,
                password                : password,
            }
            ,function(response){
                console.log(response);
                if(response)
                {
                    setTimeout(function(){
                    window.location.href='{{url(Request::path())}}';
                }, 1000);
                }
                else
                {
                    alert('ups.. terjadi kesalahan!');
                }
                
        });
    });

    //kofirmasi alamat rumah
    $('.konfirmasi_alamat').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var id              = $(this).attr('data-id');
        if( isMobile.any ) { 
            var address_id          = $('.address_id_m_'+id).val();
            var nama_alamat          = $('.address_id_m_'+id+' option:selected').data('alamat');
            var detil_alamat          = $('.address_id_m_'+id+' option:selected').data('alamatdetail');
        } else {
            var address_id          = $('.address_id_'+id).val();
            var nama_alamat          = $('.address_id_'+id+' option:selected').data('alamat');
            var detil_alamat          = $('.address_id_'+id+' option:selected').data('alamatdetail');
        }
        
        var title           = $(this).attr('data-title');
        var keterangan      = $('.keterangan').val();
        //alert(keterangan);
        if(address_id == '')
        {
            alert('Pilihan alamat rumah tidak boleh kosong');
            return false;
        }
        $('#konfirmasi_alamat').modal();
        $('.target_alamat').html('<strong class="text-bold">'+nama_alamat+'</strong><br><p class="text-medium text-regular mt-2">'+detil_alamat+'</p>');
        $('.target_modal_title').html(title);
        $('.target_keterangan').html(keterangan);

        $('#keterangan_product').val(keterangan);
        $('#target_address').val(address_id);
        customer_redeem_id  = $(this).attr('customer-redeem-id');
        track_id            = id;
    });

    $('.alamat_benar').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var address_id            = $('#target_address').val();
        var keterangan_product    = $('#keterangan_product').val();
        //alert(customer_redeem_id+'-'+track_id+'-'+address_id);

        // $.post('{{ url(ENV('PREFIX').'/transaction/konfirmasi_alamat') }}', 
        //     {
        //         _token                  : '{!!csrf_token()!!}',
        //         customer_redeem_id      : customer_redeem_id,
        //         track_id                : track_id,
        //         address_id              : address_id,
        //         keterangan_product      : keterangan_product,
        //     }
        //     ,function(response){
        //         if(response)
        //         {
        //             setTimeout(function(){
        //             window.location.href='{{url(Request::path())}}';
        //         }, 1000);
        //         }
        //         else
        //         {
        //             alert('ups.. terjadi kesalahan!');
        //         }
                
        // });

        $.ajax({
            type: "post",
            url: "{{ url(ENV('PREFIX').'/transaction/konfirmasi_alamat') }}",
            data: {
                _token                  : '{!!csrf_token()!!}',
                customer_redeem_id      : customer_redeem_id,
                track_id                : track_id,
                address_id              : address_id,
                keterangan_product      : keterangan_product,
            },
            beforeSend: function(response) {

            },
            success: function (response) {
                console.log(response);
                setTimeout(function(){
                    window.location.href='{{url(Request::path())}}';
                }, 1000);
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    });

    $('.konfimasi_penerimaan').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        var id              = $(this).attr('data-id');
        var title           = $(this).attr('data-title');
        $('#konfirmasi_terima').modal();
        $('.target_modal_title').html(title);
        customer_redeem_id  = $(this).attr('customer-redeem-id');
        //alert(customer_redeem_id);
        track_id            = id;
    });

    $('.terima_benar').click(function(e){
        e.preventDefault();
        jQuery.noConflict();
        $.post('{{ url(ENV('PREFIX').'/transaction/konfirmasi_penerimaan') }}', 
            {
                _token                  : '{!!csrf_token()!!}',
                customer_redeem_id      : customer_redeem_id,
                track_id                : track_id,
            }
            ,function(response){
                console.log(response);
                if(response)
                {
                    setTimeout(function(){
                    window.location.href='{{url(Request::path())}}';
                    }, 1000);
                }
                else
                {
                    alert('Ups.. terjadi kesalahan');
                }
                
        });
    });

});
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop