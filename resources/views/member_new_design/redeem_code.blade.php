@extends('frontend.layouts.default')
@section('title')
Playworld Signin
@stop

@section('addons')

@stop
@section('content')
<?php 
$attributes = [
    'data-theme' => 'light',
    'data-type' => 'image',
];
?>
<link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<style type="text/css">
	.split.button {
	width: 100%; 
	padding: 0 10px;
	font-size: 12px!important;
	}
	
	.split.button span:after {
		line-height: 45px!important;
		font-size: 27px!important;
	}
	
	.split.button.facebook {
	background: #3b5998;
	text-transform: uppercase; }

	.split.button.facebook span {
	background: #2d4373; }

	.split.button.facebook span:after {
	border: none;
	font-family: "foundation-icons";
	content: "\f1c4";
	font-size: 3rem;
	margin-left: 0.3rem; }

	.split.button.twitter {
	background: #55acee;
	text-transform: uppercase; }

	.split.button.twitter span {
	background: #2795e9; }

	.split.button.twitter span:after {
	border: none;
	font-family: "foundation-icons";
	content: "\f1e4";
	font-size: 3rem;
	line-height: 5.25rem;
	margin-left: 0.3rem; }

	.split.button.google {
	background: #d50f25;
	text-transform: uppercase; }

	.split.button.google span {
	background: #a50c1d; }

	.split.button.google span:after {
	border: none;
	font-family: "foundation-icons";
	content: "\f1ca";
	font-size: 3rem;
	line-height: 6.25rem;
	margin-left: 0.3rem;}

	.split.button.left-icon {
	text-align: right; }

	.split.button.left-icon span {
	left: 0; }

	button.button.split{
	border: none!important;
	position: relative;
	padding-right: 5.0625rem;

	-webkit-appearance: none;
	-moz-appearance: none;
	border-radius: 0;
	border-style: solid;
	border-width: 0;
	cursor: pointer;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-weight: normal;
	line-height: normal;
	margin: 0 0 1.25rem;
	position: relative;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	padding: 15px 3rem 15px 4.7rem;
	font-size: 1.3rem;
	background-color: #008CBA;
	border-color: #007095;
	color: #FFFFFF;
	transition: background-color 300ms ease-out;
	}
	button.split.button span {
	display: block;
	height: 100%;
	position: absolute;
	right: 0;
	top: 0;
	width: 56px;
	}
	.makeitnarrow {
		max-width: 640px;
		margin: 0 auto;
	}

	.card {
		border-radius: 8px!important;
		box-shadow: 0 5px 20px rgba(0,0,0,.07);
	}

	.card input[type="text"] {
		border-radius: 5px;
		border: 1px solid #e5e5e5;
		background: #fff;
		box-shadow: none!important;
		height: 50px;
	}

	.alert.alert-success {
		background: #f6fff3;
	}
</style>
@if(Request::get('msisdn') != '')
<style type="text/css">
	.member-login .login-container
	{
		max-width: 600px!important;
	}
</style>

@endif
<!-- Main Section -->
@include('frontend.mainmodal')
<section>
	<div class="container">
		<div class="row sg">
			<div class="col-md-4 show-for-large">
				@include('member.partials.widget')
			</div>
			<div class="col-md-8">
				<div class="bg-white px-4 py-5">
					<div class="row mb-2 mb-md-3">
							<div class="col-xs-12 push-2 text-30 text-xs-18 text-center">
								{{-- Code belom masuk --}}
								@if(isset($alert))
									
								{{-- code udah masuk --}}
								@else
									<span class="text-bold">Masukkan Kode PW Anda</span><br>
									<div class="row mt-3 mb-0 mb-md-4">
										<div class="col-md-3 show-for-large">&nbsp;</div>
										<div class="col-md-6">
											<span class="bg-warning text-18 text-xs-13 pill text-light py-3 px-4 text-18 text-xs-13 pill py-3 px-4"><span class="text-normal">Kode PW :</span> <strong>@if(isset($code)) {{ $code->code }} @endif</strong></span>
										</div>
										<div class="col-md-3">&nbsp;</div>
									</div>
								@endif
							</div>
						</div>
					<div class="px-2 px-md-4">
						<div class="p-4 py-5">
							{{-- <form action="{{ route('vip:submit') }}" method="POST"> --}}
								<div class="row mb-2 mb-md-4">
									<div class="col-md-4 mb-2 mb-md-0 pt-2 text-bold">
										PW ID
									</div>
									<div class="col-md-8">
										<input class="form-control" type="text" name="customer" id="customer" placeholder="Playworld ID" required value="@if(Auth::user()){{Auth::user()->msisdn}}@endif">
										
										@if(!Auth::user())
											<div class="clearfix mb-3"></div>
											<div class="alert alert-success p-3">
												<div class="mb-3 text-dark text-13">
													Jika belum memiliki <strong>PW ID</strong>, silahkan klik salah satu tombol di bawah ini.
												</div>
												<!-- BTN LOGIN -->
												<a href="{{ url('/auth/facebook?sos='.Request::fullUrl()) }}"><button class="facebook button split text-12"> <span></span>sign in with facebook</button></a>
												<a href="{{ url('/auth/google?sos='.Request::fullUrl()) }}"><button class="google button split text-12"> <span></span>sign in with Google</button></a>
											</div>
										@endif
									</div>
								</div>
								<div class="row mb-2 mb-md-4">
									<div class="col-md-4 mb-2 mb-md-0 pt-2 text-bold">
										Kode PW
									</div>
									<div class="col-md-8">
										<input class="form-control" type="text" id="vip" placeholder="Masukkan VIP code anda" name="vip" required>
									</div>
								</div>
								<div class="row mb-2 mb-md-4">
									<div class="col-md-4 mb-2 mb-md-0 pt-2 text-bold">
										Captcha
									</div>
									<div class="col-md-8">
										<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="{{env('CAPTCHA_SITEKEY')}}"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 mb-2 mb-md-0 pt-2 show-for-large">
										&nbsp;
									</div>
									<div class="col-md-8">
										<button class="btn btn-success btn-block btn-lg success-triggergit">Submit</button>
									</div>
								</div>
							{{-- </form> --}}
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 show-for-large">&nbsp;</div>
		</div>
	</div>
</section>

@stop

@section('scripts')
    <script>
    	var door = 0;
    	function recaptchaCallback() {
    		door = 1;
    	};
    	jQuery(document).ready(function($){
    		$('.success-trigger').click(function(e){
	            e.preventDefault();
	            if(door == 0)
	        	{
	        		alert('Captcha belum di pilih!');
	        		return false;
	        	}
	            var nomorhandphone = $('#nomorhandphone').val();
	            $('.user-respond').addClass('is_loading');
	            $('.loader-container').show();
	            $.post('{{ url('number_registration') }}', 
	            	{
	            		_token      : '{!!csrf_token()!!}',
	            		nomorhandphone : nomorhandphone 
	            	}, 
	            	function(response){
	            		if(response['error'] == true)
	            		{
	            			$('.gagal').show();
	            			$('.fail_messages').html(response['messages']['nomorhandphone']);
	             			$('.pre-process').hide();
	            		}
	            		else
	            		{
	            			$('.berhasil').show();
	            			$('.success_messages').html(response['messages']['nomorhandphone']);
	             			$('.pre-process').hide();
	            		}
	            		$('.user-respond').removeClass('is_loading');
	            		$('.loader-container').hide();
	            	});
	        });
	        $('.back').click(function(){
	        	$('.gagal').hide();
     			$('.pre-process').show();
	        });

	        $('.pixel').click(function(e){
	        	fbq('track', 'CompleteRegistration');
	        });
    	});
    </script>
    <script>
        $('.success-triggergit').on('click', function(e){
            e.preventDefault();
            pwmodal('open');
            var vip      	= $('#vip').val();
			var customer    = $('#customer').val();
			pwModalHeaderText('');
            // alert(vip+' '+customer);
            // return false;
            $.ajax({
                type: 'POST',
                url : '{{ route('vip:submit') }}',
                data:{
                    _token       	: '{{ csrf_token() }}',
                    vip        		: vip,
                    customer       	: customer
                },
                beforeSend:function(response){
					
				},
				statusCode: {
					500: function() {
						//GAGAL:
						$('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
							<img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
							<p>KODE GAGAL</p>\
						</div>');
						pwModalBtnText('Kembali');
						$('.button-main-content').data('onclick', 'pwmodal("close")');
					}
				},
                success:function(response){
					var json = JSON.parse(JSON.stringify(response));
					// console.log(response);
					
					if (json['status'] == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>'+json['message']+'</p>\
						</div>');
						pwModalBtnText('Lanjutkan');
						var link = json['link'];
						pwModalBtnLoc(link);			
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>'+json['message']+'</p>\
						</div>');
						pwModalBtnText('Kembali');
						$('.button-main-content').data('onclick', 'pwmodal("close")');
                    }

                    pwmodal('loaded');				
                },
                error:function(response){

                }
            })
        })
    </script>
    <script src="https://www.google.com/recaptcha/api.js"
        async defer>
    </script>
@stop

