@extends('frontend.layouts.app')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">

            @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body px-4">
                        <div class="row bs4 sg">
                            <div class="col-md-8">
                                <h4 class="mb-4"><strong>Daftar Konten</strong></h4>
                            </div>
                            <div class="col-md-4">
                                <select name="Pilih Konten" class="form-control">
                                    <option value="eskul">Content</option>
                                </select>
                            </div>
                        </div>
                        <hr>

                        <div class="mt-4">
                            @if (count($content) > 0)
                                @foreach ($content as $data)    
                                    <div class="konten">
                                        <div class="row bs4">
                                            <div class="col-md-2 col-3 mb-4 mb-md-0">
                                                <img src="{{ StaticAsset('assets/frontend/logo.png') }}" alt="">
                                            </div>
                                            <div class="col-md-10 col-9">
                                                <h4 class="text-18 text-xs-16 mt-0">
                                                    <a href="{{ route('content.detail', $data->content->slug) }}">{{ $data->content->title }}</a>
                                                </h4>
                                                <div class="text-gray text-14">
                                                    {{-- Kasih kondisi poin or uang di sini aja ji. --}}
                                                    {{-- oke bang --}}
                                                    
                                                    {{ $data->created_at->format('d/m/Y') }} | 
                                                    @if ($data->type == 1)
                                                        <img src="{{ StaticAsset('assets/frontend/img/m-menu2/p.png') }}" width="15" class="mr-1" alt=""> {{ commaToDot('1000') }}
                                                    @else    
                                                        Rp {{ commaToDot($data->content->voucher->amount) }}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="text-center p-5">
                                    Tidak Ada Data
                                </div>
                            @endif

                            @if (count($content) > 0)
                                <div class="mt-3 mb-3">
                                    <div class="text-center text-md-right">
                                        {!! $content->render() !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div> <!-- .card -->
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>

@include('member.partials.footer')
@stop
@section('scripts')
<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop