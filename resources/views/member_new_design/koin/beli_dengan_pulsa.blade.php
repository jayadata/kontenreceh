@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>
{{-- <script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="VT-client-6SCMz_XPOiSANWuc"></script> --}}

<script type="text/javascript"
          src="https://app.midtrans.com/snap/snap.js"
          data-client-key="VT-client-x2ev7o4LZ3xTbyIv"></script> 



@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">

            @include('member.partials.sidebar')
    
            {{-- CONTENT --}}
            <div class="col-md-8">
                @include('member_new_design.partial.notifdatauser')
                        
                @include('member_new_design.partial.flash_point')

                <div class="card mb-3">
                    <div class="card-body">
                        {{-- <h4 class="mb-4"><strong>Beli Koin</strong></h4>
                        <hr> 
                        <br>
                        <p>
                            Kepada seluruh customer Playword,
                            Pembelian Koin dengan Pulsa akan mengalami proses perbaikan sistem
                            dan akan kembali aktif Kamis, 26 April 2018 Pukul 06:00 <br><br>

                            Sementara waktu, anda dapat memilih pembayaran Tanpa Pulsa. <br><br>

                            Terimakasih.
                        </p> --}}

                        
                        <div class="mt-md-4 mt-0">                          
                            <div class="hadiah-wrapper">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#denganpulsa" aria-controls="denganpulsa" role="tab" data-toggle="tab">Dengan Pulsa</a></li>
                                    <li role="presentation"><a href="#dengantransfer" aria-controls="dengantransfer" role="tab" data-toggle="tab">Tanpa Pulsa</a></li>
                                    {{-- <li role="presentation"><a href="#transferkoin" aria-controls="transferkoin" role="tab" data-toggle="tab">Transfer Koin</a></li> --}}
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content hadiahpanel">
                                    <div role="tabpanel" class="tab-pane active" id="denganpulsa">
                                        <h3 class="hide">Pembelian VIP</h3>
                                        <hr class="hide">
                                        <div class="pt-4 pb-3">
                                            <form action="#" class="operator-choice">
                                                <div class="left mr-5 choicelabel">
                                                    Pilih Operator
                                                </div>
                                                <div class="show-for-large left">
                                                    <label class="left mr-5">
                                                        <input type="radio" class="operatorku" name="operator" value="xlaxis" checked> 
                                                        XL &amp; Axis
                                                    </label>
                                                    <label class="left mr-5">
                                                        <input type="radio" class="operatorku" name="operator" value="indosat">
                                                        Indosat
                                                    </label>
                                                    <label class="left mr-5">
                                                        <input type="radio" class="operatorku" name="operator" value="telkomsel">
                                                        Telkomsel
                                                    </label>
                                                    <label class="left mr-5 disable">
                                                        <input type="radio" class="operatorku" name="operator" value="smartfren" disabled>
                                                        Smartfren
                                                    </label>
                                                </div>
                                                <div class="hide-for-large right">
                                                    <select name="operator" class="form-control select-operator">
                                                        <option value="xlaxis">XL &amp; Axis</option>
                                                        <option value="indosat">Indosat</option>
                                                        <option value="telkomsel">Telkomsel</option>
                                                        <option value="smartfren" disabled>Smartfren</option>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </form>
                                        </div> {{-- pt-4 pb-3 --}}

                                        {{-- CICILAN SMS --}}
                                        <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                            Pembelian VIP Berlangganan
                                        </div>

                                        <div class="untukindosat">
                                            <div class="pt-4 pr-md-5 pb-4 pl-0 bg-white brd-1">
                                                <div class="beliputus-item bp-single" style="width:100%;border:0">
                                                    <div class="row sg">
                                                        <div class="col-md-2 col-xs-4">
                                                            <a href="#">
                                                                <img src="{{ StaticAsset('assets/frontend/img/koin/vip-s.svg') }}" width="80" alt="">
                                                            </a>
                                                        </div>

                                                        <div class="col-xs-8 hide-for-large">
                                                            <div class="text-light bg-warning px-3 py-2  mb-4 mb-md-0" style="display:block;border-radius:5px">
                                                                Minggu Ke-1<br>
                                                                <strong>7 Hari VIP</strong>
                                                            </div>

                                                            <div class="text-light bg-warning px-3 py-2" style="display:block;border-radius:5px">
                                                                Minggu Selanjutnya<br>
                                                                <strong>5 Hari VIP</strong>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-10 col-xs-12 text-medium mt-4 mt-md-0">
                                                            <h3 class="mt-0 hide"><strong><a href="javascript:;">VIP 7 Hari</a></strong></h3>
                                                            <div class="text-light bg-warning px-3 py-3 mb-3 mb-md-0 show-for-large" style="display:inline-block;border-radius:5px">
                                                                Minggu Ke-1<br>
                                                                <strong>7 Hari VIP</strong>
                                                            </div>

                                                            <div class="text-light bg-warning px-3 py-3 show-for-large" style="display:inline-block;border-radius:5px">
                                                                Minggu Selanjutnya<br>
                                                                <strong>5 Hari VIP</strong>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Pembelian Pertama mendapatkan 2 Hari VIP Seharga Rp. 2200</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Selanjutnya Perpanjangan Hari VIP dikirim dalam 2 SMS per minggu</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Rp. 2200 per SMS untuk Perpanjangan 2 Hari VIP</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Setiap pengiriman SMS dalam kelipatan ke-3 akan diberikan Extra VIP 1 Hari</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Harga sudah termasuk pajak PPN 10%</div>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg agreement">
                                                                    <div class="col-md-12">
                                                                        <label>
                                                                            <input type="checkbox" value="setuju" class="mr-4 ml-1"> Saya setuju dengan ketentuan di atas
                                                                            <div class="clearfix mb-3"></div>
                                                                        </label>

                                                                        <a href="#" disabled="disabled" class="btn btn-warning text-white popup-trigger btn-disabled" data-onshow="indosat_subscribe('2.200', '', '2', '0')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>{{-- end hadiah --}}
                                        </div>

                                        <div class="untukxl">

                                            <div class="pt-4 pr-md-5 pb-4 pl-0 bg-white brd-1">
                                                <div class="beliputus-item bp-single" style="width:100%;border:0">
                                                    <div class="row sg">
                                                        <div class="col-md-2 col-xs-4">
                                                            <a href="#">
                                                                <img src="{{ StaticAsset('assets/frontend/img/koin/vip-s.svg') }}" alt="" width="80">
                                                            </a>
                                                        </div>
                                                        <div class="col-xs-8 hide-for-large">
                                                            <div class="text-light bg-warning px-3 py-2  mb-4 mb-md-0" style="display:block;border-radius:5px">
                                                                Minggu Ke-1<br>
                                                                <strong>9 Hari VIP</strong>
                                                            </div>
                                                            <div class="text-light bg-warning px-3 py-2" style="display:block;border-radius:5px">
                                                                Minggu Selanjutnya<br>
                                                                <strong>7 Hari VIP</strong>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-10 col-xs-12 text-medium mt-4 mt-md-0">
                                                            <h3 class="mt-0 hide"><strong><a href="javascript:;">VIP 9 Hari</a></strong></h3>

                                                            <div class="text-light bg-warning px-3 py-3  mb-3 mb-md-0 show-for-large" style="display:inline-block;border-radius:5px">
                                                                Minggu Ke-1<br>
                                                                <strong>9 Hari VIP</strong>
                                                            </div>
                                                            <div class="text-light bg-warning px-3 py-3 show-for-large" style="display:inline-block;border-radius:5px">
                                                                Minggu Selanjutnya<br>
                                                                <strong>7 Hari VIP</strong>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Pembelian Pertama mendapatkan 2 Hari VIP Seharga Rp. 2200</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div> Selanjutnya Perpanjangan Hari VIP dikirim dalam 3 SMS per minggu</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Rp. 2200 per SMS untuk Perpanjangan 2 Hari VIP</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Setiap pengiriman SMS dalam kelipatan ke-3 akan diberikan Extra VIP 1 Hari</div>
                                                            </div>

                                                            <div class="my-3">
                                                                <div class="circha orange"><i class="fa fa-check"></i></div>
                                                                <div>Harga sudah termasuk pajak PPN 10%</div>
                                                            </div>

                                                            <div class="mt-3">
                                                                <div class="row sg agreement">
                                                                    <div class="col-md-12">
                                                                        <label>
                                                                            <input type="checkbox" value="setuju" class="mr-4 ml-1"> Saya setuju dengan ketentuan di atas
                                                                            <div class="clearfix mb-3"></div>
                                                                        </label>



                                                                        <a href="#" btn-disabled class="btn btn-warning text-white popup-trigger btn-disabled" 
                                                                        data-onshow="xl_subscribe('2.200', '', '2', '0')" data-buttonaction="pwmodal('close')" disabled="disabled">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>{{-- end hadiah --}}

                                            {{-- Pembelian VIP Tanpa Berlangganan --}}
                                            <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                                Pembelian VIP Tanpa Berlangganan
                                            </div>

                                            <div class="beliputus mb-3">                                        
                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 2 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 2.200</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white popup-trigger" 
                                                                        data-onshow="xl_pay('2.200', 'PW3', '2', '0', '40141')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 3 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 3.300</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white popup-trigger" 
                                                                        data-onshow="xl_pay('3.300', 'PW4', '3', '0', '40141')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 5 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 5.500</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white popup-trigger" 
                                                                        data-onshow="xl_pay('5.500', 'PW5', '5', '0', '40141')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 8 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 8.800</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white popup-trigger" 
                                                                        data-onshow="xl_pay('8.800', 'PW6', '8', '0', '40141')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 10 + Xtra 1 Hari</a></strong></h3>
                                                            <span class="btn btn-outline-success btn-xs"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 11.000</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white popup-trigger" 
                                                                        data-onshow="xl_pay('11.000', 'PW7', '11', '0', '40141')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 15 + Xtra 2 Hari</a></strong></h3>
                                                            <span class="btn btn-outline-success btn-xs"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 16.500</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white popup-trigger" 
                                                                        data-onshow="xl_pay('16.500', 'PW8', '15', '2', '40141')" data-buttonaction="pwmodal('close')">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix clear"></div>
                                            </div>
                                        </div>

                                        <div class="untuktelkomsel">

                                            <div class="pt-4 pr-md-5 pb-4 pl-0 bg-white brd-1">
                                                <div class="mt-3 mb-3 text-center"><div class="pw-nodata">Tidak ada data</div></div>
                                                {{-- <div class="beliputus-item bp-single" style="width:44%;border:0">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-4">
                                                            <a href="#">
                                                                <img src="{{ StaticAsset('assets/frontend/img/koin/koin-bunch.jpg') }}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 75 + (25 Koin First REG)</a></strong></h3>
                                                            <span class="btn btn-outline-success btn-xs"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                                            <div class="hadiah-detail mt-2 text-small">
                                                                <strong>Rp. 6.600</strong> per minggu <br>
                                                                3 sms/minggu inc. PPN 10% <br>
                                                                1 sms = 25 Koin = Rp. 2.200 <br>
                                                                25 Koin untuk setiap REG
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white" data-toggle="modal" data-target="#konfirmasi" data-harga="Rp. 2.200" data-pw=""  data-pwd="telpon ke" data-pwdbz="" data-sms="" data-nomor="*123*789*40*1*1#" data-koin="75 Koin" data-ekstra="0 Hari">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div> --}}
                                            </div>{{-- end hadiah --}}

                                            {{-- Pembelian VIP Tanpa Berlangganan --}}
                                            <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                                Pembelian VIP Tanpa Berlangganan
                                            </div>

                                            <div class="beliputus mb-3">                                        
                                                {{-- <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 2 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 2.200</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white beli_lepas" data-toggle="modal" data-target="#konfirmasi" data-harga="Rp. 2.200" data-pwd="REG [spasi]" data-pwdbz="kirim ke" data-pw="TEL1" data-sms="" data-nomor="97788" data-koin="2 Hari"
                                                                        data-product-id='10001' 
                                                                        data-ekstra="0 Hari">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                {{-- <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 3 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 3.300</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white beli_lepas" data-toggle="modal" data-target="#konfirmasi" data-harga="Rp. 3.300" data-pwd="REG [spasi]" data-pwdbz="kirim ke" data-pw="TEL2" data-sms="" data-nomor="97788" data-koin="3 Hari" data-product-id='10002' data-ekstra="0 Hari">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 2 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 2.000</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" data-onshow="telkomsel_dialog('2.200', '2', '40141')" data-buttonaction="pwmodal('close')" class="btn btn-warning popup-trigger">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 5 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 5.500</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" data-onshow="telkomsel_dialog('5.500', '5', '40065')" data-buttonaction="pwmodal('close')" class="btn btn-warning popup-trigger">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 8 Hari</a></strong></h3>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 8.800</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white beli_lepas" data-toggle="modal" data-target="#konfirmasi" data-harga="Rp. 8.800" data-pwd="REG [spasi]" data-pwdbz="kirim ke" data-pw="TEL4" data-sms="" data-nomor="97788" data-koin="8 Hari" data-product-id='10004' data-ekstra="0 Hari">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}

                                                <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 10 + Xtra 1 Hari</a></strong></h3>
                                                            <span class="btn btn-outline-success btn-xs"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 11.000</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" data-onshow="telkomsel_dialog('11.000', '12', '40066')" data-buttonaction="pwmodal('close')" class="btn btn-warning popup-trigger">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- <div class="beliputus-item">
                                                    <div class="row sg">
                                                        <div class="col-md-4 col-xs-2 col-sm-4">
                                                            <a href="#">
                                                                <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                            </a>
                                                        </div>
                                                        <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                            <h3 class="mt-0"><strong><a href="javascript:;">VIP 15 + Xtra 2 Hari</a></strong></h3>
                                                            <span class="btn btn-outline-success btn-xs"><i class="fa fa-thumbs-o-up"></i> LEBIH HEMAT</span>
                                                            <div class="hadiah-detail mt-2">
                                                                <strong>Rp. 16.500</strong>
                                                                <span class="show-for-large">
                                                                    per sms <br>
                                                                    inc. PPN 10% <br>
                                                                </span>
                                                            </div>
                                                            <div class="mt-2">
                                                                <div class="row sg">
                                                                    <div class="col-md-12">
                                                                        <a href="#" class="btn btn-warning text-white beli_lepas" data-name="PW16500" data-product-id='40067' data-koin="15+2" data-harga="16.500">Beli</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="clearfix clear"></div>
                                            </div>
                                        </div>

                                        <div class="clearfix clear"></div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="dengantransfer">
                                        <div class="hide"><h3>Pembelian VIP Tanpa Berlangganan</h3>
                                        <hr></div>
                                        <div class="pt-4 pb-3">
                                            <form action="#" class="operator-choice">
                                                <div class="left mr-5 choicelabel">
                                                    Pilih Metode
                                                </div>
                                                <div class="show-for-large left">
                                                    <label class="left mr-5">
                                                        <input type="radio" class="operatorku" name="metode" value="transfer" checked> 
                                                        Transfer
                                                    </label>
                                                    {{-- <label class="left mr-5">
                                                        <input type="radio" class="metodeku" name="metode" value="card">
                                                        Credit Card
                                                    </label>
                                                    <label class="left mr-5">
                                                        <input type="radio" class="metodeku" name="metode" value="debit" disabled>
                                                        Direct Debit
                                                    </label> --}}
                                                </div>
                                                <div class="hide-for-large right">
                                                    <select name="metode" class="form-control select-metode">
                                                        <option value="transfer">Transfer</option>
                                                        {{-- <option value="card">Credit Card</option>
                                                        <option value="debit" disabled>Direct Debit</option> --}}
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>
                                            </form>
                                        </div>

                                        {{-- Pembelian VIP Tanpa Berlangganan --}}
                                        <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                            Pembelian VIP Tanpa Berlangganan
                                        </div>
                                        <div class="beliputus mb-3">

                                            <div class="beliputus-item">
                                                <div class="row sg">
                                                    <div class="col-md-4 col-xs-2 col-sm-4">
                                                        <a href="#">
                                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">VIP 16 Hari</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 10.000</strong>
                                                            <span class="show-for-large">
                                                                 <br>
                                                                inc. PPN 10% <br>
                                                            </span>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" id='pay-button' class="btn btn-warning text-white pay-button" data-toggle="modal" data-target="#konfirmasi-no" data-voucher='12' data-koin="150+15" data-harga="16.500">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="beliputus-item">
                                                <div class="row sg">
                                                    <div class="col-md-4 col-xs-2 col-sm-4">
                                                        <a href="#">
                                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">VIP 40 Hari</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 25.000</strong>
                                                            <span class="show-for-large">
                                                                 <br>
                                                                inc. PPN 10% <br>
                                                            </span>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" id='pay-button' class="btn btn-warning text-white pay-button" data-toggle="modal" data-target="#konfirmasi-no" data-voucher='9'>Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="beliputus-item">
                                                <div class="row sg">
                                                    <div class="col-md-4 col-xs-2 col-sm-4">
                                                        <a href="#">
                                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">VIP 80 Hari</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 50.000</strong>
                                                            <span class="show-for-large">
                                                                 <br>
                                                                inc. PPN 10% <br>
                                                            </span>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" id='pay-button' class="btn btn-warning text-white pay-button" data-toggle="modal" data-target="#konfirmasi-no" data-voucher='10'>Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="beliputus-item">
                                                <div class="row sg">
                                                    <div class="col-md-4 col-xs-2 col-sm-4">
                                                        <a href="#">
                                                            <img src="{{StaticAsset('assets/frontend/img/koin/vip-3.svg')}}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 col-xs-10 col-sm-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">VIP 160 Hari</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 100.000</strong>
                                                            <span class="show-for-large">
                                                                 <br>
                                                                inc. PPN 10% <br>
                                                            </span>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" id='pay-button' class="btn btn-warning text-white pay-button" data-toggle="modal" data-target="#konfirmasi-no" data-voucher='11'>Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix clear"></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="transferkoin">
                                        
                                        <h3><img src="{{ StaticAsset('assets/frontend/img/koin/koin-trx.png') }}" alt=""> Transfer Koin</h3>
                                        <hr>
                                        
                                        <!-- transfer koin widget -->
                                        <div class="tambahpoin_widget hargakoin">
                                            <div class="tab-contents-holder card-body">
                                                <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                                LIST HARGA
                                                oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
                                                <div class=" smlbtnpls tch denganpulsa">
                                                    <!-- List harga -->
                                                    <div class="clearfix text-left">
                                                        
                                                        <div class="row sg mb-3">
                                                            <div class="col-md-2 trf_lbl">
                                                                Sisa Koin
                                                            </div>
                                                            <div class="col-md-10 text-warning">
                                                                <strong>{{nomor_cantik($balance[0]->sisa)}}</strong>
                                                            </div>
                                                        </div>

                                                        <div class="row sg mb-4">
                                                            <div class="col-md-2 trf_lbl">
                                                                MSISDN Tujuan
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input name="msisdn_tujuan" type="text" placeholder="Misal: 363787859" class="form-control msisdn_tujuan">
                                                            </div>
                                                        </div>

                                                        <div class="row sg mb-4">
                                                            <div class="col-md-2 trf_lbl">
                                                                Jumlah Transfer
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input name="jumlah_transfer" type="text" placeholder="Misal: 2000" class="form-control jumlah_transfer">
                                                            </div>
                                                        </div>
                                                        <a class="btn btn-warning transfer_sekarang">TRANSFER SEKARANG</a>
                                                    </div>
                                                </div>
                                                <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                                TANPA PULSA
                                                oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
                                                <div class="tch tanpapulsa">
                                                    <div class="clearfix">
                                                        <label>
                                                            <div class="left">
                                                                <input type="radio" value="3" name="tambah_saldo" data-harga="2,200" data-koin="20" data-extra="0" data-pw="PW3" data-sms="http://www.gudangapp.com/xlp/?kc=PW3&amp;sdc=97788&amp;cb=http://playworld.id/rand&amp;desc=Kamu+akan+membeli+20+KOIN+PLAYWORLD+senilai+Rp.+2200+%28inc.+tax%29+%2F+Segera+gunakan+KOIN+nya+untuk+bisa+mengumpulkan+POIN+dan+memilih+hadiah+langsung+yang+kamu+inginkan+di+www.playworld.id&amp;img=https://playworld.id/assets/frontend/img/banner_btn_non_berlangganan.png&amp;eid=9e179"> IDR 25,000
                                                                <span class="ghost"></span>
                                                            </div>
                                                            <div class="right">
                                                                <span class="label label-success">
                                                                    40 Hari
                                                                </span>
                                                            </div>
                                                        </label>
                                                        <label>
                                                            <div class="left">
                                                                <input type="radio" value="3" name="tambah_saldo" data-harga="2,200" data-koin="20" data-extra="0" data-pw="PW3" data-sms="http://www.gudangapp.com/xlp/?kc=PW3&amp;sdc=97788&amp;cb=http://playworld.id/rand&amp;desc=Kamu+akan+membeli+20+KOIN+PLAYWORLD+senilai+Rp.+2200+%28inc.+tax%29+%2F+Segera+gunakan+KOIN+nya+untuk+bisa+mengumpulkan+POIN+dan+memilih+hadiah+langsung+yang+kamu+inginkan+di+www.playworld.id&amp;img=https://playworld.id/assets/frontend/img/banner_btn_non_berlangganan.png&amp;eid=9e179"> IDR 50,000
                                                                <span class="ghost"></span>
                                                            </div>
                                                            <div class="right">
                                                                <span class="label label-success">
                                                                    80 Hari
                                                                </span>
                                                            </div>
                                                        </label>
                                                        <label>
                                                            <div class="left">
                                                                <input type="radio" value="3" name="tambah_saldo" data-harga="2,200" data-koin="20" data-extra="0" data-pw="PW3" data-sms="http://www.gudangapp.com/xlp/?kc=PW3&amp;sdc=97788&amp;cb=http://playworld.id/rand&amp;desc=Kamu+akan+membeli+20+KOIN+PLAYWORLD+senilai+Rp.+2200+%28inc.+tax%29+%2F+Segera+gunakan+KOIN+nya+untuk+bisa+mengumpulkan+POIN+dan+memilih+hadiah+langsung+yang+kamu+inginkan+di+www.playworld.id&amp;img=https://playworld.id/assets/frontend/img/banner_btn_non_berlangganan.png&amp;eid=9e179"> IDR 100,000
                                                                <span class="ghost"></span>
                                                            </div>
                                                            <div class="right">
                                                                <span class="label label-success">
                                                                    160 Hari
                                                                </span>
                                                            </div>
                                                        </label>
                                                    </div>

                                                    <div class="clearfix martop-30">
                                                        <strong>Informasi </strong> <br>
                                                        <ul>
                                                            <li><small>Harga di atas sudah termasuk PPN 10%</small></li>
                                                            <li><small>Pembayaran mengggunakan Transfer atau Credit Card</small></li>
                                                        </ul>
                                                    </div>
                                                    <div class="clearfix">
                                                        <button class="btn btn-default btn-block btn-lg"">Beli</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- .tambahpoin_widget -->
                                    </div> <!-- .tab-pane -->
                                </div> <!-- .hadiahpanel -->

                            </div> <!-- .hadiah-wrapper -->                    
                            
                        </div> {{-- .mt-md-4.mt-0 --}}

                    </div> {{-- .card-body --}}
                </div> <!-- .card -->
            </div> {{-- .col-md-9 --}}
        </div> {{-- .row.sg --}}
    </div> {{-- .container --}}
</section> {{-- .profile --}}


<div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-40 mt-0 mt-md-2" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><img src="{{ StaticAsset('assets/frontend/icons/android-chrome-36x36.png') }}" alt=""> <strong class="text-gray text-16" style="line-height: 50px;border-left: 1px solid #e5e5e5;padding-left: 10px;margin-left: 10px;display:inline-block;color: #000!important;padding-top:4px">Pembelian VIP Berlangganan</strong></h4>
            </div>
            <div class="modal-body">
                <div class="not-available text-center">
                    <div class="ciricon">
                        <i class="fa fa-exclamation"></i>
                    </div>

                    <h1>BELUM AKTIF</h1>
                    <p>
                        Metode pembayaran ini belum dapat digunakan,<br>
                        silahkan gunakan metode pembayaran lain,<br>
                        mohon maaf atas ketidaknyamanan ini.
                    </p>
                </div>
                {{-- DETAIL --}}
                <div class="detailed-list">
                    <div class="dl-item">
                        <div class="dl-label">
                            <strong>Harga Pembelian VIP Pertama</strong>
                        </div>
                        <div class="dl-value harga-replace">
                            Rp 6,600
                        </div>
                    </div>
                    <div class="dl-item">
                        <div class="dl-label">
                            <strong>Jumlah Hari VIP</strong>
                        </div>
                        <div class="dl-value koin-replace">
                            9 Hari
                        </div>
                    </div>
                    <div class="dl-item hide">
                        <div class="dl-label">
                            <strong>VIP Xtra</strong>
                        </div>
                        <div class="dl-value ekstra-replace">
                            0
                        </div>
                    </div>
                </div> {{-- .detailed-list --}}

                <div class="text-gray pt-5 pr-5 pl-5 pb-0 guide text-center text-dark">
                    

                    <div class="subsribing">
                        {{-- Untuk melanjutkan pembelian silahkan sms ke 97788 dengan format sebagai berikut: --}}
                        <span class="hide-for-large">Untuk melanjutkan proses pembelian, klik tombol di bawah ini</span>

                        <div class="mt-4 mb-4 pt-3 pb-3 text-center hide-for-large">
                            <a href="" class="btn btn-warning btn-block btn-lg tel-replace">Beli Sekarang</a>
                        </div>
                        
                        <span class="show-for-large">Untuk melanjutkan proses pembelian Telepon/Call ke</span>

                        <div class="text-center mb-3 hide-for-large">Atau kirim SMS ke <strong style="color: #000">97789</strong> dengan format</div>

                        <div class="text-center show-for-large">
                            <div class="mt-4 mb-4 pt-3 pb-3 bg-gray text-center rounded px-4" style="display: inline-block">
                                <span class="so nomor-replace"></span>
                            </div>
                        </div>

                        <span class="show-for-large">Atau kirim SMS ke <strong style="color: #000">97789</strong> dengan format</span>

                        <div class="text-center">
                            <div class="mt-4 mb-4 pt-3 pb-3 bg-gray text-center rounded px-4" style="display: inline-block">
                                {{-- <span class="pwd-replace show-for-large">REG [spasi]</span> 
                                <span class="so pw-replace">PW</span> 
                                <span class="pwdbz-replace">kirim ke</span>  
                                <span class="so nomor-replace">97788</span>
                                <span class="so product-replace"> </span> --}}
                                <span class="so">REG <code>[spasi]</code> <span class="pw-replace">PW</span></span>
                                <span class="sms-replace"></span>
                            </div>
                        </div>
                    </div>

                    <div class="payonce">
                        <span> Untuk melanjutkan proses pembelian kirim SMS ke <strong>97788</strong> dengan format </span>

                        <div class="text-center">
                            <div class="mt-4 mb-4 pt-3 pb-3 bg-gray text-center rounded px-4" style="display: inline-block">
                                <span class="pw-replace so">PW</span>
                            </div>
                        </div>
                    </div>
                    {{-- Anda bisa menggunakan handphone untuk kemudahan proses pembelian KOIN. --}}
                </div>

            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
                <a href="" class="btn btn-danger btn-block btn-lg" data-dismiss="modal">Tutup</a>
                {{-- <a href="" class="btn btn-warning btn-block btn-lg hide-for-large sms-replace">Beli</a> --}}
            </div>
        </div>
    </div>
</div>

@include('frontend.mainmodal')
@include('member.partials.footer')
@stop
@section('scripts')
<script>
    
    /*USER DATAA*/
    // Render product detil
    @if (Auth::user())
        var obj = {
            'nama': '{{ Auth::user()->first_name.' '.Auth::user()->last_name }}',
            'telp': '{{ Auth::user()->no_telp }}',
            'mail': '{{ Auth::user()->email }}'
        };
        var user = JSON.stringify(obj);
    @endif;

    /* AXIS/XL LANGGANAN */
    function xl_subscribe( $harga, $sms, $vip, $extra ){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip) + parseInt($extra);
            render_harga( 'Amount', $harga, false );

            // Render product detail
            render_productDetail( 'VIP BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "xl_sendsms('"+$harga+"', 'REG')", '*123*789*40*1*1#', false);

            // Change text
            pwModalBtnText('Tutup');

            // end load state
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);

            //render_text( subscribehtml );
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* AXIS/XL BELI PUTUS */
    function xl_pay( $harga, $sms, $vip, $extra, $id ){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip) + parseInt($extra);
            render_harga( 'Amount', $harga, false );

            // Render product detil
            render_productDetail( 'VIP NON BERLANGGANAN '+$totalvip+' Hari<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "xl_sendsms('"+$sms+"', 'NON-REG')", null);

            // end load state
            pwModalBtnText('Tutup');
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* AXIS/XL BUY PROCESS */
    function xl_sendsms($id, $type) {
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $id;
            var type    = $type;
            // alert(type);
            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor telkomsel yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('xl:send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    /* INDOSAT LANGGANAN */
    function indosat_subscribe( $harga, $sms, $vip, $extra ){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip) + parseInt($extra);
            render_harga( 'Amount', $harga, false );

            // Render product detail
            render_productDetail( 'VIP BERLANGGANAN<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "indosat_sendsms('1850', 'REG')", '*123*595*5#', true);

            // Change text
            pwModalBtnText('Tutup');

            // end load state
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);

            // render_text( subscribehtml );
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* INDOSAT BUY PROCESS */
    function indosat_sendsms($id, $type) {
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var voucher = $id;
            var type    = $type;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor telkomsel yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ route('indosat:send') }}",
                data: {
                    voucher      : voucher,
                    nomorhandphone  : nomorhandphone,
                    type  : type,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {
                    console.log(response);
                    if (response == 1) {
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                            <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                        </div>');
                    }else{
                        //GAGAL:
                        $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                            <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                            <p>Pengiriman SMS Gagal silakan refresh dan coba lagi</p>\
                        </div>');
                    }

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    /* TELKOMSEL POPUP DIALOG */
    function telkomsel_dialog( $harga, $vip, $id){
        @if (Auth::user())
            // Render Harga
            var $totalvip = parseInt($vip);
            render_harga( 'Amount', $harga, false );

            // Render product detil
            render_productDetail( 'VIP NON BERLANGGANAN '+parseInt($totalvip)+' Hari<br><small>(Sudah termasuk PPN 10%)</small>', $harga, user, false, $harga );

            // Render payment method
            render_peymentMethod(true, "telkomsel_sendsms('"+$id+"')", null);

            // end load state
            pwModalBtnText('Tutup');
            pwmodal('loaded');

            setTimeout(function() {
                tab_click();
            }, 100);
        @else
            pleaselogin();
            pwmodal('loaded');
        @endif
    }

    /* TELKOMSEL BUY PROCESS */
    function telkomsel_sendsms($id){
        @if (Auth::user())
            var nomorhandphone = $('#nomorhandphone').val();
            var product_id = $id;

            if( nomorhandphone == '' ) {
                alert('Mohon masukkan nomor telkomsel yang valid');
                return false;
            }

            $.ajax({
                type: "post",
                url: "{{ url('api/mdmedia/request') }}",
                data: {
                    product_id      : product_id,
                    nomorhandphone  : nomorhandphone,
                },
                beforeSend: function(response) {
                    pwmodal('loading');
                },
                success: function (response) {

                    $('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_success.png') }}" alt="" width="90"><br><br>\
                        <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                    </div>');

                    //GAGAL:
                    /*$('.pw-modal--data-storage').html('<div class="text-center mt-5 pt-5">\
                        <img src="{{ StaticAsset('assets/frontend/img/redeemicon/icon_error.png') }}" alt="" width="90"><br><br>\
                        <p>Kami telah mengirimkan SMS ke nomor Anda</p>\
                    </div>');*/

                    pwModalBtnText('Tutup');
                    $('.button-main-content').data('onclick', 'pwmodal("close")');
                    udahbeli = true;

                    pwmodal('loaded');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        @endif
    }

    jQuery(document).ready(function($){
        $('#konfirmasi').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget); // Button that triggered the modal
            var harga   = button.data('harga');
            var koin    = button.data('koin');
            var ekstra  = button.data('ekstra');
            var sms     = button.data('sms');
            var nomor   = button.data('nomor');
            var pw      = button.data('pw');
            var pwd     = button.data('pwd');
            var pwdbz   = button.data('pwdbz');
            var product = button.data('product-id');
            var modal   = $(this);

            modal.find('.harga-replace').text(harga);
            modal.find('.koin-replace').text(koin);
            modal.find('.ekstra-replace').text(ekstra);
            modal.find('.sms-replace').attr('href', sms);
            modal.find('.nomor-replace').text(nomor);
            modal.find('.tel-replace').attr('href','tel:'+nomor);
            modal.find('.pw-replace').text(pw);
            modal.find('.pwd-replace').text(pwd);
            modal.find('.pwdbz-replace').text(pwdbz);
            modal.find('.product-replace').text(product);

            if( button.parents('#dengantransfer').length ) {
                modal.find('.btn').text('Lanjutkan').attr('href', 'http://google.com');
                $('.not-available').hide();
                $('.detailed-list, .guide').show();
            } else if( button.parents('#denganpulsa').length ) {
                var operator = $("input[name='operator']:checked").val();
                if(  operator == 'smartfren' ) {
                    $('.not-available').show();
                    $('.detailed-list, .guide').hide();
                } else if( operator != 'telkomsel' || operator != 'smartfren' ) {
                    $('.not-available').hide();
                    $('.detailed-list, .guide').show();
                }
            } else {
                modal.find('.btn').text('Tutup').attr('data-dismiss', 'modal');
            }

            if( button.parents('.bp-single').length ) {
                modal.find('.modal-title').find('strong').text('Pembelian VIP Berlangganan');
                modal.find('.subsribing').show();
                modal.find('.payonce').hide();
                //modal.find('.guide').find('>span').text('Untuk melanjutkan proses pembelian VIP berlangganan silahkan:');
            } else {
                modal.find('.modal-title').find('strong').text('Pembelian VIP Tanpa Berlangganan');
                modal.find('.subsribing').hide();
                modal.find('.payonce').show();
                //modal.find('.guide').find('>span').text('Untuk melanjutkan proses pembelian VIP Tanpa berlangganan silahkan:');
            }
        });

        /* Localhost */
        var product_id   = '';
        var koin      = '';
        var harga      = '';
        $('.beli_lepas').click(function(e){
            e.preventDefault();
            product_id      = $(this).attr('data-product-id');
            koin      = $(this).attr('data-koin');
            harga      = $(this).attr('data-harga');

            $('.target_koin').text(koin);
            $('.target_harga').text(harga);

            $('#telkomsel-modal').modal('show');
        });

       /* $('#telkomsel-modal').on('shown.bs.modal', function (e) {
            console.log(koin);
            setTimeout(function() {
                $('.target_koin').text(koin);
                $('.target_harga').text(harga);
            }, 200);
        });*/

        

        $('.lanjutbray').click(function(e){
            $('.nomor-masuk').hide(0);
            $('.processing').show(0);
            var nomorhandphone      = $('#nomorhandphone').val();

            $.ajax({
                type: "post",
                url: "{{ url('api/mdmedia/request') }}",
                data: {
                    product_id      : product_id,
                    nomorhandphone      : nomorhandphone,
                },
                beforeSend: function(response) {

                },
                success: function (response) {
                    console.log(response);

                    setTimeout(function() {
                        $('.spinner-telkomsel').hide(0, function(){
                            $('.wording-telkomsel').text(response['message']);
                            $('.wording-telkomsel').prepend('<div class="success-icon-telkomsel"><img src="{{ StaticAsset('assets/frontend/img/success.png') }}" class="mb-3" width="90" alt=""/></div>');
                            $('.lanjutbray').hide().siblings('.btn-danger').toggleClass('btn-danger btn-primary').text('Ok');
                        });
                    }, 2000);
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        });

        $('#telkomsel-modal').on('hidden.bs.modal', function (e) {
            $('.wording-telkomsel').text('Sedang memproses...');
            $('.processing').hide();
            $('.nomor-masuk, .spinner-telkomsel').show(); 
            $('.lanjutbray').show().siblings('.btn-primary').toggleClass('btn-primary btn-danger').text('Batalkan Pembelian');
            // test masuk
        });

        $('.transfer_sekarang').click(function(event){
            event.preventDefault();
            msisdn_tujuan   = $('.msisdn_tujuan').val();
            jumlah_transfer = $('.jumlah_transfer').val();
            my_balance   = '{{$balance[0]->sisa}}';
            //alert(my_balance);
            if(msisdn_tujuan == '' || jumlah_transfer == '')
            {
                alert('Data msidn tujuan atau nominal jumlah transfer tidak boleh kosong.');
                return false;
            }
            if(jumlah_transfer > {{$balance[0]->sisa}})
            {
                $('.pointidakcukup').show();
                $('.text_msisdn_tujuan').html(msisdn_tujuan);
                $('.text_jumlah_transfer').html(jumlah_transfer);
                $('.text_my_balance').html(my_balance);
                return false;
            }
            else
            {
                $('.poincukup').show();
                //alert(msisdn);
                $('.text_msisdn_tujuan').html(msisdn_tujuan);
                $('.text_jumlah_transfer').html(jumlah_transfer);
                $('.text_my_balance').html(my_balance);
                

            }
        });

        $('.lanjut_transfer').click(function(event){
            event.preventDefault();
            $('.mt-app').addClass('is_loading');
            if(msisdn_tujuan != '' || jumlah_transfer != '')
            {
                $.post('{{ url('member/transfer_koin') }}', 
                {
                    _token              : '{!!csrf_token()!!}',
                    msisdn_tujuan       : msisdn_tujuan,
                    jumlah_transfer     : jumlah_transfer
                }, 
                function(response){
                    if(response.status != 3)
                    {
                        //mt-outtest transaksi_gagal
                        $('.mt-outtest').hide();
                        $('.fail_text').html(response.text);
                        $('.transaksi_gagal').show();
                    }
                    else
                    {
                        $('.mt-outtest').hide();
                        $('.sucess_text').html(response.text);
                        $('.transaksi_berhasil').show();
                        /*setTimeout(function(){
                            window.location.href = '{{ url(Request::path()) }}';
                        }, 1000);*/
                    }
                    $('.mt-app').removeClass('is_loading');

                });
            }
            else
            {
                alert('failed');
            }
        });

        //var payButton = document.getElementsByClassName('pay-button');
        //var payButton = document.getElementById("pay-button");
        
        //payButton.addEventListener('click', function () {
        $('.pay-button').click(function () {
            var voucher_id = $(this).attr('data-voucher');

            // fbq('track', 'Purchase', {
            //     value: 1,
            //     currency: 'IDR',
            // });
            //alert(voucher_id);
            $.ajax({
                type: "post",
                url: base_url +'/member/payment/buy',
                data: {
                    _token     :  '{!!csrf_token()!!}',
                   voucher_id  : voucher_id
                },
                beforeSend: function(response) {

                },
                success: function (response) {
                    // console.log(response);
                    var err = eval("(" + response.responseText + ")");
                    //alert(err.Message);

                    //snap.pay(response);

                    snap.pay(response,{
                        onSuccess: function(response){console.log('success');console.log(response);},
                        onPending: function(response){
                            console.log('pending');
                            console.log(response);
                            location.href = "https://playworld.id/member/status_transfer";
                        },
                        onError: function(response){console.log('error');console.log(response);},
                        onClose: function(){console.log('customer closed the popup without finishing the payment');}
                    });
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);

                }
                
                // error: function(response){
                //     if (!$.trim(response)){   
                //         alert("What follows is blank: " + response);
                //     }
                //     else{   
                //         alert("What follows is not blank: " + response);
                //     }
                //    console.log(response);
                // } 
            });
        });
    });
</script>
<script src="https://www.google.com/recaptcha/api.js"
    async defer>
</script>
<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop