@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<!--End of Zendesk Chat Script-->
<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">

             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                @include('member_new_design.partial.notifdatauser')
                
                @include('member_new_design.partial.flash_point')

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                HISTORI PENUKARAN KOIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        {{-- <h4 class="mb-4"><strong>Mutasi Koin</strong></h4> --}}
                        <h4 class="mb-4"><strong>Mutasi VIP</strong></h4>
                        <hr>
                        <div class="my-games">
                            {{-- <div class="profile-header-total clearfix">
                                <div class="right" style="min-width: 200px;">
                                    Filter Type 
                                    <select name="filter_balance" id="" class="form-control right ml-3 filter_balance">
                                        <option value="">All</option>
                                        <option value="beli" {{(Request::get('filter_balance') == 'beli')?'selected="selected':''}}>Beli</option>
                                        <option value="transfer" {{(Request::get('filter_balance') == 'transfer')?'selected="selected':''}}>Transfer</option>
                                        <option value="terima" {{(Request::get('filter_balance') == 'terima')?'selected="selected':''}}>Terima</option>
                                        <option value="tukar" {{(Request::get('filter_balance') == 'tukar')?'selected="selected':''}}>Tukar</option>

                                        <option value="add" {{(Request::get('filter_balance') == 'add')?'selected="selected':''}}>Add</option>
                                        <option value="fraud" {{(Request::get('filter_balance') == 'fraud')?'selected="selected':''}}>Fraud</option>
                                    </select>
                                </div>
                                <div class="right">
                                    <strong>{{number_format($balance[0]->sisa)}}/{{ number_format($balance[0]->total_balance) }}</strong>
                                </div>
                            </div> --}}

                            <div class="">
                                <table class="user-games table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="30">No</th>
                                            <th width="60">Tanggal</th>
                                            <th width="100">TRX Code</th>
                                            <th>Type</th>
                                            <th>Detail</th>
                                            <th width="80">Hari</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $i = 1;
                                    ?>

                                        @foreach ($my_balance as $row)

                                            <tr>
                                                <td align="center">{{$i++}}</td>
                                                <td>{{$row->created_at->format('d/m/Y')}}</td>
                                                <td>{{($row->balance_code != '')?$row->balance_code:'-'}}</td>
                                                <td>{{$row->type}}</td>
                                                <td>
                                                    @if($row->type == 'add' || $row->type == 'fraud')
                                                        {{$row->keterangan}}
                                                    @else
                                                        @if($row->quiz_id != '')
                                                            @if($row->quiz->news->is_playgol == 0)
                                                            Mengikuti Quiz : <a href="{{ url('news/view', $row->quiz->news_id).'/'.slug($row->quiz->news->title) }}" >{{$row->quiz->news->title}}</a>
                                                            @else 
                                                            Mengikuti Quiz : <a href="{{ url('playgoool/view', $row->quiz->news_id).'/'.slug($row->quiz->news->title) }}" >{{$row->quiz->news->title}}</a>
                                                            @endif
                                                        @elseif($row->arisan_id != '')
                                                        Mengikuti Tukar Poin : <a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->arisan->title) }}">{{$row->arisan->title}}</a> 
                                                        @else
                                                        {{-- {{$row->keterangan}} --}}
                                                        Pembelian VIP
                                                        @endif
                                                    @endif
                                                    
                                                </td>
                                                <td align="left"> 
                                                    @if(/*$row->type == 'tukar' ||*/ $row->type == 'transfer' || $row->type == 'fraud')
                                                        <span class="label label-danger">- {{ number_format($row->nominal/10) }}</span>
                                                    @else
                                                        <span class="label label-success">{{ number_format($row->nominal/10) }}</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="mt-3 mb-3">
                            <div class="right">
                                @if(Request::get('filter_balance') == '')
                                 {!!$my_balance->render()!!}
                                 @else 
                                 {!!$my_balance->appends(['filter_balance' => Request::get('filter_balance')])->render()!!}
                                 @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div> <!-- .card -->
                    </div>

                </div> <!-- .card -->
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>


@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
    jQuery(document).ready(function($){
        $('.filter_balance').change(function(){
            //alert('test');
            if($(this).val() == '')
            {
                window.location.href ="{{ url('member/history_penggunaan_vip') }}";
            }
            else
            {
                window.location.href ="{{ url('member/history_penggunaan_vip') }}?filter_balance="+$(this).val();

            }
        });
    });
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop