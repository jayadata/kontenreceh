@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')

@include('member.partials.modal')

<section class="profile">
    <div class="container">
        <div class="row sg">
            @include('member.partials.sidebar')

            <div class="col-md-8">
                @include('member_new_design.partial.notifdatauser')
                
                @include('member_new_design.partial.flash_point')

                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Status Pembelian VIP</strong></h4>
                        <hr> 
                        <div class="mt-md-4 mt-0">                          
                            <div class="hadiah-wrapper">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab">Pending</a></li>
                                    <li role="presentation"><a href="#selesai" aria-controls="selesai" role="tab" data-toggle="tab">Selesai</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content hadiahpanel">
                                    <!-- ooooooooooooooooooooooooooo
                                    PENDING
                                    oooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane active" id="pending">
                                        <div class="pt-4 pb-3">
                                            
                                            {{-- WRAPPER STATITS --}}
                                            <div class="sts-trf">
                                                
                                                {{-- HEAD --}}
                                                <div class="sts-trf-head">
                                                    <div class="row sg">
                                                        <div class="col-xs-3">
                                                            <strong class="text-14 text-xs-11">TANGGAL</strong>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <strong class="text-14 text-xs-11">TRANSAKSI</strong>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <strong class="text-14 text-xs-11">INVOICE#</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                @foreach($pending as $data)
                                                    <?php
                                                        $object=$data->veritrans_callback;
                                                        $va=json_decode($object);
                                                        $va_number = $va->va_numbers;
                                                    ?>
                                                    <div class="sts-trf-item">
                                                        <!-- content Loop here. -->
                                                        <div class="row sg">
                                                            <div class="col-xs-3">
                                                                {{ $data->created_at->format('d-m-Y') }}
                                                            </div>
                                                            <div class="col-xs-3">
                                                                Pembelian VIP: 
                                                                <?php $val = ($data->value)/10; ?>
                                                                {{ round($val) }}
                                                            </div>
                                                            <div class="col-xs-3">
                                                                {{ $data->order_id }}
                                                            </div>
                                                            <div class="col-xs-3 text-right">
                                                                <a href="javascript:;" class="toggler text-dark pr-2" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div> <!-- .row.sg -->

                                                        <!-- Detail -->
                                                        <div class="toggle-box">
                                                            <div class="row sg">
                                                                <div class="col-md-6">
                                                                    <h4 class="mb-4">Detail Pembayaran</h4>
                                                                    
                                                                    <table class="rounded user-games table-responsive text-gray2">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Transfer ke</td>
                                                                                @foreach($va_number as $row)
                                                                                    <td>
                                                                                        <strong>
                                                                                            {{ strtoupper($row->bank) }}
                                                                                        </strong>
                                                                                    </td>
                                                                                @endforeach
                                                                            </tr>
                                                                            <tr>
                                                                                <td>No. Virtual Account</td>
                                                                                @foreach($va_number as $row)
                                                                                    <td>
                                                                                        <strong>
                                                                                            {{ $row->va_number }}
                                                                                        </strong>
                                                                                    </td>
                                                                                @endforeach
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Nama Perusahaan / Produk</td>
                                                                                <td><strong>PT. Jayadata Indonesia</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Jumlah Transfer</td>
                                                                                <td>
                                                                                    <strong>Rp. {{ number_format($data->amount, 0) }}
                                                                                    </strong>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="row sg mb-2">
                                                                        <div class="col-md-2 col-xs-2 pr-0">
                                                                            <img class="info-icon" src="<?php echo StaticAsset('assets/frontend/img/icon_infoBlue.png'); ?>" alt="">
                                                                        </div>
                                                                        <div class="col-md-10 col-xs-10 pl-0">
                                                                            <h4 class="mt-0">Informasi Penting</h4>
                                                                            <hr class="my-3">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row sg">
                                                                        <div class="col-md-2 col-xs-2 pr-0">
                                                                            &nbsp;
                                                                        </div>
                                                                        <div class="col-md-10 col-xs-10 pl-0">
                                                                            <ul class="noliststyle p-0 m-0 infopenting">
                                                                                <li>Order ini akan <strong>expired pada 

                                                                                <?php 
                                                                                    $text = substr($data->transaction_time,0,16);
                                                                                    
                                                                                    $text = str_replace('', '_', $text);

                                                                                    //echo $text;
                                                                                    //date("d-M-Y H:i", strtotime($data->created_at."+1 day"))
                                                                                ?>   
                                                                                
                                                                                {{-- {{ $data->created_at->format('d-M-Y H:i') }} --}}
                                                                                {{ date("d-M-Y H:i", strtotime($data->created_at."+1 day")) }}

                                                                                {{-- 11-01-2018 02:00:00 --}}</strong></li>

                                                                                <li>Jika anda telah melakukan pembayaran, secara otomatis transaksi ini akan berubah menjadi <strong>"Selesai"</strong> tanpa perlu konfirmasi dari anda</li>

                                                                                <li>Untuk membatalkan transaksi ini cukup dengan tidak melakukan pembayaran</li>
                                                                            </ul>
                                                                            {{-- <a href="#" class="btn btn-warning">Konfirmasi Transfer <i class="fa fa-chevron-right"></i></a> --}}
                                                                        </div>                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!-- .sts-trf-item -->
                                                @endforeach

                                            </div> {{-- .sts-trf --}}

                                        </div> {{-- pt-4 pb-3 --}}
                                    </div>

                                    <!--
                                    SELESAI
                                    oooooooooooooooooooooooooooo -->
                                    <div role="tabpanel" class="tab-pane" id="selesai">
                                        <div class="pt-4 pb-3">
                                            
                                            {{-- WRAPPER STATITS --}}
                                            <div class="sts-trf">
                                                
                                                {{-- HEAD --}}
                                                <div class="sts-trf-head">
                                                    <div class="row sg">
                                                        <div class="col-xs-3">
                                                            <strong class="text-14 text-xs-11">TANGGAL</strong>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <strong class="text-14 text-xs-11">TRANSAKSI</strong>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <strong class="text-14 text-xs-11">INVOICE#</strong>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                @foreach($finish as $data)
                                                    <?php
                                                        $object=$data->veritrans_callback;
                                                        $va=json_decode($object);
                                                        $va_number = $va->va_numbers;
                                                    ?>
                                                    <div class="sts-trf-item">
                                                        <!-- content Loop here. -->
                                                        <div class="row sg">
                                                            <div class="col-xs-3">
                                                                {{ $data->created_at->format('d-m-Y') }}
                                                            </div>
                                                            <div class="col-xs-3">
                                                                Pembelian VIP:
                                                                <?php $val = ($data->value)/10; ?>
                                                                {{ round($val) }}
                                                            </div>
                                                            <div class="col-xs-3">
                                                                {{ $data->order_id }}
                                                            </div>
                                                            <div class="col-xs-3 text-right">
                                                                <a href="javascript:;" class="toggler text-dark pr-2" data-toggle-target="toggle-box">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div> <!-- .row.sg -->

                                                        <!-- Detail -->
                                                        <div class="toggle-box">
                                                            <div class="row sg">
                                                                <div class="col-md-6">
                                                                    <h4 class="mb-4">Detail Pembayaran</h4>
                                                                    
                                                                    <table class="rounded user-games table-responsive text-gray2">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Transfer ke</td>
                                                                                @foreach($va_number as $row)
                                                                                    <td>
                                                                                        <strong>
                                                                                            {{ strtoupper($row->bank) }}
                                                                                        </strong>
                                                                                    </td>
                                                                                @endforeach
                                                                            </tr>
                                                                            <tr>
                                                                                <td>No. Virtual Account</td>

                                                                                @foreach($va_number as $row)
                                                                                    <td>
                                                                                        <strong>
                                                                                            {{ $row->va_number }}
                                                                                        </strong>
                                                                                    </td>
                                                                                @endforeach
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Nama Perusahaan / Produk</td>
                                                                                <td><strong>PT. Jayadata Indonesia</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Jumlah Transfer</td>
                                                                                <td>
                                                                                    <strong>Rp. {{ number_format($data->amount, 0) }}
                                                                                    </strong>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    @if($data->transaction_status == 'settlement')
                                                                        <div class="row sg mb-2">
                                                                            <div class="col-md-2 col-xs-2 pr-0">
                                                                                <img class="info-icon" src="<?php echo StaticAsset('assets/frontend/img/icon_successGreen.png'); ?>" alt="">
                                                                            </div>
                                                                            <div class="col-md-10 col-xs-10 pl-0">
                                                                                <h4 class="mt-0">Pembayaran telah kami terima, VIP telah berhasil kami masukan ke akun anda</h4>

                                                                                <a href="{{ route('cetak_status_transfer', [$data->id]) }}" target="_BLANK" class="btn btn-warning">Download Invoice <i class="fa fa-chevron-right"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                        <div class="row sg mb-2">
                                                                            <div class="col-md-2 col-xs-2 pr-0">
                                                                                <img class="info-icon" src="<?php echo StaticAsset('assets/frontend/img/redeemicon/icon_error.png'); ?>" alt="">
                                                                            </div>
                                                                            <div class="col-md-10 col-xs-10 pl-0">
                                                                                <h4 class="mt-0">Kami telah membatalkan transaksi ini, karena pembayaran tidak kami terima hingga batas waktu yang ditentukan hingga {{ $data->callback_at }}</h4>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> <!-- .sts-trf-item -->
                                                @endforeach
                                            </div> {{-- .sts-trf --}}
                                        </div> {{-- pt-4 pb-3 --}}
                                    </div>

                                </div> <!-- .hadiahpanel -->
                            </div> <!-- .hadiah-wrapper -->                        
                        </div> {{-- .mt-md-4.mt-0 --}}
                    </div> {{-- .card-body --}}
                </div> <!-- .card -->
            </div> <!-- .col-md-9 -->
        </div>
    </div>
</section>

@include('member.partials.footer')
@stop
@section('scripts')
<script>
    jQuery(document).ready(function($){
        $('.filter_balance').change(function(){
            //alert('test');
            if($(this).val() == '')
            {
                window.location.href ="{{ url('member/history_penggunaan_koin') }}";
            }
            else
            {
                window.location.href ="{{ url('member/history_penggunaan_koin') }}?filter_balance="+$(this).val();

            }
        });
    });
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop