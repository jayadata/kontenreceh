@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">

             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Koleksi Cap</strong></h4>
                        <hr>
                    </div>
                </div> <!-- .card -->
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>


@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop