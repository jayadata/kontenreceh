<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Playworld Invoice</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <style>
    .row.sg {
        margin-left: -10px;
        margin-right: -10px;
    }

    .row.sg div[class*="col-"] {
        padding-left: 10px;
        padding-right: 10px;
    }

    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    img {
        max-width: 100%;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:last-of-type {
        text-align: right;
    }
    
    .invoice-box table tr td:only-child {
        text-align: left;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    .border td {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px!important;
    }

    tr.border {
        border:none!important;
    }

    .btn {
        display: inline-block;
        padding: 10px 15px;
        border: 2px solid #656666;
        line-height: 1;
        color: #656666;
        text-decoration: none;
        border-radius: 5px;
        cursor: pointer;
    }

    @media print {
      div.footer {
        position: fixed;
        bottom: 0;
      }
      .cetak {
        display: none
      }
    }
    .kontaklist {
        position: relative;
        margin-top: 30px;
    }

    @media screen and (max-width: 768px) {
        .kontaklist>div {
            margin-bottom: 10px;
        }
        .kontaklist>div:nth-child(1) {
            margin: 0;
        }
        .help_search .row {
            margin-bottom: 0;
        }
        .help_search h3 {
            margin: 0;
        }
        .help_search .col-md-3 img {
            margin: 0 auto;
            max-width: 100%;
            display: block;
        }
        .help_search .aybkb {
            padding-top: 30px;
        }
        .htc_item>.bs-callout {
            margin: 0;
        }
        .htc_item h4 {
            margin: 0;
        }
        .accordion_help,
        .accordion_help p {
            font-size: 13px;
        }
    }

    .kontaklist h2 {
        font-size: 10px;
        margin: 0;
        margin-bottom: 5px;
    }

    .kontaklist h2:last-of-type {
        margin-bottom: 0;
    }

    @media screen and (max-width: 768px) {
        .kontaklist h2 {
            font-size: 16px;
        }
    }

    .kontaklist h2 a {
        color: #7f807f;
    }

   /* @media screen {
      div.footer {
        display: none;
      }
    }*/

    @media screen and (max-width: 414px) {
      .kontaklist h2 a {
        font-size: 13px;
      }
    }

    .kontaklist .left img {
        max-width: 30px;
        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
    }

    .kontaklist span {
        font-size: 12px;
        color: #b1b1b1;
    }

    .kontaklist .left:nth-child(1) {
        float: left;
        margin-right: 10px;
        width: 30px;
    }

    .kontaklist .left:nth-child(2) {
        float: none!important;
        overflow: hidden;
    }

    .hubungi {
        padding-top: 80px;
        padding-bottom: 80px;
        position: relative;
        background-size: 130px;
        overflow: visible!important;
        position: relative;
    }

    .hubungi:before {
        content: "";
        width: 150px;
        height: 150px;
        bottom: -10px;
        left: -70px;
        background: url(../img/operator.png) no-repeat center center;
        background-size: 100%;
        position: absolute;
    }

    .hubkam {
        position: absolute;
        left: 80px;
        top: 10px;
        font-weight: bold;
    }

    .hubkam a {
        color: #fff;
    }

    .hubkam strong {
        font-size: 27px;
        font-family: "ProximaNovaBold", sans-serif
    }

    .hubkam small {
        font-size: 16px;
    }

    .phoneicon {
        display: inline-block;
        width: 30px;
        height: 30px;
        float: left;
        margin-right: 10px;
        vertical-align: top;
    }

    strong.slowresponse {
        font-size: 20px;
        display: block;
        margin-top: 10px;
        line-height: 1;
    }

    .help_isi {
        display: none;
        color: #626363;
        font-size: 16px;
    }

    .help_isi h3 {
        margin-top: 0;
    }

    div .help_isi:nth-child(1) {
        display: block;
    }

    .detailinfo {
        margin-bottom: 30px;
    }

    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table valign="middle">
                        <tr>
                            <td class="title">
                                <img src="https://playworld.id/assets/frontend/img/smalllogo.png" style="width:100%; max-width:200px;">
                            </td>
                            <td valign="middle">
                                <br><span href="#" class="btn cetak" onclick="cetak()">Cetak</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <h2>INVOICE</h2>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td width="100">
                                Nomor
                            </td>
                            <td>
                                <strong>{{ $finish->order_id }}</strong>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr class="border">
                            <td>
                                Tanggal
                            </td>
                            <td>
                                <strong>{{ $finish->created_at->format('d-M-Y H:i') }}</strong>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <?php $name=substr($customer->msisdn, 0, 8).'XXXX';?>
                                <strong>Invoiced To</strong><br>
                                {{strtoupper(($customer->first_name) != '' ? $customer->first_name : 'NONE')}}<br>
                                MSISDN: {{  $customer->msisdn }}
                            </td>
                            
                            <td>
                                <strong>Status</strong><br>
                                <strong style="color: #3ab54a"> Sudah dibayar</strong>
                            </td>
                        </tr>
                        <tr>
                            <?php
                                $object=$finish->veritrans_callback;
                                $va=json_decode($object);
                                $va_number = $va->va_numbers;
                            ?>

                            @foreach($va_number as $row)
                                <td>
                                    <strong>Pay To</strong><br>
                                    Playworld<br>
                                    BCA VA. {{ $row->va_number }}
                                </td>
                            @endforeach
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td width="300">
                    Nama
                </td>
                
                <td>
                    Jumlah
                </td>
                
                <td>
                    Extra
                </td>
                
                <td>
                    Subtotal
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Pembelian Koin
                </td>
                
                <td>
                    {{ $finish->value }}
                </td>
                
                <td>
                    0
                </td>
                
                <td>
                    Rp. {{ number_format($finish->amount, 0) }}
                </td>
            </tr>
            
            <tr class="total">
                <td>TOTAL</td>
                <td></td>
                <td></td>
                
                <td>
                   <strong>Rp. {{ number_format($finish->amount, 0) }}</strong>
                </td>
            </tr>
        </table>
    </div>

    <div class="footer mt-5">
        <div class="container">
                <div class="row sg">
                    <div class="col-md-2 col-xs-4 show-for-large">
                        <img src="https://playworld.id/assets/frontend/img/playworld-logo-dark.png" alt="" width="140">
                    </div>
                    <div class="col-md-10 col-xs-12 helpsearch pl-3 pl-md-5">
                        <div class="row kontaklist mt-0 sg">
                            <div class="col-md-3 col-xs-6">
                                <div class="left">
                                    <img src="https://playworld.id/assets/frontend/img/bantuan/cs.png" alt="">
                                </div>
                                <div class="left">
                                    <h2><strong><a href="tel:02122457232">021 - 22457232</a></strong></h2>
                                    <span>Hari Kerja 09.00 - 18.00</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="left">
                                    <img src="https://playworld.id/assets/frontend/img/bantuan/sms.png" alt="">
                                </div>
                                <div class="left">
                                    <h2><strong><a href="tel:0818303029">0818-30-30-29 (XL)</a></strong></h2>
                                    <h2><strong><a href="tel:081519060929">0815-190-609-29 (ISAT)</a></strong></h2>
                                    <span>Whatsapp / SMS</span>
                                </div>
                            </div>
                            <div class="clearfix hide-for-large"></div>
                            <div class="col-md-3 col-xs-6">
                                <div class="left">
                                    <img src="https://playworld.id/assets/frontend/img/bantuan/smartphone.png" alt="">
                                </div>
                                <div class="left">
                                    <h2><strong><a href="tel:0818303029">0818-30-30-29 (XL)</a></strong></h2>
                                    <h2><strong><a href="tel:081519060929">0815-190-609-29 (ISAT)</a></strong></h2>
                                    <span>24 jam slow response</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="left">
                                    <img src="https://playworld.id/assets/frontend/img/bantuan/email.png" alt="">
                                </div>
                                <div class="left">
                                    <h2><strong><a href="mailto:halo@playworld.id">halo@playworld.id</a></strong></h2>
                                    <span>Email Official</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-4 col-xs-12  hubungi">
                        <div class="hubkam">
                            HUBUNGI KAMI<br>
                            <strong><a href="tel:02122457232">021 - 22457232</a></strong><br>
                            <small>Senin s/d Jumat 09:00 - 18:00</small> <br>
                            <strong class="slowresponse"><a href="tel:087885157347">0818 3030 29</a><br><small>24 Jam / Slow Response</small></strong>
                        </div>
                    </div> -->
                </div>
        </div>
    </div>

    <script>
        function cetak() {
            window.print();
        }
    </script>
</body>
</html>