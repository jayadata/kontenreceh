@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')


<section class="profile">
    <div class="container">
        <div class="row sg">

             @include('member.partials.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Koleksi Cap</strong></h4>
                        <hr>
                                        <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                PEMBELIAN KOIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card">
                    <div class="card-body">
                        <div class="hadiah-wrapper">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li role="presentation" class="active"><a href="#denganpulsa" aria-controls="denganpulsa" role="tab" data-toggle="tab">Dengan Pulsa</a></li>
                                <li role="presentation"><a href="#dengantransfer" aria-controls="dengantransfer" role="tab" data-toggle="tab">Dengan Transfer &amp; Kartu</a></li>
                                <li role="presentation"><a href="#transferkoin" aria-controls="transferkoin" role="tab" data-toggle="tab">Transfer Koin</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content hadiahpanel">
                                <div role="tabpanel" class="tab-pane active" id="denganpulsa">
                                    <div class="pt-3 pb-3">
                                        <form action="#">
                                            <div class="left mr-5">
                                                Pilih Opertator
                                            </div>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="xlaxis">
                                                XL & Axis
                                            </label>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="indosat">
                                                Indosat
                                            </label>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="telkomsel">
                                                Telkomsel
                                            </label>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="smartfren">
                                                Smartfren
                                            </label>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>

                                    {{-- CICILAN SMS --}}
                                    <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                        Cicilan Per SMS
                                    </div>
                                    <hr>
                                    <div class="hadiah is_hemat mb-5">
                                        <div class="row sg">
                                            <div class="col-md-2">
                                                <a href="#">
                                                    <img src="http://placehold.it/100x100" alt="" class="show">
                                                </a>
                                            </div>
                                            <div class="col-md-10 text-medium">
                                                <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                <div class="hadiah-detail mt-2">
                                                    <strong>Rp. 6.600 per minggu</strong> <br>
                                                    Cicilan per SMS <br>
                                                    3 sms/minggu inc. ppn 10% <br>
                                                    1 sms = 25 Koin = Rp. 2.200
                                                </div>
                                                <div class="mt-2">
                                                    <div class="row sg">
                                                        <div class="col-md-3">
                                                            <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{-- end hadiah --}}

                                    {{-- BELI PUTUS --}}
                                    <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                        Beli Putus
                                    </div>

                                    <div class="row sg equalizeplis mt-3">
                                        <div class="col-md-4">
                                            <div class="hadiah is_hemat">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="dengantransfer">
                                    {{-- hadiah --}}
                                    <div class="hadiah">
                                        <div class="row sg">
                                            <div class="col-md-2">
                                                <a href="#">
                                                    <img src="http://placehold.it/100x100" alt="" class="show">
                                                </a>
                                            </div>
                                            <div class="col-md-10 text-medium">
                                                <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                <div class="jenis text-medium text-gray">
                                                    HTP1709050004 | Non Fisik
                                                </div>
                                                <div class="hadiah-detail mt-2">
                                                    <div class="row sg mb-2">
                                                        <div class="col-md-2">
                                                            Tanggal
                                                        </div>
                                                        <div class="col-md-10">
                                                            : <strong>11 November 2017</strong>
                                                        </div>
                                                    </div>
                                                    <div class="row sg mb-2">
                                                        <div class="col-md-2">
                                                            Koin ditukar
                                                        </div>
                                                        <div class="col-md-10">
                                                            : <strong>20</strong>
                                                        </div>
                                                    </div>
                                                    <div class="row sg">
                                                        <div class="col-md-2">
                                                            Poin ditukar
                                                        </div>
                                                        <div class="col-md-10">
                                                            : <strong>100</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mt-2">
                                                    <div class="row sg">
                                                        <div class="col-md-6">
                                                            <a href="javascript:;" class="btn btn-dead text-white text-small">05 Sep 2017 14:42:06 - Sedang Diproses</a>
                                                        </div>
                                                        <div class="col-md-6 text-md-right">
                                                            <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2" data-toggle-target="detail">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="toggle-box">
                                            <hr class="mt-3">
                                            <div class="row sg mt-3">
                                                <div class="col-md-3">
                                                    <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                </div>
                                                <div class="col-md-9 text-small">
                                                    <div class="btn btn-link text-dark">Mendapatkan barang</div>
                                                </div>
                                            </div><hr class="mt-3">
                                            <div class="row sg mt-3">
                                                <div class="col-md-3">
                                                    <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                </div>
                                                <div class="col-md-9 text-small">
                                                    <div class="btn btn-link text-dark">Masukkan Nomor Seluler</div>
                                                </div>
                                            </div><hr class="mt-3">
                                            <div class="row sg mt-3">
                                                <div class="col-md-3">
                                                    <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                </div>
                                                <div class="col-md-9 text-small">
                                                    <div class="btn btn-link text-dark">Sedang diproses</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{-- end hadiah --}}
                                </div>
                                <div role="tabpanel" class="tab-pane" id="transferkoin">
                                    {{-- hadiah --}}
                                    <div class="hadiah">
                                        <div class="row sg">
                                            <div class="col-md-2">
                                                <a href="#">
                                                    <img src="http://placehold.it/100x100" alt="" class="show">
                                                </a>
                                            </div>
                                            <div class="col-md-10 text-medium">
                                                <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                <div class="jenis text-medium text-gray">
                                                    HTP1709050004 | Non Fisik
                                                </div>
                                                <div class="hadiah-detail mt-2">
                                                    <div class="row sg mb-2">
                                                        <div class="col-md-2">
                                                            Tanggal
                                                        </div>
                                                        <div class="col-md-10">
                                                            : <strong>11 November 2017</strong>
                                                        </div>
                                                    </div>
                                                    <div class="row sg mb-2">
                                                        <div class="col-md-2">
                                                            Koin ditukar
                                                        </div>
                                                        <div class="col-md-10">
                                                            : <strong>20</strong>
                                                        </div>
                                                    </div>
                                                    <div class="row sg">
                                                        <div class="col-md-2">
                                                            Poin ditukar
                                                        </div>
                                                        <div class="col-md-10">
                                                            : <strong>100</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mt-2">
                                                    <div class="row sg">
                                                        <div class="col-md-6">
                                                            <a href="javascript:;" class="btn btn-dead text-white text-small">05 Sep 2017 14:42:06 - Sedang Diproses</a>
                                                        </div>
                                                        <div class="col-md-6 text-md-right">
                                                            <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2" data-toggle-target="detail">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="toggle-box">
                                            <hr class="mt-3">
                                            <div class="row sg mt-3">
                                                <div class="col-md-3">
                                                    <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                </div>
                                                <div class="col-md-9 text-small">
                                                    <div class="btn btn-link text-dark">Mendapatkan barang</div>
                                                </div>
                                            </div><hr class="mt-3">
                                            <div class="row sg mt-3">
                                                <div class="col-md-3">
                                                    <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                </div>
                                                <div class="col-md-9 text-small">
                                                    <div class="btn btn-link text-dark">Masukkan Nomor Seluler</div>
                                                </div>
                                            </div><hr class="mt-3">
                                            <div class="row sg mt-3">
                                                <div class="col-md-3">
                                                    <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                </div>
                                                <div class="col-md-9 text-small">
                                                    <div class="btn btn-link text-dark">Sedang diproses</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{-- end hadiah --}}
                                </div>
                            </div> <!-- .hadiahpanel -->

                        </div>
                    </div>
                </div>
                    </div>


                </div> <!-- .card -->
            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>


@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop