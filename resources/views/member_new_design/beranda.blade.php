@extends('frontend.layouts.default')
@section('title')
Playworld Member
@stop

@section('stylesheets')
{{-- some text goes here --}}
<style type="text/css">
    .ahmad
    {
        cursor: pointer;
    }
</style>
<!--Start of Zendesk Chat Script-->
{{-- <script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4oVqlVfrK7ChfpfbeTXBuvStr2U2EXrh";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script> --}}
<!--End of Zendesk Chat Script-->

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9651550;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>

@stop
@section('content')

<!-- Section -->
<section class="profile">
    <div class="container">
        <div class="row sg">

            @include('member_new_design.partial.sidebar')

            {{-- CONTENT --}}
            <div class="col-md-8">
                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                BARANG YANG DIDAPATKAN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Barang Yang Didapatkan</strong></h4>
                        <hr>
                        <div class="mt-5">
                            <div class="hadiah-wrapper">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#tabwait" aria-controls="tabwait" role="tab" data-toggle="tab">Menunggu Proses</a></li>
                                    <li role="presentation"><a href="#tabonproses" aria-controls="tabonproses" role="tab" data-toggle="tab">Sedang Diproses</a></li>
                                    <li role="presentation"><a href="#tabkonfirm" aria-controls="tabkonfirm" role="tab" data-toggle="tab">Konfirmasi Penerimaan</a></li>
                                    <li role="presentation"><a href="#tabditerima" aria-controls="tabditerima" role="tab" data-toggle="tab">Daftar Barang Diterima</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content hadiahpanel">
                                    <div role="tabpanel" class="tab-pane active" id="tabwait">

                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        HTP1709050004 | Non Fisik
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-3">
                                                                <select name="" id="" class="form-control">
                                                                    <option value="081218312677">081218312677</option>
                                                                    <option value="081218312677">081218312677</option>
                                                                    <option value="081218312677">081218312677</option>
                                                                    <option value="081218312677">081218312677</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <a href="#" class="btn btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Masukkan Nomor Handphone</a>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <a href="#" class="btn btn-success text-white">Buat Baru</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}

                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Original Adidas Sepatu Running Energy</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        HTP1709050004 | Fisik - Delivery
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-3">
                                                                <select name="" id="" class="form-control">
                                                                    <option value="nan">Rumah Jakarta</option>
                                                                    <option value="nan">Rumah Bandung</option>
                                                                    <option value="nan">RUmah Malaysia</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <a href="#" class="btn btn-warning btn-block text-white">Masukkan Alamat</a>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <a href="#" class="btn btn-success text-white">Buat Baru</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}

                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Shiny Tea - Gift Voucher Rp 25.000,-</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        HTP1709050004 | Fisik - On Merchant
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-3">
                                                                <a href="#" class="btn btn-warning text-white">Staff Redeem</a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div> {{-- end hadiah --}}

                                        {{-- Pagination --}}
                                        <div class="mb-3">
                                            <ul class="pagination right"><li class="disabled"><span>«</span></li> <li class="active"><span>1</span></li><li><a href="https://playworld.id/tukarpoin?page=2">2</a></li><li><a href="https://playworld.id/tukarpoin?page=3">3</a></li><li><a href="https://playworld.id/tukarpoin?page=4">4</a></li><li><a href="https://playworld.id/tukarpoin?page=5">5</a></li> <li><a href="https://playworld.id/tukarpoin?page=2" rel="next">»</a></li></ul> {{-- end pagination --}}

                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="tabonproses">
                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        HTP1709050004 | Non Fisik
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6">
                                                                <a href="javascript:;" class="btn btn-dead text-white text-small">05 Sep 2017 14:42:06 - Sedang Diproses</a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2" data-toggle-target="detail">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Mendapatkan barang</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Masukkan Nomor Seluler</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Sedang diproses</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="tabkonfirm">
                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        HTP1709050004 | Non Fisik
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6">
                                                                <a href="javascript:;" class="btn btn-dead text-white text-small">05 Sep 2017 14:42:06 - Sedang Diproses</a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2" data-toggle-target="detail">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Mendapatkan barang</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Masukkan Nomor Seluler</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Sedang diproses</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="tabditerima">
                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="jenis text-medium text-gray">
                                                        HTP1709050004 | Non Fisik
                                                    </div>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-2">
                                                        <div class="row sg">
                                                            <div class="col-md-6">
                                                                <a href="javascript:;" class="btn btn-dead text-white text-small">05 Sep 2017 14:42:06 - Sedang Diproses</a>
                                                            </div>
                                                            <div class="col-md-6 text-md-right">
                                                                <a href="javascript:;" class="toggler btn btn-link text-medium text-dark pr-2" data-toggle-target="detail">Tampilkan</a><i class='fa fa-chevron-down'></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="toggle-box">
                                                <hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Mendapatkan barang</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Masukkan Nomor Seluler</div>
                                                    </div>
                                                </div><hr class="mt-3">
                                                <div class="row sg mt-3">
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-block btn-dead text-small">05 Sep 2017 14:42:06</a>
                                                    </div>
                                                    <div class="col-md-9 text-small">
                                                        <div class="btn btn-link text-dark">Sedang diproses</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}
                                    </div>
                                </div> <!-- .hadiahpanel -->

                            </div>
                        </div>
                    </div>
                </div> {{-- .card --}}

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                STATUS BARANG INCARAN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Status Barang Incaran</strong></h4>
                        <hr>
                        <div class="mt-5">
                            <div class="hadiah-wrapper">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#statusincaran" aria-controls="statusincaran" role="tab" data-toggle="tab">Status Barang Incaran</a></li>
                                    <li role="presentation"><a href="#sudahselesai" aria-controls="sudahselesai" role="tab" data-toggle="tab">Sudah Selesai</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content hadiahpanel">
                                    <div role="tabpanel" class="tab-pane active" id="statusincaran">
                                        
                                        @for ($i = 0; $i < 3; $i++)
                                            {{-- hadiah --}}
                                            <div class="hadiah">
                                                <a href="#" class="h_chevron"><i class="fa fa-chevron-right"></i></a>
                                                <div class="row sg">
                                                    <div class="col-md-2">
                                                        <a href="#">
                                                            <img src="http://placehold.it/100x100" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-10 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <div class="row sg mb-2">
                                                                <div class="col-md-2">
                                                                    Peringkat
                                                                </div>
                                                                <div class="col-md-10">
                                                                    : <strong>3</strong>
                                                                </div>
                                                            </div>
                                                            <div class="row sg mb-2">
                                                                <div class="col-md-2">
                                                                    Tanggal
                                                                </div>
                                                                <div class="col-md-10">
                                                                    : <strong>11 November 2017</strong>
                                                                </div>
                                                            </div>
                                                            <div class="row sg mb-2">
                                                                <div class="col-md-2">
                                                                    Koin ditukar
                                                                </div>
                                                                <div class="col-md-10">
                                                                    : <strong>20</strong>
                                                                </div>
                                                            </div>
                                                            <div class="row sg">
                                                                <div class="col-md-2">
                                                                    Poin ditukar
                                                                </div>
                                                                <div class="col-md-10">
                                                                    : <strong>100</strong>
                                                                </div>
                                                            </div>
                                                        </div> <!-- //.hadiah-detail -->
                                                    </div>
                                                </div>
                                            </div>{{-- end hadiah --}}
                                        @endfor                                            

                                        {{-- Pagination --}}
                                        <div class="mb-3">
                                            <ul class="pagination right"><li class="disabled"><span>«</span></li> <li class="active"><span>1</span></li><li><a href="https://playworld.id/tukarpoin?page=2">2</a></li><li><a href="https://playworld.id/tukarpoin?page=3">3</a></li><li><a href="https://playworld.id/tukarpoin?page=4">4</a></li><li><a href="https://playworld.id/tukarpoin?page=5">5</a></li> <li><a href="https://playworld.id/tukarpoin?page=2" rel="next">»</a></li></ul> {{-- end pagination --}}

                                            <div class="clearfix"></div>
                                        </div>

                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="sudahselesai">
                                        
                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <a href="#" class="h_chevron"><i class="fa fa-chevron-right"></i></a>
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Status
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>Menang</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div> <!-- //.hadiah-detail -->
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}

                                        {{-- hadiah --}}
                                        <div class="hadiah">
                                            <a href="#" class="h_chevron"><i class="fa fa-chevron-right"></i></a>
                                            <div class="row sg">
                                                <div class="col-md-2">
                                                    <a href="#">
                                                        <img src="http://placehold.it/100x100" alt="" class="show">
                                                    </a>
                                                </div>
                                                <div class="col-md-10 text-medium">
                                                    <h3 class="mt-0"><strong><a href="javascript:;">Voucher Pulsa Semua Operator Rp. 10.000</a></strong></h3>
                                                    <div class="hadiah-detail mt-2">
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Status
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>Kalah</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Tanggal
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>11 November 2017</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg mb-2">
                                                            <div class="col-md-2">
                                                                Koin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>20</strong>
                                                            </div>
                                                        </div>
                                                        <div class="row sg">
                                                            <div class="col-md-2">
                                                                Poin ditukar
                                                            </div>
                                                            <div class="col-md-10">
                                                                : <strong>100</strong>
                                                            </div>
                                                        </div>
                                                    </div> <!-- //.hadiah-detail -->
                                                </div>
                                            </div>
                                        </div>{{-- end hadiah --}}

                                    </div>
                                </div> <!-- .hadiahpanel -->

                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                HISTORI PENUKARAN POIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Histori Penggunaan Poin</strong></h4>
                        <hr>
                        <div class="my-games">
                             <div class="profile-header-total clearfix">
                                <div class="left">
                                    <select name="filter_poin" class="form-control filter_poin">
                                       <option value="">All</option>
                                       <option value="dapat" {{(Request::get('filter_poin') == 'dapat')?'selected="selected"':''}}>Dapat</option>
                                       <option value="tukar" {{(Request::get('filter_poin') == 'tukar')?'selected="selected"':''}}>Tukar</option>
                                   </select>
                                </div>
                                <div class="right">
                                    <strong>{{number_format($point[0]->point)}}/{{number_format($point[0]->point_awal)}}</strong>
                                </div>
                            </div>

                            <div class="profile-content p-0">
                                <table class="user-games table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="30">No</th>
                                            <th width="60">Tanggal</th>
                                            <th width="100">Activity</th>
                                            <th width="100">Tipe</th>
                                            <th>Detail</th>
                                            <th width="80">Koin</th>
                                            <th width="80">Poin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i = 1;
                                        ?>
                                        @foreach($my_point as $row)
                                        <tr>
                                            <td align="center">{{$i++}}</td>
                                            <td>{{ $row->created_at->format('d/m/Y') }}</td>
                                            <td>{{$row->activity}}</td>
                                            <td>{{$row->status}}</td>
                                            <td>
                                                @if($row->news_id != '')
                                                On Article - {{substr($row->news->title, 0, 25)}}...
                                                @else 
                                                    @if($row->status == 'tukar' && $row->arisan_id != '')
                                                        Tukar poin on - <a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->arisan->title) }}"> {{$row->arisan->title}}</a>
                                                    @else 
                                                        Tukar poin on - {{$row->keterangan}}
                                                    @endif
                                                @endif
                                            </td>
                                            <td align="right"> {{($row->koint == 0)?'':'-'}}{{number_format($row->koint)}}</td>
                                            <td align="right">{{($row->status == 'tukar')?'-':''}} {{ number_format($row->point) }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- . card -->

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                HISTORI PENUKARAN KOIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="mb-4"><strong>Histori Penggunaan Koin</strong></h4>
                        <hr>
                        <div class="my-games">
                            <div class="profile-header-total clearfix">
                                <div class="left">
                                    <select name="filter_balance" id="" class="form-control filter_balance">
                                        <option value="">All</option>
                                        <option value="beli" {{(Request::get('filter_balance') == 'beli')?'selected="selected':''}}>Beli</option>
                                        <option value="transfer" {{(Request::get('filter_balance') == 'transfer')?'selected="selected':''}}>Transfer</option>
                                        <option value="terima" {{(Request::get('filter_balance') == 'terima')?'selected="selected':''}}>Terima</option>
                                        <option value="tukar" {{(Request::get('filter_balance') == 'tukar')?'selected="selected':''}}>Tukar</option>
                                    </select>
                                </div>
                                <div class="right">
                                    <strong>{{number_format($balance[0]->sisa)}}/{{ number_format($balance[0]->total_balance) }}</strong>
                                </div>
                            </div>

                            <div class="profile-content">
                                <table class="user-games table-responsive">
                                    <thead>
                                        <tr>
                                            <th width="30">No</th>
                                            <th width="60">Tanggal</th>
                                            <th width="100">TRX Code</th>
                                            <th>Type</th>
                                            <th>Detail</th>
                                            <th width="80">Koin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $i = 1;
                                    ?>

                                        @foreach ($my_balance as $row)

                                            <tr>
                                                <td align="center">{{$i++}}</td>
                                                <td>{{$row->created_at->format('d/m/Y')}}</td>
                                                <td>{{($row->balance_code != '')?$row->balance_code:'-'}}</td>
                                                <td>{{$row->type}}</td>
                                                <td>
                                                @if($row->quiz_id != '')
                                                    @if($row->quiz->news->is_playgol == 0)
                                                    Mengikuti Quiz : <a href="{{ url('news/view', $row->quiz->news_id).'/'.slug($row->quiz->news->title) }}" >{{$row->quiz->news->title}}</a>
                                                    @else 
                                                    Mengikuti Quiz : <a href="{{ url('playgoool/view', $row->quiz->news_id).'/'.slug($row->quiz->news->title) }}" >{{$row->quiz->news->title}}</a>
                                                    @endif
                                                @elseif($row->arisan_id != '')
                                                Mengikuti Tukar Poin : <a href="{{ url('tukarpoin', $row->arisan_id).'/'.slug($row->arisan->title) }}">{{$row->arisan->title}}</a> 
                                                @else
                                                {{$row->keterangan}}
                                                @endif
                                                </td>
                                                <td align="right"> {{($row->type == 'tukar' || $row->type == 'transfer')?'-':''}} {{number_format($row->nominal)}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->

                <!-- oooooooooooooooooooooooooooooooooooooooooooooo
                PEMBELIAN KOIN
                ooooooooooooooooooooooooooooooooooooooooooooooo -->
                <div class="card">
                    <div class="card-body">
                        <div class="hadiah-wrapper">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li role="presentation" class="active"><a href="#denganpulsa" aria-controls="denganpulsa" role="tab" data-toggle="tab">Dengan Pulsa</a></li>
                                <li role="presentation"><a href="#dengantransfer" aria-controls="dengantransfer" role="tab" data-toggle="tab">Dengan Transfer &amp; Kartu</a></li>
                                <li role="presentation"><a href="#transferkoin" aria-controls="transferkoin" role="tab" data-toggle="tab">Transfer Koin</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content hadiahpanel">
                                <div role="tabpanel" class="tab-pane active" id="denganpulsa">
                                    <div class="pt-3 pb-3">
                                        <form action="#">
                                            <div class="left mr-5">
                                                Pilih Operator
                                            </div>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="xlaxis" checked>
                                                XL & Axis
                                            </label>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="indosat">
                                                Indosat
                                            </label>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="telkomsel">
                                                Telkomsel
                                            </label>
                                            <label class="left mr-5">
                                                <input type="radio" name="operator" value="smartfren">
                                                Smartfren
                                            </label>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>

                                    {{-- CICILAN SMS --}}
                                    <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                        Cicilan Per SMS
                                    </div>
                                    <hr>
                                    <div class="hadiah is_hemat mb-5">
                                        <div class="row sg">
                                            <div class="col-md-2">
                                                <a href="#">
                                                    <img src="http://placehold.it/100x100" alt="" class="show">
                                                </a>
                                            </div>
                                            <div class="col-md-10 text-medium">
                                                <h3 class="mt-0"><strong><a href="javascript:;">75 KOIN</a></strong></h3>
                                                <div class="hadiah-detail mt-2">
                                                    <strong>Rp. 6.600 per minggu</strong> <br>
                                                    Cicilan per SMS <br>
                                                    3 sms/minggu inc. ppn 10% <br>
                                                    1 sms = 25 Koin = Rp. 2.200
                                                </div>
                                                <div class="mt-2">
                                                    <div class="row sg">
                                                        <div class="col-md-3">
                                                            <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>{{-- end hadiah --}}

                                    {{-- BELI PUTUS --}}
                                    <div class="pt-4 pr-3 pb-4 pl-3 bg-gray">
                                        Beli Putus
                                    </div>

                                    <div class="row sg equalizeplis mt-3">
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="{{ StaticAsset('assets/frontend/img/coin-2.png') }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">20 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="{{ StaticAsset('assets/frontend/img/coin-2.png') }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">30 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="{{ StaticAsset('assets/frontend/img/coins-stack.png') }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">50 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="{{ StaticAsset('assets/frontend/img/coins-stack.png') }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">80 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah is_hemat">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="{{ StaticAsset('assets/frontend/img/rich.png') }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">100+10 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="hadiah is_hemat">
                                                <div class="row sg">
                                                    <div class="col-md-4">
                                                        <a href="#">
                                                            <img src="{{ StaticAsset('assets/frontend/img/rich.png') }}" alt="" class="show">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-8 text-medium">
                                                        <h3 class="mt-0"><strong><a href="javascript:;">150+15 KOIN</a></strong></h3>
                                                        <div class="hadiah-detail mt-2">
                                                            <strong>Rp. 6.600 per minggu</strong> <br>
                                                            inc. ppn 10% <br>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="row sg">
                                                                <div class="col-md-12">
                                                                    <a href="#" class="btn btn-block btn-warning text-white" data-toggle="modal" data-target="#konfirmasi">Beli</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="dengantransfer">
                                    <h1>Dengan Kartu</h1>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="transferkoin">
                                    <div class="tambahpoin_widget hargakoin">
                                        <div class="widget-head trd">
                                            <h2>TRANSFER KOIN</h2>
                                        </div>
                                        <div class="login-tab-head notab">
                                            <a href="javascript;" class="active">Sisa KOIN</a>
                                            <a href="javascript;" class="active text-right">{{nomor_cantik($balance[0]->sisa)}}</a>
                                        </div>
                                        <div class="tab-contents-holder pb-5 pt-4">
                                            <!-- ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                            LIST HARGA
                                            oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo -->
                                            <div class=" smlbtnpls tch denganpulsa p-0">
                                                <!-- List harga -->
                                                <div class="clearfix text-left p-0">
                                                    MSISDN TUJUAN <br>
                                                    <input name="msisdn_tujuan" type="text" placeholder="Misal: 363787859" class="form-control msisdn_tujuan">
                                                    <div class="clearfix"></div><br>
                                                    JUMLAH TRANSFER <br>
                                                    <input name="jumlah_transfer" type="text" placeholder="Misal: 2000" class="form-control jumlah_transfer">
                                                    <div class="clear"></div> <br>
                                                </div>

                                                <div class="clearfix">
                                                    <a class="btn btn-success btn-block btn-lg transfer_sekarang">TRANSFER SEKARANG</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- .end transfer koin -->
                                </div>
                            </div> <!-- .hadiahpanel -->

                        </div>
                    </div>
                </div>

            </div> {{-- end CONTENT --}}

        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="konfirmasiLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-center" id="konfirmasiLabel"><strong>Voucher Pulsa Semua Operator Rp. 10.000</strong></h4>
            </div>
            <div class="modal-body text-center">
                Apakah nomor handphone ini telah benar ?
                <h4>081218312677</h4>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-success" data-dismiss="modal">Ya Benar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Salah</button>
            </div>
        </div>
    </div>
</div>

@include('member.partials.modal')


@include('member.partials.footer')
@stop
@section('scripts')
<script>
jQuery(document).ready(function($){
    $('.download_link').click(function(event) {
        event.preventDefault();
        var r = confirm("Untuk bisa mendownload game anda harus melengkapi data profile anda, lengkapi sekarang!");
        if (r == true) {
            window.location.href = "{{ url('/member/profile') }}";
        } else {
            return false;
        }
    });

    var counter = 60;
    var t = 1;
    var tr = '';
    //cdreset();
    var page_like_or_unlike_callback = function(url, html_element) {
            //alert("page_like_or_unlike_callback");
            $("#konfirmasi").modal('toggle');
            $.post("{{url('member/like_content')}}",
                {
                    id: {{$user['id']}},
                },
                function(data, status){
                    console.log("Data: " + data + "\nStatus: " + status);
                });
            //location.reload();
            setTimeout(function(){
                window.location.reload();
            }, 500)
    }

    $('.filter').change(function()
     {
        //alert('test');
        if($(this).val() == '')
        {
            window.location.href ="{{ url('member/dashboard') }}";
        }
        else
        {
            window.location.href ="{{ url('member/dashboard') }}?filter="+$(this).val();

        }
    });
    $('.filter_balance').change(function(){
        //alert('test');
        if($(this).val() == '')
        {
            window.location.href ="{{ url('member/dashboard') }}";
        }
        else
        {
            window.location.href ="{{ url('member/dashboard') }}?filter_balance="+$(this).val();

        }
    });
    $('.filter_poin').change(function(){
        //alert('test');
        if($(this).val() == '')
        {
            window.location.href ="{{ url('member/dashboard') }}";
        }
        else
        {
            window.location.href ="{{ url('member/dashboard') }}?filter_poin="+$(this).val();

        }
    });
    var msisdn_tujuan = '';
    var jumlah_transfer = '';

    $('.transfer_sekarang').click(function(event){
        event.preventDefault();
        msisdn_tujuan   = $('.msisdn_tujuan').val();
        jumlah_transfer = $('.jumlah_transfer').val();
        my_balance   = '{{$balance[0]->sisa}}';
        if(msisdn_tujuan == '' || jumlah_transfer == '')
        {
            alert('Data msidn tujuan atau nominal jumlah transfer tidak boleh kosong.');
            return false;
        }
        if(jumlah_transfer > {{$balance[0]->sisa}})
        {
            $('.pointidakcukup').show();
            $('.text_msisdn_tujuan').html(msisdn_tujuan);
            $('.text_jumlah_transfer').html(jumlah_transfer);
            $('.text_my_balance').html(my_balance);
            return false;
        }
        else
        {
            $('.poincukup').show();
            //alert(msisdn);
            $('.text_msisdn_tujuan').html(msisdn_tujuan);
            $('.text_jumlah_transfer').html(jumlah_transfer);
            $('.text_my_balance').html(my_balance);
            

        }
    });

    $('.lanjut_transfer').click(function(event){
        event.preventDefault();
        $('.mt-app').addClass('is_loading');
        if(msisdn_tujuan != '' || jumlah_transfer != '')
        {
            $.post('{{ url('member/transfer_koin') }}', 
            {
                _token              : '{!!csrf_token()!!}',
                msisdn_tujuan       : msisdn_tujuan,
                jumlah_transfer     : jumlah_transfer
            }, 
            function(response){
                if(response.status != 3)
                {
                    //mt-outtest transaksi_gagal
                    $('.mt-outtest').hide();
                    $('.fail_text').html(response.text);
                    $('.transaksi_gagal').show();
                }
                else
                {
                    $('.mt-outtest').hide();
                    $('.sucess_text').html(response.text);
                    $('.transaksi_berhasil').show();
                    setTimeout(function(){
                        window.location.href = '{{ url(Request::path()) }}#';
                    }, 1000);
                }
                $('.mt-app').removeClass('is_loading');

            });
        }
        else
        {
            alert('failed');
        }
    });
});
</script>

<script>
/*FB.Event.subscribe('edge.create', function(href, widget) {
alert('You just liked the page!');
});*/
</script>
@stop