<a href="{{ url('toko') }}">
    <div class="card bg-yellow text-white pt-2 pt-md-4 pb-3 pb-md-4 p px-3 px-md-5 mb-3 mb-md-4">
        <div class="row sg">
            <div class="col-md-3">
                <div class="row sg">
                    <div class="col-xs-5 col-md-12"> 
                        <div class="text-huge text-warning text-bold mb-3 mt-2 mt-md-0">
                            FLASH POIN
                        </div>
                    </div>
                    <div class="col-xs-7 col-md-12 mt-1 mt-md-0">
                        <a href="{{ url('tukarpoin') }}" class="btn btn-fancy btn-block mb-3">
                            Tukar Poin <i class="fa fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row sg fpb smallfpb">

                    @if(isset($ini_flash_banget))
                        @if(count($ini_flash_banget) > 0)
                            @foreach($ini_flash_banget as $flash_point)
                                @if($flash_point->is_redeem == 0)
                                    
                                    <div class="col-md-6 col-xs-12">
                                        <div class="card">
                                            <div class="pos-a badge bg-warning text-white text-small" style="right:3px;top:3px;font-size:10px!important;">
                                                {{number_format($flash_point->arisan->target_point)}}
                                            </div>
                                            <div class="card-body">
                                                <div class="row sg">
                                                    <div class="col-xs-4">
                                                        <img src="{{ newsImageUrl($flash_point->arisan->banner) }}" alt="">
                                                    </div>
                                                    <div class="col-xs-7 pt-2">
                                                        <a href="{{ url('tukarpoin') }}" class="text-medium lh-1 text-gray2 text-semibold">{{$flash_point->arisan->title}}</a>
                                                        <div class="clearfix"></div>
                                                        <span class="text-small text-gray">
                                                            Sponsored by <img src="{{ newsImageUrl($flash_point->vendor->avatar) }}" alt="" width="60">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @else 
                                    
                                    <div class="col-md-6 col-xs-12">
                                        <div class="card">
                                            <div class="pos-a badge bg-warning text-white text-small" style="right:3px;top:3px;font-size:10px!important;">
                                                {{number_format($flash_point->redeem->min_point)}}
                                            </div>
                                            <div class="card-body">
                                                <div class="row sg">
                                                    <div class="col-xs-4">
                                                        <img src="{{ newsImageUrl($flash_point->redeem->image) }}" alt="">
                                                    </div>
                                                    <div class="col-xs-7 pt-2">
                                                        <a href="{{ url('tukarpoin') }}" class="text-medium lh-1 text-gray2 text-semibold">{{$flash_point->redeem->title}}</a>
                                                        <div class="clearfix"></div>
                                                        <span class="text-small text-gray">
                                                            Sponsored by <img src="{{ newsImageUrl($flash_point->vendor->avatar) }}" alt="" width="60">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endif
                            @endforeach
                        @endif 
                    @endif

                </div>                            
            </div>
        </div>
    </div>
</a>