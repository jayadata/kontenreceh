@if(Auth::user())
<div class="px-4 collpased">
    <div class="row">
        <div class="col-xs-12 pl-0">
            
            {{-- ID akun dan Total Poin --}}
            <div class="row sg mb-4">
                <div class="col-xs-6">
                    <div class="row sg">
                        <div class="col-xs-4 pr-1">
                            <img src="{{ StaticAsset('assets/frontend/img/m-menu2/id.png') }}"width="25"  alt="">
                        </div>
                        <div class="col-xs-8 pl-0">
                            <div class="text-15 text-semibold">
                                ID Akun
                            </div>
                            <div class="text-13 text-gray">
                                {{ Auth::user()->msisdn }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(Auth::user())
    <div>
        <hr class="mb-0">
        <div class=" py-3">
            <div class="row">
                <div class="col-xs-6">
                    <a href="{{ url('member/profile') }}"><img src="{{ StaticAsset('assets/frontend/img/m-menu2/setings.png') }}" width="25" class="mr-2" alt=""> Pengaturan</a>
                </div>
                <div class="col-xs-6">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><img src="{{ StaticAsset('assets/frontend/img/m-menu2/out.png') }}" width="25" class="mr-2" alt=""> <span class="text-danger">Keluar</span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>

        <hr class="my-0">

        <div class=" py-3">
            <div class="m-menu2-link">
                <a href="{{ route('member.content.redeem') }}"></a>
                <div class="row sg">
                    <div class="col-xs-2 pr-0">
                        <img src="{{ StaticAsset('assets/frontend/img/m-menu2/pwcode.png') }}" width="30" alt="">
                    </div>
                    <div class="col-xs-10 pl-0">
                        <div class="text-15 text-semibold">Kode Qomiqu</div>
                        <div class="text-13 text-gray">Cek kode Qomiqu yang belum kamu gunakan</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="bg-gray">
        <div class=" py-3">
            <div class="row">
                
                <div class="col-xs-12">
                    <a href="{{ url('auth/signin') }}"><img src="{{ StaticAsset('assets/frontend/img/m-menu2/out.png') }}" width="25" class="mr-2" alt=""> <span class="text-danger">Daftar / Login</span></a>
                </div>
                
            </div>
        </div>
    </div>
@endif
