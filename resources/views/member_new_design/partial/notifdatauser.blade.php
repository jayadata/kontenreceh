@if(Auth::user()->email == NULL || Auth::user()->no_telp == NULL)
	<div class="alert alert-info alert-dismissible fade in px-4 py-2 py-md-4" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>

		<div class="row sg pt-3">
			<div class="col-md-1 col-xs-2 text-center pt-3">
				<img src="{{ StaticAsset('assets/frontend/img/information.png') }}" alt="">
			</div>
			<div class="col-md-11 col-xs-10">
				<p class="text-18 mt-0 text-xs-13">Lengkapi Data Diri Anda Termasuk E-Mail untuk Bisa Mendapatkan Barang dan Membeli Keanggotaan VIP</p>
			</div>
		</div>
	</div>
@endif