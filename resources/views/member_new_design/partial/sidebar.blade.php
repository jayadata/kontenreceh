{{-- LEFT SIDEBAR --}}
<div class="col-md-3">
<div class="card">
    
    {{-- Profile --}}
    <div class="card-body">
        <div class="row sg">
            <div class="col-md-3">
                 @if(Auth::user()->avatar == '')
                   @if(Auth::user()->fb_login == 1)
                       <img src="https://graph.facebook.com/v2.6/{{ Auth::user()->fb_id }}/picture?type=normal" class="avatar img-rounded img-thumbnail">
                    @else
                        <img src="{{ StaticStaticAsset('assets/frontend/img/member/avatar-fallback.png') }}" alt="{{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }}" class="avatar img-rounded img-thumbnail"> 
                    @endif

               @else
                   <img src="{{ avatar_storage(Auth::user()->avatar) }}" alt="{{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }}" class="img-rounded avatar">
               @endif
            </div>
            <div class="col-md-9">
                <strong>{{ (Auth::user()->first_name) != '' ? Auth::user()->first_name : Auth::user()->msisdn }} </strong>
                <div class="clearfix"></div>
                <span class="text-small text-gray">{{Auth::user()->msisdn}}</span>
            </div>
            <div class="col-md-12 mt-3">
                <div>
                    <img src="{{ StaticAsset('assets/frontend/img/tukarpoin/koin.png') }}" alt="" width="20" height="20" class="mr-2">
                    <span class="text-gray text-medium">{{nomor_cantik($home_balance[0]->sisa)}} Koin</span>
                </div>
                <div class="mt-3">
                    <img src="{{ StaticAsset('assets/frontend/img/tukarpoin/poin.png') }}" alt="" width="20" height="20" class="mr-2">
                    <span class="text-gray text-medium">{{$home_balance[0]->point}} Poin</span>
                </div>
            </div>
        </div>
    </div> <hr>

    {{-- TUKAR POIN --}}
    <div class="card-body">
        <div class="row sg">
            <div class="col-md-12">
                <h4><strong>Tukar Poin</strong></h4>
                <ul class="noliststyle pl-0 fancylist">
                    <li><a href="{{ url(ENV('PREFIX').'/barang_yang_didapat') }}">Barang Yang Didapatkan</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/status_barang_incaran') }}">Status Barang Incaran</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/koleksi_cap') }}">Koleksi Cap</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/histori_penggunaan_poin') }}">Histori Penggunaan Poin</a></li>
                </ul>
            </div>
        </div>
    </div><hr>

    {{-- KOIN --}}
    <div class="card-body">
        <div class="row sg">
            <div class="col-md-12">
                <h4><strong>Koin</strong></h4>
                <ul class="noliststyle pl-0 fancylist transaksikoin">
                    <li><a href="{{ url(ENV('PREFIX').'/beli_dengan_pulsa') }}">Beli Dengan Pulsa</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/beli_dengan_pulsa') }}#dengantransfer">Beli Tanpa Pulsa</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/beli_dengan_pulsa') }}#transferkoin">Transfer Koin</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/history_penggunaan_koin') }}">Histori Penggunaan Koin</a></li>
                </ul>
            </div>
        </div>
    </div><hr>

    {{-- AKUN SAYA --}}
    <div class="card-body">
        <div class="row sg">
            <div class="col-md-12">
                <h4><strong>Akun Saya</strong></h4>
                <ul class="noliststyle pl-0 fancylist">
                    <li><a href="{{ url(ENV('PREFIX').'/pengaturan') }}">Pengaturan</a></li>
                    <li><a href="{{ url(ENV('PREFIX').'/customer_care') }}">Hubungi Customer Care</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
</div> {{-- end LEFT SIDEBAR --}}