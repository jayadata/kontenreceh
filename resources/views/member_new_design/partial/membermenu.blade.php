{{-- @if(isset($set) > 0) --}}
@if(Auth::user())
    @if(Auth::user()->expire > date('Y-m-d'))
        <a href="{{ url('member/beli_dengan_pulsa') }}"><strong class="badge badge-pill bg-green mr-2">VIP</strong> Hingga <strong>{{ $newDate = date("d-M-Y", strtotime(Auth::user()->expire)) }}</strong></a>
    @else <a href="#" class="badge badge-pill bg-warning text-white mr-2">Basic</a>

    @endif                   

<hr> <!-- =================================================== -->

<div>
    {{-- Poin --}}
    <img src="{{ StaticAsset('assets/frontend/img/tukarpoin/poin.png') }}" alt="" width="16" height="16" class="mr-1">
    <span class="text-gray text-medium">{{$home_balance[0]->point}} Poin</span>
</div>

{{-- PROGRESS --}}
<div class="mt-3 mob">
    <div>
        <div class="circle-letter" style="float:left;margin-right:5px;border-color: #ffb71d;color:#ffb71d;width:17px;height:17px;">
            D
        </div> 
        <span class="text-gray text-medium">Daily: 
            @if($max_poin[0]->total_poin === NULL)
                <span style="width:0%"><span>0/200</span></span>
            @else
                @if($max_poin[0]->total_poin > 200)
                    <span style="width:100%"><span>200/200</span></span>
                @else
                    <?php $percent = ($max_poin[0]->total_poin*100)/200; ?>
                    <span style="width:{{ $percent }}%"><span>{{ $max_poin[0]->total_poin }}/200</span></span>
                @endif                                
            @endif
        </span>
    </div>
    <div class="header-prog mt-3" style="width: 100%">
        @if($max_poin[0]->total_poin === NULL)
            <span style="width:0%"><span>0/200</span></span>
        @else
            @if($max_poin[0]->total_poin > 200)
                <span style="width:100%"><span>200/200</span></span>
            @else
                <?php $percent = ($max_poin[0]->total_poin*100)/200; ?>
                <span style="width:{{ $percent }}%"><span>{{ $max_poin[0]->total_poin }}/200</span></span>
            @endif                            
        @endif                        
    </div>
     <div class="clearfix"></div>
</div>

<hr> <!-- =================================================== -->

<a href="{{ url('member/barang_yang_didapat') }}?notif=trx" class="text-14">Barang Yang Didapatkan</a> <span class="label label-pill label-danger pull-right">@if(isset($notification_redeem) && count($notification_redeem) > 0){{ count($notification_redeem) }} @else 0 @endif</span>

<hr> <!-- =================================================== -->

<ul>
    <li><a href="{{ url('member/histori_penggunaan_poin') }}">Mutasi Poin</a></li>
    <li><a href="{{ url('member/beli_dengan_pulsa') }}">Keanggotaan VIP</a></li>
    <li><a href="{{ url('member/history_penggunaan_vip') }}">Mutasi VIP</a></li>
    <li><a href="{{ url('member/status_transfer') }}">Status Transfer</a></li>
    <li><a href="{{ url('member/new_design/redeem') }}">Input Kode PW</a></li>
    <li><a href="{{ route('code:list') }}">Kode PW</a></li>
</ul>

<hr> <!-- =================================================== -->

<ul>
    <li><a href="{{ url('member/pengaturan') }}">Pengaturan</a></li>
</ul>

<hr> <!-- =================================================== -->

<ul>
    <li><a href="{{ url('member/logout') }}">Logout</a></li>
</ul>
@endif