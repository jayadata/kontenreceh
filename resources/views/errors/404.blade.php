@extends('frontend.layouts.app')

@section('content')
	<section>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7">
					<div class="kr-card">
						<div class="kr-card-body">
							<div class="alert alert-danger">
								<div class="row sm">
									<div class="col-1 pr-0">
										<img src="{{ asset('assets/frontend/exclamation-mark.png') }}" alt="" class="img-fluid makered">
									</div>
									<div class="col-11">
                                        <p>Halaman yang anda cari tidak kami temukan di <a href="https://qomiqu.com">https://qomiqu.com</a></p>
                                        <p>Jika anda memiliki pertanyaan atau keluhan dengan situs ini silahkan hubungi whatsapp <a href="tel:087885410267">0878 - 8541 - 0267</a> (24 Jam)</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@stop

@section('script')

@stop