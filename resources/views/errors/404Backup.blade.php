<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="YAGITU.com - Bikin Kamu Ketawa Adalah Tujuan Kami">
	<meta name="keywords" content="yagitu, ketawa, humor, jokes, receh, ngakak, senyum, kesel, unch, uwu, konten">
	<meta name="author" content="YAGITU.COM">
	<title>YAGITU.com</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
	<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
	<link rel="shortcut icon" href="{{ asset('assets/frontend/favicon.ico') }}">
    <style>
        @media screen and (max-width: 500px) {
            .kr-card .kr-card-body {
                padding: 10px;
            }
            .text-small {
                font-size:10px;
            }
            .btn {
                padding-left: 5px;
                padding-right: 5px;
            }
        }
    </style>
</head>
<body>
	@include('frontend.partials.header')
	<section>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7">
					<div class="kr-card">
						<div class="kr-card-body">
							<div class="alert alert-danger">
								<div class="row sm">
									<div class="col-1 pr-0">
										<img src="{{ asset('assets/frontend/exclamation-mark.png') }}" alt="" class="img-fluid makered">
									</div>
									<div class="col-11">
                                        <p>Halaman yang anda cari tidak kami temukan di <a href="https://yagitu.com">https://yagitu.com</a></p>
                                        <p>Jika anda memiliki pertanyaan atau keluhan dengan situs ini silahkan hubungi whatsapp <a href="tel:0818-30-30-29">0818-30-30-29</a> (24 Jam)</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>

	<script>
		jQuery(document).ready(function(){
			var elem = document.querySelector('.main-carousel');
			var flkty = new Flickity( elem, {
				// options
				cellAlign: 'left',
				contain: true,
				pageDots: true,
			});

			flkty.on( 'change', function( index ) {
				var size = $('.main-carousel').find('.carousel-cell').eq(index).find('img').data('size');
				var url = $('.main-carousel').find('.carousel-cell').eq(index).find('img').attr('src');
				console.log(size);
				$('.imgsize').text(size);
				$('.downloadurl').attr('href',url).attr('download', index+'.jpg');
			});

			var size = $('.main-carousel').find('.carousel-cell').eq(0).find('img').data('size');
			$('.imgsize').text(size);
		});
	</script>
</body>
</html>