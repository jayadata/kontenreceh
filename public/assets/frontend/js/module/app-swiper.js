define(['jquery', 'swiper'], function($){

    $(function(){
        // Convert App Card to be swipe-able on Mobile
        if (isMobile.any) {



        };

        // Initiate Swiper on Tablet
        // if (isMobile.tablet && ($(window).height() > $(window).width())) {
        //     // Add swiper container to Apps lists
        //     $('.append-swiper').each( function(){
        //         $(this).addClass('swiper-container');
        //         $(this).append('<div class="swiper-scrollbar"></div>');
        //     });

        //     // Add swiper classes to Apps Card
        //     $('.append-swiper .app-container').each( function(){
        //         $(this).addClass('swiper-wrapper');
        //         $(this).children('.app-card').addClass('swiper-slide');
        //     });

        //     var appSwiper = new Swiper ('.append-swiper', {
        //         grabCursor: true,
        //         loop: false,
        //         slidesPerView: 5,
        //         slideClass: 'app-card',
        //         scrollbar: '.swiper-scrollbar',
        //         scrollbarHide: false,
        //         spaceBetween: 15,
        //         variableWidth: true
        //     });
        // };

        // Initiate Swiper on Phone
        if (isMobile.phone) {
            // Add swiper container to Apps lists
            $('.append-swiper').each( function(){
                $(this).addClass('swiper-container');
                $(this).append('<div class="swiper-scrollbar"></div>');
            });

            // Add swiper classes to Apps Card
            $('.append-swiper .app-container').each( function(){
                $(this).addClass('swiper-wrapper');
                $(this).children('.app-card').addClass('swiper-slide');
            });

            var appSwiper = new Swiper ('.append-swiper', {
                grabCursor: true,
                loop: false,
                slidesPerView: 2,
                slideClass: 'app-card',
                scrollbar: '.swiper-scrollbar',
                scrollbarHide: false,
                spaceBetween: 15,
                variableWidth: true
            });
        };
    });

});