define(['jquery', 'fancybox'], function($){

    $(function(){
        // Download Confirmation
        $('.modal-trigger').fancybox({
            closeBtn: false
        });
    });

});
