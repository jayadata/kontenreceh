define(['jquery', 'clipboard'], function($, Clipboard){

    var clipboard = new Clipboard('.clipboard');

    clipboard.on('success', function(e) {
        // console.info('Copied Data:', e.text);
        $('.clipboard').tooltip('show');
    });

    if (isMobile.any) {
        // Shorten clipboard text
        $('.clipboardText').text('Copy');
    };

});