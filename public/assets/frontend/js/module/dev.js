define(['jquery'], function(){
    $(function(){

        var $belumBeli                  = $('#belumBeli'),
            $sudahBeli                  = $('#sudahBeli'),
            $onlinePurchase             = $('#onlinePurchase'),
            $smsPurchase                = $('#smsPurchase'),
            $depositPurchase            = $('#depositPurchase');
            $belumLangganan             = $('#belumLangganan');
            $sudahLangganan             = $('#sudahLangganan');

        var $belumBeliDiv               = $('.belumbeli'),
            $sudahBeliDiv               = $('.sudahbeli'),
            $onlinePurchaseDiv          = $('.belipulsa'),
            $smsPurchaseDiv             = $('.belisms'),
            $depositPurchaseBelumDiv    = $('.belidepositbelum'),
            $depositPurchaseSudahDiv    = $('.belidepositsudah');

        // Default Options

        $belumBeli.on('click', function(){
            $belumBeliDiv.siblings().addClass('hide');
            $belumBeliDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
            $(this).addClass('active').siblings().removeClass('active');
        });

        $sudahBeli.on('click', function(){
            $sudahBeliDiv.siblings().addClass('hide');
            $sudahBeliDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
            $(this).addClass('active').siblings().removeClass('active');
            $sudahLangganan.addClass('hide').siblings().addClass('hide');
        });

        // Purchase Options

        $onlinePurchase.on('click', function(){
            $onlinePurchaseDiv.siblings().addClass('hide');
            $onlinePurchaseDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
        });

        $smsPurchase.on('click', function(){
            $smsPurchaseDiv.siblings().addClass('hide');
            $smsPurchaseDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
        });

        $depositPurchase.on('click', function(){
            $depositPurchaseBelumDiv.siblings().addClass('hide');
            $depositPurchaseBelumDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
            $belumLangganan.removeClass('hide').addClass('active');
            $sudahLangganan.removeClass('hide')
        });

        // Deposit Options

        $sudahLangganan.on('click', function() {
            $(this).addClass('active').siblings().removeClass('active');
            $depositPurchaseSudahDiv.siblings().addClass('hide');
            $depositPurchaseSudahDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
        });

        $belumLangganan.on('click', function() {
            $(this).addClass('active').siblings().removeClass('active');
            $depositPurchaseBelumDiv.siblings().addClass('hide');
            $depositPurchaseBelumDiv.removeClass('hide');
            $('.devApp').removeClass('hide');
        });


    });
});