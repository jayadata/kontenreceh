define(['jquery', 'swiper'], function($){

    $(function(){
        // Convert News Category to be swipe-able
        if (isMobile.any) {
            // Add swiper container and classes
            $('.news-category:not(.menudesktop)').addClass('swiper-container')
                .children('ul').addClass('swiper-wrapper')
                .children('li').addClass('swiper-slide');
        };

        if (isMobile.tablet) {
            var catSwiper = new Swiper ('.news-category:not(.menudesktop)', {
                grabCursor: true,
                loop: false,
                slidesPerView: 6,
                spaceBetween: 0,
                freeMode: true,
                freeModeSticky: true,
                variableWidth: true
            });
        };

        if (isMobile.phone) {
            var catSwiper = new Swiper ('.news-category:not(.menudesktop)', {
                grabCursor: true,
                loop: false,
                slidesPerView: 'auto',
                spaceBetween: 0,
                freeMode: true,
                freeModeSticky: true,
                variableWidth: true,
            });
        };
    });

});

