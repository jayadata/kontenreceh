define(['jquery', 'colorthief'], function($, colorThief){

    $(window).on("load", function() {
        var colorThief  = new ColorThief();
        var targetImage = $('.appIconImage');
        var imageSource = targetImage[0];
        var ctContainer = $('.color-output');
        var color       = colorThief.getColor(imageSource);
        $('.title-head').css('background-color', 'rgb('+ color +')');
    });

});