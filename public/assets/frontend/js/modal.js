var $ = jQuery;
var udahbeli = false;

// change modal header text
function pwModalHeaderText( $text ) {
	$('.text-page-title-content').text(  $text  );
}

// change modal button text
function pwModalBtnText( $text ) {
	$('.button-main-content .text-button-main span').text(  $text  );
}

/* change modal button href */
function pwModalBtnLoc( $url ) {
	$('.button-main-content').data('onclick', '').on('click', function(e){
        e.preventDefault();
        window.location.href=$url;
    });
}

/* push content */
function pwModalPushContent( $string ) {
	$('.pw-modal--data-storage').html($string);
}

// TAB CLICK
function tab_click() {
	$('.pw-modal .nav-tabs .text-actionable').on('click', function(e){
		e.preventDefault();
		var link = $(this),
			tabs = $(this).parents('.tabs'),
			target = $(this).data('target'),
			li = link.parent('li');
		li.addClass('active').siblings('li').removeClass('active');	

		$(target).addClass('active').siblings('.tab-pane').removeClass('active');
	});
}

// Render Harga
function render_harga( $label, $harga, $ispoin ) {
	if( $ispoin == true ) {
		//var rp = '<span class="circha orange text-bold text-20" style="width:25px;height:25px;line-height:22px;position:relative;top:10px;">P</span>';
		var rp = '<img src="'+ imgassetsurl +'/m-menu2/p.png" width="22" alt="" />';
	} else {
		var rp = 'Rp ';
	}
	var myvar = '<div class="card-container">'+
	'				<div class="amount">'+
	'					<div class="amount-title pull-left"><span class="text-amount-title">'+ $label +'</span></div>'+
	'					<div class="amount-content pull-right"><span class="text-amount-rp">'+rp+' </span><span class="text-amount-amount">'+$harga+'</span></div>'+
	'				</div>'+
	'			</div>';
	$('.pw-modal--data-storage').append(myvar);
}

// Render Keterangan
function render_keterangan( $text ) {
	var info =  '<div class="clearfix"></div><div class="row sg mx-0 mt-4">  '  + 
 '   <div class="col-md-2 col-xs-2 pr-3">  '  + 
 '   <img src="https://be2ad46f1850a93a8329-aa7428b954372836cd8898750ce2dd71.ssl.cf6.rackcdn.com/assets/frontend/img/redeemicon/icon_InfoBlue.png" alt="" width="50">  '  + 
 '   </div>  '  + 
 '   <div class="col-md-10 col-xs-10">  '  + 
 '   <p class="text-semimedium p-0 text-xs-large">'+$text+'</p>  '  + 
 '   </div>  '  + 
 '  </div>  ' ; 

 	$('.pw-modal--data-storage').append(info);
}

// Render Plain Text
function render_text( $text ) {
	var todo = '<div class="clearfix"></div><div class="text-gray pt-5 pr-5 pl-5 pb-0 guide text-center text-dark">'+ $text +'</div>'

 	$('.pw-modal--data-storage').append( todo );
}

// Render Plain Text
function render_smsformat( $text ) {
	var smsbox =  
'   <div class="clearfix"></div>  '  + 
'   <div class="text-center">  '  + 
'   <div class="mt-4 mb-4 pt-3 pb-3 bg-white text-center rounded px-4" style="display: inline-block">  '  + 
'   <span class="pw-replace so">'+ $text +'</span>  '  + 
'  </div>  ' +
'  </div>  ' ;

 	$('.pw-modal--data-storage').append( smsbox );
}

// Render Product Detail
function render_productDetail( $nama, $harga, $userdetails, $ispoin, $sisapoin ) {
	if( $ispoin == true ) {
		var rp = '<img src="'+ imgassetsurl +'/m-menu2/p.png" width="15" alt="" />';
	} else {
		var rp = '';
	}

	var $userdetails = JSON.parse($userdetails);
	var harga = '<div class="card-container">  '  + 
	'       <div class="tabs">  '  + 
	'           <ul class="nav-tabs nav-justified">  '  + 
	'               <li class="active"><a href="#" class="text-actionable" data-target=".oder-detail"><span>order details</span></a></li>  '  + 
	'               <li class=""><a href="#" class="text-actionable" data-target=".cust-detail"><span>customer details</span></a></li>  '  + 
	'           </ul>  '  + 
	'           <div class="tab-content">  '  + 
	'               <div class="tab-pane active oder-detail">  '  + 
	'                   <div class="content-table">  '  + 
	'                       <table class="table">  '  + 
	'                           <tbody>  '  + 
	'                               <tr>  '  + 
	'                                   <th class="table-item text-body-title">Item(s)</th>  '  + 
	'                                   <th class="table-amount text-body-title">Harga</th>  '  +
	'                               </tr>  '  + 
	'                               <tr>  '  + 
	'                                   <td class="table-item text-body"><span class="item-name">'+$nama+'</span></td>  '  + 
	'                                   <td class="table-amount text-body">'+' '+rp+' '+$harga+'</td>  '  + 
	'                               </tr>  '  + 
	'                           </tbody>  '  + 
	'                       </table>  '  + 
	'                   </div>  '  + 
	'               </div>  '  + 
	'               <div class="tab-pane cust-detail">  '  + 
	'                   <div class="content-text-block">  '  + 
	'                       <div class="col-xs-12">  '  + 
	'                           <div class="text-block">  '  + 
	'                               <div class="text-body-title">Name</div>  '  + 
	'                               <div class="text-body">'+ $userdetails["nama"] +'</div>  '  + 
	'                           </div>  '  + 
	'                           <div class="text-block">  '  + 
	'                               <div class="text-body-title">Phone number</div>  '  + 
	'                               <div class="text-body">'+ $userdetails["telp"] +'</div>  '  + 
	'                           </div>  '  + 
	'                           <div class="text-block">  '  + 
	'                               <div class="text-body-title">Email</div>  '  + 
	'                               <div class="text-body">'+ $userdetails["mail"] +'</div>  '  + 
	'                           </div>  '  + 
	'                       </div>  '  + 
	'                       <div class="col-xs-6"></div>  '  + 
	'                   </div>  '  + 
	'               </div>  '  + 
	'           </div>  '  + 
	'       </div>  '  + 
	'  </div>  ' ; 
	$('.pw-modal--data-storage').append(harga);
	setTimeout(function() {
	    tab_click();
	}, 100);
}

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

// Render Payment Method
function render_peymentMethod( $isLangganan = false, $function = '', $ussd_num, $isInstant = false ) {

	var id = makeid();
	if( $isLangganan == true && $ussd_num != null ) {
		$ussd =  
		'<div class="content-text-block">  '  + 
		'    <div class="col-xs-12">  '  + 
		'        <div class="text-block pb-4">  '  + 
		'            <div class="text-14 text-center">(Harus menggunakan Handphone dan Kartu GSM yang sesuai) Silahkan lakukan panggilan atau telfon ke nomor di bawah ini</div>' +
		'            <div class="text-20 text-success text-bold text-center mt-4">'+$ussd_num+'</div>' +
		'            <div class="mt-4"><a href="tel:'+$ussd_num+'" class="btn btn-block btn-success">Lanjutkan</a></div>' +
		'        </div>  '  + 
		'    </div>  '  + 
		'    <div class="col-xs-6"></div>  '  + 
		'</div>  ' ;
	} else {
		$ussd = '<div class="text-14 text-center py-5 my-5">Pilihan ini tidak tersedia</div>';
	}

	if( $isInstant == true ) {
		$instant = 
		'<div class="content-text-block">  '  + 
		'    <div class="col-xs-12">  '  + 
		'        <div class="text-block pb-4">  '  + 
		'            <div class="text-14 text-center">(Harus menggunakan Handphone dan Kartu GSM yang sesuai) Anda akan melakukan pembelian melalui WAP Payment Gateway Indosat, tekan tombol di bawah untuk melanjutkan</div>' +
		'            <div class="mt-4"><a href="http://150.107.148.9/app/wap/ad" class="btn btn-block btn-success">Lanjutkan</a></div>' +
		'        </div>  '  + 
		'    </div>  '  + 
		'    <div class="col-xs-6"></div>  '  + 
		'</div>  ' ;
	} else {
		$instant = '<div class="text-14 text-center py-5 my-5">Pilihan ini tidak tersedia</div>';
	}

	var numberinput = ' <div class="form-row align-items-center">  '  + 
	'   <div class="col-auto">  '  + 
	'   <div class="input-group mb-2 mb-sm-0">  '  + 
	'   <div class="input-group-addon" style="padding:0 15px">+62</div>  '  + 
	'   <input required style="padding: 35px 12px" name="nomorhandphone" type="text" class="form-control" id="nomorhandphone" placeholder="Nomor Handphone" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">  '  + 
	'   </div>  '  + 
	'   </div>  '  + 
	'  </div>  ' ; 
	
	var harga = '<div class="card-container">  '  + 
	'       <div class="text-14 px-4 pt-4">  '  + 
	'           <i class="fa fa-info-circle text-info mr-2"></i> Pilih salah satu metode pembelian berikut' +
	'       </div>  '  + 
	'       <hr/>  '  + 
	'       <div class="tabs">  '  + 
	'           <ul class="nav-tabs nav-justified">  '  + 
	'               <li class="active"><a href="#" class="text-actionable" data-target=".sms"><span>SMS</span></a></li>  '  + 
	'           </ul>  '  + 
	'           <div class="tab-content">  '  + 
	'               <div class="tab-pane active sms">  '  + 
	' 					<div class="px-4 py-4">  '  + 
	'	 					<div class="row sg">  '  + 
	'	 						<div class="col-xs-2"><i class="fa fa-check-circle text-30 text-success"></i></div>'  + 
	'	 						<div class="col-xs-10 text-14 pl-0">  '  + 
	'	 							Masukkan nomor handphone kamu' +
	'	  						</div>  ' + 
	'	  					</div>  ' + 
	'	 					<div class="row sg">  '  + 
	'	 						<div class="col-xs-2">&nbsp;</div>'  + 
	'	 						<div class="col-xs-10 pl-0">  '  + 
	'	 							<div class="form-row align-items-center">  '  + 
	'	   								<div>  '  + 
	'	   									<div class="input-group mb-2 mb-sm-0">  '  + 
	'	  										<div class="input-group-addon" style="padding:0 15px">+62</div>  '  + 
	'	   										<input required style="padding: 35px 12px" name="nomorhandphone" type="text" class="form-control" id="nomorhandphone" placeholder="Nomor Handphone" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57">  '  + 
	'	   									</div>  '  + 
	'	  								</div>  ' + 
	'	  							</div>  ' + 
	'	  						</div>  ' + 
	'	  					</div>  ' + 
	'						<div class="row sg mt-4">  '  + 
	'	 						<div class="col-xs-2"><i class="fa fa-check-circle text-30 text-success"></i></div>'  + 
	'	 						<div class="col-xs-10 text-14 pl-0">  '  + 
	'	 							Centang kotak di bawah ini untuk melanjutkan proses' +
	'	  						</div>  ' + 
	'	  					</div>  ' + 
	'	 					<div class="row sg">  '  + 
	'	 						<div class="col-xs-2">&nbsp;</div>'  + 
	'	 						<div class="col-xs-10 pl-0">  '  + 
	'	 							<div class="form-row align-items-center">  '  + 
	'	   								<div>  '  + 
	'	   									<div id="'+id+'"></div>' +
	'	  								</div>  ' + 
	'	  							</div>  ' + 
	'	  						</div>  ' + 
	'	  					</div>  ' + 
	'                       <div class="mt-4"><a href="#" id="cap-'+id+'" data-onclick="'+$function+'" class="btn btn-block btn-success smskirim" disabled="disabled">Lanjutkan</a></div>' +
	'  					</div>  ' + 
	'  					<div class="clearfix"></div>  ' + 
	'               </div>  '  + 
	'           </div>  '  + 
	'       </div>  '  + 
	'  </div>  ' ; 
	$('.pw-modal--data-storage').append(harga);

	setTimeout(function() {
	    tab_click();
	    grecaptcha.render(id, {
	        sitekey: '6LdM5qoUAAAAAJafMCuguG0xIfaP8soCacKh5ory',
	        callback: function(response) {
				$('#cap-'+id).removeAttr('disabled');
	        }
	    });

	    $('#cap-'+id).on('click', function(e){
	    	e.preventDefault();
		    // if there is any callback
		    var callback = jQuery(e.currentTarget).data("onclick");

		    var x = eval(callback);
		    if (typeof x == 'function') {
		        x();
		    }	
	    });	    
	}, 100);
}

// Informasi
function render_informasi() {
	var id = makeid();
	var login = 
	'   <div class="card-container mt-2">  '  + 
	'       <div class="text-14 px-4 pt-4">  '  + 
	'           <i class="fa fa-info-circle text-info mr-2"></i> Informasi' +
	'       </div>  '  + 
	'       <hr/>  '  + 
	'   	<div class="card-body text-13">  '  + 
	'			<div class="row sg">  '  + 
	'	 			<div class="col-xs-2"><i class="fa fa-check-circle text-30 text-success"></i></div>'  + 
	'	 			<div class="col-xs-10 text-14 pl-0">  '  + 
	'	 				Setuju untuk POIN saya ditukar sesuai informasi di atas' +
	'	  			</div>  ' + 
	'	  		</div>  ' + 
	'			<div class="row sg">  '  + 
	'	 			<div class="col-xs-2"><i class="fa fa-check-circle text-30 text-success"></i></div>'  + 
	'	 			<div class="col-xs-10 text-14 pl-0">  '  + 
	'	 				Centang kotak di bawah ini untuk melanjutkan proses' +
	'	  			</div>  ' + 
	'	  		</div>  ' + 
	'			<div class="row sg mt-3">  '  + 
	'	 			<div class="col-xs-2">&nbsp;</div>'  + 
	'	 			<div class="col-xs-10 text-14 pl-0">  '  + 
	'	 				<div id="'+id+'"></div>' +
	'	  			</div>  ' + 
	'	  		</div>  ' + 
	'		</div>  ' ; 
	'	</div>  ' ; 
	$('.pw-modal--data-storage').append( login );
	setTimeout(function() {
	    tab_click();
	    grecaptcha.render(id, {
	        sitekey: '6LdM5qoUAAAAAJafMCuguG0xIfaP8soCacKh5ory',
	        callback: function(response) {
	            $('#'+id).parents('.pw-modal').find('.button-main').removeClass('disabled').find('a').removeAttr('disabled');
	        }
	    });
	    $('#'+id).parents('.pw-modal').find('.button-main').removeClass('disabled').find('a').removeAttr('disabled');
	}, 100);
}

// Belom login
function pleaselogin( $loginurl = null ) {
	var login = 
	'   <div class="text-center mt-5 pt-5">  '  + 
	'		<svg width="55" height="55" x="0" y="0" class="stencil--easier-to-select" style="opacity: 1;"><defs></defs><rect x="0" y="0" width="55" height="55" fill="transparent" class="stencil__selection-helper"></rect><path d="M24.75 41.25C24.75 41.25 30.25 41.25 30.25 41.25 30.25 41.25 30.25 24.75 30.25 24.75 30.25 24.75 24.75 24.75 24.75 24.75 24.75 24.75 24.75 41.25 24.75 41.25 24.75 41.25 24.75 41.25 24.75 41.25M27.5 0C12.32 0 0 12.32 0 27.5 0 42.68 12.32 55 27.5 55 42.68 55 55 42.68 55 27.5 55 12.32 42.68 0 27.5 0 27.5 0 27.5 0 27.5 0M27.5 49.5C15.372499999999999 49.5 5.5 39.6275 5.5 27.5 5.5 15.372499999999999 15.372499999999999 5.5 27.5 5.5 39.6275 5.5 49.5 15.372499999999999 49.5 27.5 49.5 39.6275 39.6275 49.5 27.5 49.5 27.5 49.5 27.5 49.5 27.5 49.5M24.75 19.25C24.75 19.25 30.25 19.25 30.25 19.25 30.25 19.25 30.25 13.75 30.25 13.75 30.25 13.75 24.75 13.75 24.75 13.75 24.75 13.75 24.75 19.25 24.75 19.25 24.75 19.25 24.75 19.25 24.75 19.25" stroke-width="0" stroke="none" stroke-dasharray="none" fill="rgb(0, 0, 0)" fill-rule="evenOdd"></path></svg>  '  + 
	'		<br><br>  '  + 
	'     '  + 
	'		<h1 class="text-dark text-semibold" style="font-size:20px;">ANDA BELUM LOGIN</h1>  '  + 
	'		<p>  '  + 
	'			Silahkan klik tombol LOGIN di bawah ini untuk bisa mengikuti program ini  '  + 
	'		</p>  <p><a href="'+$loginurl+'" class="btn btn-warning">Login</a></p>'  + 
	'	</div>  ' ; 
	$('.pw-modal--data-storage').append(login);
	$('.button-main-content .text-button-main span').text('Tutup').on('click', function(){
		pwmodal('close');
	});
}

// Modal Actions
function pwmodal( $action, $callback = null ) {
	var speed = 100;
	// close
	if ( $action == 'close' ) {
		$('.pw-modal').hide();
		$('.pw-modal--loader').fadeIn(speed);
		$('.pw-modal--data-storage').html('').fadeOut(speed);
		if( $('.vip-step').length ) {
			$('.vip-step.step-1').addClass('active');
			$('.vip-step.step-2, .vip-step.step-3').removeClass('active');
		}
		pwModalBtnText('Continue');
		if( udahbeli ==  true ) {
			// daripada ribet ye kan 
			location.reload();
		}
		
	// loading
	} else if( $action == 'loading' ) {
		$('.pw-modal--loader').fadeIn(speed);
		$('.pw-modal--data-storage').html('').fadeOut(speed);
	// Loaded
	} else if( $action == 'loaded' ) {
		setTimeout(function() {
			$('.pw-modal--loader').fadeOut(speed, function(){
				$('.pw-modal--data-storage').fadeIn(speed);
			});			
		}, speed);
	} else if( $action == 'open' ) {
		// if there is any callback
		var callback = $callback;

		// show the modal first
		$('.pw-modal').show(0, function(){
			var x = eval(callback);
			if (typeof x == 'function') {
			    x();
			}
		});		
	}
}

jQuery(document).ready(function(){
	// Modal Close
	$('.header-back').on('click', function(e){
		e.preventDefault();
		if( $(this).find('.modal-close-dark').length ) {
			pwmodal('close');
		}
	});

	// Modal Trigger
	function popuptrigger(){
		$('.popup-trigger').on('click', function(e){
			e.preventDefault();
			// if there is any callback
			var callback = $(this).data("onshow");
			var onclosefunc = $(this).data("buttonaction");

			if( ! $(this).attr('disabled') ) {
				// show the modal first
				$('.pw-modal').show(0, function(){
					var x = eval(callback);
					if (typeof x == 'function') {
						x();
					}

					$(this).find('.button-main-content').data('onclick', onclosefunc);
				});
			} 
		});
	}

	popuptrigger();

	// Modal button action 
	$('.button-main-content').on('click', function(e){
		e.preventDefault();
		// if there is any callback
		var callback = jQuery(e.currentTarget).data("onclick");

		var x = eval(callback)
		if (typeof x == 'function') {
		    x();
		}	
	});
});