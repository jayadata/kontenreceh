jQuery(function($){

    $('.article-main').find('p').find('img').each(function(){
        var $t = $(this);

        $t.wrap('<span class="img-lazystate"></span>');

        $t.attr(
            'data-src', $t.attr('src')
        ).addClass('lazyload');
        /*).attr('src', imgassetsurl + '/ajax-loader.gif').addClass('lazyload');*/
        /*.attr('src', imgassetsurl + '/ajax-loader.gif').*/
    }); 

    $('.nc-item-thumb').find('img').each(function(){
        var $t = $(this);
        $t.attr(
            'data-src', $t.attr('src')
        ).attr('src', '').addClass('lazyload');
    });

   /* $('a[href^="http"]').on('click', function(){
        var $target = $(this).attr('target');
        if( $target != '_BLANK' ) {
            $('.fakeloader').show();
        }
    });*/

    function letsequal() {
        $('.equalizerv2').each(function(){
            $(this).equalizer({
                // height = type of height to use
                // "o" or "outer" = "outerHeight" - includes height + padding + border + margin
                // "i" or "inner" = "innerHeight" - includes height + padding + border
                // default        = "height"      - use just the height
                columns    : '.eq_target',     // elements inside of the wrapper
                useHeight  : 'i',    // height measurement to use
            });
        });
    }

    // EQUALIZER GLOBAL SETTINGS
    $(window).load(function(){
        if( $('.equalizerv2').length && !isMobile.any ) {
           letsequal();
        }
        let images = document.querySelectorAll("img");
        new LazyLoad(images);
    });

    // Mobile detection
    if (isMobile.any) {
        // Add mobile class to body
        //$('body').addClass('mobile');
        // Enable buy download button
        $('#modalDownloadConfirmation').removeClass('disabled')
    }

    function equalWidth(){
        var windowWidth = $(window).width(),
            items       = $('.menudesktop > ul > li').length;

        if( windowWidth > 1361 ){
            MaxItem = 8;
            
        } else if( windowWidth <= 1360 ) {
            MaxItem = 6;
        }

        var equal = (windowWidth / MaxItem) - (5/100);

        if( !$('.menumore').length ){
            $(".menudesktop > ul.list-inline").append('<li class="resmenu-parent"><a href="#" class="menumore" style="background: #1f1f1f">More +</a><ul id="restmenu"></ul></li>');
            var moveStart = MaxItem-1;

            if( !$('#restmen li').length ){
                $('.menudesktop > ul > li').slice( moveStart,items ).appendTo('#restmenu');
            }
        } 

       $('.menudesktop > ul > li').find('a').css('width', equal);
    }

    equalWidth();

    $(window).on('resize', function(){
        equalWidth();
    });

    $('.news-item').each(function(){
        $(this).addClass('in-viewport');
    });

    $('.app-card').each(function(){
        $(this).addClass('in-viewport');
    });

    if (isMobile.tablet && ($(window).height() > $(window).width())) {
        $('.latest-news-item').each(function(){
            $(this).removeClass('news-wide');
        });
    }

    // Lazy Load
    // $('.lazy').lazyload({
    //     effect : 'fadeIn',
    //     failure_limit : 16
    // });

    // Main Menu
    $('#menu-button').click(function(){
        $(this).toggleClass('active');
        $('body').toggleClass('menu-active').removeClass('cat-active');
        $('#userMenu').addClass('hidden');
    });

    // Login
    $('.user-login').click(function (){
        $(this).toggleClass('active');
        $('#userMenu').toggleClass('hidden');
        $('#loginContainer').removeClass('hidden');
        $('#forgotContainer').addClass('hidden');
    });

    $('#goToForgot, #goToLogin').click(function(){
        $('#loginContainer').toggleClass('hidden');
        $('#forgotContainer').toggleClass('hidden');
    });

    // Category Menu
    $('.app-category h3').click(function(){
        $(this).toggleClass('active');
        $('body').toggleClass('cat-active');
    });

    // Close Notification
    $('#closeNotice').on('click', function(){
        $.when($('.notice').fadeOut(500)).done(function() {
            $('body').removeClass('has-notice');
        });
    });

    /* Login card tab */
    $('.tambahpoin_widget').each(function(){
        var widget = $(this),
            trig = $(this).find('.login-tab-head a');

        trig.click(function(e){
            e.preventDefault();
            var button = $(this),
                target = button.attr('data-target');

            widget.find('.tch.'+target).show().siblings('.tch').hide();
            button.addClass('active').siblings().removeClass('active');

            if( widget.length ) {
                $('.ghostradio label.input-'+target).click();
            }
        });
    });

    $('.member-tab-button a').on('click', function(e){
        e.preventDefault();
        var btn = $(this);
            target = btn.data('target');
        btn.addClass('active').siblings('a').removeClass('active');
        $(target).toggle().siblings().toggle();
    });

    if( isMobile.any ) {
        $('.makeitDrop').each(function(i){
            var box = $(this),
                tabs = box.find('.nav-tabs'),
                tabelement = tabs.find('li');
            console.log($(tabelement[0]).text());

            if( tabelement.length ) {
                box.prepend('<a href="" class="btn btn-light btn-block text-left m-nav-toggle py-3 rds-0"><span>'+$(tabelement[0]).text()+'</span><i class="fa fa-chevron-down float-right right"></i></a>')
            }
            
            box.find('.m-nav-toggle').on('click', function(e){
                e.preventDefault();
                $(this).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
                $(this).parents('.makeitDrop').find('.nav-tabs').toggle();
                return false;
            });

            tabelement.find('a').on('click', function(){
                $(this).parents('.nav-tabs').hide();
                box.find('.m-nav-toggle span').text($(this).text());
                console.log($(this).text());
            });
        });
    }

    if( $('.login-container').length ) {
        var widget = $(this),
            trig = $(this).find('.login-tab-head a');

        trig.click(function(e){
            e.preventDefault();
            var button = $(this),
                target = button.attr('data-target');

            widget.find('.tch.'+target).show().siblings('.tch').hide();
            button.addClass('active').siblings().removeClass('active');
        });
    }

    $('.show-denganpulsa').click(function(e){
        e.preventDefault();
        $('.denganpulsa').show();
         $('.tch > .my-games, .tch > h2').hide();
    });

    $('.show-tanpapulsa').click(function(e){
        e.preventDefault();
        $('.tanpapulsa').show();
         $('.tch > .my-games, .tch > h2').hide();
    });

    $( "select[name=operator]" ).change(function() {
        // Check input( $( this ).val() ) for validity here
        if( $( this ).val() == 'indosat' ) {
            $('.form3').show();
            $('.forxl').hide();
            $('.untukxl').hide();
            $('.untuktelkomsel').hide();
            $('.untukindosat').show();
        } else if( $( this ).val() == 'telkomsel' ){
            $('.form3').hide();
            $('.untukindosat').hide();
            $('.untukxl').hide();
            $('.untuktelkomsel').show();
        } else {
            $('.form3').hide();
            $('.forxl').show();
            $('.untukxl').show();
            $('.untukindosat').hide();
            $('.untuktelkomsel').hide();
        }
    });

    if( $( "#select[name=operator] option:selected" ).val() == 'indosat'){
        $('.form3').show();
        $('.forxl').hide();
        $('.untukxl').hide();
        $('.untuktelkomsel').hide();
        $('.untukindosat').show();
    } else if( $( this ).val() == 'telkomsel' ){
        $('.form3').hide();
        $('.untukindosat').hide();
        $('.untukxl').hide();
        $('.untuktelkomsel').show();
    } else {
        $('.form3').hide();
        $('.forxl').show();
        $('.untukxl').show();
        $('.untukindosat').hide();
        $('.untuktelkomsel').hide();
    }

    $( "input[type=radio][name=operator]" ).change(function() {
        // Check input( $( this ).val() ) for validity here
        if( $( this ).val() == 'indosat' ) {
            $('.untukxl').hide();
            $('.untukindosat').show();
            $('.untuktelkomsel').hide();
        } else if( $( this ).val() == 'xlaxis' ) {
            $('.untukxl').show();
            $('.untukindosat').hide();
            $('.untuktelkomsel').hide();
        } else if( $( this ).val() == 'telkomsel' ){
            $('.form3').hide();
            $('.untukindosat').hide();
            $('.untukxl').hide();
            $('.untuktelkomsel').show();
        } 
    });

    if( $( "input[type=radio][name=operator]:checked" ).val() == 'indosat'){
        $('.untukxl').hide();
        $('.untukindosat').show();
        $('.untuktelkomsel').hide();
    } else if( $( "input[type=radio][name=operator]:checked" ).val() == 'telkomsel' ){
        $('.untukindosat').hide();
        $('.untukxl').hide();
        $('.untuktelkomsel').show();
    } else {
        $('.untukxl').show();
        $('.untukindosat').hide();
        $('.untuktelkomsel').hide();
    }

    // Headroom
    /*$('header').headroom({
        offset: 52,
        tolerance : 5,
        onPin : function() {
            $('.app-category, #news-category.is_stuck').css('top', 52);
        },
        onUnpin : function() {
            $('.app-category, #news-category').css('top', 0);
        },
        onTop : function() {

        },
        onNotTop : function() {
            $('#news-category').css('top', 0);
        }
    });*/

    // Timeago
    // $('time.posted').timeago();

    /*// Slick Slider Coba Games Ini
    if (isMobile.phone) {
        $('.games-list, .app-index-member .app-small').slick({
            dots     : false,
            infinite : false,
            autoplay : false,
            adaptiveHeight: false,
            slidesToShow: 2,
            swipeToSlide: true,
            slidesToScroll: 2
        });
        $(window).on('load', function(){
            $('.SC_TBlock .ts-wrapper').slick({
                dots     : false,
                infinite : false,
                autoplay : false,
                adaptiveHeight: false,
                slidesToShow: 2,
                swipeToSlide: true,
                slidesToScroll: 2
            });
        });
    };*/

    $('.widtitle').each(function(){
        $(this).wrapInner('<span></span>')
    });

    if (!isMobile.phone) {
        // Sticky article aside
        $(window).on('load', function(){
            $('.pakai #sidebar').stick_in_parent({
                offset_top: 72,
                parent: '#main',
                spacer: false
            });
        });

        $(window).on('load', function(){
            $('.tpsidebar').stick_in_parent({
                offset_top: 134,
                parent: '.parent-sticky',
                spacer: false
            });
        });

        // Sticky article aside
        $(window).on('load', function(){
            $('.nestedsidebar').stick_in_parent({
                offset_top: 0,
                parent: '.nestedmain',
                spacer: false
            });
        });

        // Sticky article aside
        $(window).on('load', function(){
            $('.stickyplis').stick_in_parent({
                offset_top: 130,
                parent: '.helppage',
                spacer: false
            });
        });

        $('.pakai #sidebar, .nestedsidebar, .stickyplis, .tpsidebar').on('sticky_kit:bottom', function(e) {
            $(this).parent().css('position', 'static');
        }).on('sticky_kit:unbottom', function(e) {
            $(this).parent().css('position', 'relative');
        });
    }

    if( $('.induk').length ){
        $('.induk').each(function(){
            var ini = $(this);
            ini.find('>a').click(function(e){
                e.preventDefault();
                ini.find('.fa').toggleClass('fa-plus fa-minus');
                ini.find('>ul').slideToggle(200);
            });

            if( ini.find('li').hasClass('active') ){
                ini.find('.child-menu').show();
                ini.find('.fa').toggleClass('fa-minus fa-plus');
            }
        });
    }

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    MEMBER NOTIF DISMISS
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
    $('.notifdismiss').click(function(e){
        e.preventDefault();
        var thebox = $(this).parent('.membernotif');
        thebox.hide(0, function(){
            thebox.remove();
        });
    });
    
    // Sticky items
    if (!isMobile.mobile) {
        $('.sticky').stick_in_parent({
            offset_top: 72
        });
    };

    function validate(formData) {
        var returnData;
        $.ajax({
            url: "{{ URL::to('member/check_login') }}",
            async: false,
            type: 'POST',
            data: formData,
            success: function(data, textStatus, jqXHR) {
                returnData = data;
            }
        });
        if (returnData != 'success') {
            //$('#form-action').validate(returnData, 'parsley-error', 'help-block');
            console.log('error data');
        } else {
            return 'success';
        }
    }

    function save(formData) {
        $("button[title='save']").html("processing data, please wait...");
        $.post("{{ URL::to('member/act_login') }}", formData).done(function(data) {
            if(data == 'success')
            {
                //window.location.href = "{{ url('playworld.local/member')}}";
                window.location.replace("{{ url('/member')}}");
                $('.user-login').html('Logout');
            }
            else
            {
                $("button[title='save']").html("Login");
            }
        });
    }

    function cancel() {
        $('#wp-loading').fadeIn('fast');
        $('.datagrid-panel').fadeIn();
        $('.form-panel').fadeOut();
        $('#wp-loading').fadeOut();
    }

    function form_routes(action) {
        if (action == 'save') {
            var formData = $('#form-action').serialize();
            if (validate(formData) == 'success') {
                save(formData);
            }
        } else {
            cancel();
        }
    }

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    REDEEM
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
    /*$('.app-preview').slick({
        dots            : true,
        infinite        : true,
        autoplay        : true,
        adaptiveHeight  : false
    });*/

    if( ! isMobile.phone ) {
        $('.rdn-box, .equalizeplis').each( function (){
            $(this).equalize({children: '.same_height'});
        });
    }

    if( isMobile.tablet ) {
        $('.tablet_equalizeplis').each( function (){
            $(this).equalize({children: '.same_height'});
        });
    }

    if( ! isMobile.phone ) {
        $('.quiz.row').equalize('outerHeight', {children: '.sameheight'});
    }

    if( ! isMobile.any && $('.measure_target').length ) {
        $(window).load(function(){
            var right_height = $('.measure_target').outerHeight();
            var left_height = $('.left_target').height();
            var top_height = $('.top_height').outerHeight();
            var selisih_kiri = parseInt( right_height - top_height - 116 );
            var selisih = parseInt( right_height - left_height );
            setTimeout(function() {
                $('.left_target .scroll-y').css('height', selisih_kiri);
            }, 2000);
        }); 
    }

    /* REDEEM CATEGORUES */

    if( $('.rscatslide').length ) {
        /*var Flickity = require('flickity');*/

        if( $('.rscatslide').length ) {
            var elem = document.querySelector('.rscatslide');
            var flkty = new Flickity( elem, {
                // options
                pageDots: false,
                cellAlign: 'left',
                wrapAround: true,
                contain: true
            });
        }
    }

    
    if( $('.mbs.wide').length ) {
        /* Redeem detail slideshow */
        function bannerslide() {
            /*var Flickity = require('flickity');*/
            var mbs = document.querySelector('.mbs.wide');
            var flkty = new Flickity( mbs, {
                // options
                cellSelector: '.rpi',
                pageDots: false,
                cellAlign: 'left',
                wrapAround: false,
                prevNextButtons: false,
                autoPlay: true,
                draggable: false
            });
        }
    }
    
    if( $('.mbs.fordesktop').length ) {
        function bannerslide_desktop() {
            /*var Flickity = require('flickity');*/
            var mbs = document.querySelector('.mbs.fordesktop');
            var flkty = new Flickity( mbs, {
                // options
                cellSelector: '.rpi',
                pageDots: false,
                cellAlign: 'left',
                wrapAround: false,
                prevNextButtons: false,
                autoPlay: true,
                draggable: false
            });
        }
    }

    if( $('.hbs.fordesktop').length ) {
        function header_slide() {
            /*var Flickity = require('flickity');*/
            var mbs = document.querySelector('.hbs.fordesktop');
            var flkty = new Flickity( mbs, {
                // options
                cellSelector: '.rpi',
                pageDots: false,
                cellAlign: 'left',
                wrapAround: false,
                prevNextButtons: false,
                autoPlay: true,
                draggable: false
            });
        }
    }

    if( $('.redpro-subitem').length ) {
        function redeempromo_items_mobile() {
            /*var Flickity = require('flickity');*/
            var mbs = document.querySelector('.redpro-subitem');
            var flkty = new Flickity( mbs, {
                // options
                cellSelector: '.col-md-4',
                pageDots: false,
                cellAlign: 'left',
                wrapAround: true,
                contain: true
            });
        }
    }

    if( $('.fpb').length ) {
        function fpb_slide() {
            var count = $('.fpb > .col-xs-12').length;

            if( !isMobile.any ) {
                if( count >= 3 ) {
                    var galleryElems = document.querySelectorAll('.fpb');
                    for ( var i=0, len = galleryElems.length; i < len; i++ ) {
                      var galleryElem = galleryElems[i];
                      new Flickity( galleryElem, {
                        // options
                        cellSelector: '.col-xs-12',
                        pageDots: true,
                        cellAlign: 'left',
                        wrapAround: true,
                        contain: true,
                        autoPlay: false
                      });
                    }
                } else {
                    $('.fpb').css('display', 'block');
                }
            } else {
                var galleryElems = document.querySelectorAll('.fpb');
                for ( var i=0, len = galleryElems.length; i < len; i++ ) {
                  var galleryElem = galleryElems[i];
                  new Flickity( galleryElem, {
                    // options
                    cellSelector: '.col-xs-12',
                    pageDots: true,
                    cellAlign: 'left',
                    wrapAround: true,
                    contain: true,
                    autoPlay: false
                  });
                }
            }
            
        }

        $(window).load(function(){
            fpb_slide();
        })
    }

    if( $('.fpb2').length ) {
        function fpb_slides() {
            var galleryElems = document.querySelectorAll('.fpb2');
            for ( var i=0, len = galleryElems.length; i < len; i++ ) {
              var galleryElem = galleryElems[i];
              new Flickity( galleryElem, {
                // options
                cellSelector: '.cell-item',
                pageDots: false,
                cellAlign: 'left',
                wrapAround: false,
                contain: true,
                autoPlay: false
              });
            }
        }

        $(window).load(function(){
            fpb_slides();
        })
    }

    if( $('.menuslide').length ) {
        function menuslide() {
            var galleryElems = document.querySelectorAll('.menuslide');
            for ( var i=0, len = galleryElems.length; i < len; i++ ) {
              var galleryElem = galleryElems[i];
              new Flickity( galleryElem, {
                // options
                cellSelector: '.cell-item',
                pageDots: false,
                cellAlign: 'left',
                wrapAround: false,
                prevNextButtons: false,
                autoPlay: false,
                freeScroll: true
              });
            }
        }

        $(window).load(function(){
            menuslide();
        })
    }

    $('.m-profile').on('click', function(){
        $('.settingan').toggle();
        $(this).toggleClass('collpased expand')
    });

    if( $('.mobileonlyslides').length ) {
        function fpb_slides() {
            var galleryElems = document.querySelectorAll('.mobileonlyslides');
            for ( var i=0, len = galleryElems.length; i < len; i++ ) {
              var galleryElem = galleryElems[i];
              new Flickity( galleryElem, {
                // options
                cellAlign: 'left',
                pageDots: true,
                autoPlay: false,
                prevNextButtons: true,
                wrapAround: true,
                groupCells: true
              });
            }
        }
        
        if( isMobile.any ) {
            $(window).load(function(){
                fpb_slides();
            })
        }
            
    }

    if( $('.gameslide').length ) {
        function gameslide() {
            var galleryElems = document.querySelector('.gameslide');
            new Flickity( galleryElems, {
              cellAlign: 'left',
              pageDots: true,
              autoPlay: false,
              prevNextButtons: true,
              wrapAround: true,
              groupCells: true
            });
        }

        $(window).load(function(){
            gameslide();
        })
    }

    if( $('.fpb3').length ) {
        function fpb_slides3() {
            var count = $('.fpb3 .cell-item').length;

            if( isMobile.any ) {
                var galleryElems = document.querySelectorAll('.fpb3');
                for ( var i=0, len = galleryElems.length; i < len; i++ ) {
                  var galleryElem = galleryElems[i];
                  new Flickity( galleryElem, {
                    // options
                    cellSelector: '.cell-item',
                    pageDots: false,
                    cellAlign: 'left',
                    wrapAround: false,
                    contain: false,
                    autoPlay: false
                  });
                }
            } else {
                if( count >= 3 ) {
                    var galleryElems = document.querySelectorAll('.fpb3');
                    for ( var i=0, len = galleryElems.length; i < len; i++ ) {
                      var galleryElem = galleryElems[i];
                      new Flickity( galleryElem, {
                        // options
                        cellSelector: '.cell-item',
                        pageDots: false,
                        cellAlign: 'left',
                        wrapAround: false,
                        contain: false,
                        autoPlay: false
                      });
                    }
                }       
            }  
        }

        fpb_slides3();
    }

    if( $('.latest-news-list.compactmode').length ) {
        function latest_news_slider_poin_ekstra() {
            

            var galleryElems = document.querySelectorAll('.latest-news-list');
            for ( var i=0, len = galleryElems.length; i < len; i++ ) {
              var galleryElem = galleryElems[i];
              new Flickity( galleryElem, {
                // options
                    cellSelector: '.col-md-3',
                    pageDots: true,
                    cellAlign: 'left',
                    wrapAround: false,
                    contain: true,
                    adaptiveHeight: false,
                    groupCells: true
              });
            }
        }

        $(window).load(function(){
            latest_news_slider_poin_ekstra();
        });
    }

    if( isMobile.phone ) {
        if( $('.mbs.wide').length ) {
            bannerslide();
        }
        if( $('.mbs.fordesktop').length ) {
            bannerslide_desktop();
        }
        //redeempromo_items_mobile();
    }

    if( ! isMobile.any ){
        if( $('.mbs.fordesktop').length ) {
            bannerslide_desktop();
        }
        if( $('.hbs.fordesktop').length ) {
            header_slide();
        }
    }
    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    MOBILE MENU
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */

    $('#promotoppoin').prependTo('body');
    $('.mt-outtest, .modal, .pw-modal').prependTo('body');

    $('.m_menu_trigger').on('click', function(e){
        e.preventDefault();
        $(this).find('i.fa').toggleClass('fa-search fa-close');
        $('.m_search').toggle();
    });

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    MEGA MENU AJAX
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
    var open = false;
    $('.hdr-m-menutrig').click(function() {
        /*$('.hdr2-tb-menu').toggle(0);*/
        $(this).find('i.fa').toggleClass('fa-bars fa-close')
        if( open == false ) {
            $('body').addClass('trunk-active').removeClass('trunk-inactive');
            open = true;
        } else if( open == true ) {
            $('body').addClass('trunk-inactive').removeClass('trunk-active');
            open = false;
        }
    });

    $('.trunk .close').click(function(e) {
        e.preventDefault();
        $('body').addClass('trunk-inactive').removeClass('trunk-active');
    });

    if( !isMobile.any ){
        $('.hdr2-tb-menu li').each(function(){
            $(this).hover(function(){
                var target = $(this).attr('data-target');
                if ( $(this).is( ".megamenuable" ) ) {
                    $('.pw-megamenu.'+target).show().siblings('.pw-megamenu').hide();
                } else {
                    $('.pw-megamenu').hide();
                }
            });
        });

        $('.pw-megamenu').hover(function(){
            // do nothing bro
        }, function(){
            $('.pw-megamenu').hide();
        });
    }
    

    $('.mm-menu li').each( function(){
        var menu = $(this),
            target = menu.attr('data-target');
        menu.hover(function(){

            if( menu.hasClass('current') ) { return false };
            menu.addClass('current').siblings().removeClass('current');

            $(".mm-rp-ajax-handler[data-megatab='" + target +"']").addClass('active').siblings().removeClass('active');

            /*$('.mm-rp-ajaxcontainer').fadeOut(50, function(){
                $('.mm-rp-ajax-handler').remove();
                setTimeout(function() {

                    setTimeout(function() {
                        $('.mm-rp-ajaxcontainer').fadeIn(50);
                    }, 200);

                }, 200);
            });*/
        });
    } );

    $('.tab-trigger-wrapper > div').click(function(){
        var target = $(this).attr('data-tabtarget');
        if( $(this).hasClass('active') ) return false;
        $(this).addClass('active').siblings().removeClass('active');
        $('.tab_content.'+target).addClass('current').siblings('.tab_content').removeClass('current');
    });

    /*Login logic*/
    $('.ketentuan.berlangganan').click(function(e){
        e.preventDefault();
        $('.popupketentuan.berlangganan').show();
    });
    $('.tutuppopup').click(function(e){
        e.preventDefault();
        $('.popupketentuan.berlangganan').hide();
    });

    /*Login logic*/
    $('.nonberlanggananlink').click(function(e){
        e.preventDefault();
        $('.popupketentuan.nonberlangganan').show();
    });
    $('.tutuppopup').click(function(e){
        e.preventDefault();
        $('.popupketentuan.nonberlangganan').hide();
    });

    /* Payment method */
    $('.paymenttrigger-old').click(function(e){
        e.preventDefault();
        var operator = $("input[name='operator']:checked").val();
        //alert(operator);
        // if($("input[name='tambah_saldo']:checked").length == 0)
        // {
        //     alert('Koin yang akan di beli belum di pilih!');
        //     return false;
        // }
        // if ($("input[name='paymentmethod']:checked").val() && $("input[name='paymentmethod']:checked").val() == 'pulsa' ) {
            var text_harga = $("input[name='tambah_saldo']:checked").attr('data-harga');
            var text_koin  = $("input[name='tambah_saldo']:checked").attr('data-koin');
            var text_extra = $("input[name='tambah_saldo']:checked").attr('data-extra');
            var text_pw    = $("input[name='tambah_saldo']:checked").attr('data-pw');
            var text_sms   = $("input[name='tambah_saldo']:checked").attr('data-sms');
            var text_nomor   = $("input[name='tambah_saldo']:checked").attr('data-nomor'); 

            // var text_harga = $(".paymenttrigger[name='tambah_saldo']").attr('data-harga');
            // //(text_harga);
            // //exit;
            // var text_koin  = $(".paymenttrigger[name='tambah_saldo']").attr('data-koin');
            // //alert(text_koin);
            // //exit;
            // var text_extra = $(".paymenttrigger[name='tambah_saldo']").attr('data-extra');
            // var text_pw    = $(".paymenttrigger[name='tambah_saldo']").attr('data-pw');
            // var text_sms   = $(".paymenttrigger[name='tambah_saldo']").attr('data-sms');
            // var text_nomor   = $(".paymenttrigger[name='tambah_saldo']").attr('data-nomor');            

            $('.text_harga').html(text_harga);
            $('.text_koin').html(text_koin);
            $('.text_extra').html(text_extra);
            $('.text_pw').html(text_pw);
            $('.text_nomor').html(text_nomor);
            $('.text_sms').attr('href', text_sms);
            if(operator == 'indosat')
            {
                $('.beli_pulsa').show();

            }
            else if(operator == 'telkomsel')
            {
                $('.beli_pulsa').show();
            }
            else if(operator == 'smartfren')
            {
                $('.gak_aktif').show();
            }
            else
            {
                $('.beli_pulsa').show();

            }

        // }
        // else
        // {

            /*PROSES TRANSAKSI MIDTRANS*/
            /*var payButton = document.getElementById('pay-button');
            payButton.addEventListener('click', function () {*/

               /* var voucher_id = $("input:radio[name=tambah_saldo]:checked").val();
                //alert(voucher_id);
                //snap.pay('<SNAP_TOKEN>');
                $.post( base_url +'/member/payment/buy', 
                    {
                        _token     :  '{!!csrf_token()!!}',
                       voucher_id  : voucher_id
                    }, 
                    function(response){
                        //alert(response);
                        if(response != 'error')
                        {
                            snap.pay(response);

                        }
                        else
                        {   
                            alert('tejadi kesalahan sistem, harap ulangi kembali');
                        }

                });*/
                // $('.gak_aktif').show();
            /*});*/
        //}
    });

    //$('.payment-mid').click(function(e){
       // e.preventDefault();
        // var operator = $("input[name='operator']:checked").val();
        
        

    $('.paymenttrigger').each(function(){

        var thisbutton = $(this);

        thisbutton.on('click', function(e){
            e.preventDefault();
            var operator = $("input[name='operator']:checked").val();

            var btn = $(this);
            var text_harga = btn.attr('data-harga');
            var text_koin  = btn.attr('data-koin');
            var text_extra = btn.attr('data-extra');
            var text_pw    = btn.attr('data-pw');
            var text_sms   = btn.attr('data-sms');
            var text_nomor   = btn.attr('data-nomor');        

            $('.text_harga').html(text_harga);
            $('.text_koin').html(text_koin);
            $('.text_extra').html(text_extra);
            $('.text_pw').html(text_pw);
            $('.text_nomor').html(text_nomor);
            $('.text_sms').attr('href', text_sms);
            if(operator == 'indosat')
            {
                $('.beli_pulsa').show();

            }
            else if(operator == 'telkomsel')
            {
                $('.beli_pulsa').show();
            }
            else if(operator == 'smartfren')
            {
                $('.gak_aktif').show();
            }
            else
            {
                $('.beli_pulsa').show();

            }
        });

    });

    /*Payment di redeem detail*/
    $('#paymenttrigger_2, .bayar_pulsa').click(function(e){
        e.preventDefault();
        var text_harga = $("input[name='tambah_saldo']:checked").attr('data-harga');
            var text_koin  = $("input[name='tambah_saldo']:checked").attr('data-koin');
            var text_extra = $("input[name='tambah_saldo']:checked").attr('data-extra');
            var text_pw    = $("input[name='tambah_saldo']:checked").attr('data-pw');
            var text_sms   = $("input[name='tambah_saldo']:checked").attr('data-sms');

            var target_koin     = $('.tukar_koin_value').val();
            var target_poin     = $('.tukar_poin_value').val();

            $('.target_koin').html(target_koin);
            $('.target_poin').html(target_poin);
            $('.text-amount-amount.target_koin').html(target_koin);

            $('.text_harga').html(text_harga);
            $('.text_koin').html(text_koin);
            $('.text_extra').html(text_extra);
            $('.text_pw').html(text_pw);
            $('.text_sms').attr('href', text_sms);
            $('.mt-outtest').show();
            $('body').addClass('dead');
    });

    /* ARISAN */
    $('#koin_to_poin_trigger, #use_poin').on('click', function(e){
        e.preventDefault();
        $('.mt-outtest').show();
    });

    $('.trxpopup').each(function(){
        var tombol = $(this),
            target = tombol.attr('data-trx');

        tombol.click(function(e) {
            e.preventDefault();
            $('.mt-outtest.'+target).show();
        });
    });

    $('a.tutupaja').click(function(e) {
        e.preventDefault();
        $('.mt-outtest').hide();
        $('body').removeClass('dead');
    });


    //var atas = parseInt($('#main-wrapper').find('.container').offset().top);
    $('.htc_item .bs-callout:nth-child(1)').removeClass('inactive');

    $(document).ready(function(){
        var hash_help = window.location.hash;
        setTimeout(function() {
            hash_help && $('a[href^="' + hash_help + '"]').trigger('click');
        }, 300);
    

        //click event
        $('.bs-callout').click(function(e){
            e.preventDefault();
            var button = $(this),
                target = button.attr('data-target');
            window.location.hash = this.hash;
            $('.help_isi.'+target).slideDown(300).siblings('.help_isi').slideUp(300);
            $('.accordion_help.'+target).slideToggle(300, function(){
                $('.accordion_help img').each(function(){
                    var img = $(this).attr('src');
                    if( $(target).parent('a').length <= 1  ) {
                        $(this).wrap('<a href="'+img+'" class="zoomplease"></a>');

                        setTimeout(function() {
                            $('.zoomplease').magnificPopup({type:'image'});
                        }, 200);
                        
                    }
                    
                });
            }).siblings('.accordion_help').slideUp(300);
            $(this).removeClass('inactive').siblings().addClass('inactive');

            // $('html, body').animate({
            //     scrollTop: atas
            // }, 300);
        });

        $('.help_isi a:not(.external), .accordion_help a:not(.external)').on('click', function(){
            var link = $(this),
                target = link.attr('href'),
                target2 = target.replace('#acc_', 'asu_');
            console.log(target2);

            $('.htc_item a#'+target2).trigger('click');
        });

        $('.agreement').each(function(){
            var ini = $(this),
                check = ini.find('input[type="checkbox"]'),
                btn = ini.find('.btn');

            check.change(function() {
                if(this.checked) {
                    btn.attr('disabled', false);
                    /*setTimeout(function() {
                       btn.trigger('click'); 
                    }, 200);*/
                } else {
                    btn.attr('disabled', true);
                    btn.stopPropagation();
                }
            });
        });

        $('.btn').click(function(e){
            if( $(this).is('[disabled=disabled]') ) {
                return false;
                alert('anjing');
                $(this).stopPropagation();
            }
        });

        $('.youtube-pop').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    });



    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
    Get parameter value of tab to trigger click halaman bantuan
    oooooooooooooooooooooooooooooooooooooooooooooooooooooo */

    var url_string = window.location.href;
    var url = new URL(url_string);
    var param = url.searchParams.get("tab");

    if( param ) { 
        $('.bs-callout[data-target="' + param + '"]').trigger('click');
    }

    /*$('.alamatlabels li a').click(function(e){
        e.preventDefault();
        var button = $(this),
            target = button.attr('data-target');

        $('.alamat_tab.'+target).slideDown(300).siblings('.alamat_tab').slideUp(300);
        button.addClass('current').parents('li').siblings().find('a').removeClass('current');
    });*/

    $('.tab-pane').each(function(){
        var item = $(this),
            addbutton = item.find('.tambahin'),
            cancelbtn = item.find('.batal'),
            editbtn = item.find('.editbtn'),
            visiblebox = item.find('.visible-box'),
            editbox = item.find('.edit-box'),
            hiddenbox = item.find('.to-add-box');

        addbutton.on('click', function(e) {
            e.preventDefault();
            hiddenbox.show();
            visiblebox.hide();
        });

        cancelbtn.on('click', function(e) {
            e.preventDefault();
            hiddenbox.hide();
            visiblebox.show();
            editbox.hide();
        });

        editbtn.click(function(){
            var id = $(this).attr('data-target');
                $('.edit-box.edit-'+id).show();
                visiblebox.hide();
        });
    });

    $('.prodfilter .card-body.is_list').each(function(){
        $(this).append('<a href="javascript:;" class="togglelist"><span>Lihat Semua</span> <i class="fa fa-chevron-down"></i></a>');
    });

    $(window).load(function(){
        $('.togglelist').each(function(){
            $(this).click(function(e){
                e.preventDefault();
                var button = $(this);
                var thelabel = $(this).parent('.card-body').find('label');
                button.find('span').text(function(i, text){
                  return text === "Lihat Semua" ? "Tutup" : "Lihat Semua";
                });
                button.find('.fa').toggleClass('fa-chevron-down fa-chevron-up')
                thelabel.toggleClass('appear');
            });                
        });

        $('.is_list').each(function(){
            var thebox = $(this),
                thelabel = thebox.find('label'),
                input = thelabel.find('input[type=checkbox]:checked');
            if( input && input.parent('label').not(":visible")) {
                input.parent('label').not(":visible").parent('.is_list').find('.togglelist').text('tutup');
                input.parent('label').not(":visible").parent('.is_list').find('label').addClass('appear');
            }
        }); 
    });

    if( isMobile ) {
        $('.user-settings').on('click', function(e){
            $(this).parent('li').find('>ul').show();
        });
    }

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    NOTIFIKASI
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
    $('.notif-trigger').each(function(){
        var pencet = $(this),
            target = pencet.attr('data-notiftarget');
        pencet.hover(function(){
            $('.notif-box[data-notif="'+target+'"]').show().siblings('.notif-box').hide();
        });
    });

    $('.notif-box').mouseleave(function(){
        $('.notif-box').hide();
    });

    // Reposition
    function repositonMenu() {
        var _position = ($(window).width() - ($('.user-detail').offset().left + $('.user-detail').outerWidth()));
        var _position = (_position - 30);
        $('#usermenu-hov').css('right', _position);
        console.log(_position);
    }

    $(document).ready(function(){
        repositonMenu();
    });
    
    $(window).on('resize', function(){
        repositonMenu();
    });

    $(document).click(function(e) {
        var target = e.target;

        if ( !$(target).is('.notif-box') ) {
            $('.notif-box').hide();
        }
    });        

    if( $('.pemenanglist').length ) {
        $('.pemenanglist').each(function(){
            if( $(this).hasClass('nomore') ) return false;
            var winnerwrap = $(this);
            var jumlah = winnerwrap.find('>li').length;
            if( jumlah >= 3 ) {
                winnerwrap.find('li').hide();
                winnerwrap.find('li:lt(3)').show();
                winnerwrap.parent('.card-body').append('<a href="javascript:;" class="text-small showpemenang btn btn-warning btn-pill">Show All</a>');

                $('.showpemenang').on('click', function(){
                    var button = $(this);
                    winnerwrap.find('li').not('li:lt(3)').toggle();
                    button.text(function(i, text){
                      return text === "Show All" ? "Hide" : "Show All";
                    });
                });
            }
        });
    }

    var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    if( hash ) {
        var thetext1 = hash && $('ul.nav a[href="' + hash + '"]').text();
            $('.subhead').text(thetext1);
    }  
    

    $('.nav-tabs a').click(function(e){
        e.preventDefault();
        var thetext = $(this).text();
        window.location.hash = this.hash;
        $('.subhead').text(thetext);
    });

    $('.transaksikoin a').click(function(){
        var url = $(this).attr('href');
        var hash = url.substring(url.indexOf('#'));
        $('ul.nav a[href="' + hash + '"]').tab('show');
    });
    
    $('.globalToggler').on('click', function(){
        var target = $(this).data('target');
            $('#'+target).toggle();
        if( $(this).find('.fa-bars').length || $(this).find('.fa-times').length ) {
            $(this).find('.fa').toggleClass('fa-bars fa-times');
            console.log('ini bars');
        } else {
            $(this).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
        }
    });

    // Social Mobile
    if (isMobile.any){
        $('.social_more').on('click', function(){
            $(this).addClass('hidden').parent().css('width', 0);
            $('.more-button').removeClass('hidden-xs');
        });            
    }

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
    News Gallery
    oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
    var carouselContainers = document.querySelectorAll('.gallery_v2_wrapper');

    for ( var i=0; i < carouselContainers.length; i++ ) {
        var container = carouselContainers[i];
        initCarouselContainer( container );
    }

    function initCarouselContainer( container ) {
        $(window).load(function(){
            var carousel = container.querySelector('.gallery_v2');
            var nestedslide = container.querySelector('.nestedslide');
            if(isMobile.any) {
                var autoheightmode = true;
            } else {
                var autoheightmode = true;
            }
            console.log(autoheightmode);
            var flkty = new Flickity( carousel, {
                adaptiveHeight: autoheightmode,
                draggable: false
            });
            var nested = new Flickity( nestedslide, {
                cellAlign: 'left',
                draggable: false
            });

            var carouselStatus = container.querySelector('.gallv2_progress');

            function updateStatus() {
                var slideNumber = flkty.selectedIndex + 1;
                var actualnumber = flkty.slides.length - 1;

                carouselStatus.innerHTML = '<span>' + slideNumber + '</span>/' + actualnumber;

                if( slideNumber == flkty.slides.length ) {
                    $('.gallery_v2').addClass('hidebutton');
                } else {
                    $('.gallery_v2').removeClass('hidebutton');
                }
            }
            updateStatus();

            flkty.on( 'select', updateStatus );

            $('.replay_gallery').click(function(e){
                e.preventDefault();
                flkty.select( 0 );
            });
        });
    }

    $('.gall_tab_trigger a').on('click', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-target');
        $(this).addClass('current').siblings('a').removeClass('current');
        $('.tabtarget.'+target).addClass('active').siblings('.tabtarget').removeClass('active');
    });

    $('.hadiah, .sts-trf-item').each(function(){
        var hadiah = $(this),
            toggler = hadiah.find('.toggler');

        toggler.on('click', function(){
            hadiah.find('.toggle-box').slideToggle(100);
            toggler.text(function(i, text){
              return text == "Tampilkan" ? "Sembunyikan" : "Tampilkan";
            });
            toggler.next('.fa').toggleClass('fa-chevron-down fa-chevron-up')
        });
    });

    $('.hadiah').each(function(){
        var $hadiah = $(this),
            $share = $hadiah.find('.fb-sb'),
            $btn = $hadiah.find('.disabled');

        $share.on('click', function(){
            setTimeout(function() {
                $hadiah.find('.disabled').removeClass('disabled');
            }, 4500);
        });        
    });

    if( $('.calculator').length ) {
        var total = 0;
        $('input[name=poin], input[name=koin]').on('keyup',function() {
            //if( $(this).val() != false ) {
                calculate();
            //}            
        });

        var calculate = function() {
            var kointot = parseInt( $('input[name=koin]').val()*20 );
            var pointot = parseInt( $('input[name=poin]').val() );

            if( isNaN(parseFloat(kointot)) ) {
                kointot = 0;
            }

            if( isNaN(parseFloat(pointot)) ) {
                pointot = 0;
            }

            var result = parseInt(kointot + pointot);
            $('.targethitung').text(result);
        };
    }

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
    TERTINGGI WIDGET
    oooooooooooooooooooooooooooooooooooooooooooooooooooooo */
    $('.tertinggiload').click(function(e){
        e.preventDefault();
        var btn = $(this);
        $(this).parents('.tertinggi').find('li').show(0, function(){
            setTimeout(function() {
                $('li.morelnk').hide();
                $(document.body).trigger("sticky_kit:recalc");
            }, 200);
        });        
    });   

    /* ooooooooooooooooooooooooooooooooooooooooooooooooooooo
    GALLERY ZOOM
    oooooooooooooooooooooooooooooooooooooooooooooooooooooo */ 
    if( !isMobile.any ) {
        //initiate the plugin and pass the id of the div containing gallery images
        $("#img_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, responsive: false}); 

        //pass the images to Fancybox
        $("#img_01").bind("click", function(e) {  
          var ez =   $('#img_01').data('elevateZoom'); 
            $.fancybox(ez.getGalleryList());
          return false;
        });
    }

    if( isMobile.any ) {
        $('#gal1 a').click(function(e){
            e.preventDefault();
            $img = $(this).attr('data-image');
            $('.ez-wrap').find('img').attr('src', $img);
            $(this).parent().addClass('active');
        });
    }

    if( isMobile.any ) {
        $('.eta-head + ul').hide();
    
        $('.eta-head').on('click', function(e){
            e.preventDefault();
            $('.eta-head').toggleClass(' expanded');
            $('.eta-head + ul').toggle();
        });
    }

    // KELAS 
    $('.rating').each(function(){
        var $wrap = $(this),
            $span = $wrap.find('span');
        $span.css( 'width', $span.attr('data-width') + '%' );
    });

    $('.rdmore').each(function(){
        $(this).readmore({
            speed: 75,
            collapsedHeight: 70,
            lessLink: '<a href="#">Read less</a>'
        });
    });

    $('.big-rdmore').each(function(){
        var jajaja = $(this);
        $(this).readmore({
            speed: 75,
            collapsedHeight: 500,
            moreLink: '<a href="#">Load More <i class="fa fa-plus"></i></a>',
            lessLink: '<a href="#">Close</a>',
            afterToggle: function(trigger, element, expanded) {
                if(expanded) { // The "Close" link was clicked
                    jajaja.addClass('buka');
                } else if( ! expanded ) {
                    jajaja.removeClass('buka');
                }
            }
        });
    });

    if( isMobile.any ) {
        $('.rdmore-m').each(function(){
            $(this).readmore({
                speed: 75,
                collapsedHeight: 67,
                lessLink: '<a href="#">Read less</a>'
            });
        });
    }

    // Prevent hash jump
    if (location.hash) {               // do the test straight away
        window.scrollTo(0, 0);         // execute it straight away
        setTimeout(function() {
            window.scrollTo(0, 0);     // run it a bit later also for browser compatibility
        }, 1);
    }

    // HELP PAGE IMAGE ZOOM
    $(document).ready(function(){
        $('.help_isi img').each(function(){
            var img = $(this).attr('src');
            $(this).wrap('<a href="'+img+'" class="zoomplease"></a>');
        });
    });

    $(window).load(function(){
        $('.zoomplease').magnificPopup({type:'image'});
    });
});

var mySVGsToInject = document.querySelectorAll('img.nav-icon');

// Do the injection
SVGInjector(mySVGsToInject);