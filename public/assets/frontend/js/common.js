// Configure RequireJS
requirejs.config({
    baseUrl: base_url+'/'+js_url,
    paths: {
        // Libraries
        'jquery':       'lib/jquery',
        'bootstrap':    'lib/bootstrap.min',
        'clipboard':    'lib/clipboard.min',
        'countdown':    'lib/jquery.countdown.min',
        'colorthief':   'lib/color-thief.min',
        'fancybox':     'lib/fancybox/jquery.fancybox',
        'fullpage':     'lib/fullpage/jquery.fullpage.min',
        'headroom':     'lib/headroom/headroom',
        'headroomjq':   'lib/headroom/jquery.headroom',
        'lodash':       'lib/lodash.min',
        'gsaptimeline': 'lib/TimelineMax.min',
        'gsaptween':    'lib/TweenMax.min',
        'isMobile':     'lib/isMobile.min',
        'lazyload':     'lib/jquery.lazyload.min',
        'scrollbar':    'lib/perfectscrollbar/perfect-scrollbar.jquery.min',
        'slick':        'lib/slick/slick.min',
        'sticky':       'lib/jquery.sticky-kit.min',
        'svginjector':  'lib/svg-injector.min',
        'swiper':       'lib/swiper/swiper.min',
        'timeago':      'lib/jquery.timeago',
        'wow':          'lib/wow.min',
        'stickyMojo':   'lib/stickyMojo',
        'equalizer':    'lib/equalize.min',
        'flickity':     'lib/flickity',
    },
    shim: {
        bootstrap:      ['jquery'],
        fancybox:       ['jquery'],
        sticky:         ['jquery'],
        equalizer:      ['jquery'],
        lazyload:       ['jquery'],
        headroomjq:     ['jquery']
    }
});