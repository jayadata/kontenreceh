define(['jquery', 'isMobile', 'slick', 'swiper', 'flickity'], function($){

    $(function(){
        // Social Mobile
        if (isMobile.any){
            $('.social_more').on('click', function(){
                $(this).addClass('hidden').parent().css('width', 0);
                $('.more-button').removeClass('hidden-xs');
            });            
        }

        /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
        News Gallery
        oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
        var Flickity = require('flickity');
        var carouselContainers = document.querySelectorAll('.gallery_v2_wrapper');

        for ( var i=0; i < carouselContainers.length; i++ ) {
            var container = carouselContainers[i];
            initCarouselContainer( container );
        }

        function initCarouselContainer( container ) {
            var carousel = container.querySelector('.gallery_v2');
            var nestedslide = container.querySelector('.nestedslide');
            var flkty = new Flickity( carousel, {
                adaptiveHeight: true,
                draggable: false
            });
            var nested = new Flickity( nestedslide, {
                cellAlign: 'left',
                draggable: false
            });

            var carouselStatus = container.querySelector('.gallv2_progress');

            function updateStatus() {
                var slideNumber = flkty.selectedIndex + 1;
                var actualnumber = flkty.slides.length - 1;

                carouselStatus.innerHTML = '<span>' + slideNumber + '</span>/' + actualnumber;

                if( slideNumber == flkty.slides.length ) {
                    $('.gallery_v2').addClass('hidebutton');
                } else {
                    $('.gallery_v2').removeClass('hidebutton');
                }
            }
            updateStatus();

            flkty.on( 'select', updateStatus );

            $('.replay_gallery').click(function(e){
                e.preventDefault();
                flkty.select( 0 );
            });
        }

        $('.gall_tab_trigger a').on('click', function(e) {
            e.preventDefault();
            var target = $(this).attr('data-target');
            $(this).addClass('current').siblings('a').removeClass('current');
            $('.tabtarget.'+target).addClass('active').siblings('.tabtarget').removeClass('active');
        });
    });

    $(function(){
        // Convert News Category to be swipe-able
        if (isMobile.any) {
            // Add swiper container and classes
            $('.news-category:not(.menudesktop)').addClass('swiper-container')
                .children('ul').addClass('swiper-wrapper')
                .children('li').addClass('swiper-slide');
        };

        if (isMobile.tablet) {
            var catSwiper = new Swiper ('.news-category:not(.menudesktop)', {
                grabCursor: true,
                loop: false,
                slidesPerView: 6,
                spaceBetween: 0,
                freeMode: true,
                freeModeSticky: true,
                variableWidth: true
            });
        };

        if (isMobile.phone) {
            var catSwiper = new Swiper ('.news-category:not(.menudesktop)', {
                grabCursor: true,
                loop: false,
                slidesPerView: 'auto',
                spaceBetween: 0,
                freeMode: true,
                freeModeSticky: true,
                variableWidth: true,
            });
        };

    });

});

/*define(['jquery', 'stickyMojo'], function($){

    $(function(){
        $('#sidebar').stickyMojo({footerID: '#promo', contentID: '#main'});
    });

});*/