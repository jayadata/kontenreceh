define(['jquery', 'slick'], function($){

    $(function(){
        // Home Hero
        $('#hero-slider').slick({
            dots     : true,
            infinite : true,
            autoplay : true,
            adaptiveHeight: false
        });
    });

});