define(['jquery', 'fullpage'], function(){

	$(function(){
	    // About FullPage
		$('#about-wrapper').fullpage({
			fixedElements: '#header',
			navigation: true
		});
	});

});