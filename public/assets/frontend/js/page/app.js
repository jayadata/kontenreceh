define(['jquery', 'slick'], function($){

    $(function(){

        // Preview Slider
        $('.app-preview').slick({
            dots            : false,
            infinite        : true,
            autoplay        : true,
            adaptiveHeight  : false
        });

        // Select permalink
        $('#shortlink').on('click', function(){
            $(this).select();
        });
    });

});