define(['jquery', 'isMobile', 'bootstrap', 'equalizer', 'wow', 'lazyload', 'headroom', 'headroomjq', 'sticky','timeago', 'flickity'], 
    function(){
    require([
        'module/svginjector'
    ]);
    var equalize = require('equalizer');
    jQuery(function($){
        // Mobile detection
        if (isMobile.any) {
            // Add mobile class to body
            $('body').addClass('mobile');
            // Enable buy download button
            $('#modalDownloadConfirmation').removeClass('disabled')
        }

        function equalWidth(){
            var windowWidth = $(window).width(),
                items       = $('.menudesktop > ul > li').length;

            if( windowWidth > 1361 ){
                MaxItem = 8;
                
            } else if( windowWidth <= 1360 ) {
                MaxItem = 6;
            }

            var equal = (windowWidth / MaxItem) - (5/100);

            if( !$('.menumore').length ){
                $(".menudesktop > ul.list-inline").append('<li class="resmenu-parent"><a href="#" class="menumore" style="background: #1f1f1f">More +</a><ul id="restmenu"></ul></li>');
                var moveStart = MaxItem-1;

                if( !$('#restmen li').length ){
                    $('.menudesktop > ul > li').slice( moveStart,items ).appendTo('#restmenu');
                }
            } 

           $('.menudesktop > ul > li').find('a').css('width', equal);
            
        }

        equalWidth();

        $(window).on('resize', function(){
            equalWidth();
        });

        $('.news-item').each(function(){
            $(this).addClass('in-viewport');
        });

        $('.app-card').each(function(){
            $(this).addClass('in-viewport');
        });

        if (isMobile.tablet && ($(window).height() > $(window).width())) {
            $('.latest-news-item').each(function(){
                $(this).removeClass('news-wide');
            });
        }

        // Lazy Load
        // $('.lazy').lazyload({
        //     effect : 'fadeIn',
        //     failure_limit : 16
        // });

        // Main Menu
        $('#menu-button').click(function(){
            $(this).toggleClass('active');
            $('body').toggleClass('menu-active').removeClass('cat-active');
            $('#userMenu').addClass('hidden');
        });

        // Login
        $('.user-login').click(function (){
            $(this).toggleClass('active');
            $('#userMenu').toggleClass('hidden');
            $('#loginContainer').removeClass('hidden');
            $('#forgotContainer').addClass('hidden');
        });

        $('#goToForgot, #goToLogin').click(function(){
            $('#loginContainer').toggleClass('hidden');
            $('#forgotContainer').toggleClass('hidden');
        });

        // Category Menu
        $('.app-category h3').click(function(){
            $(this).toggleClass('active');
            $('body').toggleClass('cat-active');
        });

        // Close Notification
        $('#closeNotice').on('click', function(){
            $.when($('.notice').fadeOut(500)).done(function() {
                $('body').removeClass('has-notice');
            });
        });

        /* Login card tab */
        $('.tambahpoin_widget').each(function(){
            var widget = $(this),
                trig = $(this).find('.login-tab-head a');

            trig.click(function(e){
                e.preventDefault();
                var button = $(this),
                    target = button.attr('data-target');

                widget.find('.tch.'+target).show().siblings('.tch').hide();
                button.addClass('active').siblings().removeClass('active');

                if( widget.length ) {
                    $('.ghostradio label.input-'+target).click();
                }
            });
        });

        if( $('.login-container').length ) {
            var widget = $(this),
                trig = $(this).find('.login-tab-head a');

            trig.click(function(e){
                e.preventDefault();
                var button = $(this),
                    target = button.attr('data-target');

                widget.find('.tch.'+target).show().siblings('.tch').hide();
                button.addClass('active').siblings().removeClass('active');
            });
        }

        $('.show-denganpulsa').click(function(e){
            e.preventDefault();
            $('.denganpulsa').show();
             $('.tch > .my-games, .tch > h2').hide();
        });

        $('.show-tanpapulsa').click(function(e){
            e.preventDefault();
            $('.tanpapulsa').show();
             $('.tch > .my-games, .tch > h2').hide();
        });

        $( "select[name=operator]" ).change(function() {
            // Check input( $( this ).val() ) for validity here
            if( $( this ).val() == 'indosat' ) {
                $('.form3').show();
                $('.forxl').hide();
            } else {
                $('.form3').hide();
                $('.forxl').show();
            }
        });

        // Headroom
        $('header').headroom({
            offset: 52,
            tolerance : 5,
            onPin : function() {
                $('.app-category, #news-category.is_stuck').css('top', 52);
            },
            onUnpin : function() {
                $('.app-category, #news-category').css('top', 0);
            },
            onTop : function() {

            },
            onNotTop : function() {
                $('#news-category').css('top', 0);
            }
        });

        // Timeago
        // $('time.posted').timeago();

        /*// Slick Slider Coba Games Ini
        if (isMobile.phone) {
            $('.games-list, .app-index-member .app-small').slick({
                dots     : false,
                infinite : false,
                autoplay : false,
                adaptiveHeight: false,
                slidesToShow: 2,
                swipeToSlide: true,
                slidesToScroll: 2
            });
            $(window).on('load', function(){
                $('.SC_TBlock .ts-wrapper').slick({
                    dots     : false,
                    infinite : false,
                    autoplay : false,
                    adaptiveHeight: false,
                    slidesToShow: 2,
                    swipeToSlide: true,
                    slidesToScroll: 2
                });
            });
        };*/

        $('.widtitle').each(function(){
            $(this).wrapInner('<span></span>')
        });

        if (!isMobile.phone) {
            // Sticky article aside
            $(window).on('load', function(){
                $('.pakai #sidebar').stick_in_parent({
                    offset_top: 72,
                    parent: '#main',
                    spacer: false
                });
            });

            $(window).on('load', function(){
                $('.tpsidebar').stick_in_parent({
                    offset_top: 134,
                    parent: '.parent-sticky',
                    spacer: false
                });
            });

            // Sticky article aside
            $(window).on('load', function(){
                $('.nestedsidebar').stick_in_parent({
                    offset_top: 0,
                    parent: '.nestedmain',
                    spacer: false
                });
            });

            // Sticky article aside
            $(window).on('load', function(){
                $('.stickyplis').stick_in_parent({
                    offset_top: 62,
                    parent: '.helppage',
                    spacer: false
                });
            });

            $('.pakai #sidebar, .nestedsidebar, .stickyplis, .tpsidebar').on('sticky_kit:bottom', function(e) {
                $(this).parent().css('position', 'static');
            }).on('sticky_kit:unbottom', function(e) {
                $(this).parent().css('position', 'relative');
            });
        }

        if( $('.induk').length ){
            $('.induk').each(function(){
                var ini = $(this);
                ini.find('>a').click(function(e){
                    e.preventDefault();
                    ini.find('.fa').toggleClass('fa-plus fa-minus');
                    ini.find('>ul').slideToggle(200);
                });

                if( ini.find('li').hasClass('active') ){
                    ini.find('.child-menu').show();
                    ini.find('.fa').toggleClass('fa-minus fa-plus');
                }
            });
        }

        /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
        MEMBER NOTIF DISMISS
        oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
        $('.notifdismiss').click(function(e){
            e.preventDefault();
            var thebox = $(this).parent('.membernotif');
            thebox.hide(0, function(){
                thebox.remove();
            });
        });
        
        // Sticky items
        if (!isMobile.mobile) {
            $('.sticky').stick_in_parent({
                offset_top: 72
            });
        };

        function validate(formData) {
            var returnData;
            $.ajax({
                url: "{{ URL::to('member/check_login') }}",
                async: false,
                type: 'POST',
                data: formData,
                success: function(data, textStatus, jqXHR) {
                    returnData = data;
                }
            });
            if (returnData != 'success') {
                //$('#form-action').validate(returnData, 'parsley-error', 'help-block');
                console.log('error data');
            } else {
                return 'success';
            }
        }

        function save(formData) {
            $("button[title='save']").html("processing data, please wait...");
            $.post("{{ URL::to('member/act_login') }}", formData).done(function(data) {
                if(data == 'success')
                {
                    //window.location.href = "{{ url('playworld.local/member')}}";
                    window.location.replace("{{ url('/member')}}");
                    $('.user-login').html('Logout');
                }
                else
                {
                    $("button[title='save']").html("Login");
                }
            });
        }

        function cancel() {
            $('#wp-loading').fadeIn('fast');
            $('.datagrid-panel').fadeIn();
            $('.form-panel').fadeOut();
            $('#wp-loading').fadeOut();
        }

        function form_routes(action) {
            if (action == 'save') {
                var formData = $('#form-action').serialize();
                if (validate(formData) == 'success') {
                    save(formData);
                }
            } else {
                cancel();
            }
        }

        /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
        REDEEM
        oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
        /*$('.app-preview').slick({
            dots            : true,
            infinite        : true,
            autoplay        : true,
            adaptiveHeight  : false
        });*/

        if( ! isMobile.phone ) {
            $('.rdn-box, .equalizeplis').each( function (){
                $(this).equalize({children: '.same_height'});
            });
        }

        if( isMobile.tablet ) {
            $('.tablet_equalizeplis').each( function (){
                $(this).equalize({children: '.same_height'});
            });
        }

        if( ! isMobile.phone ) {
            $('.quiz.row').equalize('outerHeight', {children: '.sameheight'});
        }

        /* REDEEM CATEGORUES */

        if( $('.rscatslide').length ) {
            var Flickity = require('flickity');

            if( $('.rscatslide').length ) {
                var elem = document.querySelector('.rscatslide');
                var flkty = new Flickity( elem, {
                    // options
                    pageDots: false,
                    cellAlign: 'left',
                    wrapAround: true,
                    contain: true
                });
            }
        }

        
        if( $('.mbs.wide').length ) {
            /* Redeem detail slideshow */
            function bannerslide() {
                var Flickity = require('flickity');
                var mbs = document.querySelector('.mbs.wide');
                var flkty = new Flickity( mbs, {
                    // options
                    cellSelector: '.rpi',
                    pageDots: false,
                    cellAlign: 'left',
                    wrapAround: false,
                    prevNextButtons: false,
                    autoPlay: true,
                    draggable: false
                });
            }
        }
        
        if( $('.mbs.fordesktop').length ) {
            function bannerslide_desktop() {
                var Flickity = require('flickity');
                var mbs = document.querySelector('.mbs.fordesktop');
                var flkty = new Flickity( mbs, {
                    // options
                    cellSelector: '.rpi',
                    pageDots: false,
                    cellAlign: 'left',
                    wrapAround: false,
                    prevNextButtons: false,
                    autoPlay: true,
                    draggable: false
                });
            }
        }

        if( $('.hbs.fordesktop').length ) {
            function header_slide() {
                var Flickity = require('flickity');
                var mbs = document.querySelector('.hbs.fordesktop');
                var flkty = new Flickity( mbs, {
                    // options
                    cellSelector: '.rpi',
                    pageDots: false,
                    cellAlign: 'left',
                    wrapAround: false,
                    prevNextButtons: false,
                    autoPlay: true,
                    draggable: false
                });
            }
        }

        if( $('.redpro-subitem').length ) {
            function redeempromo_items_mobile() {
                var Flickity = require('flickity');
                var mbs = document.querySelector('.redpro-subitem');
                var flkty = new Flickity( mbs, {
                    // options
                    cellSelector: '.col-md-4',
                    pageDots: false,
                    cellAlign: 'left',
                    wrapAround: true,
                    contain: true
                });
            }
        }

        if( isMobile.phone ) {
            if( $('.mbs.wide').length ) {
                bannerslide();
            }
            if( $('.mbs.fordesktop').length ) {
                bannerslide_desktop();
            }
            //redeempromo_items_mobile();
        }

        if( ! isMobile.any ){
            if( $('.mbs.fordesktop').length ) {
                bannerslide_desktop();
            }
            if( $('.hbs.fordesktop').length ) {
                header_slide();
            }
        }
        /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
        MOBILE MENU
        oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
        $('.m_menu_trigger').on('click', function(e){
            e.preventDefault();
            $(this).find('i.fa').toggleClass('fa-search fa-close');
            $('.m_search').toggle();
        });

        /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
        MEGA MENU AJAX
        oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
        $('.hdr-m-menutrig').click(function() {
            $('.hdr2-tb-menu').toggle(0);
            $(this).find('i.fa').toggleClass('fa-bars fa-close');
        });

        if( !isMobile.any ){
            $('.hdr2-tb-menu li').each(function(){
                $(this).hover(function(){
                    var target = $(this).attr('data-target');
                    if ( $(this).is( ".megamenuable" ) ) {
                        $('.pw-megamenu.'+target).show().siblings('.pw-megamenu').hide();
                    } else {
                        $('.pw-megamenu').hide();
                    }
                });
            });

            $('.pw-megamenu').hover(function(){

            }, function(){
                $('.pw-megamenu').hide();
            });
        }
        

        $('.mm-menu li').each( function(){
            var menu = $(this),
                target = menu.attr('data-target');
            menu.hover(function(){

                if( menu.hasClass('current') ) { return false };
                menu.addClass('current').siblings().removeClass('current');

                $(".mm-rp-ajax-handler[data-megatab='" + target +"']").addClass('active').siblings().removeClass('active');

                /*$('.mm-rp-ajaxcontainer').fadeOut(50, function(){
                    $('.mm-rp-ajax-handler').remove();
                    setTimeout(function() {

                        setTimeout(function() {
                            $('.mm-rp-ajaxcontainer').fadeIn(50);
                        }, 200);

                    }, 200);
                });*/
            });
        } );

        $('.tab-trigger-wrapper > div').click(function(){
            var target = $(this).attr('data-tabtarget');
            if( $(this).hasClass('active') ) return false;
            $(this).addClass('active').siblings().removeClass('active');
            $('.tab_content.'+target).addClass('current').siblings('.tab_content').removeClass('current');
        });

        /*Login logic*/
        $('.ketentuan.berlangganan').click(function(e){
            e.preventDefault();
            $('.popupketentuan.berlangganan').show();
        });
        $('.tutuppopup').click(function(e){
            e.preventDefault();
            $('.popupketentuan.berlangganan').hide();
        });

        /*Login logic*/
        $('.nonberlanggananlink').click(function(e){
            e.preventDefault();
            $('.popupketentuan.nonberlangganan').show();
        });
        $('.tutuppopup').click(function(e){
            e.preventDefault();
            $('.popupketentuan.nonberlangganan').hide();
        });

        /* Payment method */
        $('.paymenttrigger-old').click(function(e){
            e.preventDefault();
            var operator = $("input[name='operator']:checked").val();
            //alert(operator);
            // if($("input[name='tambah_saldo']:checked").length == 0)
            // {
            //     alert('Koin yang akan di beli belum di pilih!');
            //     return false;
            // }
            // if ($("input[name='paymentmethod']:checked").val() && $("input[name='paymentmethod']:checked").val() == 'pulsa' ) {
                var text_harga = $("input[name='tambah_saldo']:checked").attr('data-harga');
                var text_koin  = $("input[name='tambah_saldo']:checked").attr('data-koin');
                var text_extra = $("input[name='tambah_saldo']:checked").attr('data-extra');
                var text_pw    = $("input[name='tambah_saldo']:checked").attr('data-pw');
                var text_sms   = $("input[name='tambah_saldo']:checked").attr('data-sms');
                var text_nomor   = $("input[name='tambah_saldo']:checked").attr('data-nomor'); 

                // var text_harga = $(".paymenttrigger[name='tambah_saldo']").attr('data-harga');
                // //(text_harga);
                // //exit;
                // var text_koin  = $(".paymenttrigger[name='tambah_saldo']").attr('data-koin');
                // //alert(text_koin);
                // //exit;
                // var text_extra = $(".paymenttrigger[name='tambah_saldo']").attr('data-extra');
                // var text_pw    = $(".paymenttrigger[name='tambah_saldo']").attr('data-pw');
                // var text_sms   = $(".paymenttrigger[name='tambah_saldo']").attr('data-sms');
                // var text_nomor   = $(".paymenttrigger[name='tambah_saldo']").attr('data-nomor');            

                $('.text_harga').html(text_harga);
                $('.text_koin').html(text_koin);
                $('.text_extra').html(text_extra);
                $('.text_pw').html(text_pw);
                $('.text_nomor').html(text_nomor);
                $('.text_sms').attr('href', text_sms);
                if(operator == 'indosat')
                {
                    $('.beli_pulsa').show();

                }
                else if(operator == 'telkomsel')
                {
                    $('.gak_aktif').show();
                }
                else if(operator == 'smartfren')
                {
                    $('.gak_aktif').show();
                }
                else
                {
                    $('.beli_pulsa').show();

                }

            // }
            // else
            // {

                /*PROSES TRANSAKSI MIDTRANS*/
                /*var payButton = document.getElementById('pay-button');
                payButton.addEventListener('click', function () {*/

                   /* var voucher_id = $("input:radio[name=tambah_saldo]:checked").val();
                    //alert(voucher_id);
                    //snap.pay('<SNAP_TOKEN>');
                    $.post( base_url +'/member/payment/buy', 
                        {
                            _token     :  '{!!csrf_token()!!}',
                           voucher_id  : voucher_id
                        }, 
                        function(response){
                            //alert(response);
                            if(response != 'error')
                            {
                                snap.pay(response);

                            }
                            else
                            {   
                                alert('tejadi kesalahan sistem, harap ulangi kembali');
                            }

                    });*/
                    // $('.gak_aktif').show();
                /*});*/
            //}
        });

        $('.paymenttrigger').each(function(){

            var thisbutton = $(this);

            thisbutton.on('click', function(e){
                e.preventDefault();
                var operator = $("input[name='operator']:checked").val();

                var btn = $(this);
                var text_harga = btn.attr('data-harga');
                var text_koin  = btn.attr('data-koin');
                var text_extra = btn.attr('data-extra');
                var text_pw    = btn.attr('data-pw');
                var text_sms   = btn.attr('data-sms');
                var text_nomor   = btn.attr('data-nomor');        

                $('.text_harga').html(text_harga);
                $('.text_koin').html(text_koin);
                $('.text_extra').html(text_extra);
                $('.text_pw').html(text_pw);
                $('.text_nomor').html(text_nomor);
                $('.text_sms').attr('href', text_sms);
                if(operator == 'indosat')
                {
                    $('.beli_pulsa').show();

                }
                else if(operator == 'telkomsel')
                {
                    $('.gak_aktif').show();
                }
                else if(operator == 'smartfren')
                {
                    $('.gak_aktif').show();
                }
                else
                {
                    $('.beli_pulsa').show();

                }
            });

        });

        /*Payment di redeem detail*/
        $('#paymenttrigger_2').click(function(e){
            e.preventDefault();
            var text_harga = $("input[name='tambah_saldo']:checked").attr('data-harga');
                var text_koin  = $("input[name='tambah_saldo']:checked").attr('data-koin');
                var text_extra = $("input[name='tambah_saldo']:checked").attr('data-extra');
                var text_pw    = $("input[name='tambah_saldo']:checked").attr('data-pw');
                var text_sms   = $("input[name='tambah_saldo']:checked").attr('data-sms');

                $('.text_harga').html(text_harga);
                $('.text_koin').html(text_koin);
                $('.text_extra').html(text_extra);
                $('.text_pw').html(text_pw);
                $('.text_sms').attr('href', text_sms);
                $('.mt-outtest').show();
        });

        /* ARISAN */
        $('#koin_to_poin_trigger, #use_poin').on('click', function(e){
            e.preventDefault();
            $('.mt-outtest').show();
        });

        $('.trxpopup').each(function(){
            var tombol = $(this),
                target = tombol.attr('data-trx');

            tombol.click(function(e) {
                e.preventDefault();
                $('.mt-outtest.'+target).show();
            });
        });

        $('a.tutupaja').click(function(e) {
            e.preventDefault();
            $('.mt-outtest').hide();
        });


        $('.htc_item .bs-callout:nth-child(1)').removeClass('inactive');
        $('.bs-callout').click(function(){
            var button = $(this),
                target = button.attr('data-target');
            $('.help_isi.'+target).slideDown(300).siblings('.help_isi').slideUp(300);
            $('.accordion_help.'+target).slideToggle(300).siblings('.accordion_help').slideUp(300);

            $(this).removeClass('inactive').siblings().addClass('inactive');
        });

        /*$('.alamatlabels li a').click(function(e){
            e.preventDefault();
            var button = $(this),
                target = button.attr('data-target');

            $('.alamat_tab.'+target).slideDown(300).siblings('.alamat_tab').slideUp(300);
            button.addClass('current').parents('li').siblings().find('a').removeClass('current');
        });*/

        $('.tab-pane').each(function(){
            var item = $(this),
                addbutton = item.find('.tambahin'),
                cancelbtn = item.find('.batal'),
                editbtn = item.find('.editbtn'),
                visiblebox = item.find('.visible-box'),
                editbox = item.find('.edit-box'),
                hiddenbox = item.find('.to-add-box');

            addbutton.on('click', function(e) {
                e.preventDefault();
                hiddenbox.show();
                visiblebox.hide();
            });

            cancelbtn.on('click', function(e) {
                e.preventDefault();
                hiddenbox.hide();
                visiblebox.show();
                editbox.hide();
            });

            editbtn.click(function(){
                var id = $(this).attr('data-target');
                    $('.edit-box.edit-'+id).show();
                    visiblebox.hide();
            });
        });

        $('.prodfilter .card-body.is_list').each(function(){
            $(this).append('<a href="javascript:;" class="togglelist">Lihat Semua</a>');
        });

        $(window).load(function(){
            $('.togglelist').each(function(){
                $(this).click(function(e){
                    e.preventDefault();
                    var button = $(this);
                    var thelabel = $(this).parent('.card-body').find('label');
                    button.text(function(i, text){
                      return text === "Lihat Semua" ? "Tutup" : "Lihat Semua";
                    });
                    thelabel.toggleClass('appear');
                });                
            });

            $('.is_list').each(function(){
                var thebox = $(this),
                    thelabel = thebox.find('label'),
                    input = thelabel.find('input[type=checkbox]:checked');
                if( input && input.parent('label').not(":visible")) {
                    input.parent('label').not(":visible").parent('.is_list').find('.togglelist').text('tutup');
                    input.parent('label').not(":visible").parent('.is_list').find('label').addClass('appear');
                }
            }); 
        });

        if( isMobile ) {
            $('.user-settings').on('click', function(e){
                $(this).parent('li').find('>ul').show();
            });
        }

        /* ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
        NOTIFIKASI
        oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo */
        $('.notif-trigger').each(function(){
            var pencet = $(this),
                target = pencet.attr('data-notiftarget');
            pencet.hover(function(){
                $('.notif-box[data-notif="'+target+'"]').show().siblings('.notif-box').hide();
            });
        });

        $('.notif-box').mouseleave(function(){
            $('.notif-box').hide();
        });

        $(document).click(function(e) {
            var target = e.target;

            if ( !$(target).is('.notif-box') ) {
                $('.notif-box').hide();
            }
        });        

        if( $('.pemenanglist').length ) {
            $('.pemenanglist').each(function(){
                var winnerwrap = $(this);
                var jumlah = winnerwrap.find('>li').length;
                if( jumlah >= 3 ) {
                    winnerwrap.find('li').hide();
                    winnerwrap.find('li:lt(3)').show();
                    winnerwrap.parent('.card-body').append('<a href="javascript:;" class="text-small showpemenang btn btn-warning btn-pill">Show All</a>');

                    $('.showpemenang').on('click', function(){
                        var button = $(this);
                        winnerwrap.find('li').not('li:lt(3)').toggle();
                        button.text(function(i, text){
                          return text === "Show All" ? "Hide" : "Show All";
                        });
                    });
                }
            });
        }

        var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        if( hash ) {
            var thetext1 = hash && $('ul.nav a[href="' + hash + '"]').text();
                $('.subhead').text(thetext1);
        }  
        

        $('.nav-tabs a').click(function(){
            var thetext = $(this).text();
            window.location.hash = this.hash;
            $('.subhead').text(thetext);
        });

        $('.transaksikoin a').click(function(){
            var url = $(this).attr('href');
            var hash = url.substring(url.indexOf('#'));
            $('ul.nav a[href="' + hash + '"]').tab('show');
        });
        
        $('.globalToggler').on('click', function(){
            var target = $(this).data('target');
                $('#'+target).toggle();
            if( $(this).find('.fa-bars').length || $(this).find('.fa-times').length ) {
                $(this).find('.fa').toggleClass('fa-bars fa-times');
                console.log('ini bars');
            } else {
                $(this).find('.fa').toggleClass('fa-chevron-down fa-chevron-up');
            }
        });
    });
});