define(['jquery', 'sticky'], function($){

    $(function(){
        // Sticky News Category
        $('#news-category').stick_in_parent({
            offset_top: 0
        });
    });

});