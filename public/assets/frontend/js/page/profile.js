define(['jquery', 'isMobile'], function($) {

    $(function() {
        // Profile My Games
        /*$('.user-games tr').click(function() {
            $(this).toggleClass('expand').siblings().removeClass('expand');
        });*/

        if (isMobile.phone){
            $('.mobile-user-menu').click(function(){
                $(this).toggleClass('onOpen');
                $('#profile-menu').toggle();
            });
        }

        $('.hadiah').each(function(){
            var hadiah = $(this),
                toggler = hadiah.find('.toggler');

            toggler.on('click', function(){
                hadiah.find('.toggle-box').slideToggle(100);
                toggler.text(function(i, text){
                  return text == "Tampilkan" ? "Sembunyikan" : "Tampilkan";
                });
                toggler.next('.fa').toggleClass('fa-chevron-down fa-chevron-up')
            });
        });
    });

});