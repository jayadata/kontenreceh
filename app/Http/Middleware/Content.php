<?php

namespace App\Http\Middleware;

use Closure;

class Content
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $request->get()
        return $next($request);
    }
}
