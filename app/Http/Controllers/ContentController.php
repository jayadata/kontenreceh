<?php

namespace App\Http\Controllers;

use DB;
use App\Dn;
use App\Code;
use ZipArchive;
use App\Content;
use Carbon\Carbon;
use App\ContentImage;
use App\ContentReview;
use App\ContentAccount;
use App\ContentCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(Request $request)
    {
        // $data['content_account'] = ContentAccount::where('slug', $slug)->first();
        $data['content_account'] = ContentAccount::where('is_published', 1)->first();
        $data['content']         = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->take(6)->get();
        $data['count']           = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->count();

        return view('frontend.fakta.materi.index', $data);
    }

    public function index(Request $request, $trx)
    {
        /* index sementara off */
        /* 
            if ($request->get('category') == '') {
                $data['content_account']      = ContentAccount::where('is_published', 1)->get();
            }else{
                $data['content_account']      = ContentAccount::where('is_published', 1)->where('category_id', $request->get('category'))->get();
            }

            $data['content_categories']   = ContentCategory::where('is_published', 1)->get();
            
            return view('frontend.fakta.index', $data); 
        */

        // $data['content_account'] = ContentAccount::where('slug', $slug)->first();
        $data['content_account'] = ContentAccount::where('is_published', 1)->first();
        $data['content']         = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->take(6)->get();
        $data['count']           = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->count();

        if ($trx != 'NULL') {
            $session_id = substr($trx, -4);
            $msisdn     = substr($trx, 0, -4);

            $checkDn = Dn::where(DB::raw('RIGHT(session_id, 4)'), $session_id)
                ->where('msisdn', $msisdn)
                ->orderBy('created_at', 'DESC')
                ->first();

            if (isset($checkDn)) {
                $code = Code::where('trx', $checkDn->session_id)->first();
                if (isset($code)) {
                    if ($code->status == 0) {
                        return view('frontend.trx.index', compact('code'));
                        // return redirect()->route('vip.form', [$checkCode->encrypt]);    
                    } else {
                        return abort(404);
                    }
                } else {
                    /* create ctach */ }
            } else {
                return view('frontend.fakta.materi.index', $data);
            }
        } else {
            return view('frontend.fakta.materi.index', $data);
        }



        /* 
            if (isset($checkDn)) {
                $checkCode = Code::where('trx', $checkDn->session_id)->first();
                
                if (isset($checkCode)) {
                    if ($checkCode->status == 0) {
                        return redirect()->route('vip.form', ['?='.$checkCode->encrypt]);    
                    }else{
                        return abort(404);
                    }
                }

                return view('frontend.trx.index');

            }else{
                $data['content_account'] = ContentAccount::where('is_published', 1)->first();
                $data['content']         = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->take(6)->get();
                $data['count']           = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->count();
        
                return view('frontend.fakta.materi.index', $data);
            } 
        */
    }

    public function catalog(Request $request, $slug = 'qomiqu')
    {
        $data['content_account'] = ContentAccount::where('slug', $slug)->first();
        $data['content']         = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->take(6)->get();
        $data['count']           = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->count();

        return view('frontend.fakta.materi.index', $data);
    }

    public function loadmore(Request $request)
    {
        $data['content_account'] = ContentAccount::where('slug', $request->slug)->first();
        $data['count']           = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->count();
        $data['more']            = Content::where('account_id', $data['content_account']->id)->where('is_published', 1)->skip($request->offset)->take($data['count'])->get();

        return view('frontend.fakta.loadmore', $data);
    }

    public function rating(Request $request)
    {
        if (Auth::user()) {
            $reiview = new ContentReview;
            $reiview->customer_id   = Auth::user()->id;
            $reiview->where         = $request->input('where');
            $reiview->where_id      = $request->input('id');
            $reiview->rate          = $request->input('rating');
            $reiview->review        = $request->input('review');
            $reiview->save();

            return 200;
        } else {
            return 201;
        }
    }

    public function reviewMore(Request $request)
    {
        // $data['review'] = KelasReview::where('where', $request->input('where'))->where('where_id', $request->input('id'))->where('id', '>', $request->input('loadmore'))->paginate(3);

        // $data['review'] = KelasReview::where('where', $request->input('where'))->where('where_id', $request->input('id'))->paginate(3)->setCurrentPage(2);

        // dd($data['review']['currentPage']);

        // $data['review'] = KelasReview::where('where', $request->input('where'))->where('where_id', $request->input('id'))->get();

        $data['review'] = FaktaReview::review($request->input('where'), $request->input('id'));
        // dd($data['review']);

        // $data['aku']           = Arisan::PerhituganMantap($data);
        $tes = collect($data['review']);

        $article = collect(json_decode($tes));
        // $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPage = $request->input('paging');
        // dd($currentPage);
        $perPage = 3;
        $currentResults = $article->slice(($currentPage - 1) * $perPage, $perPage)->all();

        $paginate =  new LengthAwarePaginator($currentResults, $article->count(), $perPage);
        $paginate = $paginate->setPath('');
        $data['review'] = $paginate;

        // dd($article->slice(($currentPage - 1) * $perPage, $perPage)->total());

        // dd($data['review']);

        if ($currentPage > $data['review']->lastPage()) {
            die();
        }

        return view('frontend.fakta.materi.reviewmore', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show(Content $content, $slug)
    {
        if (Auth::user()) {
            $data['content'] = Content::where('slug', $slug)->first();

            $data['review_id']      = $data['content']->id;
            $data['review_where']   = 'content';

            $data['review'] = ContentReview::where('where', 'content')->where('where_id', $data['content']->id)->take(3)->get();

            if (Auth::user()) {
                $data['reviewed'] = ContentReview::where('where', 'content')->where('where_id', $data['content']->id)->where('customer_id', Auth::user()->id)->get();
            }
            return view('frontend.fakta.materi.detail', $data);
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content)
    {
        //
    }

    /* Download */

    public function download($id)
    {
        $image = ContentImage::where('id', $id)->first();
        $link = contentImageUrl($image->image);
        $name = strtolower(str_replace('-', ' ', $image->image));

        @$target = file_get_contents($link);
        if ($target != null) {
            $fileName = $name . '.png';

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            ob_clean();
            ob_end_flush();
            $handle = fopen($link, 'rb');
            while (!feof($handle)) {
                echo fread($handle, 1000);
            }
            return true;
        }

        return false;
    }

    /* Download All */

    public function downloadAll($id)
    {
        $data = ContentImage::where('content_id', $id)->get();

        $files = $data; /*Image array*/

        # create new zip opbject
        $zip = new ZipArchive();

        # create a temp file & open it
        $tmp_file = tempnam('.', '');
        $zip->open($tmp_file, ZipArchive::CREATE);

        # loop through each file
        foreach ($files as $file) {
            $link = contentImageUrl($file->image);

            # download file
            $download_file = file_get_contents($link);

            #add it to the zip
            $zip->addFromString(basename($file->image), $download_file);
        }

        # close zip
        $zip->close();

        # send the file to the browser as a download
        header('Content-disposition: attachment; filename=download.zip');
        header('Content-type: application/zip');
        readfile($tmp_file);
    }
}
