<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContentBuy;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Code;

class MemberController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

    public function profile(){
        return view('member_new_design.pengaturan.profile');
    }

    public function content(){

        $data['content'] = ContentBuy::where('customer_id', Auth::user()->id)->paginate();
        
        return view('member_new_design.list_content', $data);
    }

    public function profileUpdate(Request $request)
    {
        $customer = User::where('msisdn', $request->input('msisdn'))->where('id', $request->input('id'))->first();
        if(isset($customer)){
            if ($request->hasFile('avatara')) 
            {
                $image = $request->file('avatar');
                $filename  = microtime(true) . '.' . $image->getClientOriginalExtension();
    
                /*ukuran thumbs*/
                $image_thumb = Image::make($image->getRealPath())->fit(120, 120);
                $image_thumb = $image_thumb->stream();
                Storage::put(config('laratezz.avatar_storage').$filename, $image_thumb->__toString());
    
                $avatar = $filename;
            }
                
            $customer->email  = $request->input('email');
            $customer->name   = $request->input('name');
            $customer->telp   = $request->input('telp');
    
            $customer->save();
            
            return redirect('member/profile')->with('message', 'Data Berhasil di Update');
        }else{
            return redirect('member/profile')->with('message', 'Something wrong please contact admin');
        }
        
        
    }

    public function contentRedeem(){
        if (Auth::user()) {
            
            $data['code'] = Code::where('msisdn', Auth::user()->msisdn)->orderBy('created_at', 'DESC')->paginate();
            return view('member_new_design.list_code', $data)->with('i');
        }
    }
}
