<?php

namespace App\Http\Controllers;

use App\ContentReview;
use Illuminate\Http\Request;

class ContentReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContentReview  $contentReview
     * @return \Illuminate\Http\Response
     */
    public function show(ContentReview $contentReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContentReview  $contentReview
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentReview $contentReview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContentReview  $contentReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentReview $contentReview)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContentReview  $contentReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentReview $contentReview)
    {
        //
    }
}
