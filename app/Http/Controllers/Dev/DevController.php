<?php

namespace App\Http\Controllers\Dev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class DevController extends Controller
{
    public function devAccount(Request $request){
        $email              = 'dev@ionex.co.id';
        $password           = '1234';

        $login              = Auth::attempt(['email'    => $email, 'password' => $password]);
        if($login)
        {
            // dd(Auth::user());
            // return Auth::user()->email;
            return redirect('/');

        }
        else
        {
            return 'gagal';
        }
    }

    public function createAccount(Request $request){
        return User::create([
            'name' => 'Developer',
            'email' => 'dev@ionex.co.id',
            'password' => Hash::make('1234'),
        ]);
    }
}
