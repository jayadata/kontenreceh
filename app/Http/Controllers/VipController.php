<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Content;
use App\ContentBuy;
use App\Code;
use App\Dn;
use App\Voucher;
use Illuminate\Support\Facades\Session;

class VipController extends Controller
{
    public function index(Request $request){

        /* old
            $trx        = $request->get('trx');

            $session_id = substr($trx, -4);
            $msisdn     = substr($trx, 0, -4);
        

            $checkDn = Dn::where(DB::raw('RIGHT(session_id, 4)'), $session_id)
                ->where('msisdn', $msisdn)
                ->orderBy('created_at', 'DESC')
                ->first();

            if (isset($checkDn)) {
                $checkCode = Code::where('trx', $checkDn->session_id)->first();
                
                if (isset($checkCode)) {
                    if ($checkCode->status == 0) {
                        return redirect()->route('vip.form', ['?='.$checkCode->encrypt]);    
                    }else{
                        return abort(404);
                    }
                }
            }

            return view('frontend.trx.index'); 
        */
    }

    public function submit(Request $request){
    
        $customer = $request->input('customer');
        $code     = $request->input('code');
        $date     = date('Y-m-d');
        
        $check_customer = User::where('msisdn', $customer)->first();
        if (isset($check_customer)) {
            $check_code     = Code::where('code', $code)->where('status', 0)->orderBy('created_at', 'DESC')->first();
            
            if (isset($check_code)) {
                $dn         = Dn::where('session_id', $check_code->trx)->orderBy('created_at', 'DESC')->first();
                
                if (isset($dn)) {
                    $voucher    = Voucher::where('charge', $dn->price)->first();
                    $koin       = ($voucher->coin)/10;
                    $day        = round($koin);

                    if ($dn->type == 'PUSH1') {

                        if(substr($check_customer->msisdn, 0, 2) == '62'){
                            $counter = 1;
                        }else{
                            $counter = 2;
                        }

                        if ($date > $check_customer->expire) {
                            $expire = date("Y-m-d", strtotime("+$day day"));
                        }else{
                            $expire = date("Y-m-d", strtotime($check_customer->expire."+$day day"));
                        }

                        $msg        = 'VIP sukses ditambahkan';
                        $url        = 'https://playworld.id/member';

                        $check_customer->counter    = $counter;
                        $check_customer->expire     = $expire;
                        $check_customer->save();

                        if ($check_customer) {
                            /*Jika Semua proses sudah selesai*/

                            $balance = new Balance;
                            $balance->customer_id           = $check_customer->id;
                            $balance->balance_code          = balance_code();
                            $balance->nominal               = $koin;
                            $balance->value                 = $koin;
                            $balance->status_balance        = 1;
                            $balance->save();

                            $balance_log = new BalanceLog;
                            $balance_log->customer_id       = $check_customer->id;
                            $balance_log->balance_code      = $balance->balance_code;
                            $balance_log->nominal           = $koin;
                            $balance_log->type              = 'beli';
                            $balance_log->keterangan        = 'TRX CODE'.$balance->balance_code;
                            $balance_log->save();   
                        }

                    }else{
                        if ($dn->type == 'PUSHX') {

                            /*Set Counter for inc vip*/ //ganti code, nanti bikin algoritma. ceknya ke dn untuk counternya
                            if ($check_customer->counter == 0) {
                                $day  = $day+1;
                                $koin = $koin+10;
                            }else{
                                $day = $day;
                            }

                            if ($date > $check_customer->expire) {
                                /*Customer Expire banget*/
                                $expire = date("Y-m-d", strtotime("+$day day"));
                            }else{
                                /*Tanggal sekarang blm melebihin expire vip customer*/
                                $expire = date("Y-m-d", strtotime($check_customer->expire."+$day day"));
                            }
                            
                            if ($check_customer->counter == 0 || $check_customer->counter == NULL) {
                                /*Counter Inc*/
                                if(substr($check_customer->msisdn, 0, 2) == '62'){
                                    $counter    = $check_customer->counter + 1;
                                }else{
                                    $counter    = $check_customer->counter + 2;
                                }
                                $check_customer->counter = $counter;
                            }else{
                                /*Counter Dec*/
                                $counter    = $check_customer->counter - 1;
                                $check_customer->counter = $counter;
                            }

                            $msg        = 'VIP sukses ditambahkan';
                            $url        = 'https://qomiqu/member/profile';

                            $check_customer->expire = $expire;
                            $check_customer->save();

                            if ($check_customer) {
                                /*Jika Semua proses sudah selesai*/

                                $balance = new Balance;
                                $balance->customer_id           = $check_customer->id;
                                $balance->balance_code          = balance_code();
                                $balance->nominal               = $koin;
                                $balance->value                 = $koin;
                                $balance->status_balance        = 1;
                                $balance->save();

                                $balance_log = new BalanceLog;
                                $balance_log->customer_id       = $check_customer->id;
                                $balance_log->balance_code      = $balance->balance_code;
                                $balance_log->nominal           = $koin;
                                $balance_log->type              = 'beli';
                                $balance_log->keterangan        = 'TRX CODE'.$balance->balance_code;
                                $balance_log->save();   
                            }

                        }else{
                            $vocuher = Voucher::where('code', 'like', '%' . $dn->subkey . '%')->where('pulsa', 1)->first();
                            if(isset($vocuher)){
                                if(substr($dn->subkey, 0, 2) == 'Q4'){
                                    Session::put('harga', $dn->price);
                                    $materi = Content::where('voucher_id', $vocuher->id)->first();
                                    
                                    $msg        = 'Pembelian '.$materi->title.' Sukses';
                                    if(env('APP_ENV') == 'local'){
                                        $url        = 'http://localhost/kr/public/comic/'.$materi->slug; 
                                    }else{
                                        $url        = 'https://qomiqu.com/comic/'.$materi->slug; 
                                    }

                                    $buy = new ContentBuy();
                                    $buy->customer_id   = $check_customer->id;
                                    $buy->content_id    = $materi->id;
                                    $buy->save();   
                                }else{
                                    /*NON EVENT*/
                                    if ($date > $check_customer->expire) {
                                        $expire = date("Y-m-d", strtotime("+$day day"));
                                    }else{
                                        $expire = date("Y-m-d", strtotime($check_customer->expire."+$day day"));
                                    }
                                    $msg        = 'VIP sukses ditambahkan';
                                    $url        = 'https://qomiqu/member';

                                    $check_customer->expire = $expire;
                                    $check_customer->save();
                                }
                            }else{
                                return response()->json(['status' => '0', 'message' => 'Terjadi Kesalahan Sistem, Silahkan hubungi admin', 'link' => '']);
                            }
                        }
                    }

                    $check_code->use_to = $check_customer->msisdn;
                    $check_code->status = 1;
                    $check_code->save();
                    
                    return response()->json(['status' => '1', 'message' => $msg, 'link' => $url]);
                }else{
                    /* create catch */
                }
            }else{
                return response()->json(['status' => '0', 'message' => 'Code tidak ditemukan atau kode sudah expire', 'link' => '']);
            }    
        }else{
            return response()->json(['status' => '0', 'message' => 'Customer tidak ditemukan', 'link' => '']);
        }
        
    }

    public function xlSend(Request $request){
        $type = $request->input('type');
        if ($type == 'REG') {
            $voucher = $request->input('voucher');
            $data    = Voucher::where('code', $voucher)->first();
            
            $number_awal  = $request->input('nomorhandphone');
            $number_awal  = str_replace(' ', '', $number_awal);
            
            if (substr($number_awal, 0, 1) == '0') {
                $from = '/' . preg_quote(substr($number_awal, 0, 1), '/') . '/';
                $number_awal = preg_replace($from, '+62', $number_awal, 1);
            } else if (substr($number_awal, 0, 3) == '+62') {
                $number_awal = $number_awal;
            } else {
                $number_awal = '+62' . $number_awal;
            }

            $userkey = "sindbe"; //userkey lihat di zenziva
            $passkey = "pwd"; // set passkey di zenziva
            $telepon = $number_awal;
            $message = 'Anda akan beli keanggotaan VIP BERLANGGANAN Rp. '.number_format($data->amount).'. Silahkan ketik REG<spasi>PW ke 97789 untuk melanjutkan, jika tidak, abaikan SMS ini. CS: 02122457232';
            $url = "https://reguler.zenziva.net/apps/smsapi.php?";
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey=' . $userkey . '&passkey=' . $passkey . '&nohp=' . $telepon . '&pesan=' . urlencode($message));
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            $results = curl_exec($curlHandle);
            curl_close($curlHandle);

            $XMLdata = new \SimpleXMLElement($results);
            $status = $XMLdata->message[0]->text;

            if ($status) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $voucher = $request->input('voucher');
            $data    = Voucher::where('code', $voucher)->first();
            
            $number_awal  = $request->input('nomorhandphone');
            $number_awal  = str_replace(' ', '', $number_awal);
            
            if (substr($number_awal, 0, 1) == '0') {
                $from = '/' . preg_quote(substr($number_awal, 0, 1), '/') . '/';
                $number_awal = preg_replace($from, '+62', $number_awal, 1);
            } else if (substr($number_awal, 0, 3) == '+62') {
                $number_awal = $number_awal;
            } else {
                $number_awal = '+62' . $number_awal;
            }

            $userkey = "sindbe"; //userkey lihat di zenziva
            $passkey = "pwd"; // set passkey di zenziva
            $telepon = $number_awal;
            $message = 'Anda akan beli komik QOMIQU.COM Rp '.number_format($data->amount).'. Silahkan ketik KOMIK7<spasi>'.substr($data->code, 7).' kirim ke 99559 atau abaikan SMS utk membatalkan. Info : 02122457232';
            $url = "https://reguler.zenziva.net/apps/smsapi.php?";
            
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey=' . $userkey . '&passkey=' . $passkey . '&nohp=' . $telepon . '&pesan=' . urlencode($message));
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            $results = curl_exec($curlHandle);
            curl_close($curlHandle);

            $XMLdata = new \SimpleXMLElement($results);
            $status = $XMLdata->message[0]->text;

            if ($status) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function indosatSend(Request $request){
        $type = $request->input('type');
        if ($type == 'REG') {
            $voucher = $request->input('voucher');
            $data    = Voucher::where('code', $voucher)->first();
            
            $number_awal  = $request->input('nomorhandphone');
            $number_awal  = str_replace(' ', '', $number_awal);
            
            if (substr($number_awal, 0, 1) == '0') {
                $from = '/' . preg_quote(substr($number_awal, 0, 1), '/') . '/';
                $number_awal = preg_replace($from, '+62', $number_awal, 1);
            } else if (substr($number_awal, 0, 3) == '+62') {
                $number_awal = $number_awal;
            } else {
                $number_awal = '+62' . $number_awal;
            }

            $userkey = "sindbe"; //userkey lihat di zenziva
            $passkey = "pwd"; // set passkey di zenziva
            $telepon = $number_awal;
            $message = 'Anda akan beli keanggotaan VIP BERLANGGANAN Rp. '.number_format($data->amount).'. Silahkan ketik REG<spasi>PW ke 97789 untuk melanjutkan, jika tidak, abaikan SMS ini. CS: 02122457232';
            $url = "https://reguler.zenziva.net/apps/smsapi.php?";
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey=' . $userkey . '&passkey=' . $passkey . '&nohp=' . $telepon . '&pesan=' . urlencode($message));
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            $results = curl_exec($curlHandle);
            curl_close($curlHandle);

            $XMLdata = new \SimpleXMLElement($results);
            $status = $XMLdata->message[0]->text;

            if ($status) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $voucher = $request->input('voucher');
            $data    = Voucher::where('code', $voucher)->first();
            
            $number_awal  = $request->input('nomorhandphone');
            $number_awal  = str_replace(' ', '', $number_awal);
            
            if (substr($number_awal, 0, 1) == '0') {
                $from = '/' . preg_quote(substr($number_awal, 0, 1), '/') . '/';
                $number_awal = preg_replace($from, '+62', $number_awal, 1);
            } else if (substr($number_awal, 0, 3) == '+62') {
                $number_awal = $number_awal;
            } else {
                $number_awal = '+62' . $number_awal;
            }

            $userkey = "m89k62"; //userkey lihat di zenziva
            $passkey = "pwd"; // set passkey di zenziva
            $telepon = $number_awal;
            $message = 'Anda akan beli komik QOMIQU.COM Rp '.number_format($data->amount).'. Silahkan ketik KOMIK7<spasi>'.substr($data->code, 7).' kirim ke 99559 atau abaikan SMS utk membatalkan. Info : 02122457232';
            $url = "https://alpha.zenziva.net/apps/smsapi.php?";
            
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey=' . $userkey . '&passkey=' . $passkey . '&nohp=' . $telepon . '&pesan=' . urlencode($message));
            curl_setopt($curlHandle, CURLOPT_HEADER, 0);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
            curl_setopt($curlHandle, CURLOPT_POST, 1);
            $results = curl_exec($curlHandle);
            curl_close($curlHandle);

            $XMLdata = new \SimpleXMLElement($results);
            $status = $XMLdata->message[0]->text;

            if ($status) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
