<?php

namespace App\Http\Controllers;

use App\ContentCategory;
use Illuminate\Http\Request;

class ContentCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContentCategory  $contentCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ContentCategory $contentCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContentCategory  $contentCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentCategory $contentCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContentCategory  $contentCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentCategory $contentCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContentCategory  $contentCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentCategory $contentCategory)
    {
        //
    }
}
