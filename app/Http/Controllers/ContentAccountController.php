<?php

namespace App\Http\Controllers;

use App\ContentAccount;
use Illuminate\Http\Request;

class ContentAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContentAccount  $contentAccount
     * @return \Illuminate\Http\Response
     */
    public function show(ContentAccount $contentAccount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContentAccount  $contentAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentAccount $contentAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContentAccount  $contentAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentAccount $contentAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContentAccount  $contentAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentAccount $contentAccount)
    {
        //
    }
}
