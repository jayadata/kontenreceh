<?php

namespace App\Http\Controllers;

use App\ContentImage;
use Illuminate\Http\Request;

class ContentImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContentImage  $contentImage
     * @return \Illuminate\Http\Response
     */
    public function show(ContentImage $contentImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContentImage  $contentImage
     * @return \Illuminate\Http\Response
     */
    public function edit(ContentImage $contentImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContentImage  $contentImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContentImage $contentImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContentImage  $contentImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContentImage $contentImage)
    {
        //
    }
}
