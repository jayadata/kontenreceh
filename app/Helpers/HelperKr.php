<?php
function slug($title)
{
    return strtolower(preg_replace('/[^\w]+/', '-', $title));
}

function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}

function get_filesize( $url ) {
    $ch = curl_init( $url );

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);

    $data = curl_exec($ch);
    $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
    $size = formatBytes( $size, 0 );

    curl_close($ch);

    return $size;
}

function assetLocal($name){
    return asset('assets/frontend/content/'.$name);
}

function StaticAsset($file)
{
    if (env('APP_ENV') == 'production') {
        // return 'https://90022369b012b8987831-5200a1e334d85529ad5ea7645793f662.ssl.cf6.rackcdn.com' . $file;
        return asset($file);
    } else {
        return asset($file);
    }
}

function contentImageUrl($path)
{
    if(env('APP_ENV') == 'production')
    {
        return 'https://90022369b012b8987831-5200a1e334d85529ad5ea7645793f662.ssl.cf6.rackcdn.com/content/' . $path;
    } else {
        return 'https://90022369b012b8987831-5200a1e334d85529ad5ea7645793f662.ssl.cf6.rackcdn.com/content/' . $path;
        // return asset('uploads/news/'. $path);
    }    
}

function review_item($name, $ava, $rating, $comment)
{
    ?>
    <div class="review-item">
        <div class="row sg bs4">
            <div class="col-3">
                <img src="<?php echo $ava; ?>" class="img-circle" alt="">
            </div>
            <div class="col-9">
                <strong class="review-user"><?php echo $name; ?></strong>

                <div class="rating mb-1 mt-2"><span data-width="<?php echo $rating; ?>" style="width: <?php echo $rating; ?>%;"></span></div>

                <div class="rdmore text-13">
                    <em><?php echo $comment; ?></em>
                </div>
            </div>
        </div>
    </div>
<?php
}
   
function setCurrent($path, $class = 'current')
{
    if (is_array($path)) {
        foreach ($path as $item) {
            if (Request::is($item)) {
                return $class;
            }
        }
    } else {
        return (Request::is($path)) ? $class : '';
    }
}

function commaToDot($param)
{
    $numnber = number_format($param, 0, '.', '.');
    return $numnber;
}