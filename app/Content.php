<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Voucher;

class Content extends Model
{
    public function account()
    {
        return $this->belongsTo('App\ContentAccount', 'account_id');
    }

    public function detail()
    {
        return $this->hasMany('App\ContentImage', 'content_id');
    }

    public function voucher(){
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }

    static function buy($customer_id, $content_id)
    {
        return ContentBuy::where('customer_id', $customer_id)->where('content_id', $content_id)->first();
    }

    static function rating($id)
    {
        return ContentReview::where('where', 'content')->where('where_id', $id)->avg('rate');
    }
}
