<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentAccount extends Model
{
    public function category(){
        return $this->belongsTo('App\ContentCategory', 'category_id');
    }
}
