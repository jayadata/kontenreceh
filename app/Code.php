<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    static function detail($trx){
    	$dn = Dn::where('session_id', $trx)->first();
    	if (isset($dn)) {
    		if ($dn->type == 'PULL') {

    			switch (substr($dn->subkey, 0, 2)) {
    			  case 'Q4':
    			    return 'Pembelian Content';
    			    break;
    			  
    			  default:
    			    return 'Pembelian VIP';
    			    break;
    			}

			}else{
				return 'Something wrong, please contact admin';
			}
    	}else{
    		/*TELKOMSEL sementara off*/
    		$md = MdMedia::where('trx_id', $trx)->first();
    		if (isset($md)) {

    			switch ($md->product_id) {
    			  case '40146':
    			    return 'Pembelian Tiket Live Quiz';
    			    break;
    			  
    			  default:
    			    return 'Pembelian VIP';
    			    break;
    			}
    		}else{
    			return 'Something Wrong Guys';
    		}
    	}
    }
}
