<?php
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (env('APP_ENV') === 'production') {
    URL::forceScheme('https');
}

/* Developer */
Route::group(['prefix' => 'devel'], function () {
    Route::get('/create-account', 'Dev\DevController@createAccount');
    Route::get('/dev-account', 'Dev\DevController@devAccount');
});

/* Frontend */

/* old 
    Route::get('/', 'ContentController@index');
    Route::get('/v/{trx}/{slug}', 'ContentController@show')->name('content.show'); 
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* auth */
Route::get('auth/signin', 'AuthController@signIn')->name('auth.signin');
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');

/* Content */
Route::get('/', 'ContentController@home');
Route::get('/{trx}', 'ContentController@index')->name('form.main');
Route::group(['prefix' => 'comic'], function () {
    /* old
        Route::get('/{slug}', 'ContentController@catalog')->name('content.catalog');
        Route::get('/detail/{slug}', 'ContentController@show')->name('content.detail');
        Route::post('/detail/loadmore', 'ContentController@loadmore')->name('content.loadmore'); 
    */

    Route::get('/{slug}', 'ContentController@show')->name('content.detail');
    Route::get('/download/{id}', 'ContentController@download')->name('download');
    Route::get('/download-all/{id}', 'ContentController@downloadAll')->name('download.all');
    Route::post('/loadmore', 'ContentController@loadmore')->name('content.loadmore');
    Route::post('/review', 'ContentController@rating')->name('content.review.post');
    Route::post('/review/more', 'ContentController@reviewMore')->name('content.review.more');
});

/* Member */
Route::group(['prefix' => 'member'], function () {
    Route::get('/profile', 'MemberController@profile')->name('member.profile');
    Route::post('/profile/update', 'MemberController@profileUpdate')->name('member.profile.update');
    Route::get('/content', 'MemberController@content')->name('member.content');
    Route::get('/redeem', 'MemberController@contentRedeem')->name('member.content.redeem');
});

/* TRX */
Route::group(['prefix' => 'trx'], function () {
    Route::get('/', 'VipController@index')->name('vip.form');
    Route::post('/submit', 'VipController@submit')->name('vip.submit');
    Route::post('/xl-send', 'VipController@xlSend')->name('vip.xl.send');
    Route::post('/indosat-send', 'VipController@indosatSend')->name('vip.indosat.send');
});

/* Static */
Route::get('/privacy-policy', function () {
    return view('frontend.static.term');
});
